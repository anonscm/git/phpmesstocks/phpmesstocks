<?php


/***************************************************************************
                    db_functions.inc  -
         object to manipulate db
                           -------------------
  begin                :  08/09/2004
  copyright            : (C) 2004 by Olivier Langella
  email                : langella@moulon.inra.fr
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *    modify it under the terms of the GNU Lesser General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/

function db_connect(& $error_message, $arr_param = array ()) {

		//connexion au serveur
	if (APP_DATABASE == 'sqlite') {
		$db_link = sqlite_open($arr_param[0]);
	} else {
		$db_link = mysql_connect(DB_SERVER, DB_LOGIN, DB_PASSWORD);
	}

	if (!$db_link) {
		$error_message = "Désolé, connexion au serveur $pserveur impossible\n";
		return (false);
	}
	//else echo"connexion avec le réseau $pserveur établie\n<br/>";

	//connexion à la base
	if (APP_DATABASE == 'mysql') {
		if (!mysql_select_db(DB_NAME, $db_link)) {
			$error_message = " Désolé, accés  à la base $pbase impossible\n";
			$error_message .= "<b>Message de MySQL : </b>".mysql_error($db_link);
			return (false);
		}
		//else echo "connexion avec la base $pbase �tablie<br/>";
	}
	//on renvoie la variable de connexion

	return $db_link;
}

function db_close($db_link) {
	if (APP_DATABASE == 'sqlite') {
		sqlite_close($db_link);
	} else {
		mysql_close($db_link);
	}

}

//function db_get_id_from('IdMachine', 'machine', array('NomMachine'=>$_POST['NomMachine'], 'IdLabo'=>$_POST['IdLabo']))

function db_insert_only($dblink, $tablename, $arr_fields, $hash_arr_with_values, $idname = -1) {

	$hash_arr_to_insert = array ();
	foreach ($arr_fields as $fieldname) {
		if (array_key_exists($fieldname, $hash_arr_with_values))
			$hash_arr_to_insert[$fieldname] = $hash_arr_with_values[$fieldname];
	}

	return db_insert($dblink, $tablename, $hash_arr_to_insert, $idname);
}

function db_update_only($dblink, $tablename, $arr_fields, $hash_arr_with_values, $where_clause) {

	$hash_arr_to_insert = array ();
	foreach ($arr_fields as $fieldname) {
		if (array_key_exists($fieldname, $hash_arr_with_values))
			$hash_arr_to_insert[$fieldname] = $hash_arr_with_values[$fieldname];
	}

	return db_update($dblink, $tablename, $hash_arr_to_insert, $where_clause);
}

function db_insert($dblink, $tablename, $fields, $idname = -1) {
		//ARGUMENTS:
		// $dblink is the connection to the database
		// $tablename is a string containing the name of the table concerned by the update
		// $fields is an associative array containing name of SQL fields as keys, and there values as value
		// $idname is OPTIONAL. Only needed if the table as a primary key associated to a sequence.
		//     in this case: $idname must contain the name of the field which id the primary key
		//RETURNS:
		// true or the new key id (if $idname is specified) if insert succeeded
		// false if it failed
		//EXAMPLE (Protic/libprotic/xml_sax/sax_spotgel_management.inc) :
		//  db_insert($db_link, "protic_peptid_ident", array("PEPTID_NAME" => "truc", "PM" => "0.555"), "peptid_id");

		//new insert function:
		// IMPORTANT: the old "la_date" string used to apply the current timestamp in a field is replaced by the string "current_timestamp"
		// IMPORTANT: the string values are automatically slashed if needed, the use of "apostrophe" is no more required
		// IMPORTANT: the string values are NOT upper cased automatically, please consider the use of db_insert_uppercase if needed
		// return the new id of the new primary key $idname if the $idname is given
		// return false if an error occured

	$sql_query = "INSERT INTO ".$tablename;
	reset($fields);
	$sequence_val = 0;
	$array_field = array ();
	$array_val = array ();
	while (list ($field, $val) = each($fields)) {
		array_push($array_field, $field);
		//echo $val;
		if (is_numeric($val)) {
			//echo "int";
			//$val = addslashes($val);
			array_push($array_val, "'".$val."'");
		} else {
			if ($val == "current_timestamp") {
				switch (APP_DATABASE) {
					case 'postgres' :
						array_push($array_val, 'current_timestamp');
						break;
					case 'sqlite' :
						array_push($array_val, 'current_timestamp');
						break;
					default : //mysql
						array_push($array_val, 'sysdate');
						break;
				}
			} else {
				$val = priv_db_quotes($val);
				array_push($array_val, "'".$val."'");
			}
		}
	}

	$sql_query .= '('.join($array_field, ", ").') VALUES ('.join($array_val, ", ").')';
	//echo $sql_query."\n";
	$exec = priv_exec($dblink, $sql_query);
	if ($exec == false) {
		echo ("problem executing: ".$sql_query);
		return false;
	}
	if ($idname != -1) {
		switch (APP_DATABASE) {
			case 'sqlite' :
				return (sqlite_last_insert_rowid($dblink));
			default : //mysql
				return (mysql_insert_id($dblink));
		}

	}
	return true;
}

function db_update($dblink, $tablename, $fields, $where_clause) {
	//ARGUMENTS:
	// $dblink is the connection to the database
	// $tablename is a string containing the name of the table concerned by the update
	// $fields is an associative array containing name of SQL fields as keys, and there values as value
	// $where_clause is a string containing the where clause
	//RETURNS:
	// true if update succeeded
	// false if failed
	//EXAMPLE (Protic/dba/databases.inc) :
	// db_update($dblink, "com_db", array("DB_CODE"=>"embl", "DB_NAME" => "EMBL"), "db_id='$db_id'")

	//new update function:
	// IMPORTANT: the old "la_date" string used to apply the current timestamp in a field is replaced by the string "current_timestamp"
	// IMPORTANT: the string values are automatically slashed if needed, the use of "apostrophe" is no more required
	// IMPORTANT: the string values are NOT upper cased automatically, please consider the use of db_insert_uppercase if needed
	// return the new id of the new primary key $idname if the $idname is given
	// return false if an error occured

	$sql_query = "UPDATE ".$tablename." SET ";
	reset($fields);
	$join_data = array ();
	$array_val = array ();
	while (list ($field, $val) = each($fields)) {
		if (is_numeric($val)) {
			array_push($join_data, $field."='".$val."'");
		}
		elseif ($val == "current_timestamp") {
			switch (APP_DATABASE) {
				case 'postgres' :
					array_push($array_val, 'current_timestamp');
					break;
				case 'sqlite' :
					array_push($array_val, 'current_timestamp');
					break;
				default : //mysql
					array_push($array_val, 'sysdate');
					break;
			}
		} else {
			//$val = addslashes($val);
			$val = priv_db_quotes($val);
			if ((strlen($val) > 0) and ($val[0] == '='))
				array_push($join_data, $field.$val);
			else
				array_push($join_data, $field."='".$val."'");
		}
	}

	$sql_query .= join($join_data, ", ");

	$sql_query .= " WHERE ".$where_clause;
	//echo $sql_query;

	$exec = priv_exec($dblink, $sql_query);
	if ($exec == false) {
		echo ("problem executing: ".$sql_query);
		return false;
	}
	return true;
}

function priv_db_quotes($val) {
	// private function used only by db_update and db_insert to construct SQL queries
	// gets a string value and returns the right quoted string depending on the database backend
	return (str_replace("'", "\'", $val));
	//return ($val);
}

function db_delete($dblink, $tablename, $where_clause) {
	$sql_query = 'DELETE FROM '.$tablename.' WHERE '.$where_clause;
	$exec = false;
	switch (APP_DATABASE) {
		case 'sqlite' :
			$exec = sqlite_query($sql_query, $dblink);
			break;
		default : //mysql
			$exec = mysql_query($sql_query, $dblink);
			break;
	}
	if ($exec == false) {
		echo ("problem executing: ".$sql_query);
		return false;
	}
	return true;
}

function db_exec($dblink, $sql_query) {
	return (priv_exec($dblink, $sql_query));
}

function priv_exec($dblink, $sql_query) {
	$exec = false;
	switch (APP_DATABASE) {
		case 'sqlite' :
			$exec = sqlite_query($sql_query, $dblink);
			break;
		default : //mysql
			$exec = mysql_query($sql_query, $dblink);
			break;
	}
	return ($exec);
}
?>