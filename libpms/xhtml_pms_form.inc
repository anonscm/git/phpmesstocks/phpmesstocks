<?php


/***************************************************************************
 xhtml_pms_form.inc  -
 object to manipulate xhtml form specific to phpmesstocks
 -------------------
 begin                :  24/09/2005
 copyright            : (C) 2005 by Olivier Langella
 email                : langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *    modify it under the terms of the GNU Lesser General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/

if (APP_ROOT_RELATIVE_PATH != "") {
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/html_functions.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_base.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_zone.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_page.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_form.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH.'shared_lib/php/generic/date_functions.inc.php');
} else
define("APP_ROOT_RELATIVE_PATH", "./");

class xhtml_pms_form extends xhtml_form {

	function xhtml_pms_form(& $xhtmlpage, $pname) {
		$this->xhtml_form($xhtmlpage, $pname);
	}

	function get_query_parameters($array_fields) {
		$this->_tab_values = array ();
		if (!array_key_exists('query_'.$this->_form->get_attribute('id'), $_SESSION)) {
			$_SESSION['query_'.$this->_form->get_attribute('id')] = array ();
		}
		foreach ($array_fields as $fieldname) {
			$this->_tab_values[$fieldname] = '';
			if (array_key_exists($fieldname, $_GET)) {
				$this->_tab_values[$fieldname] = $_GET[$fieldname];
				$_SESSION['query_'.$this->_form->get_attribute('id')][$fieldname] = $_GET[$fieldname];
			} else {
				if (array_key_exists($fieldname, $_SESSION['query_'.$this->_form->get_attribute('id')]))
				$this->_tab_values[$fieldname] = $_SESSION['query_'.$this->_form->get_attribute('id')][$fieldname];
			}
		}
		//$_SESSION['query_'.$this -> _form -> get_attribute('id')] = $this -> _tab_values;
		return ($this->_tab_values);
	}

	// add a special button to post the current form and go back... specific of the application
	function add_post_button_back($name) {
		if (array_key_exists("stack_view", $_SESSION)) {
			if ($_SESSION["stack_view"] != "") {
				//echo $_SESSION["stack_view"];
				$arr = $_SESSION["stack_view"];
				if (!is_array($arr))
				$arr = unserialize($_SESSION["stack_view"]);
				if (count($arr) > 0) {
					$this->add_post_button($name, "back", "back", array ("dba" => "new", "dbobject" => "back"));
				}
			}
		}
	}
	/*
		function input_submit($title, $pclass = -1) {
		$this -> priv_input(-1, -1, "Submit", $ptype = "submit", array("class" => $pclass));
		//	  $this->input_submit($title, $pclass);
		// $this->input_submit_image (APP_ROOT_RELATIVE_PATH."../","submit" , $pclass=-1);

		}
		*/
	function input_date($ptag, $pname, $pvalue, $pdefault) {
		if ($pvalue == '') {
			if (array_key_exists($pname, $this->_tab_values)) {
				$pvalue = datesql2datefr($this->_tab_values[$pname]);
			} else {
				if ($pdefault == 'today') {
					$pvalue = date("d")."/".date("m")."/".date("Y");
				}
			}
		} else
		$pvalue = datesql2datefr($pvalue);
		$this->input_text($ptag, $pname, $pvalue);
	}

	function input_linked_lists($ptag1, $ptag2, $pname, $pval, $arr_list1, $arr_list2, $parameters = array ()) {
		//create 2 select xhtml widget, linked by a piece of javascript code
		// the list1 changes the choice in list2
		//the name of list1 will be: $pname+"1list"
		//the name of list2 will be: $pname
		//$pval is the value of the selected item in list2

		if (($arr_list1 == false) or (count($arr_list1) == 0)) {
			$arr_list1['----'] = 0;
			$arr_list2[0]['----'] = -1;
		}
		//---------------------------------------------------------------------------------
		// JAVASCRIPT FUNCTION START
		//---------------------------------------------------------------------------------
		$javascript_function_name = 'choice'.str_replace('[]', '', $pname);

		$javascript_code = "\nfunction ".$javascript_function_name." (element1, element2) {\n";
		//$javascript_code.= "var form=document.forms['".$this -> _form -> get_attribute("id")."'];\n";
		//$javascript_code.= "var value1 = form.elements[element1].value;\n";
		//$javascript_code.= "alert(element2.value);\n";
		$javascript_code .= "switch (element1.value) {\n";
		foreach ($arr_list1 as $key => $value) {
			$javascript_code .= "case '".$value."':\n";
			//$javascript_code.= "var tags = new Array (";
			//"'Matériel','Poissons','Sécurité'); break;\n";
			$tag_table = "";
			$value_table = "";
			if (!isset ($the_list))
			$the_list = $value;
			//print $value;
			if (array_key_exists($value, $arr_list2) and is_array($arr_list2[$value])) {
				if (array_search($pval, $arr_list2[$value]) != false) {
					$the_list = $value;
				}
				foreach ($arr_list2[$value] as $key2 => $value2) {
					$tag_table .= " '".$key2."'";
					$value_table .= " '".$value2."'";
				}
			}
			$javascript_code .= "var tags = new Array (".str_replace("' '", "', '", $tag_table).");\n";
			$javascript_code .= "var values = new Array (".str_replace("' '", "', '", $value_table)."); break;\n";
		}
		$javascript_code .= "default:\n";
		$javascript_code .= "var tags = new Array ();\n";
		$javascript_code .= "var values = new Array (); break;\n";
		// if (i == 0) { return; }
		//form.Rubrique.selectedIndex = 0; for (i=0;i<3;i++) { form.Page.options[i+1].text=txt[i]; }
		$javascript_code .= "}\n";
		//change element2 with tags and values
		//element2.options
		//$javascript_code.= "element2.options = new Array();\n";
		//$javascript_code.= "element2.options = new Options()\n";
		//$n = count($arr_list2[$the_list]);
		$javascript_code .= "element2.options.length=tags.length \n";
		$javascript_code .= "for (i=0;i<tags.length;i++) { element2.options[i] = new Option(tags[i], values[i]);} \n";
		$javascript_code .= "}\n";

		$this->priv_add_javascript($javascript_code);
		//---------------------------------------------------------------------------------
		// JAVASCRIPT FUNCTION END
		//---------------------------------------------------------------------------------

		if (is_array($pval)) {
			$the_list = array ();
			$name_list1 = str_replace('[]', '', $pname).'1list[]';
			$arr_onchange_param = $parameters;
			$parameters_plus = $parameters;
			for ($i = 0; $i < count($pval); $i ++) {

				$element1 = $this->priv_get_unique_id(str_replace('[]', $i, $name_list1));
				$element2 = $this->priv_get_unique_id(str_replace('[]', $i, $pname));

				$arr_onchange_param['onChange'][$i] = $javascript_function_name.'('.$element1.', '.$element2.')';
				$parameters_plus['list'][$i] = array ();

				foreach ($arr_list1 as $key => $value) {
					if (!isset ($the_list[$i]))
					$the_list[$i] = $value;
					//print $value;
					if (array_key_exists($value, $arr_list2) and is_array($arr_list2[$value])) {
						if (array_search($pval[$i], $arr_list2[$value]) != false) {
							$the_list[$i] = $value;
							$parameters_plus['list'][$i] = $arr_list2[$value];
						}
					}
				}
				if (count($parameters_plus['list'][$i] == 0))
				$parameters_plus['list'][$i] = $arr_list2[$the_list[$i]];
				//$arr_onchange_param['list'][$i] = $javascript_function_name.'('.$element1.', '.$element2.')';
			}
			//$page->xhtml_message('input_linked_lists not available for arrays','error');
			$this->input_select($ptag1, $name_list1, $the_list, $arr_list1, $arr_onchange_param);
			$this->input_select($ptag2, $pname, $pval, array (), $parameters_plus);
			//print_r($pval);
		} else {
			$name_list1 = $pname.'1list';

			$element1 = $this->priv_get_unique_id($name_list1);
			$element2 = $this->priv_get_unique_id($pname);

			$arr_onchange_param = $parameters;
			$arr_onchange_param['onChange'] = $javascript_function_name.'('.$element1.', '.$element2.')';

			$this->input_select($ptag1, $name_list1, $the_list, $arr_list1, $arr_onchange_param);
			$this->input_select($ptag2, $pname, $pval, $arr_list2[$the_list], $parameters);
		}

	}

	function input_select($ptag, $pname, $pdefault, $plist_val, $parameters = array (), $button_getback = -1) {
		// special select field: add a post button on the right side to create/update select list
		//	$button = array("name" => "taxo_update", "title" => "new/update", "class" => "button", "param" => array("back2view" => "create_plant", "go2view" => "create_taxonomy"));
		//$form -> input_select("genotype", "taxonomy_id", "", $tab_val, "-1", $button);

		//  $form->add_post_button("pop_line_update", "new/update","button", array("back2view" => "create_plant","go2view" => "create_origin_plant"));
		if (!is_array($parameters)) {
			$class = $parameters;
			$parameters = array ();
			$parameters["class"] = $class;
		}

		if (!array_key_exists("class", $parameters)) {
			$parameters["class"] = -1;
		}
		if (!array_key_exists("size", $parameters)) {
			$parameters["size"] = -1;
		}
		//$parameters["button_getback"] = $button_getback;
		if ($button_getback != -1) {
			if ($button_getback["title"] == "new/update") {
				if (count($plist_val) == 1) {
					$button_getback["title"] = "new";
					$button_getback_new = -1;
				} else {
					$button_getback["title"] = "update";
					$button_getback_new = $button_getback;
					$button_getback_new["name"] .= "new";
					$button_getback_new["title"] = "new";
					//$button_getback_new["param"][$this -> priv_get_unique_id($pname)]= "0";
					$button_getback_new["param"][$pname] = "";
				}
			} else {
				$button_getback_new = -1;
			}
			$parameters["button_getback_new"] = $button_getback_new;

		}
		$parameters["button_getback"] = $button_getback;
		//      $parameters["class"] = $pclass;
		if (count($plist_val) == 0) {
			$plist_val = array ("" => "");
		}
		reset($plist_val);
		$this->priv_input($ptag, $pname, $pdefault, "select", $parameters, $plist_val);
	}

	function & priv_create_node_select($id, $pname, $plist, $pdefault, $parameters) {
		if ($this->_const == true) {
			$node = & $this->_xhtmldoc->create_element("span");
			$node->set_content($pdefault);
			return $node;
		} else {
			if (!array_key_exists("button_getback", $parameters))
			$parameters["button_getback"] = -1;

			$select = & $this->_xhtmldoc->create_element("select");
			//$select->append_child();
			//$node->set_content($pdefault);
			$select->set_attribute("name", $pname);
			$select->set_attribute("id", $id);
			if (array_key_exists("onChange", $parameters)) {
				if (is_array($parameters['onChange'])) {
					$select->set_attribute("onchange", $parameters['onChange'][$parameters['special_index']]);
				} else {
					$select->set_attribute("onchange", $parameters['onChange']);
				}
			}
			if (array_key_exists("list", $parameters)) {
				if (is_array($parameters['list']))
				$plist = $parameters['list'][$parameters['special_index']];
				//print_r ($plist);
				//else $select -> set_attribute("onChange", $parameters['onChange']);
			}

			if ($parameters["size"] != -1)
			$select->set_attribute("size", $parameters["size"]);
			if ($parameters["class"] != -1)
			$select->set_attribute("class", $parameters["class"]);
			// $select->set_attribute("size", 5);
			$this->priv_create_node_select_options($select, $plist, $pdefault);

			if ($parameters["button_getback"] == -1) {
				return $select;
			} else {
				$node = & $this->_xhtmldoc->create_element("span");
				$node->append_child($select);
				//$node->append_child(add_post_button($name, $text,$class="-1", $param=array()));
				$javascript_function_name = $this->priv_get_unique_id($parameters["button_getback"]["name"]);
				//$this->priv_input (-1, -1, parameters["button_getback"]["title"] , $ptype ="button", array ("class" => parameters["button_getback"]["class"], "onclick" => $javascript_function_name."()"));
				$button = & $this->priv_create_node_input($javascript_function_name, "button", -1, $parameters["button_getback"]["title"], $parameters["button_getback"]["class"], -1, -1, $javascript_function_name."()");
				$node->append_child($button);
				$this->priv_javascript_post_button_code($javascript_function_name, $parameters["button_getback"]["param"]);
				//  $this->priv_javascript_post_button_code($javascript_function_name, $param);
				if ($parameters["button_getback_new"] != -1) {
					//second button to force new item form
					//$node = $this -> _xhtmldoc -> create_element("span");
					//$node -> append_child($select);
					//$node->append_child(add_post_button($name, $text,$class="-1", $param=array()));
					$javascript_function_name = $this->priv_get_unique_id($parameters["button_getback_new"]["name"]);
					//$this->priv_input (-1, -1, parameters["button_getback"]["title"] , $ptype ="button", array ("class" => parameters["button_getback"]["class"], "onclick" => $javascript_function_name."()"));
					$button = & $this->priv_create_node_input($javascript_function_name, "button", -1, $parameters["button_getback_new"]["title"], $parameters["button_getback_new"]["class"], -1, -1, $javascript_function_name."()");
					$node->append_child($button);
					$this->priv_javascript_post_button_code($javascript_function_name, $parameters["button_getback_new"]["param"]);

				}
				return ($node);

			}

		}

	}

}
?>