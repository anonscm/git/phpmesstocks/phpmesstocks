<?php


/*****************************************************************************
// visitor.inc
// this php code is specific to pmc.
// visitor is an array that should contain all information related to the user.
// visitor.inc can be included in the protic php code to retrieve informations and 
// to fill correctly this associative array.
//
// created by Olivier Langella 8/9/2004
//
*****************************************************************************/

function set_visitor_stock($db_link, & $visitor, & $error_message, $stock_id) {
	//complémenter les droits poru ce stock
//echo 'coucou';
	$objet_requete = new db_select_pmc();
	$objet_requete->select_from(array ('IdStock'), 'StockUtilisateur','su');
	$objet_requete->left_join(array ('NomStock'), 'Stocks', 's', 'IdStock');
	$objet_requete->where_equal('su.IdUtilisateur', $visitor['pms_id']);
	$objet_requete->where_equal('s.IdStock', $stock_id);

	$objet_requete->execute_select($db_link);

	if ($objet_requete->is_sql_error()) {
		$error_message = $objet_requete->get_error();
		return (false);
	} else {
		//choix du seul stock
		$res = $objet_requete->get_result();
		if (count($res['IdStock']) == 1) {
			$visitor['stock_id'] = $res['IdStock'][0];
			$visitor['stock'] = $res['NomStock'][0];
			$_SESSION['s_visitor'] = $visitor;
			return (true);
		} else {
		}

	}
	return (false);

}

function set_visitor($db_link, & $visitor, & $error_message, $person_id = 0) {
	$tag_error = 'ERROR in function set_visitor : ';

	if (!session_id()) {
		$error_message = $tag_error.'no session started';
		return (false);
	}

	if (array_key_exists('s_visitor', $_SESSION)) {
		$visitor = $_SESSION['s_visitor'];
		if (array_key_exists('pms_id', $visitor)) {
			$person_id = $visitor['pms_id'];
		} else {
			$error_message = $tag_error.'no person_id in the current session';
			return (false);
		}
	} else {
		// definition of the associative array	
		$visitor['pms_id'] = '';
		$visitor['stock_id'] = '';
		$visitor['email'] = '';
		$visitor['login'] = '';
		$visitor['first_name'] = '';
	}

	if ($person_id > 0) {
		$visitor['pms_id'] = $person_id;
		if ($visitor['login'] == '') {
			$objet_requete = new db_select_pmc();
			$objet_requete->select_from(array ('*'), 'Utilisateurs');
			$objet_requete->where_equal('IdUtilisateur', $person_id);

			$objet_requete->execute_select($db_link);

			if ($objet_requete->is_sql_error()) {
				$error_message = $tag_error.$objet_requete->get_error();
				return (false);
			} else {
				$res = $objet_requete->get_result_first();
				//  IdUtilisateur  	 UIdentifiant  	 UNom  	 UMotdepasse  	 UAdmin  	 produitVue  	 produitAjout  	 produitSuppr  	 produitModif  	 mouvementVue  	 mouvementAjout  	 mouvementSuppr  	 mouvementModif  	 fournisseurVue  	 fournisseurAjout  	 fournisseurSuppr  	 fournisseurModif
				//echo "coucou";
				$visitor['login'] = $res['UIdentifiant'];
				//$visitor["first_name"] = $res["PrenomAdminBd"];
				$visitor['last_name'] = $res['UNom'];
				//$visitor["labo_id"] = $res["IdLabo"];
				$visitor['admin'] = false;
				if ($res['UAdmin'] == 1)
					$visitor['admin'] = true;

				//permissions pour les objets communs à tous les laboratoires:
				$liste_perm = array ('produit', 'mouvement', 'fournisseur', 'stock');
				$liste_perm_type = array ('Vue', 'Ajout', 'Suppr', 'Modif');
				foreach ($liste_perm as $repertoire) {
					foreach ($liste_perm_type as $type_perm) {
						$visitor[$repertoire.$type_perm] = false;
						if ($res[$repertoire.$type_perm] == 1) {
							$visitor[$repertoire.$type_perm] = true;
						}
					}
				}
			}
		}
		if ($visitor['stock_id'] == '') {
			$objet_requete = new db_select_pmc();
			$objet_requete->select_from(array ('IdStock'), 'StockUtilisateur');
			$objet_requete->left_join(array ('NomStock'), 'Stocks', 's', 'IdStock');
			$objet_requete->where_equal('IdUtilisateur', $person_id);

			$objet_requete->execute_select($db_link);

			if ($objet_requete->is_sql_error()) {
				$error_message = $objet_requete->get_error();
				return (false);
			} else {
				//choix du seul stock
				$res = $objet_requete->get_result();
				if (count($res) == 1) {
					$visitor['stock_id'] = $res['IdStock'][0];
					$visitor['stock'] = $res['NomStock'][0];
				}

			}
		}

		$_SESSION['s_visitor'] = $visitor;
		return (true);
	} else {
		$error_message = $tag_error.'the person_id is not valid';
		return (false);
	}
}
/*
if (is_array($_POST)) {
	if (array_key_exists("goto_labo_id", $_POST)) {
		//Change current labo		
	}
}
*/
?>