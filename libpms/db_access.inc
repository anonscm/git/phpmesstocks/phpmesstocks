<?php


/*****************************************************************************
// dbaccess.inc
// this php code is specific to pmc. It is the single location that allow/deny
// updates, inserts, deletions in the phpMyCampus database
// the array "visitor" is needed to know about the rights of the user
// it handles too a mecanism that provide browsing from form to form with a "back button"
// see the xhtml_pmc_form.inc for more informations.
//
//
// created by Olivier Langella 10/9/2004
//
*****************************************************************************/


$page->goto_zone('pms_body');

/* "back2view" treatment => allow to escape from  form to view/update 
   another form and automatically get back to the first form */
if (count($_POST) < 1) {
	//	echo "coucou blank stack_view";
	//$page->goto_zone("protic_body");
	//$page->xhtml_message("blank stack_view state: <br/>".$_SESSION["stack_view"],"debug");
	$_SESSION["stack_view"] = "";
}
if (array_key_exists("back2view", $_POST)) {
	if ($_POST["back2view"] != "") {
		//$page->goto_zone("protic_body");
		$back2view = $_POST["back2view"];
		$go2view = $_POST["go2view"];
		$_POST["back2view"] = "";
		//$arr_back["form"] = $_POST;
		// record $arr_back into the php session:
		if (array_key_exists("stack_view", $_SESSION))
			$stack_view = unserialize($_SESSION["stack_view"]);
		if (!is_array($stack_view))
			$stack_view = array ();
		//$page->xhtml_message("use stack_view, tmp state : <br/>".serialize($stack_view),"debug");
		//$stack_view = unserialize($tmp);
		array_push($stack_view, array ($back2view => $_POST));
		$_SESSION["stack_view"] = serialize($stack_view);
		//$page->xhtml_message("use stack_view, state: <br/>".$_SESSION["stack_view"],"debug");
		$_POST = array ();
		$go2view = split("&", $go2view);
		$_GET["view"] = $go2view[0];
		for ($i = 1; $i < count($go2view); $i ++) {
			$args = split("=", $go2view[$i]);
			$_GET[$args[0]] = $args[1];
		}
	}
}

//echo $_POST["dba"];
//echo $_POST["dbobject"];

if (array_key_exists("list_view", $_POST)) {
	$_SESSION['list_parameters'][$_POST['list_view']]['position'] = $_POST['list_position'];
	$_SESSION['list_parameters'][$_POST['list_view']]['offset'] = $_POST['list_offset'];
}

/* DB access to update data if needed: */
if (array_key_exists('dba', $_GET)) {
	$_POST['dba'] = $_GET['dba'];
	if (array_key_exists('dbobject', $_GET)) {
		$_POST['dbobject'] = $_GET['dbobject'];
	}
}

if (array_key_exists('dba', $_POST)) {
	if (array_key_exists('dbobject', $_POST)) {
		$array_directory = split('_', $_POST['dbobject']);

		$dbfile = APP_ROOT_RELATIVE_PATH.'dba/'.join('/', $array_directory).'.inc';
		if (file_exists($dbfile)) {
			$dbreturn = false;
			//vérification des droits
			$ok = false;
			if (count($array_directory) == 1) {
				$ok = true;
			} else {
				switch ($_POST['dba']) {
					case 'delete' :
						if ($visitor[$array_directory[0].'Suppr'])
							$ok = true;
						break;
					case 'update' :
						if ($visitor[$array_directory[0].'Modif'])
							$ok = true;
						break;

					case 'new' :
						if ($visitor[$array_directory[0].'Ajout'])
							$ok = true;
						break;
					default :
						$ok = false;
						break;

				}
			}

			if ($ok) {
				require $dbfile;
			} else {
				$page->xhtml_message('Vous n\'avez pas les autorisations nécessaires', 'error');
			}

			if ($dbreturn == true) {
				//ok, see if eventually, we have something in the stack_view and go back to it
				if (array_key_exists("stack_view", $_SESSION)) {
					//echo $_SESSION["stack_view"];
					//$page->goto_zone("protic_body");
					//$page->xhtml_message("d�sempilement: <br/>".$stack_view,"debug");
					if (is_string($_SESSION["stack_view"]))
						$stack_view = unserialize($_SESSION["stack_view"]);

					if (is_array($stack_view)) {
						if (count($stack_view) > 0) {
							list ($view, $form) = each(array_pop($stack_view));
							$_SESSION["stack_view"] = serialize($stack_view);
							$_POST = $form;
							$view = split("&", $view);
							$_GET["view"] = $view[0];
							for ($i = 1; $i < count($view); $i ++) {
								$args = split("=", $view[$i]);
								$_GET[$args[0]] = $args[1];
							}
						}
					}
				}
			}
		} else {
			$page->xhtml_message($dbfile." fichier manquant, contacter l'administrateur de phpMesStocks", "error");
		}
	}
}
?>