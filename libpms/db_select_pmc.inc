<?php


/***************************************************************************
 db_select_pmc.inc  -
 base object to easily build sql select query string
 -------------------
 begin                :  19/01/2004
 copyright            : (C) 2004 by Olivier Langella
 email                : langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *    modify it under the terms of the GNU Lesser General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/

//require_once
require_once (APP_ROOT_RELATIVE_PATH.'shared_lib/php/generic/sql_select.inc.php');
require_once (APP_ROOT_RELATIVE_PATH.'shared_lib/php/generic/db_select.inc.php');

class db_select_pmc extends db_select {

	function db_select_pmc() { //constructor
		$this->db_select();
	}

	/** \brief test égalité de chaine, case insensitive
	 */
	function where_equal_like($left, $right, $operand = "AND") {
		$right = $this->priv_add_slashes($right);
		$left .= " LIKE '";
		$right .= "'";
		$this->priv_where($operand, $left, $right);
	}

	function count_rows($db_link) {
		//echo $this->get_sql_string();
		$result = mysql_query($this->get_sql_string(), $db_link);
		if ($result == false)
		$this->add_error("mysql error: ".mysql_error()."\n");
		//	return (requete_sql_nb_lignes($db_link));
		return (mysql_affected_rows($db_link));
	}

	function count_distinct($db_link, $fieldname) {
		//$requete = "SELECT DISTINCT ($fieldname) ";
		$sql_string = "SELECT DISTINCT (".$fieldname.") ".$this->_query_post_from;
		$sql_string .= $this->_query_where;
		//$requete .= substr($this->_requete, strpos($this->_requete, "FROM "), strlen($this->_requete));

		switch (APP_DATABASE) {
			case 'sqlite' :
				$result = sqlite_query($db_link, $sql_string);
				return (sqlite_num_rows($result));
			default : //mysql
				$result = mysql_query($sql_string, $db_link);
				return (mysql_affected_rows($db_link));
		}

	}

	function priv_db_exec($db_link, $sql_string) {
		$this->_result = array ();
		$result = false;
		switch (APP_DATABASE) {
			case 'sqlite' :
				$result = sqlite_query($db_link, $sql_string);
				break;
			default : //mysql
				//echo'coucou';
				$result = mysql_query($sql_string, $db_link);
				if ($result == false)
				$this->add_error("mysql error: ".mysql_error()."\n");
				break;
		}
		if ($result == false) {
			$this->add_error("error in SQL query: ".$sql_string."\n");
		} else {
			//$table = array();
			$i = 0;
			switch (APP_DATABASE) {
				case 'sqlite' :
					while ($ligne = sqlite_fetch_array($result, SQLITE_ASSOC)) {
						//print_r($ligne);
						reset($ligne);
						while (list ($cle, $val) = each($ligne)) {
							$tmp_arr = explode('.', $cle);
							//print_r($tmp_arr);
							$this->_result[$tmp_arr[1]][$i] = $val;
						}
						$i ++;
					}
					break;
				default : //mysql
					while ($ligne = mysql_fetch_assoc($result)) {
						reset($ligne);
						while (list ($cle, $val) = each($ligne)) {
							$this->_result[$cle][$i] = $val;
						}
						$i ++;
					}
					break;
			}
		}
		return (!($this->_sql_error));
	}
}
?>