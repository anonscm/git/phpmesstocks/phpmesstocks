<?php


/***************************************************************************
 xhtml_pms_page.inc  -
 object to manipulate xhtml page specific to phpmesstocks
 -------------------
 begin                :  13/09/2005
 copyright            : (C) 2005 by Olivier Langella
 email                : langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *    modify it under the terms of the GNU Lesser General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/

if (APP_ROOT_RELATIVE_PATH != "") {
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/simpleoxml/oxml_document.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/simpleoxml/oxml_element.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH.'web_view/lib/php/generic/simpleoxml/oxml_cdata.inc.php');
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/html_functions.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_base.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_zone.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_page.inc.php");
	require_once (APP_ROOT_RELATIVE_PATH."web_view/lib/php/generic/xhtml_form.inc.php");
	//require_once (APP_ROOT_RELATIVE_PATH."libpmc/xhtml_pmc_form.inc");
} else
define("APP_ROOT_RELATIVE_PATH", "./");

class xhtml_pms_page extends xhtml_page {
	//var $_xhtmldoc;
	//var $_currentnode;
	var $_pms_up;
	var $_pms_bottom;
	var $_pms_left;
	var $_pms_body;
	//var $_xpath;

	function xhtml_pms_page() { //constructor
		$this->xhtml_page('fr', 'utf-8');
		$this->xhtml_page_add_css(APP_ROOT_RELATIVE_PATH.'libcss/main.css');
		//$this->xhtml_page_add_icon(APP_ROOT_RELATIVE_PATH."../images/pmc.png");

		$this->goto_zone('ground0');
		$this->new_zone('div', 'pms_up');
		$this->new_zone('h1', 'pms_page_title');
		$this->goto_zone('ground0');
		$this->new_zone('div', 'pms_left');
		$this->new_zone('div', 'visitor_status');
		$this->goto_zone('pms_left');
		$this->new_zone('ul', 'pms_min_menu');
		$this->goto_zone('pms_left');
		$this->new_zone('ul', 'pms_noid_menu');
		$this->goto_zone('ground0');
		$this->new_zone('span', 'pms_hmenu');
		$this->goto_zone('ground0');
		$this->new_zone('div', 'pms_body');
		$this->goto_zone('ground0');
		$this->new_zone('div', 'pms_bottom');
		/*
		 $this->new_zone("div", "pmc_en_tete");
		 $this->xhtml_image(APP_ROOT_RELATIVE_PATH.'../images/compaq.png', 'Tux en moto', array ("class" => 'gauche'));
		 $this->xhtml_image(APP_ROOT_RELATIVE_PATH.'../images/wstux.png', 'Tux sur son ordinateur', array ("class" => 'droite'));
		 $this->new_zone("h1", "pmc_page_title");
		 $this->goto_zone("pmc_en_tete");
		 $this->xhtml_message("", "spacer");

		 $this->goto_zone("ground0");
		 $this->new_zone("div", "corps");

		 $this->new_zone("div", "partie_gauche");
		 $this->goto_zone("partie_gauche");
		 $this->new_zone("div", "visitor_status");

		 $this->goto_zone("partie_gauche");
		 $this->new_zone("ul", "pmc_min_menu");
		 $this->goto_zone("partie_gauche");
		 $this->new_zone("ul", "pmc_credits");

		 $this->goto_zone("corps");
		 $this->new_zone("div", "pmc_hmenu");
		 $this->goto_zone("corps");
		 $this->new_zone("div", "pmc_body");

		 $this->goto_zone("ground0");
		 */
		$this->xhtml_pms_page_set_title(SOFTWARE_NAME);
		$this->display_basic_menus();

	}

	function display_basic_menus() {
		//basic MENUS to display
		$tab_menu = array ();
		//$tab_menu[] = html_anchor(APP_ROOT_RELATIVE_PATH.'../annuaire/index.php', 'Annuaire');
		$tab_menu[] = html_anchor(APP_ROOT_RELATIVE_PATH.'phpMesStocks/index.php', "Ancien phpMesStocks");
		$tab_menu[] = html_anchor(HOME_PAGE."?view=credits", "Credits");
		$tab_menu[] = html_anchor(HOME_PAGE."?view=installation", "Installation");
		$this->zone_ul_add_li('pms_noid_menu', $tab_menu);
	}

	function display_identified_menus(& $visitor, $hmenu = '') {

		$this->xhtml_page_add_css(APP_ROOT_RELATIVE_PATH."libcss/menu.css");
		//menus to display when the visitor is identified
		$tab_menu = array ();
		$tab_menu[] = html_anchor(HOME_PAGE."?session=logout", "Déconnexion");
		//$tab_menu[] = html_anchor(HOME_PAGE."?hmenu=parc&view=utilisateur_liste", 'gestion du laboratoire');
		//$tab_menu[] = html_anchor(HOME_PAGE."?view=labo_choix", 'changer de laboratoire');
		//$tab_menu[] = html_anchor(HOME_PAGE."?hmenu=admin", "Administration");
		//$param = array();
		$this->zone_ul_add_li('pms_min_menu', $tab_menu);

		$param = array ();
		if (array_key_exists('view', $_GET)) {
			$param['current'] = 'view='.$_GET['view'];
		}

		$tab_menu = array ();
		$tmp_link = HOME_PAGE;
		$this->goto_zone('pms_hmenu');
		//echo 'coucou'.$hmenu;
		switch ($hmenu) {
			case 'admin' :
				$tmp_link .= '?hmenu=admin&';
				$tab_menu[] = html_anchor($tmp_link.'view=password', 'mot de passe');
				if ($visitor['adminVue'])
				$tab_menu[] = html_anchor($tmp_link.'view=admin_liste', 'administrateurs');
				if ($visitor['laboVue'])
				$tab_menu[] = html_anchor($tmp_link.'view=labo_liste', 'laboratoires');
				//if ($visitor['xmlVue'])
				$tab_menu[] = html_anchor($tmp_link.'view=xml_export', 'export XML');
				$tab_menu[] = html_anchor($tmp_link.'view=xml_import', 'import XML');
				$this->xhtml_ul($tab_menu, "navlist", $param);
				break;
			case 'gestion' :
				$tmp_link .= '?hmenu=gestion&';
				$tab_menu[] = html_anchor($tmp_link.'view=stock_choix', 'Stocks');
				$tab_menu[] = html_anchor($tmp_link.'view=produit_liste', 'Produits');
				$tab_menu[] = html_anchor($tmp_link.'view=mouvement_liste', 'Mouvements');
				$tab_menu[] = html_anchor($tmp_link.'view=fournisseur_liste', 'Fournisseurs');

				$this->xhtml_ul($tab_menu, "navlist", $param);
				break;
			default :
				break;
		}
	}

	function display_not_identified_menus() {

		$this->xhtml_page_add_css(APP_ROOT_RELATIVE_PATH.'libcss/menu.css');
		//menus to display when the visitor is identified
		$tab_menu[0] = html_anchor(HOME_PAGE."?session=logout", "Connexion");
		//$param = array();
		$this->zone_ul_add_li("pms_min_menu", $tab_menu);
	}

	function xhtml_pms_page_set_title($title) {
		$node = & $this->_tab_nodes['pms_page_title'];
		$node->unlink_node();

		$node->set_content(utf8_encode($title));

		$this->xhtml_page_set_title($title);
	}

	function xhtml_pms_page_display() {
		//add a spacer:
		//$this -> xhtml_hr();
		//$this -> xhtml_message("", "spacer");
		$this->goto_zone('pms_bottom');
		//$this->xhtml_message('', 'spacer');
		$this->xhtml_hr('spacer');
		$banner = html_anchor('http://validator.w3.org/', html_image(APP_ROOT_RELATIVE_PATH.'images/valid-xhtml11.png', 'xhtml validation'), -1);
		$this->xhtml_message($banner, 'bottom_left');
		$this->xhtml_message(SOFTWARE_NAME." ".SOFTWARE_VERSION.", ".SOFTWARE_DATE, 'bottom_right');

		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
		$render = $this->_xhtmldoc->dump_mem_xhtml(1, $this->_charset, $doctype);

		header('Content-Type: text/html; charset='.$this->_charset);
		echo $render;
		//$this->xhtml_page_display();
		//last function to use in a script
	}

}
?>