<?php

/***************************************************************************
 xhtml_pmc_list.inc  -
 object to easily build lists, derived from xhtml_form.inc
 -------------------
 begin                :  21/9/2004
 copyright            : (C) 2004 by Olivier Langella
 email                : langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *    modify it under the terms of the GNU Lesser General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/
require_once (APP_ROOT_RELATIVE_PATH.'web_view/lib/php/generic/xhtml_list.inc.php');

class xhtml_pmc_list extends xhtml_list {
	var $_position;
	var $_offset;

	function xhtml_pmc_list(& $xhtmlpage, $pname) {
		$this -> _position = 0;
		$this -> _offset = 30;
		if (!array_key_exists("list_parameters", $_SESSION)) {
			$_SESSION['list_parameters'] = array();
		}
		if (array_key_exists($_GET['view'], $_SESSION['list_parameters'])) {
			$this -> _position = $_SESSION['list_parameters'][$_GET['view']]['position'];
			$this -> _offset = $_SESSION['list_parameters'][$_GET['view']]['offset'];
		}

		$this -> xhtml_list($xhtmlpage, $pname);
		$this -> input_hidden('list_view', $_GET['view']);
		$this->_form->set_attribute('class', 'liste');

	}

	function list_pmc_browse($total) {
		//
		$this -> list_browse($total, $this -> _position, $this -> _offset);
	}

	/** \brief set the basic xhtml list properties
	 *
	 * \param $pmethod is the method used to send values to the http server ("post" or "get")
	 * \param $paction is the URL to send the form values to
	 * \param $pclass is the CSS class, xhtml_form by default
	 */
	function xhtml_form_set($pmethod, $paction, $pclass = 'liste') {

		$this -> _form -> set_attribute("method", strtolower($pmethod));
		$this -> _form -> set_attribute("class", $pclass);
		//check that all & will be replaced by &amp;
		$this -> _form -> set_attribute("action", str_replace('&','&amp;',str_replace('&amp;','&',$paction)));
	}

	function set_htable_row_diff ($htable_row_diff) {
		$this->_htable_row_diff = $htable_row_diff;
	}
}
?>