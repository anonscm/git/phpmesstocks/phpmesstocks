<?php


/***************************************************************************
                      dba/produit/form.inc  -
          enregistrement de formulaire d'un produit de phpMesStocks
    begin                :  28/9/2005
    copyright            : (C) 2005 by Olivier Langella
    email                : Olivier.Langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//require_once (APP_ROOT_RELATIVE_PATH."libpmc/utilisateur_aliases.inc");

$liste_champs_produit = array ('IdProduit','CodeNomenclature','NomProduit','Conditionnement','Qrestante','ProduitNotes'); 
//recherche si un utilisateur a déjà ce nom dans le labo:
//$deja = db_get_id_from('IdMachine', 'machine', array('NomMachine'=>$_POST['NomMachine'], 'IdLabo'=>$_POST['IdLabo']));
$objet_requete = new db_select_pmc();
$objet_requete->select_from(array ('IdProduit'), 'Produits', 'p');
$objet_requete->where_equal('NomProduit', $_POST['NomProduit']);
//$deja = $objet_requete->execute_select($db_link);
$objet_requete->execute_select($db_link);
$deja = false;
if ($objet_requete->is_empty()) {
} else {
	$deja = $objet_requete->get_result_first();
	$deja = $deja['IdProduit'];
}

switch ($_POST['dba']) {
	case 'new' :
		if ($deja != false) {
			$page->xhtml_message('Attention, création impossible, ce produit existe déjà ('.$_POST['NomProduit'].')', 'error');
			return;
		}
		$IdProduit = db_insert_only($db_link, 'Produits', $liste_champs_produit, $_POST, 'IdProduit'); //		echo $IdMachine;
		break;
	case 'update' :
		if (($deja != false) and ($deja != $_POST['IdProduit'])) {
			$page->xhtml_message('Attention, création impossible, ce produit existe déjà ('.$_POST['NomProduit'].')', 'error');
			return;
		}
		db_update_only($db_link, 'Produits', $liste_champs_produit, $_POST, 'IdProduit='.$_POST['IdProduit']);
		$IdProduit = $_POST['IdProduit'];
		/*
		//suppression des téléphones:
		if (array_key_exists('tab_SupprIdTelephone', $_POST)) {
			if ($visitor['utilisateurSuppr']) {
				for ($i = 0; $i < count($_POST['tab_SupprIdTelephone']); $i ++) { //echo $_POST['tab_SupprIdInterface'][$i];
					db_delete($db_link, 'utilisateur_tel', 'IdUtilisateur='.$_POST['tab_SupprIdTelephone'][$i]);
				}

			} else {
				$page->xhtml_message('Suppression des téléphones impossible, vous n\'avez pas les privilèges suffisants', 'error');
			}
		}

		//suppression des équipes:
		if (array_key_exists('tab_SupprIdEquipe', $_POST)) {
			if ($visitor['utilisateurSuppr']) {
				for ($i = 0; $i < count($_POST['tab_SupprIdEquipe']); $i ++) { //echo $_POST['tab_SupprIdInterface'][$i];
					db_delete($db_link, 'utilisateur_equipe', 'IdUtilisateur='.$_POST['tab_SupprIdEquipe'][$i]);
				}

			} else {
				$page->xhtml_message('Suppression des équipes impossible, vous n\'avez pas les privilèges suffisants', 'error');
			}
		}
*/
		break;
	case 'delete' :
/*
		if (array_key_exists('confirm', $_GET) and ($_GET['confirm'] == 'ok')) { //on efface
			if (db_delete($db_link, 'utilisateur_tel', 'IdUtilisateur='.$_GET['IdUtilisateur'])) //suppression de la machine
				if (db_delete($db_link, 'utilisateur_equipe', 'IdUtilisateur='.$_GET['IdUtilisateur']))
					db_delete($db_link, 'utilisateur', 'IdUtilisateur='.$_GET['IdUtilisateur']);
		} else {
			require_once (APP_ROOT_RELATIVE_PATH.'libpmc/fonctions_pmc.inc');
			demande_confirmation('Etes-vous vraiment sûr de vouloir supprimer cet utilisateur ?', $page, $_GET);
			$_GET['view'] = -1;
		}
	*/
		$db_return = true;
		return;
	default :
		return;
}
/*
//ajout du nouveau téléphone
if ($_POST['AjoutNumTelephone'] != '') { //echo "coucou".$_POST['AjoutNomDNSInterface'];
	$objet_requete = new db_select_pmc();
	$objet_requete->select_from(array ('IdTelephone'), 'telephone', 't');
	$objet_requete->where_equal('t.NumTelephone', $_POST['AjoutNumTelephone'], "AND");
	$objet_requete->where_equal('t.IdLabo', $visitor['labo_id'], "AND");
	$objet_requete->execute_select($db_link);
	if ($objet_requete->is_empty()) {
		$page->xhtml_message('Le numéro de téléphone '.$_POST['AjoutNumTelephone'].' n\'existe pas dans ce laboratoire', 'error');
	} else {
		$res = $objet_requete->get_result_first();
		$IdTelephone = $res['IdTelephone'];
		if (db_insert($db_link, 'utilisateur_tel', array ('IdUtilisateur' => $IdUtilisateur, 'IdTelephone' => $IdTelephone)) == false) {
			$page->xhtml_message('L\'ajout de ce téléphone à cet utilisateur a échoué. <br/>Message d\'erreur de mysql: '.mysql_error(), 'error');
		}
	}

}
//ajout de la nouvelle équipe
if ($_POST['AjoutIdEquipe'] > 0) { //echo "coucou".$_POST['AjoutNomDNSInterface'];
	if (db_insert($db_link, 'utilisateur_equipe', array ('IdUtilisateur' => $IdUtilisateur, 'IdEquipe' => $_POST['AjoutIdEquipe'])) == false) {
		$page->xhtml_message('L\'ajout de cette équipe à l\'utilisateur a échoué. <br/>Message d\'erreur de mysql: '.mysql_error(), 'error');
	}
}
*/
$db_return = true;
?>