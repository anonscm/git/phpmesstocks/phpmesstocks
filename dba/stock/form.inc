<?php


/** \brief enregistrement d'un stock
 * 
 */

$liste_champs_stock = array ('IdStock', 'NomStock');
//recherche si un utilisateur a déjà ce nom dans le labo:
//$deja = db_get_id_from('IdMachine', 'machine', array('NomMachine'=>$_POST['NomMachine'], 'IdLabo'=>$_POST['IdLabo']));
$objet_requete = new db_select_pmc();
$objet_requete->select_from(array ('IdStock'), 'Stocks', 's');
$objet_requete->where_equal('NomStock', $_POST['NomStock']);
//$deja = $objet_requete->execute_select($db_link);
$objet_requete->execute_select($db_link);
$deja = false;
if ($objet_requete->is_empty()) {
} else {
	$deja = $objet_requete->get_result_first();
	$deja = $deja['IdStock'];
}

switch ($_POST['dba']) {
	case 'new' :
		if ($deja != false) {
			$page->xhtml_message('Attention, création impossible, ce produit existe déjà ('.$_POST['NomStock'].')', 'error');
			return;
		}
		$IdStock = db_insert_only($db_link, 'Stocks', $liste_champs_stock, $_POST, 'IdStock');
		db_insert($db_link, 'StockUtilisateur', array ('IdStock' => $IdStock, 'IdUtilisateur' => $_POST['IdUtilisateur']));

		break;
	case 'update' :
		if (($deja != false) and ($deja != $_POST['IdStock'])) {
			$page->xhtml_message('Attention, création impossible, ce produit existe déjà ('.$_POST['NomProduit'].')', 'error');
			return;
		}
		db_update_only($db_link, 'Produits', $liste_champs_produit, $_POST, 'IdProduit='.$_POST['IdProduit']);
		$IdProduit = $_POST['IdProduit'];
		/*
		//suppression des téléphones:
		if (array_key_exists('tab_SupprIdTelephone', $_POST)) {
			if ($visitor['utilisateurSuppr']) {
				for ($i = 0; $i < count($_POST['tab_SupprIdTelephone']); $i ++) { //echo $_POST['tab_SupprIdInterface'][$i];
					db_delete($db_link, 'utilisateur_tel', 'IdUtilisateur='.$_POST['tab_SupprIdTelephone'][$i]);
				}
		
			} else {
				$page->xhtml_message('Suppression des téléphones impossible, vous n\'avez pas les privilèges suffisants', 'error');
			}
		}
		
		//suppression des équipes:
		if (array_key_exists('tab_SupprIdEquipe', $_POST)) {
			if ($visitor['utilisateurSuppr']) {
				for ($i = 0; $i < count($_POST['tab_SupprIdEquipe']); $i ++) { //echo $_POST['tab_SupprIdInterface'][$i];
					db_delete($db_link, 'utilisateur_equipe', 'IdUtilisateur='.$_POST['tab_SupprIdEquipe'][$i]);
				}
		
			} else {
				$page->xhtml_message('Suppression des équipes impossible, vous n\'avez pas les privilèges suffisants', 'error');
			}
		}
		*/
		break;
	case 'delete' :
		/*
				if (array_key_exists('confirm', $_GET) and ($_GET['confirm'] == 'ok')) { //on efface
					if (db_delete($db_link, 'utilisateur_tel', 'IdUtilisateur='.$_GET['IdUtilisateur'])) //suppression de la machine
						if (db_delete($db_link, 'utilisateur_equipe', 'IdUtilisateur='.$_GET['IdUtilisateur']))
							db_delete($db_link, 'utilisateur', 'IdUtilisateur='.$_GET['IdUtilisateur']);
				} else {
					require_once (APP_ROOT_RELATIVE_PATH.'libpmc/fonctions_pmc.inc');
					demande_confirmation('Etes-vous vraiment sûr de vouloir supprimer cet utilisateur ?', $page, $_GET);
					$_GET['view'] = -1;
				}
			*/
		$db_return = true;
		return;
	default :
		return;
}

$db_return = true;
?>