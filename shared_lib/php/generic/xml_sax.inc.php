<?php


/** \file shared_lib/php/generic/xml_sax.inc.php
 * \brief object to parse xml document
 * 
 * 
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 15/6/2004
 */

/** \brief object to parse xml document
*
*  the original SAX class is described by Philippe Rigaud, PHP & MySql, O'Reilly
*/
class xml_sax {
	var $_parser;
	var $_data;
	var $_onError;
	/** \brief reference to an xhtml_page object
	*/
	var $_page;

	/** \brief an array to store error messages
	 * 
	 */
	var $_error_messages;

	/** \brief constructor
	*
	*  creates a parser and sets sax handlers.
	* if $page is provided, it outputs directly warning and error message to the xhtml_page.
	* if not, warning and error messages are simply printed to the standard output
	* \param $page reference on the xhtml_page, optional. used to display warning and error message
	*/
	function xml_sax(& $page) {

		$this->_parser = xml_parser_create();
		xml_set_object($this->_parser, & $this);
		//xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, true);
		xml_set_element_handler($this->_parser, "beginElement", "endElement");
		xml_set_character_data_handler($this->_parser, "characterData");
		$this->_onError = false;
		$this->_page = & $page;
		$this->_error_messages = array ();

	}

	/** \brief handler function used when a tag starts
	*
	* called by all starting tags. if a specialized function for this particular tag exists, it will call this specialized function with the same
	* arguments
	* \param $parser a reference on the current sax parser
	* \param $name the name of the tag
	* \param $attrs attributes attached to this tag
	*/
	function beginElement($parser, $name, $attrs) {
		if ($this->_onError)
			return;
		//echo "begin ".$name."\n";
		if (method_exists($this, "begin" . $name)) {
			call_user_func(array (
				& $this,
				"begin" . $name
			), $parser, $name, $attrs);
			//$this->"begin".$name.($parser, $name, $attrs);
		}
		elseif (get_class($this) == "xml_sax") {
			//$this -> message( "begin element : ".$name);
		}

		$this->_data = "";
	}

	/** \brief handler function used when a tag ends
	*
	* called by all ending tags. if a specialized function for this particular tag exists, it will call this specialized function with the same
	* arguments
	* \param $parser a reference on the current sax parser
	* \param $name the name of the tag
	*/
	function endElement($parser, $name) {
		if ($this->_onError)
			return;
		//echo "end ".$name."\n";
		if (method_exists($this, "end" . $name)) {
			call_user_func(array (
				& $this,
				"end" . $name
			), $parser, $name);
		}
		elseif (get_class($this) == "xml_sax") {
			//$this -> message( "end element : ".$name);
		}
	}

	/** \brief handler function to treat tag contents
	*
	* trigered automatically by the parser to handle the content of a tag
	*/
	function characterData($parser, $the_string) {
		if ($this->_onError)
			return;
		$this->_data .= $the_string;
	}

	/** \brief parse an xml file
	*
	* \param $file the name of the file to parse
	*/
	function parse($file) {
		$this->priv_parse($file);
	}

	/** \brief parse an xml string
	*
	* \param $xml_string the string to parse
	*/
	function parse_string($xml_string) {
		if (!xml_parse($this->_parser, $xml_string)) {
			if (strlen($xml_string) > 5000) {
				$this->error("Error " . xml_error_string(xml_get_error_code($this->_parser)) . ", in xml string at line " . xml_get_current_line_number($this->_parser) . ' column : ' . xml_get_current_column_number($this->_parser));
			} else {
				$this->error("Error " . xml_error_string(xml_get_error_code($this->_parser)) . ", in xml string : " . $xml_string . ", at line " . xml_get_current_line_number($this->_parser) . ' column : ' . xml_get_current_column_number($this->_parser));
			}
			return;
		}
	}

	/** \brief private: parse an xml file
	*
	* \param $file the name of the file to parse
	*/
	function priv_parse($file) {
		//unlink ($this -> _parser);
		if (!($f = fopen($file, "r"))) {
			$this->error("ERROR opening file " . $file);
			return;
		}

		while ($data = fread($f, 4096)) {
			//echo $data;
			if (!xml_parse($this->_parser, $data, feof($f))) {
				$this->error("Error " . xml_error_string(xml_get_error_code($this->_parser)) . ", in file : " . $file . ", at line " . xml_get_current_line_number($this->_parser) . ' column : ' . xml_get_current_column_number($this->_parser));
				return;
			}
		}
		fclose($f);
	}

	/** \brief reset the parser
	 * 
	 */
	function reset() {
		xml_parser_free($this->_parser);
		$this->_parser = xml_parser_create();
		xml_set_object($this->_parser, & $this);
		//xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, true);
		xml_set_element_handler($this->_parser, 'beginElement', 'endElement');
		xml_set_character_data_handler($this->_parser, 'characterData');
		$this->_onError = false;

	}

	/** \brief destructor
	*/
	function end() {
		//destructor
		xml_parser_free($this->_parser);
	}

	/** \brief generates an error message
	*
	* \param $message the message to display if an error occured
	*/
	function error($message) {
		$this->message($message, 'error');
		$this->_onError = true;
		$this->_error_messages[] = $message;
	}

	/** \brief tell if there was an error (fatal error when parsing)
	 * 
	 */
	function is_error() {
		return ($this->_onError);
	}

	/** \brief get error messages in one string
	 * 
	 */
	function get_error_messages($line_break = "\n") {
		return (join($this->_error_messages, $line_break));
	}

	/** \brief generates a message
	*
	* \param $message the message to display
	* \param $class the type of the message, optional
	*/
	function message($message, $class = -1) {
		if (is_object($this->_page)) {
			$this->_page->xhtml_message($message, $class);
		} else
			echo $message . "\n";
	}
}
?>