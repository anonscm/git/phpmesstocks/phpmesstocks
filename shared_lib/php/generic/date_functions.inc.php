<?php


/** \file shared_lib/php/generic/date_functions.inc.php
 * \brief library to manipulate vorious date format from SQL to french date
 * 
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 09/09/2002
 */

function datesql2datefr($datesql) {

	//on veut r�cup�rer la date stock�e dans la table sous forme aaaa-mm-jj et l'afficher en date fr sous forme jj/mm/aaaa
	$datesql = str_replace(' ', '', substr($datesql,0,10));
	$regs = array ();
	if (ereg('([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})', $datesql, $regs)) {
		return ($regs[3] . "/" . $regs[2] . "/" . $regs[1]);
	}

	return ($datesql);

}

/** \brief transform sql date to an array
 * 
 * \return an array with 0 => day 1 => month 2=> year
 * 
 */
function datesql2arr($datesql) {
	//SQL date to array: 0 => day 1 => month 2=> year
	$datesql = str_replace(" ", "", $datesql);
	$regs = array ();

	//echo "   datesql2arr ".$datesql;
	if (SQL_DATE_FORMAT == "YYYY-MM-DD") {
		//echo $datesql;
		ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $datesql, $regs);
		if (count($regs) < 3) {
			$arr[0] = 0;
			$arr[1] = 0;
			$arr[2] = 0;
		} else {
			$arr[0] = $regs[3];
			$arr[1] = $regs[2];
			$arr[2] = $regs[1];
		}
	} else {
		ereg("([0-9]{1,2})-([A-Z]{3})-([0-9]{4})", $datesql, $regs);
		if ($regs[1] == "") {
			ereg("([0-9]{1,2})-([A-Z]{3})-([0-9]{1,2})", $datesql, $regs);
			if ($regs[3] < 90)
				$regs[3] = $regs[3] + 2000;
			else
				$regs[3] = $regs[3] + 1900;
		}
		$arr[0] = $regs[1];
		$date_mois = $regs[2];
		switch ($date_mois) {
			case "JAN" :
				$date_mois = "1";
				break;
			case "FEB" :
				$date_mois = "2";
				break;
			case "MAR" :
				$date_mois = "3";
				break;
			case "APR" :
				$date_mois = "4";
				break;
			case "MAY" :
				$date_mois = "5";
				break;
			case "JUN" :
				$date_mois = "6";
				break;
			case "JUL" :
				$date_mois = "7";
				break;
			case "AUG" :
				$date_mois = "8";
				break;
			case "SEP" :
				$date_mois = "9";
				break;
			case "OCT" :
				$date_mois = "10";
				break;
			case "NOV" :
				$date_mois = "11";
				break;
			case "DEC" :
				$date_mois = "12";
				break;
			default :
				break;
		}
		$arr[1] = $date_mois;
		$arr[2] = $regs[3];
		//print $arr[1];
	}

	//if ($regs[3] == "") return ($datesql);
	//$Datefr = $regs[3]."/".$regs[2]."/".$regs[1];

	return ($arr);

}

function form_date_yyyy_mm_dd($date_jour, $date_mois, $date_annee, $separator = "-") {
	if (strlen($date_jour) == 1)
		$date_jour = "0" . $date_jour;
	if (strlen($date_mois) == 1)
		$date_mois = "0" . $date_mois;
	if ($date_annee < 30)
		$date_annee += 2000;
	if ($date_annee < 100)
		$date_annee += 1900;
	return ($date_annee . $separator . $date_mois . $separator . $date_jour);
}

function form_date_sql($date_jour, $date_mois, $date_annee) {
	//echo "  form_date_sql  ".$date_jour.$date_mois.$date_annee;
	if (SQL_DATE_FORMAT == "YYYY-MM-DD") {
		switch ($date_mois) {
			case "JAN" :
				$date_mois = "01";
				break;
			case "FEB" :
				$date_mois = "02";
				break;
			case "MAR" :
				$date_mois = "03";
				break;
			case "APR" :
				$date_mois = "04";
				break;
			case "MAY" :
				$date_mois = "05";
				break;
			case "JUN" :
				$date_mois = "06";
				break;
			case "JUL" :
				$date_mois = "07";
				break;
			case "AUG" :
				$date_mois = "08";
				break;
			case "SEP" :
				$date_mois = "09";
				break;
			case "OCT" :
				$date_mois = "10";
				break;
			case "NOV" :
				$date_mois = "11";
				break;
			case "DEC" :
				$date_mois = "12";
				break;
			default :
				break;
		}
		return (datefr2datesql($date_jour, $date_mois, $date_annee));
	} else {
		if (strlen($date_jour) == 1)
			$date_jour = "0" . $date_jour;
		if (strlen($date_mois) == 1)
			$date_mois = "0" . $date_mois;
		switch ($date_mois) {
			case "01" :
				$date_mois = "JAN";
				break;
			case "02" :
				$date_mois = "FEB";
				break;
			case "03" :
				$date_mois = "MAR";
				break;
			case "04" :
				$date_mois = "APR";
				break;
			case "05" :
				$date_mois = "MAY";
				break;
			case "06" :
				$date_mois = "JUN";
				break;
			case "07" :
				$date_mois = "JUL";
				break;
			case "08" :
				$date_mois = "AUG";
				break;
			case "09" :
				$date_mois = "SEP";
				break;
			case "10" :
				$date_mois = "OCT";
				break;
			case "11" :
				$date_mois = "NOV";
				break;
			case "12" :
				$date_mois = "DEC";
				break;
			default :
				break;
		}

		return ($date_jour . "-" . $date_mois . "-" . $date_annee);
	}
}

function datefr2datesql($date_jour, $date_mois = -1, $date_annee = -1) {
	if (($date_mois == -1) && ($date_annee == -1)) {
		// 1 seul argument: $date_jour contient une chaine du type jj/mm/aaaa
		$date_jour = str_replace(" ", "", $date_jour); //�liminer les espaces

		$regs = array ();
		ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $date_jour, $regs);

		if (strlen($regs[3]) == 2) {
			//	if ($regs[3] > 70) $regs[3] = "19".$regs[3];
			$regs[3] = "20" . $regs[3];
		}

		$datesql = $regs[3] . "-" . $regs[2] . "-" . $regs[1];

		return $datesql;
	} else {
		// 3 arguments: jour, mois, annee
		//$chaine=str_replace(" ","",$date_jour);
		//$chaine=str_replace(" ","",$date_mois);
		//$chaine=str_replace(" ","",$date_annee);

		if (strlen($date_jour) == 1)
			$date_jour = "0" . $date_jour;
		if (strlen($date_mois) == 1)
			$date_mois = "0" . $date_mois;

		$joursql = $date_jour;
		$moissql = $date_mois;

		if (strlen($date_annee) == "4")
			$anneesql = $date_annee;

		else {

			$toto = "20";
			$titi = $date_annee;
			$anneesql = $toto . $titi;
		}

		$datesql = "$anneesql-$moissql-$joursql";

		return $datesql;
	}
}
?>