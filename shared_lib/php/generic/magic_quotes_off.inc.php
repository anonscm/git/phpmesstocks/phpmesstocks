<?php


/** \file shared_lib/php/generic/magic_quotes_off.inc.php
*
* see if the PHP environment is set with magic_quotes ON
* if so: it stripslashes from the $_POST array
* it is necessary to get rid of problems with other PHP environment and to use other functions (database functions)
* the same way if the data are provided by a file, or by a form in PHP
*
* \author Olivier Langella <olivier.langella@moulon.inra.fr> 
*/

/** \brief strip slashes from array of strings
*
* \param $array reference on an array 
*/

function stripslashes_array(& $array) {
	while (list ($key) = each($array)) {
		if (is_array($array[$key])) {
			stripslashes_array($array[$key]);
		} else {
			$array[$key] = stripslashes($array[$key]);
		}
	}
}

/*
	if the php server automatically adds slashes to posted values: remove it
*/
if (get_magic_quotes_gpc()) {
	stripslashes_array($_POST);
}
?>