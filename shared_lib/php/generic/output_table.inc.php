<?php


/** \file shared_lib/php/generic/output_table.inc.php 
 * \brief output directly elements of a table
 * 
 * by default, this output xhtml table
 */
require_once (APP_ROOT_RELATIVE_PATH . 'shared_lib/php/generic/utf8.inc.php');

class output_table {
	var $_out;
	var $_output_filename;

	function output_table() {
		$this->_out = null;
	}

	function set_output_filename($output_filename) {
		$this->_output_filename = $output_filename;
	}

	function http_header() {
		//			Header('Content-type:  text/plain');
		$this->priv_http_header();
	}
	function priv_http_header() {
		if ($this->_output_filename !== null) {
			Header('Content-Disposition: attachment; filename=' . $this->_output_filename);
		}

	}

	function get_type() {
		return ('xhtml');
	}

	function start_table($param = array ()) {
		if (count($param) > 0) {
			$temp = '<table';
			foreach ($param as $key => $value) {
				$temp .= ' ' . $key . '="' . $value . '"';
			}
			$temp .= '>';
			$this->priv_echo($temp);
		} else {
			$this->priv_echo('<table>');
		}
	}

	function start_line() {
		$this->priv_echo('<tr>');
	}

	function header_cell($message) {
		$this->priv_echo('<th>' . $message . '</th>');
	}

	/** \brief display a table cell
	 * 
	 * \param $message string to display in the cell OR an array of string to display as a list
	 */
	function cell($message) {
		if (is_array($message)) {
			foreach ($message as $i => $temp) {
				$message[$i] = str_replace(' ', '&nbsp;', $temp);
			}
		}
		$this->priv_cell($message);
	}
	function priv_cell($message) {
		if (is_array($message)) {
			$this->priv_echo('<td><ul><li>' . join('</li><li>', $message) . '</li></ul></td>');
		} else {
			$this->priv_echo('<td>' . $message . '</td>');
		}
	}

	/** \brief display a table cell with an url link
	 * 
	 * \param $message string to display in the cell OR an array of string to display as a list
	 * \param $url string containing the URL OR array of string containin URLs
	 */
	function cell_anchor($message, $url) {
		if (is_array($message)) {
			foreach ($message as $i => $temp) {
				$message[$i] = '<a href="' . $url[$i] . '"/>' . $temp . '</a>';
			}
		} else {
			$message = '<a href="' . $url . '"/>' . $message . '</a>';
		}
		$this->priv_cell($message);
	}

	/** \brief display a table cell with an url link in a separate tab or window
	 * 
	 * \param $message string to display in the cell OR an array of string to display as a list
	 * \param $url string containing the URL OR array of string containin URLs
	 */
	function cell_tab_anchor($message, $url) {
		if (is_array($message)) {
			foreach ($message as $i => $temp) {
				$message[$i] = '<a href="' . $url[$i] . '" onclick="window.open(this.href); return false;">' . $temp . '</a>';
			}
		} else {
			$message = '<a href="' . $url . '" onclick="window.open(this.href); return false;">' . $message . '</a>';
		}
		$this->priv_cell($message);
	}

	function end_line() {
		$this->priv_echo('</tr>' . "\n");
	}

	function end_table() {
		$this->priv_echo('</table>');
	}

	function priv_echo($towrite) {
		if ($this->_out === null) {
			echo utf8_ensure($towrite);
		}
	}
}
?>