<?php


/** \file shared_lib/php/generic/db_select.inc.php
 * 
 * object describing an interface to execute sql queries from the sql_select object
         this have to be implemented for any database backend to work correctly (Postgresql, Oracle...)
 * 
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 24/01/2005
 */

//require_once

/** \brief interface to execute sql queries and cache system
*
* object describing an interface to execute sql queries from the sql_select object
*       this have to be implemented for any database backend to work correctly (Postgresql, Oracle...)
* this object implements too a mecanism to keep results of sql queries that acts as a cache system
* \author Olivier Langella <olivier.langella@moulon.inra.fr>
*/

class db_select extends sql_select {

	/** \brief boolean true in case of sql error
	*/
	var $_sql_error;
	/** \brief string details on the sql error
	*/
	var $_sql_error_message;

	/** \brief array that stores result
	*/
	var $_result;

	/** \brief constructor, call the base object sql_selelct
	*/

	function db_select() { //constructor
		$tmp = array ();
		$this->_result = & $tmp;
		$this->_sql_error = false;
		$this->sql_select();
	}

	function is_sql_error() {
		return ($this->_sql_error);
	}

	function get_error() {
		return ($this->_sql_error_message);
	}

	/** \brief count distinct rows based on a fieldname
	*
	* \param $db_link the link to the database
	* \param $fieldname the field name we want to count rows on
	* \return integer for the number of rows containing different fieldnames 
	*/
	function count_distinct($db_link, $fieldname) {
		echo 'db_select member function count_distinct() is not implemented, please check your PHP code';
		exit (0);
	}

	/** \brief execute the current sql query
	*
	* execute the current sql_query: this relies on the implementation of "priv_db_select" for each database backend 
	* (oracle, postgresql...)
	*
	* \param $db_link the link to the database
	* \return boolean true if the select succeeded false otherwise
	*/
	function execute_select($db_link) {
		//return ($this -> priv_db_exec($db_link, $this -> get_sql_string()));
		return ($this->priv_db_exec($db_link, $this->get_sql_string()));
		//return ($this -> _sql_error);
	}

	/** \brief get a result of a query
	*
	* \return array the result is a 2D array with first dimension the name of the field and second dimension row numbers
	*		or an empty array if there is no result
	*/
	function & get_result() {
		$tmp = & $this->_result;
		return ($tmp);
	}

	/** \brief return only the first row of the result
	*
	* \return array the result is a 1D array with the name of the field as key and corresponding values
	*		or an empty array if there is no result
	*/
	function get_result_first() { //the result is a 1D array with the name of the field as first dimension
		$res2 = array ();
		foreach ($this->_result as $fieldname => $value) {
			//echo $fieldname;
			$res2[$fieldname] = $value[0];
		}
		return ($res2);
	}

	/** \brief private function: execute the current sql query
	*
	* execute the current sql_query: this relies on the implementation of "priv_db_select" for each database backend 
	* (oracle, postgresql...)
	*
	* \param $db_link the link to the database
	* \param $sql_string the sql query to execute
	* \return boolean true if the select succeeded false otherwise
	*/
	function priv_db_exec($db_link, $sql_string) {
		//return exec_requete($sql_string, $db_link);
		echo 'db_select member function priv_db_exec() is not implemented, please check your PHP code';
		exit (0);

	}

	function add_error($message) {
		$this->_sql_error = true;
		$this->_sql_error_message .= $message;
	}
}
?>