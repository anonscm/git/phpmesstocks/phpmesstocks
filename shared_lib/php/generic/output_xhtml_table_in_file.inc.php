<?php


/** \file shared_lib/php/generic/output_xhtml_table_in_file.inc.php 
 * \brief output directly elements of a table
 * 
 * by default, this output xhtml table
 */

class output_xhtml_table_in_file extends output_table {

	function output_xhtml_table_in_file() {
		$this->output_table();
	}

	function get_type() {
		return ('xhtmlraw');
	}

	function http_header() {
		Header('Content-type:  application/xhtml+xml');
		$this->priv_http_header();
	}

	function start_table($param = array ()) {
		$this->priv_echo('<?xml version="1.0"?>
						<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
						<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
						<title>PROTICdb query builder-report settings
						</title>
						</head>
						<body>');

		if (count($param) > 0) {
			$temp = '<table';
			foreach ($param as $key => $value) {
				$temp .= ' ' . $key . '="' . $value . '"';
			}
			$temp .= '>';
			$this->priv_echo($temp);
		} else {
			$this->priv_echo('<table>');
		}
	}
	function end_table() {
		$this->priv_echo('</table></body>');
		$this->priv_echo('</html>');
	}

	function cell_tab_anchor($message, $url) {
		if (is_array($message)) {
			foreach ($message as $i => $temp) {
				$message[$i] = $temp;
			}
		} else {
			$message = $message;
		}
		$this->priv_cell($message);
	}

}
?>