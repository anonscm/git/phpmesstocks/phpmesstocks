<?php


/** \file shared_lib/php/generic/write_xml.inc.php
 * \brief very simple functions to write xml tags
 * 
 * XML outputs are in UNICODE (utf8), '&' char is substituted with '&amp;'
 * 
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 09/09/2002
 */

if (APP_ROOT_RELATIVE_PATH != '') {
	require_once (APP_ROOT_RELATIVE_PATH . 'shared_lib/php/generic/utf8.inc.php');
}

/** \brief check that a date is XML well formed as xs:date data type
 * 
 * \param $date a SQL date
 * \return xs:date xml date
 */
function xml_date($date) {
	return (substr($date, 0, 10));
}

/** \brief open an xml tag and write to the output stream
 * 
 * Don't forget to close the tag with "xml_close_tag"
 * 
 * \param $output_stream the ressource to print the tag OR if NULL, echo to the standard output
 * \param $tag the name of the tag to write
 * \param $arr_attributes hash table with the name of the attribute as key and the value
 */

function xml_open_tag(& $output_stream, $tag, $arr_attributes = '') {
	$xml = '<' . $tag;
	$xml .= priv_xml_tag_attributes($arr_attributes);
	$xml .= '>';
	priv_xml_print_to_stream($output_stream, $xml);
}

/** \brief close an xml tag and write to the output stream
 * 
 * \param $output_stream the ressource to print the tag OR if NULL, echo to the standard output
 * \param $tag the name of the tag to write
 */

function xml_close_tag(& $output_stream, $tag) {
	$xml = '</' . $tag . ">\n";
	priv_xml_print_to_stream($output_stream, $xml);
}

/** \brief write an entire xml tag to the output stream
 * 
 * \param $output_stream the ressource to print the tag OR if NULL, echo to the standard output
 * \param $tag the name of the tag to write
 * \param $content the content of this xml element
 * \param $arr_attributes hash table with the name of the attribute as key and the value
 */
function xml_tag(& $output_stream, $tag, $content = '', $arr_attributes = '') {
	$xml = '<' . $tag;
	$xml .= priv_xml_tag_attributes($arr_attributes);
	if ((is_string($content)) and ($content == '')) {
		$xml .= '/>';
	} else {
		$xml .= '>';
		if (!is_array($content)) {
			$content = str_replace('&amp;', '&', $content);
			$content = str_replace('&', '&amp;', $content);
			$content = str_replace('<', '&lt;', $content);

			$xml .= utf8_ensure($content);
		}
		$xml .= '</' . $tag . ">\n";
	}
	priv_xml_print_to_stream($output_stream, $xml);
}

/** \brief print a string to an output stream OR the standard output OR append to a string
 * 
 */
function priv_xml_print_to_stream(& $output_stream, $xml) {
	if (is_null($output_stream)) {
		echo $xml;
	} else
		if (is_string($output_stream)) {
			$output_stream .= $xml;
		} else {
			fwrite($output_stream, $xml);
		}
}

/** \brief build a string containing attributes and their values
 * 
 * \param $arr_attributes hash table with the name of the attribute as key and the value
 * \return a string containing formatted attributes and their values
 */
function priv_xml_tag_attributes($arr_attributes) {
	$xml = '';
	if (is_array($arr_attributes)) {
		foreach ($arr_attributes as $key => $value) {
			if (is_bool($value)) {
				if ($value) {
					$xml .= ' ' . $key . '="true"';
				} else {
					$xml .= ' ' . $key . '="false"';
				}
			} else
				if (!is_array($value)) {
					$value = str_replace('&amp;', '&', $value);
					$value = str_replace('&', '&amp;', $value);

					//$value = str_replace('<', '&lt;', $value);
					//$value = str_replace('\'', '&apos;', $value);
					$value = str_replace('"', '&quot;', $value);

					$xml .= ' ' . $key . '="' . utf8_ensure($value) . '"';
				}
		}
	}
	return ($xml);
}
?>