<?php


/** \file shared_lib/php/generic/output_csv_table.inc.php 
 * \brief output directly elements of a table
 * 
 * by default, this output xhtml table
 */
require_once (APP_ROOT_RELATIVE_PATH . 'shared_lib/php/generic/output_table.inc.php');

class output_csv_table extends output_table {

	function output_csv_table() {
		$this->output_table();
	}

	function get_type() {
		return ('comma_separated_value_file');
	}

	function http_header() {
		Header('Content-type:  text/csv');
		$this->priv_http_header();
	}

	function start_table() {
		//$this->priv_echo('<table>');
	}
	function start_line() {
		//$this->priv_echo('<tr>');
	}
	function header_cell($message) {
		$this->priv_echo($message . ",");
	}

	function cell($message) {
		if (is_array($message)) {
			$this->priv_echo(join(' | ', $message) . ",");

		} else {
			$this->priv_echo($message . ",");
		}
	}
	function cell_anchor($message, $url) {
		$this->cell($message);
	}

	function cell_tab_anchor($message, $url) {
		$this->cell($message);
	}

	function end_line() {
		$this->priv_echo("\n");
	}

	function end_table() {
		//$this->priv_echo('</table>');
	}
}
?>