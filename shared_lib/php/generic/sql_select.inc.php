<?php


/** \file shared_lib/php/generic/sql_select.inc.php
 * \brief base object to easily build sql select query string
 * 
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 28/12/2004
 */

/** \brief build sql select query
*
* build easily SQL select request with arrays and member functions
* this prevent SQL syntax error (PHP syntax error are reported quickly) and the object deals with
* SGBD SQL syntax specificity (Postgres and Oracle)
* \author Olivier Langella <langella@moulon.inra.fr>
*
*/

class sql_select {

	var $_query_pre_from;
	var $_query_post_from;
	var $_query_where;
	var $_query_order_by;
	var $_query_group_by;
	var $_query_having;
	var $_query_limit;
	var $_bool_from;
	var $_bool_where;
	var $_bool_where_bloc_start;
	var $_bool_order_by;
	var $_bool_group_by;
	var $_bool_having;

	var $_right_super_select;

	/** \brief constructor
	*/
	function sql_select() { //constructor
		$this->_query_pre_from = '';
		$this->_query_post_from = '';
		$this->_query_where = '';
		$this->_query_group_by = '';
		$this->_query_having = '';
		$this->_query_order_by = '';
		$this->_query_limit = '';
		$this->_bool_from = false; //no "from" in the query at this momment
		$this->_bool_where = false; //no "where" in the query at this momment
		$this->_bool_where_bloc_start = false;
		$this->_bool_order_by = false;
		$this->_bool_group_by = false;
		$this->_bool_having = false;

		$this->_right_super_select = false;
		$this->_super_operator = false;

	}

	/** \brief tells if the query as a WHERE clause
	 * 
	 */
	function is_where() {
		return ($this->_bool_where);
	}

	/** \brief clear the where clause
	*/
	function clear_where() {
		$this->_query_where = '';
		$this->_bool_where = false; //no "where" in the query at this momment
		$this->_bool_where_bloc_start = false;
	}

	/** \brief get the sql string
	* 
	* \return string the sql select query
	*/
	function get_sql_string() {
		//simple select query
		$sql_string = 'SELECT ' . substr($this->_query_pre_from, 1) . ' ' . $this->_query_post_from;
		$sql_string .= $this->_query_where . $this->_query_group_by;
		$sql_string .= $this->_query_having . $this->_query_order_by;
		$sql_string .= $this->_query_limit;
		return ($sql_string);
	}

	/** \brief limit the number of rows
	*
	* limit the number of rows returned
	* \param $position the position of the first row to return
	* \param $offset the number of row to return from $position
	* \param $total total number of rows, optional, modify the offset if the superior limit exceeds the total number of rows
	*/
	function limit(& $position, & $offset, $total = -1) {
		if ($total != -1) {
			if (($total - $position) < 0) {
				$position = $total - $offset;
			}
			if ($position < 0)
				$position = 0;
		}
		$this->_query_limit = ' LIMIT ' . $position . ', ' . $offset;
	}

	/** \brief begin the sql query
	*
	* \param $array_fieldnames array containing the fieldnames to retrieve
	* \param $table the name of the table on which the select is performed
	* \param $alias alias of the table, optional
	*/
	function select_from($array_fieldnames, $table, $alias = -1) {
		if (is_array($array_fieldnames) == false) {
			echo "php syntax error in the object sql_select member function sql_select. expected array as first parameter.";
			exit;
		}

		if ($this->_bool_from) {
			$this->priv_select($array_fieldnames, ',', $table, $alias);
		} else {
			$this->priv_select($array_fieldnames, 'FROM', $table, $alias);
			$this->_bool_from = true;
		}
	}

	/** \brief left join on
	*
	* \param $array_fieldnames array containing the fieldnames to retrieve
	* \param $table the name of the table on which the join is performed
	* \param $alias alias of the table
	* \param $clause the clause to satisfy to join tables (example: "t1.field1=t2.field2")
	*/
	function left_join_on($array_fieldnames, $table, $alias, $clause) {
		//LEFT JOIN table AS alias ON (clause)
		if ($this->_bool_from) {
			$this->priv_select($array_fieldnames, "LEFT JOIN", $table, $alias, "ON (" . $clause . ")");
		} else {
			//error
		}
	}

	/** \brief left join
	*
	* \param $array_fieldnames array containing the fieldnames to retrieve
	* \param $table the name of the table on which the join is performed
	* \param $alias alias of the table
	* \param $key the name of the field used to join the 2 tables (must be the same in the 2 table, if not use left_join_on)
	*/
	function left_join($array_fieldnames, $table, $alias, $key) {
		//LEFT JOIN table AS alias USING ($key)
		if ($this->_bool_from) {
			$this->priv_select($array_fieldnames, "LEFT JOIN", $table, $alias, "USING (" . $key . ")");
		} else {
			//error
		}
	}

	/** \brief left outer join on
	*
	* the outer join retrieves null values if no corresponding records in the right table
	*
	* \param $array_fieldnames array containing the fieldnames to retrieve
	* \param $table the name of the table on which the join is performed
	* \param $alias alias of the table
	* \param $clause the clause to satisfy to join tables (example: "t1.field1=t2.field2")
	*/

	function left_outer_join_on($array_fieldnames, $table, $alias, $clause) {
		//LEFT OUTER JOIN table AS alias ON (clause)
		if ($this->_bool_from) {
			$this->priv_select($array_fieldnames, "LEFT OUTER JOIN", $table, $alias, "ON (" . $clause . ")");
		} else {
			//error
		}
	}

	/** \brief left outer join
	*
	* the outer join retrieves null values if no corresponding records in the right table
	*
	* \param $array_fieldnames array containing the fieldnames to retrieve
	* \param $table the name of the table on which the join is performed
	* \param $alias alias of the table
	* \param $key the name of the field used to join the 2 tables (must be the same in the 2 table, if not use left_outer_join_on)
	*/
	function left_outer_join($array_fieldnames, $table, $alias, $key) {
		//LEFT OUTER JOIN table AS alias USING ($key)
		if ($this->_bool_from) {
			$this->priv_select($array_fieldnames, "LEFT OUTER JOIN", $table, $alias, "USING (" . $key . ")");
		} else {
			//error
		}
	}

	/**
	* includes the result of another sql select query in a where statement: 
	* the values of the field name $fieldname must be in the result of the sql query $sql_select_object
	* \param $fieldname the name of the field to check against the $sql_select_object results
	* \param $sql_select_object reference to the sql query to include
	* \param  $operand AND by default other values are OR, NOT
	*/
	function where_not_in($fieldname, & $sql_select_object, $operand = 'AND') {
		$left = $fieldname . ' NOT IN (';
		if (is_object($sql_select_object))
			$right = $sql_select_object->get_sql_string() . ')';
		else
			$right = $sql_select_object . ')';
		$this->priv_where($operand, $left, $right);

	}

	/**
	* includes the result of another sql select query in a where statement: 
	* the values of the field name $fieldname must be in the result of the sql query $sql_select_object
	* \param $fieldname the name of the field to check against the $sql_select_object results
	* \param $sql_select_object reference to the sql query to include OR a sql string
	* \param  $operand AND by default other values are OR, NOT
	*/
	function where_in($fieldname, & $sql_select_object, $operand = 'AND') {
		$left = $fieldname . ' IN (';
		if (is_object($sql_select_object))
			$right = $sql_select_object->get_sql_string() . ')';
		else
			$right = $sql_select_object . ')';
		$this->priv_where($operand, $left, $right);

	}

	/**
	*/
	function where_in_list($fieldname, $list, $operand = 'AND') {
		$left = $fieldname . ' IN (';
		$right = implode(', ', $list) . ')';
		$this->priv_where($operand, $left, $right);

	}

	/** \brief begin a block in the WHERE statement with "("
	*
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_block_start($operand = 'AND') {
		if ($this->_bool_where_bloc_start)
			$operand = '';
		if ($this->_bool_where) {
			if ($operand == 'NOT') {
				$operand = 'AND NOT';
			}
			$this->_query_where .= $operand . ' (';
		} else {
			//$this -> priv_where("WHERE (", "", "");
			if ($operand == 'NOT') {
				$this->_query_where .= 'WHERE NOT (';
			} else {
				$this->_query_where .= 'WHERE (';
			}
			$this->_bool_where = true;
		}
		$this->_bool_where_bloc_start = true;
	}

	/** \brief ends a block in the WHERE statement with ")"
	*/
	function where_block_end() {
		if ($this->_bool_where) {
			$this->_query_where .= ") ";
		} else {
			//error
		}
		$this->_bool_where_bloc_start = false;
	}

	/** \brief adds an equality test to the sql query
	*
	* \param  $left the left value of equality test ("table1.field1" for example)
	* \param $right the right value of equality test ("table2.field3" for example)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_equal($left, $right, $operand = "AND") {
		//if (is_numeric($right) and ($option!='not_num')) {
		//	$left.= '=';
		//} else {
		if (ereg('(^=)', $right)) {
		} else {
			$right = $this->priv_add_slashes($right);
			$left .= "='";
			$right .= "'";
		}
		//}
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds a test on the NULL value
	*
	* \param  $left the left value of equality test ("table1.field1" for example)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_is_null($left, $operand = "AND") {
		$this->priv_where($operand, $left, ' IS NULL ');
	}

	/** \brief adds a test on the NULL value
	*
	* \param  $left the left value of equality test ("table1.field1" for example)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_is_not_null($left, $operand = "AND") {
		$this->priv_where($operand, $left, ' IS NOT NULL ');
	}

	/** \brief adds an difference test to the sql query
	*
	* \param  $left the left value of difference test ("table1.field1" for example)
	* \param $right the right value of difference test ("table2.field3" for example)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_different($left, $right, $operand = "AND") {
		//if (is_numeric($right)) {
		//	$left.= '!=';
		//} else {
		$right = $this->priv_add_slashes($right);
		if (ereg('(^=)', $right)) {
			$left .= '!';
		} else {
			$left .= "!='";
			$right .= "'";
		}
		//}
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds string insensitive case comparison to the sql query
	*
	* will select all rows with $field containing the $pattern
	* 
	* \param $field the field name to search
	* \param $pattern the pattern to look for ( % => wildcard)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_ilike($field, $pattern, $operand = "AND") {
		$right = $this->priv_add_slashes($pattern);
		$left = $field . ' ILIKE \'';
		$right .= '\'';
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds string comparison to the sql query
	*
	* will select all rows with $field containing the $pattern
	* 
	* \param $field the field name to search
	* \param $pattern the pattern to look for ( % => wildcard)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_like($field, $pattern, $operand = "AND") {
		$right = $this->priv_add_slashes($pattern);
		$left = $field . ' LIKE \'';
		$right .= '\'';
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds BETWEEN operator to the sql query
	*
	* greater than or equal to $first and less than or equal to $second
	* 
	* \param $left the left value of the test ("table1.field1" for example)
	* \param $first the begining of range
	* \param $second the end of range
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_between($left, $first, $second, $operand = 'AND') {
		$right = '';
		$left .= ' BETWEEN ';
		if (ereg('(^=)', $first)) {
			$left .= ereg_replace('(^=)', '', $first) . ' AND ';
		} else {
			$left .= '\'' . $this->priv_add_slashes($first) . '\' AND ';
		}
		if (ereg('(^=)', $second)) {
			$left .= ereg_replace('(^=)', '', $second) . '';
		} else {
			$left .= '\'' . $this->priv_add_slashes($second) . '\'';
		}

		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds date comparison greater than OR equal to the sql query
	*
	* will select all rows with $left date equal or superior to $right or the current date
	* \param  $left the left value of date test ("table1.field1" for example)
	* \param $right the right value of date test, by default, the current date
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_date_ge($left, $right = '', $operand = 'AND') {
		$left .= '>=';
		if ($right == '')
			$right = 'NOW()';
		else
			$right = '\'' . $right . '\'';
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds the greater than operator for the sql query
	 * 
	 * \param $left the left value of equality test ("table1.field1" for example)
	 * \param $right the right value of equality test ("table2.field3" for example)
	 * \param  $operand AND by default other values are OR, NOT
	 * @author Joan Bordeau
	 */
	function where_gt($left, $right = '', $operand = 'AND') {
		$left .= '>';
		if (ereg('(^=)', $right)) {
			$right = ereg_replace('(^=)', '', $right);
		} else {
			$right = $this->priv_add_slashes($right);
			$left .= '\'';
			$right .= '\'';
		}
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds the greater than OR equal operator for the sql query
	 * 
	 * \param  $left the left value of equality test ("table1.field1" for example)
	 * \param $right the right value of equality test ("table2.field3" for example)
	 * \param  $operand AND by default other values are OR, NOT
	 * @author Joan Bordeau
	 */
	function where_ge($left, $right = '', $operand = 'AND') {
		$left .= '>=';
		if (ereg('(^=)', $right)) {
			$right = ereg_replace('(^=)', '', $right);
		} else {
			$right = $this->priv_add_slashes($right);
			$left .= '\'';
			$right .= '\'';
		}
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds the less than operator for the sql query
	 * 
	 * \param  $left the left value of equality test ("table1.field1" for example)
	 * \param $right the right value of equality test ("table2.field3" for example)
	 * \param  $operand AND by default other values are OR, NOT
	 */
	function where_lt($left, $right = '', $operand = 'AND') {
		$left .= '<';
		if (ereg('(^=)', $right)) {
			$right = ereg_replace('(^=)', '', $right);
		} else {
			$right = $this->priv_add_slashes($right);
			$left .= '\'';
			$right .= '\'';
		}
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds the less than OR equal operator for the sql query
	 * 
	 * \param  $left the left value of equality test ("table1.field1" for example)
	 * \param $right the right value of equality test ("table2.field3" for example)
	 * \param  $operand AND by default other values are OR, NOT
	 */
	function where_le($left, $right = '', $operand = 'AND') {
		$left .= '<=';
		if (ereg('(^=)', $right)) {
			$right = ereg_replace('(^=)', '', $right);
		} else {
			$right = $this->priv_add_slashes($right);
			$left .= '\'';
			$right .= '\'';
		}
		$this->priv_where($operand, $left, $right);
	}

	/** \brief adds begin with string comparison to the sql query
	*
	* will select all rows with $left begining with $right string
	* \param  $left the left value of string test ("table1.field1" for example)
	* \param $right the right value of string test ("string_to_search" for example)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function where_begin_with($left, $right = "", $operand = "AND") {
		$right = $this->priv_add_slashes($right);
		$left .= " LIKE '";
		$right .= "%'";
		$this->priv_where($operand, $left, $right);
	}

	/** \brief group by the resulting rows
	 * 
	 * After passing the WHERE filter, the derived input table may be subject 
	 * to grouping, using the GROUP BY clause, and elimination of group rows using 
	 * the HAVING clause.
	*
	* \param  $fieldname the name of the field on which group by is performed
	*/
	function group_by($fieldname) {
		//$order_type = ASC or DESC

		if ($this->_bool_group_by) {
			$this->_query_group_by .= ', ';
		} else {
			$this->_query_group_by .= ' GROUP BY ';
			$this->_bool_group_by = true;
		}
		$this->_query_group_by .= $fieldname;

	}

	/** \brief having equal look for equality on a group
	 * 
	 * example: GROUP BY protic_order.order_id
			HAVING COUNT(protic_well.ms_sample_id)=9
	 * 
	*
	* \param  $fieldname the name of the field on which "having" is performed
	 * \param $right the right value of equality test ("table2.field3" for example)
	* \param  $operand AND by default other values are OR, NOT	
	*/
	function having_equal($fieldname, $right, $operand = 'AND') {

		if ($this->_bool_having) {
			$this->_query_having .= ' ' . $operand . ' ';
		} else {
			$this->_query_having .= ' HAVING ';
			$this->_bool_having = true;
		}
		$this->_query_having .= $fieldname . '=' . $right;

	}

	/** \brief sorts the resulting rows
	*
	* \param  $field the name of the field on which sort is performed
	* \param  $order_type ASC by default (ascendant) other value is DESC (descendant)
	*/
	function order_by($field, $order_type = "ASC") {
		//$order_type = ASC or DESC

		if ($this->_bool_order_by) {
			$this->_query_order_by .= ", ";
		} else {

			$this->_query_order_by .= " ORDER BY ";
			$this->_bool_order_by = true;
		}

		if ($order_type == "ASC")
			$this->_query_order_by .= $field;
		else
			if ($order_type == "DESC")
				$this->_query_order_by .= $field . " DESC";

	}

	/** \brief count distinct rows from a table
	 * 
	 * the result of this query wille return a table, which contains a single row and one column. The only value
	 * is an integer that indicates the number of rows containing this fieldname in this table
	 * 
	 */
	function count_distinct_from($fieldname, $table, $alias = -1) {
		//$this->_query_pre_from .= ' count('.$fieldname.') '.$this->_query_post_from;
		if ($fieldname != '*')
			$fieldname = $table . '.' . $fieldname;
		$this->priv_select(array (
			'=COUNT(DISTINCT ' . $fieldname . ')'
		), 'FROM', $table, $alias);
		//$this->_query_post_from .= $table;
		$this->_bool_from = true;

	}

	/** \brief count rows from a table
	 * 
	 * the result of this query wille return a table, which contains a single row and one column. The only value
	 * is an integer that indicates the number of rows containing this fieldname in this table
	 * 
	 */
	function count_from($fieldname, $table, $alias = -1) {
		//$this->_query_pre_from .= ' count('.$fieldname.') '.$this->_query_post_from;
		if ($fieldname != '*')
			$fieldname = $table . '.' . $fieldname;
		$this->priv_select(array (
			'=COUNT(' . $fieldname . ')'
		), 'FROM', $table, $alias);
		//$this->_query_post_from .= $table;
		$this->_bool_from = true;

	}

	/** \brief return an integer corresponding to a COUNT SQL query
	*
	* \return integer or boolean FALSE
	*/
	function get_count_result() {
		if ($this->is_empty())
			return (false);
		reset($this->_result);
		list ($key, $val) = each($this->_result);
		return ($val[0]);
	}

	/** \brief build the where statement, private function
	*
	* \param  $operand
	* \param  $left
	* \param  $right
	*/
	function priv_where($operand, $left, $right) {
		if ($this->_bool_where_bloc_start)
			$operand = "";
		if ($this->_bool_where) {
			$this->_query_where .= $operand . " " . $left . $right . " ";
		} else {
			$this->_query_where .= "WHERE " . $left . $right . " ";
			$this->_bool_where = true;
		}
		$this->_bool_where_bloc_start = false;
	}

	/** \brief build the select statement, private function
	*/
	function priv_select($tab_nomchamps, $liaison, $table, $alias = -1, $using = -1) {

		for ($i = 0; $i < count($tab_nomchamps); $i++) {
			if (ereg('(^=)', $tab_nomchamps[$i])) {
				$tab_nomchamps[$i] = substr($tab_nomchamps[$i], 1);
				continue;
			}
			if ($alias == -1) {
				$tab_nomchamps[$i] = $table . "." . $tab_nomchamps[$i];
			} else
				$tab_nomchamps[$i] = $alias . "." . $tab_nomchamps[$i];
		}
		if (count($tab_nomchamps) > 0)
			$this->_query_pre_from .= ", " . join($tab_nomchamps, ", ");

		$this->_query_post_from .= " " . $liaison . " " . $table . " ";
		if ($alias != -1)
			$this->_query_post_from .= " " . $alias . " ";
		if ($using != -1)
			$this->_query_post_from .= $using . " ";
	}

	function priv_add_slashes($string) {
		return (addslashes($string));
	}
}
?>