<?php


/** \file shared_lib/php/generic/utf8.inc.php
 * \brief functions to manipulate UTF8 strings
 */

/**  \brief converts a string to UTF8.
 * to be sure that the given string is converted into UTF8
 * \param $str is the string to convert
 * \result an utf8 string
*/
function utf8_ensure($str) {
	return seems_utf8($str) ? $str : utf8_encode($str);
}

/**  \brief tells if a string is in UTF8 or not
 * 
 * \param $str is the string to test
 * \result a boolean TRUE if $str is an UTF8 string, FALSE otherwise
*/
function seems_utf8($Str) {
	// tell if a string is already encoded into utf8
	//usefull function taken from http://fr2.php.net/manual/en/function.utf8-encode.php

	for ($i = 0; $i < strlen($Str); $i++) {
		if (ord($Str[$i]) < 0x80)
			continue; # 0bbbbbbb
		elseif ((ord($Str[$i]) & 0xE0) == 0xC0) $n = 1; # 110bbbbb
		elseif ((ord($Str[$i]) & 0xF0) == 0xE0) $n = 2; # 1110bbbb
		elseif ((ord($Str[$i]) & 0xF8) == 0xF0) $n = 3; # 11110bbb
		elseif ((ord($Str[$i]) & 0xFC) == 0xF8) $n = 4; # 111110bb
		elseif ((ord($Str[$i]) & 0xFE) == 0xFC) $n = 5; # 1111110b
		else
			return false; # Does not match any model
		for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
			if ((++ $i == strlen($Str)) || ((ord($Str[$i]) & 0xC0) != 0x80))
				return false;
		}
	}
	return true;
}
?>