<?php


/** \file shared_lib/php/generic/execute_perl.inc.php
 * 
 * object to control execution of perl scripts
 *  
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 19/11/2004
 */

/** \brief object to control execution of perl scripts
*/
class execute_perl {

	var $_perl_command;

	/** \constructor
	*
	* \param perl5lib search path used by perl to look for user perl modules
	*/
	function execute_perl($perl5lib = '') {
		$this->_perl_command = '/usr/bin/perl';
		if (file_exists($this->_perl_command)) {
		} else {
			$this->_perl_command = '/usr/local/bin/perl';
			if (file_exists($this->_perl_command)) {
			} else {
				$this->_perl_command = exec('which perl');
				if (file_exists($this->_perl_command)) {
				} else {
					echo "ERROR: the perl interpreter was not found in your system";
					exit;
				}
			}
		}

		if ($perl5lib != '')
			putenv("PERL5LIB=" .
			$perl5lib);

	}

	/** function designed to replace the use of the php "system" command to execute perl scripts
	*
	* \param $command_line the perl script to execute with its arguments
	* \param $retval the UNIX returned value of the command: 0 => SUCCESS,  OTHER => FAILURE
	*/
	function system($command_line, & $retval) {
		system($this->_perl_command . ' ' . $command_line, $retval);
	}

	/** \brief execute the perl scripts in batch mode in background
	*
	* \param $array_command_line the perl scripts to execute with there arguments
	*/
	function background_batch_execute($array_command_line) {
		foreach ($array_command_line as $i => $command_line) {
			$array_command_line[$i] = '`' . $this->_perl_command . ' ' . $command_line . ' > /dev/null`';
		}

		$command_line = 'which bash "' . implode(' && ', $array_command_line) . '" > /dev/null &';
		//echo $command_line;
		exec($command_line);
	}

	/** \brief tells if a perl module is there or not
	*
	* \param $module_name the perl module name to look for
	* \return boolean true if it exists, false if not
	*/
	function module_exists($module_name) {
		$retval = 1;
		$command = $this->_perl_command . ' -e "use strict; use ' . $module_name . ';exit 0;"';
		system($command, $retval);
		//echo $retval;
		//echo $command;
		if ($retval == 0)
			return true;
		else
			return false;
	}

	/** \brief test if a particular DBI perl module is present
	*
	* \param $dbi_module_name the DBI perl module name to look for
	* \return boolean true if DBI perl module exists, false if not
	*/
	function dbi_module_exists($dbi_module) {
		$retval = 1;

		$script = 'use DBI;
						my @drivers = DBI->available_drivers();
						foreach \$driver ( @drivers ) {
							if (\$driver eq \"' . $dbi_module . '\"){exit 0;}
						} exit 1;';
		$command = $this->_perl_command . ' -e "' . $script . '"';
		system($command, $retval);
		if ($retval == 0)
			return true;
		else
			return false;
	}

	/** \brief display the list of available DBI perl modules
	*
	* \param $module_name the perl module name to look for
	* \return boolean true if DBI perl module exists, false if not
	*/
	function list_dbi_modules() {
		$retval = 1;

		$script = 'use DBI;
						my @drivers = DBI->available_drivers();
						foreach \$driver ( @drivers ) {
						    print \"driver: \$driver <br/>\n\";
						}';
		$command = $this->_perl_command . ' -e "' . $script . '"';
		system($command, $retval);
		if ($retval == 0)
			return true;
		else
			return false;
	}

}
?>