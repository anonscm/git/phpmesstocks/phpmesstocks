<?php


/** \file shared_lib/php/generic/output_tabulated_text_table.inc.php 
 * \brief output directly elements of a table
 * 
 * by default, this output xhtml table
 */

class output_tabulated_text_table extends output_table {

	function output_tabulated_text_table() {
		$this->output_table();
	}

	function get_type() {
		return ('tabulated_text_file');
	}

	function http_header() {
		Header('Content-type:  text/plain');
		$this->priv_http_header();
	}

	function start_table() {
		//$this->priv_echo('<table>');
	}
	function start_line() {
		//$this->priv_echo('<tr>');
	}
	function header_cell($message) {
		$this->priv_echo($message . "\t");
	}

	function cell($message) {
		if (is_array($message)) {
			$this->priv_echo(join(' | ', $message) . "\t");

		} else {
			$this->priv_echo($message . "\t");
		}
	}
	function cell_anchor($message, $url) {
		$this->cell($message);
	}

	function cell_tab_anchor($message, $url) {
		$this->cell($message);
	}

	function end_line() {
		$this->priv_echo("\n");
	}

	function end_table() {
		//$this->priv_echo('</table>');
	}
}
?>