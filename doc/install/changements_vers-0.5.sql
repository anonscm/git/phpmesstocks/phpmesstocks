

CREATE TABLE `StockUtilisateur` (
`IdStock` INT UNSIGNED NOT NULL ,
`IdUtilisateur` INT UNSIGNED NOT NULL ,
PRIMARY KEY ( `IdStock` , `IdUtilisateur` )
) COMMENT = 'mise en relation des stocks et utilisateurs';


CREATE TABLE `Stocks` (
`IdStock` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
`NomStock` VARCHAR( 100 ) NOT NULL ,
PRIMARY KEY ( `IdStock` ) ,
UNIQUE (
`NomStock`
)
) ;

CREATE TABLE `StockProduit` (
`IdStock` INT UNSIGNED NOT NULL ,
`IdProduit` INT UNSIGNED NOT NULL ,
PRIMARY KEY ( `IdStock` , `IdProduit` )
) ;
ALTER TABLE `StockProduit` ADD `Qstock` INT;

ALTER TABLE `StockProduit` ADD INDEX ( `Qstock` ) ;
ALTER TABLE `StockProduit` CHANGE `Qstock` `Qstock` INT( 11 ) NOT NULL DEFAULT '0';

ALTER TABLE `Utilisateurs` ADD `stockVue` TINYINT DEFAULT '0' NOT NULL ,
ADD `stockAjout` TINYINT DEFAULT '0' NOT NULL ,
ADD `stockModif` TINYINT DEFAULT '0' NOT NULL ,
ADD `stockSuppr` TINYINT DEFAULT '0' NOT NULL ;