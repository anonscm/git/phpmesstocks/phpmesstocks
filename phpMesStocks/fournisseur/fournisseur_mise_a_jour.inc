<?

/***************************************************************************
                          fournisseur_mise_a_jour.inc  -  mise � jour des fournisseurs
                             -------------------
    begin                :  02/01/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//dans ce fichier,on va traiter tous les changements possibles sur la base : ajouter un nouvel element, en supprimer un ou en modifier un.
require "fournisseur/fournisseur_fonctions.inc";


if (($action=="ajouter") || ($action=="modifier")) {
	//tableau des champs � enregistrer (en modif ou en ajout, r�cup�r�s dans le formulaire)
	// IdFournisseur     NomFournisseur     AdresseFournisseur     TelFournisseur     FaxFournisseur     OffreFournisseur
	$tmp_tableau = array(
			'NomFournisseur' => $form_NomFournisseur,
			'AdresseFournisseur' => $form_AdresseFournisseur,
			'CorrespFournisseur' => $form_CorrespFournisseur,
			'TelFournisseur' => $form_TelFournisseur,
			'FaxFournisseur' => $form_FaxFournisseur,
			'OffreFournisseur' => $form_OffreFournisseur
			
		);
}

if (($repertoire=="fournisseur") && ($action=="ajouter") && ($form_IdFournisseur==0)){
	
	//cr�ation du produit:
	$ins_produit = requete_sql_insert_into ("Fournisseurs", $tmp_tableau);
	$HTTP_SESSION_VARS["session_deja_enregistre"]=$ins_produit; 
	
	$resultat = exec_requete ($ins_produit, $connexion) or die ("requete ajouter non valide");
	
	if ($choix=="1") {
		$action="liste";
		$repertoire="fournisseur";
	}
	else{
		$url_IdProduit = 0;
		$action="form";
		$repertoire="fournisseur";
	}
}

//echo "coucou 	$repertoire $action $form_IdMouvement";

if (($repertoire=="fournisseur") && ($action=="modifier") && ($form_IdFournisseur != 0)){
	
	//mise � jour du produit
	$resultat = exec_requete (requete_sql_update("Fournisseurs", array('IdFournisseur' => $form_IdFournisseur),  $tmp_tableau), $connexion) or die ("requete modifier non valide");
	
	$action="liste";
	$repertoire="fournisseur";
}



//On a demander une suppression
if ( ($repertoire=="fournisseur") && ($action=="supprimer") && ($url_IdFournisseur!=0)){
	
	if($confirmer==1){
		suppression_fournisseur($url_IdFournisseur);
		
		
		$action="liste";
		$repertoire="fournisseur";
	}
	else{
		//la suppresion est demand�e mais non confirm�e
		
		//Affichage de confirmation de supression
		
		$message="Etes vous certain de vouloir supprimer ce fournisseur ?";
		echo html_confirmation($message, "index.php?repertoire=fournisseur&action=supprimer&url_IdFournisseur=".$url_IdFournisseur."&confirmer=1","index.php?repertoire=fournisseur&action=liste");

		pied_de_page ();
	}
}
	
?>