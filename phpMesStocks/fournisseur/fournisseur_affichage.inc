<?

/***************************************************************************
                          fournisseur_affichage.inc  -  affichage d'un fournisseur dans un fomulaire
                             -------------------
    begin                :  02/01/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

if ($objet_user->Login!= "anonyme") 

{
//     IdFournisseur     NomFournisseur     AdresseFournisseur     TelFournisseur     FaxFournisseur     OffreFournisseur
	$tab_nomchamps = array ("IdFournisseur",  "NomFournisseur", "AdresseFournisseur","TelFournisseur","FaxFournisseur","OffreFournisseur","CorrespFournisseur");
	
	if ($url_IdFournisseur > 0) //si $url_IdFournisseur != 0 on a cliquer sur modifier
	{
		$objet_requete= new creer_requete();
		$objet_requete->select($tab_nomchamps, "FROM", "Fournisseurs");
		$objet_requete->fin_select();
				
		$objet_requete->ajout_egal ("IdFournisseur",$url_IdFournisseur, "AND");
		$requete = $objet_requete->fin_requete();
		
		$resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
		$tab_champs = ligne_suivante_taba($resultat);
		
		$tmp_valider="enregistrer cette modification";
		$tmp_action="modifier";
	}
	
	else{//on est sur ajouter
		//initialiser le tableau de champs
		$tab_champs = array ();
		for ($i=0; $i < count($tab_nomchamps); $i++) {
			$tab_champs[$tab_nomchamps[$i]] =  "";
		}

		$tab_champs["IdFournisseur"] = 0;
		$tmp_valider="Ajouter ce fournisseur";
		$tmp_action="ajouter";
		$ligne ="";
	}
	
	// on fait un formulaire qui sera commun aux ajouts et aux modification d'OS

	$form = new formulaire("post","index.php",FALSE,"form_fournisseur",TRUE);
		
	$form->champ_cache("choix",1); //retour � la liste

	$form->champ_cache("action",$tmp_action);
	$form->champ_cache("repertoire","fournisseur");
	$form->champ_cache("form_IdFournisseur",$tab_champs["IdFournisseur"]);
	
	// IdProduit     NomProduit     Conditionnement     Qrestante     ProduitNotes
	$form->debut_table(VERTICAL,1);

//     IdFournisseur     NomFournisseur     AdresseFournisseur     TelFournisseur     FaxFournisseur     OffreFournisseur	CorrespFournisseur

//	$form->champ_liste_deroulante("", "form_MvtType", $tabMvtTypes, $ligne->MvtType);
	$form->champ_texte("Nom du fournisseur :", "form_NomFournisseur", $tab_champs["NomFournisseur"],50);

	$form->champ_texte("correspondant :", "form_CorrespFournisseur", $tab_champs["CorrespFournisseur"],50);
	$form->champ_texte("t�l�phone :", "form_TelFournisseur", $tab_champs["TelFournisseur"],50);
	$form->champ_texte("fax :", "form_FaxFournisseur", $tab_champs["FaxFournisseur"],50);
	$form->champ_texte_etendu("adresse :", "form_AdresseFournisseur",$tab_champs["AdresseFournisseur"], 5, 50);
	$form->champ_texte_etendu("offres en cours :", "form_OffreFournisseur",$tab_champs["OffreFournisseur"],5, 50);
//	$form->champ_date("date : ", "form_MvtDate", $date,"aujourd'hui");
//	$form->champ_liste_deroulante("salle : ", "form_IdSalle", $tab_req_salles, $ligne->IdSalle);
	
	//$form->champ_liste_deroulante ("Type de t�l�phone","form_TypeTelephone", $tab_TypeTel,$ligne->TypeTelephone);
	
	$form->champ_valider("","bouton",$tmp_valider);
	
	$form->ajout_verif_champ_vide("form_NomFournisseur","Vous devez saisir un nom pour ce fournisseur");
	
	$form->fin_table();
	
	$form->fin();	
	
}	
	
else echo html_message ("Vous n'avez pas l'autorisation de manipuler la base","erreur");	
	
?>