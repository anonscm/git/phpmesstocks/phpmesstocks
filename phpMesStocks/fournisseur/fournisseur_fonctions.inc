<?
/***************************************************************************
                      fournisseur_fonctions.inc  -
           bibliotheque de fonctions specifiques aux fournisseurs
                                        -------------------
    begin                :  02/01/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
	 
//require "libphp/gestion_ip.inc";

if(!isset($module_fournisseur_fonctions)){
	$module_fournisseur_fonctions=1;
	
	require "libphp/sql_functions.inc";
	
	/*
	function mise_a_jour_produit_fournisseur ($IdProduit, $connexion, $tab_IdFournisseur, $tab_supprimer, $AjoutFournisseur, $tab_RefPrdtFournisseur) 
	{
		//modifier
		for ($i=0; $i < count ($tab_IdFournisseur); $i++) {
			$requete = requete_sql_update("PrdtFournisseurs", array('IdProduit' => $IdProduit, 'IdFournisseur' => $tab_IdFournisseur[$i]),  array("RefPrdtFournisseur" => $tab_RefPrdtFournisseur[$i]));
			$resultat = exec_requete ($requete, $connexion) or die ("requete modifier non valide");
		}

		//ajout d'un fournisseur (form_AjoutFournisseur)
		if ($AjoutFournisseur != "") {
			//ajout
			$tmp_tableau = array(
				'IdProduit' => $IdProduit,
				'RefPrdtFournisseur' => ""
			);
			
			$ligne = cherche_fournisseur ($AjoutFournisseur, $connexion);
			if (is_object($ligne)) {
				$tmp_tableau["IdFournisseur"] = $ligne->IdFournisseur;
			
				$resultat = exec_requete (requete_sql_insert_into("PrdtFournisseurs", $tmp_tableau), $connexion) or die ("requete ajouter non valide");
			}
			else {
				echo html_message("Le fournisseur (".$AjoutFournisseur.") n'a pas �t� trouv�", "erreur");
			}
		}
				
		//supprimer des fournisseurs pour ce produit
		for ($i=0; $i < count ($tab_supprimer); $i++) {
			if ($tab_supprimer[$i] < (count ($tab_IdFournisseur))) {
				$resultat = exec_requete (requete_sql_delete("PrdtFournisseurs",array('IdProduit' => $IdProduit , 'IdFournisseur' => $tab_IdFournisseur[$tab_supprimer[$i]])), $connexion) or die ("requete effacer non valide");				
			}
		}
	
	}
	*/
	function suppression_fournisseur($IdFournisseur){
		
		$resultat = exec_requete (requete_sql_delete("PrdtFournisseurs", array('IdFournisseur' => $url_IdFournisseur)), $connexion) or die ("requete modifier non valide");
		
		if ($resultat > 0) $resultat = exec_requete (requete_sql_delete("Fournisseurs", array('IdFournisseur' => $url_IdFournisseur)), $connexion) or die ("requete modifier non valide");

		return ($resultat);

	}


}
	
?>




