<?

/***************************************************************************
                          fournisseur_objet_derive.inc  - 
                   Affichage des fournisseurs
                             -------------------
    begin                :  02/01/2003
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

  
 //on fait une classe liste derivee_machine a partir de la classe objet_liste_resultat 
// on va pouvoir afficher la liste
//require "libphp/gestion_ip.inc";



if (!isset($module_liste_derivee_fournisseur)){
	
	$module_liste_derivee_fournisseur = 1;
	
	require "libphp/objet_liste_resultat.inc";
	
	class	liste_derivee_fournisseur extends liste_resultat {
		
		function liste_derivee_fournisseur ($resultat, $nom, $page) {
			//constructeur
			$this->liste_resultat($resultat,$nom, $page);
			
		}
	
//on affiche les titres	
				
		function afficher_effectif() {
			//pour afficher le nombre de lignes sélectionnnées
			
			echo html_message($this->_nb_lignes." fournisseurs sélectionnés","info");
		}

		function afficher_titres(){
			
			TblCellule ("Nom");
			TblCellule ("produits");
			
		}

			//on met les données de la tables machine dans la liste	
		function afficher_ligne($row){
						
		// IdProduit     NomProduit     Conditionnement     Qrestante     ProduitNotes
			//if ($GLOBALS["req_IdUtilisateur"]==$GLOBALS["AdminBD"]->IdUtilisateur)
			TblCellule ($row->NomFournisseur);
			TblCellule (ancre("index.php?repertoire=produit&action=liste&req_NomFournisseur=".$row->NomFournisseur."&req_IdUtilisateur=0","liste"));
					
			//On ajoute en fin de ligne les boutons Versions Modifier, supprimer	
			
			$this->ajouter_case_modifier("fiche du fournisseur","index.php?repertoire=fournisseur&action=form&url_IdFournisseur=$row->IdFournisseur");
				
			$this->ajouter_case_supprimer("Supprimer","index.php?repertoire=fournisseur&action=supprimer&url_IdFournisseur=$row->IdFournisseur");
				//bouton selectionner
			
			$this->ajouter_case_selectionner ("Selectionner","index.php?repertoire=fournisseur&action=selectionner&url_IdFournisseur=$row->IdFournisseur");
			
			//$this->_tabliste->afficher_ligne();
		}
	}
}	

?>