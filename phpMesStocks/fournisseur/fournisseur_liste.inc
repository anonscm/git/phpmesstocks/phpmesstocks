<?

/***************************************************************************
                          fournisseur_liste.inc  - 
                   Affichage des fournisseurs en liste
                             -------------------
    begin                :  02/01/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//require "./libstocks/tab_utilisateurs.inc";
//require "./libcampus/tab_categorie.inc";
//$tab_req_utilisateurs = charge_tableUtilisateurs();
//$tab_req_utilisateurs[0] = "----";

 
require "./libphp/memoire_req.inc";
rappel_memoire_req($HTTP_SESSION_VARS, $repertoire, array ("req_NomFournisseur"));
rappel_liste_pos($HTTP_SESSION_VARS, $repertoire);

if ($req_IdUtilisateur == "") $req_IdUtilisateur = $objet_adminBD->IdUtilisateur;
//$tab_tri["ORDER BY u.NomUtilisateur"] = "Noms";
//$tab_tri["ORDER BY u.PrenomUtilisateur"] = "Pr�noms";

//$tri = " ORDER BY f.NomFournisseur";
rappel_tri($HTTP_SESSION_VARS, $repertoire, "fournisseurs");



/************************************************
	FABRICATION de la barre de recherche/tri
*************************************************/

TblDebut("trirecherche");
//ligne de titres des colonnes:
		
TblDebutLigne();
/*	
TblDebutCellule();
// formulaire de s�lection du tri:
$form = new formulaire("post","index.php?repertoire=fournisseur&action=liste",FALSE,"form",FALSE);
$form->debut_table(HORIZONTAL,1);	
$form->champ_liste_deroulante("Tri","tri",$tab_tri, $tri);
$form->champ_valider("","bouton","trier","trier");
	
$form->fin_table();

$form->fin();
TblFinCellule();
*/
TblDebutCellule();

 // forumulaire de recherche rapide:

$form = new formulaire("post","index.php?repertoire=fournisseur&action=liste",FALSE,"form",FALSE);

$form->debut_table(HORIZONTAL,1);	

//$form->champ_liste_deroulante("utilisateurs","req_IdUtilisateur",$tab_req_utilisateurs, $req_IdUtilisateur);

$form->champ_texte("Nom du fournisseur","req_NomFournisseur",$req_NomFournisseur,10);

$form->champ_valider("","bouton","rechercher","rechercher");

$form->fin_table();

$form->fin();

TblFinCellule();

TblFinLigne();
TblFin();


/************************************************
	FIN de la barre de recherche/tri
*************************************************/


/************************************************
	FABRICATION de la requ�te
*************************************************/
//requete qui permetra d'afficher la liste
//
$objet_requete= new creer_requete();

$objet_requete->select(array("IdFournisseur", "NomFournisseur"), "FROM", "Fournisseurs", "f");

$objet_requete->fin_select();
//$objet_requete->ajout_egal ("pu.IdUtilisateur",$objet_adminBD->IdUtilisateur, "AND");

if ($req_NomFournisseur != "") $objet_requete->ajout_like ("f.NomFournisseur",$req_NomFournisseur, "AND");

if ($tri == "fournisseurs"){
  $objet_requete->ajout_tri("f.NomFournisseur");
}

//$tri = " ORDER BY f.NomFournisseur";
		
$requete = $objet_requete->fin_requete($connexion,$tri);


/************************************************
	fin de la FABRICATION de la requ�te
*************************************************/


$page_par_page = new objet_page($position,$objet_requete->_nblignes,$pas);

//execution de la requete si repertoire = machine et action = liste on execute la requete 

if (($repertoire=="fournisseur") &&($action=="liste")){
	$resultat = exec_requete_select ($page_par_page->modif_requete($requete), $connexion) or die ("requete non valide");
}
		
////on regarde les droits de celui qui s'est connect� � la base ces droits sont contenus dans l'objet global $objet_user	
	
// on implemente un nouvel objet deriv� specifique aux machines pour l'affichage de la liste
		
$listeresultat= new liste_derivee_fournisseur ($resultat, "liste_fournisseur", $page_par_page);
			
// suivant les droits de la personne qui s'est connnect�e on affiche ou pas les boutons ajouter modifier..


if ($objet_adminBD->fournisseurAjout==1)
	$listeresultat->set_ajouter ("index.php?repertoire=fournisseur&action=form&urlIdFournisseur=0", "ajouter un fournisseur");

if ($objet_adminBD->fournisseurModif==1)
	$listeresultat->set_modifier () ;
		
if ($objet_adminBD->fournisseurSuppr==1)
	$listeresultat->set_supprimer (); 

				
	
if ($objet_requete->_nblignes=="0") {
	echo html_message ("Il n'y a pas de fournisseur correspondant � votre demande","erreur");
	$listeresultat->afficher_liste();
}	
else
	$listeresultat->afficher_liste();
/*	        
//formulaire pour faire une recherche : on ajoute le bouton rechercher
 
$form = new formulaire("post","index.php?repertoire=utilisateur&action=form_recherche",FALSE,"",FALSE);
$form->debut_table(HORIZONTAL,1);	
if (($repertoire=="machine") &($action=="requete"))
	$form->champ_valider("","bouton","faire une nouvelle recherche","valider_liste");
	
else
	$form->champ_valider("","bouton","faire une  recherche","valider_liste");
	
$form->fin_table();

$form->fin();
*/
	
	
		
	






?>
