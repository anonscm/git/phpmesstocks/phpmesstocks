
<p> Point d'entr�e pour la mise � jour des informations de "<? echo "$logiciel_nom $logiciel_version";?>" </p>

<?
/***************************************************************************
                          login.inc  -  formulaire d'identification des utilisateurs de phpMonSeminaire
                             -------------------
    begin                :  20/11/2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

	include "libphp/objet_formulaire.inc";
	class FormulaireLogin extends Formulaire {
		function FormulaireLogin($action, $nom, $titre_bouton) {
			//constructeur
			$this->Formulaire($action, $nom, $titre_bouton);
		}
		
		function fabrique_corps() {
			$this->formulaire .= "<p>";
			$this->champ_texte("Identifiant :", "IdentifiantF", $this->IdentifiantF,20);
			$this->formulaire .= "</p>";
			$this->formulaire .= "<p>";
			$this->champ_motdepasse("Mot de passe :", "MotDePasseF", $this->MotDePasseF,10);
			$this->formulaire .= "</p>";
		}
		
		var $IdentifiantF;
		var $MotDePasseF;
	}

	$form = new FormulaireLogin("index.php","login","Connexion");
	
	$form->echo_formulaire();


?>
