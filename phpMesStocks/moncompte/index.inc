<?
                          
/***************************************************************************
                      index.inc  -
          point d'entree du repertoire "moncompte" de phpMyCampus
                             -------------------
    begin                :  07/10/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
/*
Ce fichier permet de g�rer le compte des internautes conn�ect�s � "phpMyCampus" comme par exemple, changer son mot de passe:
action = "chgtdemotdepasse"

*/

if (!isset($action)) pied_de_page();

if ($action == "enregistrermotdepasse") {
	if ($objet_adminBD->UMotdepasse == "") $objet_adminBD->UMotdepasse = md5 ($objet_adminBD->UMotdepasse);
	
	if (md5($form_ancpassword) != $objet_adminBD->UMotdepasse) {
		echo html_message("Votre mot de passe n'est pas valide. Veuillez recommencer:", "erreur");
		$action = "chgtmotdepasse";
	}
	else {
		if ($form_password1 != $form_password2) {
			echo html_message("Le nouveau mot de passe n'a pas �t� saisi correctement. Veuillez recommencer:", "erreur");
			$action = "chgtmotdepasse";
		}
		else {
			if ($form_password1 != "") $form_password1 = md5($form_password1);
			// IdUtilisateur     UIdentifiant     UNom     UMotdepasse
			$resultat = exec_requete (requete_sql_update("Utilisateurs", array('IdUtilisateur' => $objet_adminBD->IdUtilisateur), array('UMotdepasse' => $form_password1)), $connexion) or die ("requete modifier non valide");
			echo html_message("Le nouveau mot de passe est enregistr�", "info");
		}
	}	


}

switch ($action) {
	case "chgtmotdepasse":
		$form = new formulaire("post","index.php",FALSE,"form_utilisateur",TRUE);
		$form->debut_table(VERTICAL,1);
		$form->champ_cache ("repertoire","moncompte" );
		$form->champ_cache ("action","enregistrermotdepasse" );
		
		$form->champ_motdepasse ("Entrez votre ancien mot de passe: ","form_ancpassword","", "15" );
		$form->champ_motdepasse ("Entrez votre nouveau mot de passe: ","form_password1","", "15" );
		$form->champ_motdepasse ("Entrez, une deuxi�me fois, votre nouveau mot de passe: ","form_password2","","15");
		$form->champ_valider("","","valider","enregistrer");
		$form->fin ();
	break;
}