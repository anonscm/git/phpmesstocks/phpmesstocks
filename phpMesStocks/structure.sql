# phpMyAdmin MySQL-Dump
# version 2.3.2
# http://www.phpmyadmin.net/ (download page)
#
# Serveur: localhost
# G�n�r� le : Jeudi 10 Avril 2003 � 16:49
# Version du serveur: 3.23.49
# Version de PHP: 4.2.3
# Base de donn�es: `phpMesStocks`
# --------------------------------------------------------

#
# Structure de la table `Fournisseurs`
#

CREATE TABLE Fournisseurs (
  IdFournisseur int(11) NOT NULL auto_increment,
  NomFournisseur varchar(50) NOT NULL default '',
  AdresseFournisseur text NOT NULL,
  CorrespFournisseur varchar(100) NOT NULL default '',
  TelFournisseur varchar(20) NOT NULL default '',
  FaxFournisseur varchar(20) NOT NULL default '',
  OffreFournisseur text NOT NULL,
  PRIMARY KEY  (IdFournisseur),
  UNIQUE KEY IdFournisseur (IdFournisseur,NomFournisseur),
  KEY IdFournisseur_2 (IdFournisseur,NomFournisseur)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Structure de la table `Mouvements`
#

CREATE TABLE Mouvements (
  IdMouvement int(11) NOT NULL auto_increment,
  IdProduit int(11) NOT NULL default '0',
  IdSalle int(11) NOT NULL default '0',
  IdUtilisateur int(11) NOT NULL default '1',
  MvtType tinyint(1) unsigned NOT NULL default '1',
  MvtDate date NOT NULL default '0000-00-00',
  MvtQuantite int(11) NOT NULL default '0',
  MvtNotes tinytext NOT NULL,
  PRIMARY KEY  (IdMouvement),
  UNIQUE KEY IdMouvement_2 (IdMouvement),
  KEY IdMouvement (IdMouvement,IdProduit,IdSalle),
  KEY Type (MvtType)
) TYPE=ISAM PACK_KEYS=1;
# --------------------------------------------------------

#
# Structure de la table `PrdtFournisseurs`
#

CREATE TABLE PrdtFournisseurs (
  IdProduit int(11) NOT NULL default '0',
  IdFournisseur int(11) NOT NULL default '0',
  RefPrdtFournisseur varchar(30) NOT NULL default '',
  KEY IdProduit (IdProduit,IdFournisseur),
  KEY RefPrdtFournisseur (RefPrdtFournisseur)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Structure de la table `PrdtUtilisateurs`
#

CREATE TABLE PrdtUtilisateurs (
  IdProduit int(11) NOT NULL default '0',
  IdUtilisateur int(11) NOT NULL default '0',
  PRIMARY KEY  (IdProduit,IdUtilisateur),
  KEY IdProduit (IdProduit,IdUtilisateur)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Structure de la table `Produits`
#

CREATE TABLE Produits (
  IdProduit int(11) NOT NULL auto_increment,
  CodeNomenclature varchar(20) NOT NULL default '',
  NomProduit varchar(50) NOT NULL default '',
  Conditionnement varchar(50) NOT NULL default '',
  Qrestante int(11) NOT NULL default '0',
  ProduitNotes tinytext NOT NULL,
  PRIMARY KEY  (IdProduit),
  UNIQUE KEY IdProduit_2 (IdProduit,NomProduit),
  KEY IdProduit (IdProduit,NomProduit,Conditionnement),
  KEY Qrestant (Qrestante),
  KEY CodeNomenclature (CodeNomenclature)
) TYPE=ISAM PACK_KEYS=1;
# --------------------------------------------------------

#
# Structure de la table `Salles`
#

CREATE TABLE Salles (
  IdSalle int(11) NOT NULL auto_increment,
  NomSalle char(50) NOT NULL default '',
  PRIMARY KEY  (IdSalle),
  UNIQUE KEY IdSalle_2 (IdSalle,NomSalle),
  KEY IdSalle (IdSalle,NomSalle)
) TYPE=ISAM PACK_KEYS=1;
# --------------------------------------------------------

#
# Structure de la table `Session`
#

CREATE TABLE Session (
  IdSession varchar(200) NOT NULL default '',
  IdUser int(11) unsigned NOT NULL default '0',
  Login varchar(20) NOT NULL default '',
  Name varchar(30) NOT NULL default '',
  FirstName varchar(30) NOT NULL default '',
  Email varchar(200) NOT NULL default '',
  TempsLimite decimal(10,0) NOT NULL default '0',
  PRIMARY KEY  (IdSession),
  UNIQUE KEY IdSession (IdSession),
  KEY IdSession_2 (IdSession)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Structure de la table `Utilisateurs`
#

CREATE TABLE Utilisateurs (
  IdUtilisateur int(11) NOT NULL auto_increment,
  UIdentifiant char(20) NOT NULL default '',
  UNom char(20) NOT NULL default '',
  UMotdepasse char(50) NOT NULL default '',
  UAdmin tinyint(4) NOT NULL default '0',
  produitVue tinyint(4) NOT NULL default '0',
  produitAjout tinyint(4) NOT NULL default '0',
  produitSuppr tinyint(4) NOT NULL default '0',
  produitModif tinyint(4) NOT NULL default '0',
  mouvementVue tinyint(4) NOT NULL default '0',
  mouvementAjout tinyint(4) NOT NULL default '0',
  mouvementSuppr tinyint(4) NOT NULL default '0',
  mouvementModif tinyint(4) NOT NULL default '0',
  fournisseurVue tinyint(4) NOT NULL default '0',
  fournisseurAjout tinyint(4) NOT NULL default '0',
  fournisseurSuppr tinyint(4) NOT NULL default '0',
  fournisseurModif tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (IdUtilisateur),
  UNIQUE KEY IdUtilisateur (IdUtilisateur,UNom),
  KEY IdUtilisateur_2 (IdUtilisateur,UNom),
  KEY UIdentifiant (UIdentifiant),
  KEY fournisseurVue (fournisseurVue,fournisseurAjout,fournisseurSuppr,fournisseurModif)
) TYPE=MyISAM;

    

