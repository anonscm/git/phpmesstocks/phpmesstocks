<?

                         
/***************************************************************************
                      objet_liste_resultat.inc  -
           classe de base pour la construction de liste
           d�riv� du travail sur phpMyRezo
                             -------------------
    begin                :  12/09/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// modifi� le 20/9/2002 par Olivier Langella : cr�ation de l'objet "objet_page" pour permettre l'affichage page par page
// modifi� le 16/9/2002 par Olivier Langella : �limination de la d�pendance � l'objet tableau (ancien code pas beau) pour n'utiliser que "table.inc"

// modifi� le 12/9/2002 par Olivier Langella pour se plier aux sp�cifications du XHTML

if (!isset ($module_liste_resultat)){

$module_liste_resultat = 1;

//include "libphp/objet_tableau.inc";
//include "libphp/table.inc";
//include "libphp/HTML.inc";
//include "libphp/xhtml.inc";

class objet_page {
	var $_position;
	var $_pas;
	var $_nblignes;
	var $_denombrement;
	var $_requetesuivante;
	var $_pageparpage;
	var $_repertoire;
	var $_lien;

	function objet_page ($position="-1", $nblignes="-1", $pas="-1",$lien="-1", $denombrement="-1") {
		if ($lien != "-1") {
			$this->_repertoire = -1;
			$this->_lien = $lien;
		}
		else {
			$this->_repertoire = $GLOBALS["repertoire"];
			$this->_lien = "index.php?repertoire=".$this->_repertoire."&action=liste";
		}
		
		//nbr de lignes retourn�es par la requ�te
		if (($position=="-1") AND ($nblignes=="-1") AND ($pas=="-1")) $this->_pageparpage = FALSE;
		else $this->_pageparpage = TRUE;
		$this->_pas = $pas;
		$this->_position = $position;
		$this->_nblignes = $nblignes;
		$this->_denombrement = $denombrement;
		
		if ($this->_pageparpage == FALSE) {
			$this->_nb_lignes = mysql_affected_rows();
		}
		else {
			if ($this->_pas < 5) $this->_pas = 30;
			//if ($this->_position > ($this->_nblignes-$this->_pas)) $this->_position = ($this->_nblignes-$this->_pas);
			if (($this->_position == "") || ($this->_position < 0)) $this->_position = 0;
		}
	}
	
	function modif_requete($requete) {
		if ($this->_pageparpage == FALSE) return ($requete);

		$requete .= " LIMIT $this->_position, $this->_pas";
		return ($requete);
	}
	
	function genere_navigation() {
		if ($this->_pageparpage == FALSE) return "";
		
		//if ($this->_pas > $this->_nblignes) return "";
		//g�n�ration du code HTML n�cessaire pour afficher les boutons suivants, pr�c�dents... etc
		
		$lien = $this->_lien."&pas=".$this->_pas;
		
		TblDebut("navigation");
		//ligne de titres des colonnes:
		
		TblDebutLigne();
		
		TblDebutCellule();
				
		$form = new formulaire("post",$this->_lien."#debutliste",FALSE,"pasdepage",FALSE,"navigation");
		$form->debut_table(HORIZONTAL,1);
		$form->champ_texte("","pas",$this->_pas,5);
		$form->champ_valider("","bouton","lister","lister");
		$form->fin_table();
		$form->fin();

		TblFinCellule();
		
		if ($this->_position > 0) {
			TblCellule(ancre($lien."&position=0#debutliste", "<<"));
			TblCellule(ancre($lien."&position=".($this->_position-$this->_pas)."#debutliste", "<"));

		}
		for ($i=0,$j=0; $i < $this->_nblignes; $i+=$this->_pas,$j++) {
			if ($this->_position != $i) {
				TblCellule(ancre($lien."&position=".$i."#debutliste", $j+1));
			}
			else TblCellule($j+1);
		}
		if ($this->_position < ($this->_nblignes-$this->_pas)) {
			TblCellule(ancre($lien."&position=".($this->_position+$this->_pas)."#debutliste", ">"));
			TblCellule(ancre($lien."&position=".($this->_nblignes-$this->_pas)."#debutliste", ">>"));
		}
		
		TblFinLigne();
		TblFin();

	}	
}

class liste_resultat {
	var $_resultat;
	var $_Omodifier;
	var $_Oconsulter;
	var $_Oajouter;
	var $_Osupprimer;
	var $_Oselectionner;
	var $_Lajouter;
	var $_tabliste;

	var $_argretour;
	var $_idprecedent; //pour �viter d'afficher plusieurs fois la m�me chose
	var $_compteur; //pour �viter d'afficher plusieurs fois la m�me chose
	var $_classe; //classe css
	var $_titre_ajout;
	var $_nb_lignes;
	var $_denombrement;
	var $_pareil; //pour savoir si on affiche une ligne consid�r�e comme la m�me que la pr�c�dente
	var $_i;
	var $_imax;
	
	var $_objet_page;
	
		
	function liste_resultat ($resultat, $nom, $page="-1", $classe="-1") {
		//constructeur ,on initialise les variables membres
		if (!is_object($page)) $this->_objet_page = new objet_page();
		else $this->_objet_page = $page;
		
		if ($classe == "-1") $this->_classe ="listeresultat";
		else $this->_classe = $classe;
				
		//$this->_tabliste = new TableauHTML($nom, $this->_classe);
		$this->_resultat = $resultat;  //r�sultat d'une requete
		$this->_Oajouter = 0; //on initialise  les droits :ici on ne peut ni ajouter,ni modifier, ni supprimer
		$this->_Omodifier = 0;
		$this->_Oconsulter = 0;

		$this->_Osupprimer = 0;
		$this->_Oselectionner=0;
		$this->_nb_lignes = $this->_objet_page->_nblignes;
		$this->_denombrement = $this->_objet_page->_denombrement;
		
		$this->_imax = 2; //nombre de lignes diff�rentes maximum dans la liste (couleurs)
		$this->_i = 1;
		
	}
		
	function set_ajouter ($Lajouter,$titre_ajout ) {
		//$Lajouter .= xhtml_amp($Lajouter);
		$this->_Oajouter = 1; //on donne le droit d'ajouter
		$this->_titre_ajout = $titre_ajout;
		$this->_Lajouter = $Lajouter.$this->_argretour;
	}
	
	function set_modifier () {
		$this->_Omodifier = 1; //on peut modifier
	}
	
	function set_consulter () {
		$this->_Oconsulter = 1; //on peut modifier
	}

	function set_supprimer () {
		$this->_Osupprimer = 1; //on peut supprimer
	}
	
	function set_selectionner (){
		$this->_Oselectionner = 1;
	}
	
	function afficher_ajouter() {
		//� impl�menter dans les objets d�riv�s
		$ligne = "";
		//$this->_Lajouter = xhtml_amp($this->_Lajouter.$this->_argretour);
		$this->_Lajouter = $this->_Lajouter.$this->_argretour;
		if (($this->_Oajouter == 1) && ($this->_titre_ajout != "")) $ligne = ancre($this->_Lajouter,$this->_titre_ajout);
		//"<a href=\"$this->_Lajouter\">$this->_titre_ajout</a>";

		echo html_message($ligne, "listelienajouter");
	}
	
		

	function afficher_effectif() {
		//� impl�menter dans les objets d�riv�s
		//pour afficher le nombre de lignes s�lectionnn�es
	}

	function afficher_titres() {
		//� impl�menter dans les objets d�riv�s
	}

	function afficher_ligne($row) {
		//� impl�menter dans les objets d�riv�s

	}
	
	//ajouter une case avec le lien au bout de chaque ligne du tableau
	function ajouter_case_consulter($case, $lien, $classe="-1") {
		if ($this->_pareil == TRUE) $case = "";
		if ($this->_Oconsulter == 1) {//si on peut consulter
			
			TblCellule(ancre($lien, $case), $classe);
			//la variable tabliste prend la fonction ajouter case lien
		}
	}
	
	function ajouter_case_modifier($case, $lien, $classe="-1") {
		//$lien = xhtml_amp($lien.$this->_argretour);
		$lien = $lien.$this->_argretour;
		
		if ($this->_pareil == TRUE) $case = "";
		if ($this->_Omodifier == 1) {//si on peut mofifier
			
			TblCellule(ancre($lien, $case), $classe);
		}
			//la variable tabliste prend la fonction ajouter case lien
//		else if ($case == "Modifier") {
//			TblCellule("", $classe);
//		}
//		else {
//			//TblCellule($case, $classe);
//		}
	}
	
	//ajouter une case avec le lien au bout de chaque ligne du tableau
	function ajouter_case_selectionner ($case, $lien , $couleur="", $attributs=""){ 
		//$lien = xhtml_amp($lien.$this->_argretour);
		$lien = $lien.$this->_argretour;
		
		
		if ($this->_pareil == TRUE) $case = "";

		if ($this->_Oselectionner == 1) 
			TblCellule(ancre($lien, $case), $classe);
			//la variable tabliste prend la fonction ajouter case lien
		else if ($case == "S�lectionner") {
			TblCellule("", $classe);
		}
		else {
			//TblCellule("", $classe);
		}
		
	}
	
	
	function ajouter_case_supprimer($case ,$lien, $classe="-1") {
		
		//$lien = xhtml_amp($lien.$this->_argretour);
		$lien =$lien.$this->_argretour;
		if ($this->_pareil == TRUE) $case = "";
		if ($this->_Osupprimer == 1) {//si on peut supprimer
			TblCellule(ancre($lien, $case), $classe);
		}
		//else if ($case == "Supprimer") {
		//	TblCellule("", $classe);
		//}
		//else {
		//	//TblCellule($case, $classe);
		//}
	}
		
	function afficher_liste($idbase="") {
		echo "<div class=\"$this->_classe\">"; //div permet de diviser un doc,et de changer le stryle de pr�sentation

		$this->afficher_effectif();

		echo $this->_objet_page->genere_navigation();

		$this->_compteur = 0;
		$this->afficher_ajouter();
		//$this->_tabliste->ouvrir_tableau();
		echo ancre_nom("debutliste");

		TblDebut("listeresultat");
		//ligne de titres des colonnes:
		
		TblDebutLigne("titres");
		$this->afficher_titres();
		TblFinLigne();
		
 		while($row =mysql_fetch_object($this->_resultat))
		{
		
			
			$this->_pareil = FALSE;
			if ($idbase != "") {
				if ($this->_idprecedent == $row->$idbase) $this->_pareil = TRUE;
			}
			
			if ($this->_pareil == FALSE) {
				$this->_i--;
				if ($this->_i < 0) $this->_i = ($this->_imax -1);
			}
			
			TblDebutLigne("c".$this->_i);

			$this->afficher_ligne($row);
			TblFinLigne();
			
			if ($idbase != "")  $this->_idprecedent = $row->$idbase;
			
		}
		
		//$this->_tabliste->fermer_tableau();
		TblFin();
		
		$this->afficher_ajouter();
		
		
		$this->_objet_page->genere_navigation();
		
		echo "</div>";
	
	}
	
	
		
	
	
	
	
	
	



	

}
}
?>