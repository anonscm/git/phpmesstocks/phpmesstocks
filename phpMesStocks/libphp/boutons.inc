<?
                          
/***************************************************************************
                      boutons.inc  -
           production de boutons HTML dans des tableaux
           
                             -------------------
    begin                :  02/05/2003
    copyright            : (C) 2003 by Olivier Langella
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

if (!isset ($module_boutons))
{
  $module_boutons=1;
  
  function TblCelluleBouton ($titre, $location, $image="", $alt=""){
    TblCelluleBoutonClasse ("button", $titre, $location, $image="", $alt="");
  }
  function TblCelluleBoutonPresse ($titre, $location, $image="", $alt=""){
    echo "<td class=\"button-down\">\n";
    TblDebut();
    TblDebutLigne();
    if ($image != "") TblCellule("");
    TblCellule($titre, "button-text");
    TblFinLigne();
    TblFin();
    
    echo "</td>\n";
  }
  
  function TblCelluleBoutonClasse ($classe, $titre, $location, $image="", $alt=""){
    //ATTENTION: il faut d�finir les styles:
    // td.button, td.button-up, td.button-down
    $location = str_replace("&", "&amp;", $location);
    echo "<td class=\"".$classe."\" onmouseout=\"this.className='button'\" onmouseover=\"this.className='button-up'\" onmousedown=\"this.className='button-down'\" onclick=\"document.location='".$location."'\">\n";
    TblDebut();
    TblDebutLigne();
    if ($image != "") TblCellule("");
    TblCellule($titre, "button-text");
    TblFinLigne();
    TblFin();
    
    echo "</td>\n";
  }
}


?>
