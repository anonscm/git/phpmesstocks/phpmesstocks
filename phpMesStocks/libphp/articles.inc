<?
                          
/***************************************************************************
                      articles.inc  -
           production de code HTML pour manipuler des articles ou références biblio
           
                             -------------------
    begin                :  6/8/2002
    copyright            : (C) 2002 by Olivier Langella
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if (!isset($module_articles)){
  $module_articles = 1;
  
  function affiche_article($auteurs, $titre, $journal, $pages, $date) {
    $retour = "<strong>".$auteurs.".</strong> ";
    $retour .= $titre.". ";
    $retour .= $journal.", ";
    $retour .= $pages.", ";
    $retour .= $date.". ";
    return ($retour);
  }
  
}
?>