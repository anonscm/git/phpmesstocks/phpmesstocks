<?
                          
/***************************************************************************
                      listes.inc  -
           production de code HTML pour manipuler des listes XHTML
           
                             -------------------
    begin                :  6/8/2002
    copyright            : (C) 2002 by Olivier Langella
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if (!isset($module_listes)){
  $module_listes = 1;
  function ListeDebut ($classe = -1){
    if ( $classe == "") $classe = -1;
    $optionClasse = "";
    if ($classe != -1) $optionClasse = " class=\"".$classe."\"";
    
    echo "<ul".$optionClasse.">";	
  }
  
  function ListeFin (){
    echo "</ul>";	
  }

  function html_liste($liste, $classe = -1) {
    if ( $classe == "") $classe = -1;
    $optionClasse = "";
    if ($classe != -1) $optionClasse = " class=\"".$classe."\"";

    return  "<ul".$optionClasse.">".$liste."</ul>";
  }

  function html_elementListe($element, $classe = -1){
    if ( $classe == "") $classe = -1;
    $optionClasse = "";
    if ($classe != -1) $optionClasse = " class=\"".$classe."\"";
    
    return "<li".$optionClasse.">".$element."</li>";	

  }
}
?>