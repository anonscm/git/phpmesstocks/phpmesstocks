<?
                          
/***************************************************************************
                      xhtml.inc  -
           fonctions facilitant la génération de code XHTML
                             -------------------
    begin                :  12/09/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if (!isset($fichier_xhtml)){
	$fichier_xhtml = 1;
	
	function xhtml_amp($chaine) {
		return(str_replace("&","&amp;",$chaine));
	}

	function xhtml_cdata($chaine) {
		return $chaine;
		//return "<!--".$chaine."-->";
		//return("\n<![CDATA[\n".$chaine."\n]]>\n");
	}

}
?>