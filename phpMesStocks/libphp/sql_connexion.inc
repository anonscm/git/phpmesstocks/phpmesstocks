<?
                           
/***************************************************************************
                      sql_connexion.inc  -
           biblioth�que de fonctions de connexion et d�connexion aux bases de donn�es
                             -------------------
    begin                :  09/09/2003
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if ( !isset ($fichier_sql_connexion)){
  
  $fichier_sql_connexion =1;
  
  function connexion ($pnom, $motpasse, $pbase, $pserveur){
    
    //connexion au serveur
    $connexion = mysql_connect ($pserveur, $pnom, $motpasse) ;
    
    if (!$connexion){
      echo"D�sol�, connexion au serveur $pserveur impossible\n";
      exit;
    }
    //else echo"connexion avec le r�seau $pserveur �tablie\n<br/>";
    
    //connexion � la base
    if (!mysql_select_db ($pbase, $connexion)){
      echo " D�sol�, acc�s  � la base $pbase impossible\n";
      echo "<b>Message de MySQL : </b>" . mysql_error($connexion);
      exit;
    }
    //else echo "connexion avec la base $pbase �tablie<br/>";
    
    //on renvoie la variable de connexion
    
    return $connexion;
  }
  
  function deconnect ($connexion){
    mysql_close($connexion);
    
    
  }
  
}//fin du test sur $fichier_connexion
?>