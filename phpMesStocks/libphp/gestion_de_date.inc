<?
                          
/***************************************************************************
                      gestion_de_date.inc  -
           biblioth�que de fonctions pour transformer des dates, entre Mysql, date en Fran�ais...
                             -------------------
    begin                :  09/09/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if (!isset($module_date)){
  
  $module_date=1;
  
  function datesql2datefr ($datesql){
    
    //on veut r�cup�rer la date stock�e dans la table sous forme aaaa-mm-jj et l'afficher en date fr sous forme jj/mm/aaaa
    $datesql=str_replace(" ","",$datesql);
    
    ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $datesql, $regs );
    
    if ($regs[3] == "") return ($datesql);
    
    $Datefr = $regs[3]."/".$regs[2]."/".$regs[1];
    
    return ($Datefr);
    
  }
  
  
  function datefr2datesql($date_jour,$date_mois=-1,$date_annee=-1){
    if (($date_mois == -1)&&($date_annee == -1)) { 
      // 1 seul argument: $date_jour contient une chaine du type jj/mm/aaaa
      $date_jour=str_replace(" ","",$date_jour); //�liminer les espaces
      
      ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $date_jour, $regs );
      
      if (strlen($regs[3]) == 2) {
	//	if ($regs[3] > 70) $regs[3] = "19".$regs[3];
	$regs[3] = "20".$regs[3];
      }

      $datesql = $regs[3]."-".$regs[2]."-".$regs[1];
      
      return $datesql;
    }
    else {
      // 3 arguments: jour, mois, annee
      $chaine=str_replace(" ","",$date_jour);
      $chaine=str_replace(" ","",$date_mois);
      $chaine=str_replace(" ","",$date_annee);
      
      
      $joursql=$date_jour;
      $moissql=$date_mois;
      
      if (strlen($date_annee)=="4")
	
	$anneesql=$date_annee;
      
      else {
	
	$toto="20";
	$titi=$date_annee;
	$anneesql=$toto.$titi;
      }	
      
      $datesql="$anneesql-$moissql-$joursql";
      
      return $datesql;
    }
  }
  
  
  
  
  
}
?>