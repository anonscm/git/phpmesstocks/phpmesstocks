<?
                          
/***************************************************************************
                      objet_formulaire.inc  -
           classe de base pour la construction de formulaire HTML
           inspir� du livre MySql PHP, O'Reilly, de Philippe Rigaud
                             -------------------
    begin                :  09/09/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//04/12/2002: passage � UTF8: oui, mais non car Mozilla 1.0 ne le supporte pas...

//03/12/2002: ajout de la fonction champ_fichier par Olivier Langella

//tous les champs et boutons seront associ�s � un libell� qui indique leur utilit�
//le positionnement relatif d'un champ (ou d'un bouton) et de son libell� associ� pourra �tre soit libre, soit g�r� � l'aide de tableaux HTML
//classe se d�finit par des :
//propri�t�s de la classe ->var
//m�thodes->function

/* 
	documentation de l'interface de l'objet "formulaire"
	
	constructeur :
	function formulaire($pmethode,$paction,$ptransfertfichier = FALSE , $pnom ="FORM", $ponsubmit = FALSE, $pclasse = "formulaire")

	Divisions:
	division_debut(classe="")
	division_fin()	
	
	Champs:
	champ_affichage ($plibelle, $pval, $pclasse="form_affichage")
	champ_texte ($plibelle, $pnom, $pval, $ptaille=10, $ptaillemax=0)
	champ_decimal ($plibelle, $pnom, $pval, $ptaille=6, $ptaillemax=0)
	champ_fichier ($plibelle, $pnom, $pval, $ptaille=10, $ptaillemax=0)
	champ_date ($plibelle, $pnom, $date_au_format_mysql, $date_par_defaut="")
	champ_coord_gps($plibelle,$pnom,$pgps, $gps_par_defaut="")
	champ_ip ($plibelle, $pnom, $ip_au_format_mysql, $date_par_defaut="")
	champ_texte_etendu ($plibelle,$pnom, $pval, $plig, $pcol,$pclasse="-1")
	champ_cache ($pnom, $pval)
	champ_radio ($plibelle, $pnom, $pval, $pliste)
	champ_checkbox ($plibelle, $pnom, $pval, $pliste=array("1"=>""))
	champ_liste_deroulante($plibelle,$pnom, $pliste, $pdefault , $pclasse="")
	champ_valider ($plibelle, $pnom,$pval,$pclasse="-1") 
	champ_motdepasse($plibelle,$pnom, $pval, $ptaille)

	V�rifications:
	ajout_verif_champ_vide ($pnom_champ,$palert)
	ajout_verif_champ_min_max ($pnom_champ,$palert,$pmin,$pmax)
	ajout_verif_champ_lg ($pnom_champ,$palert,$plgmin,$plgmax)
	ajout_confirmation ($pconfirm)
*/

// modifi� le 12/9/2002 par Olivier Langella pour se plier aux sp�cifications du XHTML
// modifi� le 11/9/2002 par Olivier Langella: ajout de la fonction champ_affichage (affichage de texte dans un formulaire) simple
// modifi� le 11/9/2002 par Olivier Langella: ajout de l'argument $date_par_defaut dans la fonction champ_date
// modifi� le 10/9/2002 par Olivier Langella: ajout du "champ_texte_etendu", documentation de l'interface
// modifi� le 12/08/2002 par Olivier Langella pour afficher plusieurs lignes de valeurs diff�rentes

//require "libphp/table.inc";
//require "libphp/xhtml.inc";
//require "libphp/gestion_ip.inc";


if (!isset ($module_formulaire))
{
  $module_formulaire=1;
  
  class formulaire {
    
    // les variables (variables membres):
    var $_entetes;
    var $_mode_table;
    var $_orientation;
    var $_nb_champs;
    var $_nb_lignes;
    var $_champs;
    var $_nom;
    var $_on_submit;
    
    var $_tab_verif_champ_vide;
    var $_tab_verif_champ_min_max;
    var $_tab_verif_champ_lg;
    
    var $_confirm;
    var $_const;
    
    //les m�thodes:
    
    //constructeur de la classe
    
    function formulaire($pmethode,$paction,$ptransfertfichier = FALSE , $pnom="formulaire", $ponsubmit = FALSE, $pclasse = "formulaire") {
      
      $paction = xhtml_amp($paction);
      $this->_const = FALSE;
      $this->_mode_table = FALSE; //on initialise mode_table � false c-a-d :on n'est pas en mode table
      $this->_on_submit = $ponsubmit;
      
      if ($pnom=="") $pnom="formulaire";
      $this->_nom=$pnom;
      $this->_i_lignes = 0;
      $enctype = "";
      if ($ptransfertfichier) $enctype = "enctype='multipart/form-data'"; 
      
      //echo "<div class=\"$pclasse\"><form method=\"$pmethode\" ".$enctype."action=\"$paction\" name=\"$this->_nom\" onsubmit=\"";
      echo "<div class=\"$pclasse\"><form method=\"$pmethode\" ".$enctype."action=\"$paction\" onsubmit=\"";
      echo "return verif_entree_" . $this->_nom."(this)";
      //echo xhtml_preservespace( "return verif_entree_" . $this->_nom."()" );
      echo "\">\n";
      echo "<div class=\"formulaireInterieur\">";
      //code HTML pour ouvrir le formulaire
    }
    
    //fin du formulaire
    function fin(){
      //fin de la table, au cas o�
      $this->fin_table();
      echo "</div></form></div>\n"; //code HTML pour fermer le formulaire
      
      if ($this->_on_submit==TRUE){
	
	//$script = "<script language=\"JavaScript\" type=\"text/JavaScript\">";
	$script = "<script type=\"text/javascript\">\n";
	$script .= " function verif_entree_$this->_nom (form){\n";
	
	if ($this->_tab_verif_champ_vide!=""){
	  while (list($pnom_champ,$palert) = each ($this->_tab_verif_champ_vide) ){
	    
	    $script .= $this->verification_champ_vide_javascript ($pnom_champ,$palert);
	  }
	}
	
	if ($this->_tab_verif_champ_min_max!=""){	
	  while (list($pnom_champ,$tab) = each ($this->_tab_verif_champ_min_max) ){
	    
	    $script .= $this->verification_valeur_min_max_javascript ($pnom_champ,$tab['alert'],$tab['min'],$tab['max']);
	  }
	}
	
	if ($this->_tab_verif_champ_lg !=""){	
	  while (list($pnom_champ,$tablg) = each ($this->_tab_verif_champ_lg) ){
	    
	    $script .= $this->verification_longueur_javascript ($pnom_champ,$tablg['lg_alert'],$tablg['lg_min'],$tablg['lg_max']);
	  }
	}
	
	
	
	if ($this->_confirm != "")
	  $script .= "	return (confirm(\"$this->_confirm\"));";
	else 
	  $script .= "return  true;";
	$script .= "}";
	$script .= "\n</script>";
	
	echo xhtml_cdata($script);
      }
    }
    
    function champ_libelle ($plibelle, $pnom, $pval , $ptype ="text", $parametres = array() , $pliste = array()){
      //encodage en UTF8
      //$plibelle = utf8_encode($plibelle);
      //$pval = utf8_encode($pval);
      //cr�ation du champ
      if (!array_key_exists("class", $parametres)) $parametres["class"] = "";
      $pclasse = $parametres["class"];
      
      if (!is_array($pval)) $champHTML = $this->champ_form ($ptype, $pnom, $pval, $parametres, $pliste,$pclasse);
      
      if ($plibelle != "") $plibelle = "<div class=\"libelle\">".$plibelle."</div>";
      
      //affichage du champ en tenant compte de la pr�sentation
      if ($this->_mode_table){
	if ($this->_orientation == VERTICAL){
	  
	  //nouvelle ligne, avec libell� et champ dans deux cellules
	  TblDebutLigne();
	  TblCellule ($plibelle);
	  TblCellule ($champHTML);
	  TblFinLigne();
	}
	
	else {
	  
	  //on ne peut pas afficher maintenant : on stocke dans les tableaux
	  $this->_entetes[$this->_nb_champs]=$plibelle;
	  if (!is_array($pval)) $this->_champs[0][$this->_nb_champs] = $champHTML;
	  else {
	    for ($i=0; $i < count($pval); $i++) {
	      if ($ptype != "checkbox") $this->_champs[$i][$this->_nb_champs] = $this->champ_form ($ptype, $pnom, $pval[$i], $parametres, $pliste,$pclasse);
	      else $this->_champs[$i][$this->_nb_champs] = $this->champ_form ($ptype, $pnom, $pval[$i], $parametres, array($i =>''),$pclasse);
	    }
	  }
	  $this->_nb_champs++;
	}
      }
      else{
	//affichage simple
	echo $plibelle;
	echo $champHTML;
      }
      
    }	
    //affichage d'un champ avec son libelle
    
    function division_debut ($classe=""){
      if ($this->_orientation == HORIZONTAL){
	if ($classe != "" )echo "<div class=\"".$classe."\">\n";
	else echo "<div>\n";
      }
    }
    function division_fin (){
      if ($this->_orientation == HORIZONTAL){
	echo "</div>\n"; 
      }
    }	
    
    function champ_affichage ($plibelle, $pval, $pclasse="form_affichage"){
      $this->champ_libelle ($plibelle, "", $pval, "affichage", array ("class"=>"$pclasse","balise"=>"div"));
    }
    
    function champ_fichier ($plibelle, $pnom, $pval, $ptaille=10, $ptaillemax=0){
      $this->champ_libelle ($plibelle, $pnom, $pval, "file", array ("size"=>$ptaille, "maxlength"=>$ptaillemax));
    }
    
    function champ_texte ($plibelle, $pnom, $pval, $ptaille=10, $ptaillemax=0){
      $this->champ_libelle ($plibelle, $pnom, $pval, "text", array ("size"=>$ptaille, "maxlength"=>$ptaillemax));
    }
    //cr�e une zone de texte,de taille d�termin�e dans le protocole,on peut �crire dedans ou modifier la valeur qui s'y trouve
    
    function champ_texte_etendu ($plibelle,$pnom, $pval, $plig, $pcol,$pclasse="-1"){
      $this->champ_libelle ($plibelle, $pnom, $pval, "textarea", array("rows"=>$plig,"cols"=>$pcol, "class"=>$pclasse));
    }
    
    
    function champ_prive_affichage ($affichage,$balise,$classe){
      return "<$balise class=\"$classe\">$affichage</$balise>";
    }
    
    function champ_cache ($pnom, $pval){
      $plibelle = "";
      $this->champ_libelle ($plibelle, $pnom, $pval, "hidden");
    }
    //champ non visible . permet de d�finir des variables avec leurs valeurs 
    
    
    function champ_radio ($plibelle, $pnom, $pval, $pliste){
      $this->champ_libelle ($plibelle, $pnom, $pval, "radio", array(),$pliste);
    }
    //cr�er des boutons associ�s � des valeurs,l'utilisateur peut en cocher un ou +ieurs
    
    function champ_checkbox ($plibelle, $pnom, $pval, $pliste=array("1"=>"")){
      $this->champ_libelle ($plibelle, $pnom, $pval, "checkbox", array(),$pliste);
    }
    
    function champ_textarea ($pnom, $pval, $plig, $pcol,$pclasse){
      $toto= "<textarea name=\"$pnom\" rows=\"$plig\" cols=\"$pcol\"";
      if ($pclasse != "-1") $toto .= " class=\"$pclasse\">";
      else $toto .= ">";
      $toto.= "$pval</textarea>";
      return $toto;
    }
    
    //cr�er une zone de texte,mais dans ce cas,l'utilisateur peut saisir un texte sur +ieurs lignes
    //m�thode pour cr�er un champ input g�n�ral
    
    
    function champ_input ($ptype, $pnom, $pval, $ptaille, $ptaillemax){
      $toto="<input type=\"$ptype\" name=\"$pnom\"  value=\"$pval\" size=\"$ptaille\"></input>\n";
      return $toto;
    }
    //permet de cr�er les balises HTML pour y definir les champs du formulaire
    
    function champ_input_submit  ($ptype, $pnom, $pval, $pclasse, $ptaille, $ptaillemax){
      //$pclasse="valider";
      $toto="<input type=\"$ptype\" name=\"$pnom\" value=\"$pval\" class=\"$pclasse\" size=\"$ptaille\"></input>\n";
      return $toto;
    }
    //permet de cr�er les balises HTML pour y definir les champs du formulaire
    
    
    
    function champ_liste_deroulante($plibelle,$pnom, $pliste, $pdefault , $pclasse=""){
      $parametres["class"] = $pclasse;
      $this->champ_libelle ($plibelle, $pnom, $pdefault, "select", $parametres,$pliste);
    }
    
    function champ_select ($pnom, $pliste, $pdefault , $ptaille=10, $pclasse){
      $toto="<select name=\"$pnom\"";
      if ($ptaille != "") $toto .= " size=\"$ptaille\"";
      if ($pclasse != "") $toto .= " class=\"$pclasse\"";
      $toto .= ">\n";
      while (list ($val, $libelle) = each ($pliste))  {
	if ($val != $pdefault)
	  $toto.="<option value=\"$val\">$libelle</option>\n";
	else 
	  $toto.="<option value=\"$val\" selected=\"selected\">$libelle</option>\n";
      }
      // echo"$pliste";
      return $toto."</select>\n";
    }
    //champ pour s�lectionner dans une liste
    
    
    function champ_form ($ptype, $pnom, $pval, $parametres, $pliste = array()){
      if (!array_key_exists("size", $parametres)) $parametres["size"] = "";
      if (!array_key_exists("maxlength", $parametres)) $parametres["maxlength"] = "";
      
      switch ($ptype){  //on traite les cas des differents types
	
      case "text" : case "password" : case "reset": case "file": case "hidden":
	$taille = $parametres["size"];
	$taillemax = $parametres["maxlength"];
	if ($taillemax ==0) $taillemax = $taille;
	$_champs = $this->champ_input ($ptype,$pnom, $pval, $taille, $taillemax);
	break;
	
      case "textarea":
	$lig = $parametres["rows"];
	$col = $parametres["cols"];
	$classe= $parametres["class"];
	$_champs = $this->champ_textarea ($pnom, $pval,$lig,$col,$classe);
	break;	
	
      case "affichage":
	$balise = $parametres["balise"];
	$classe = $parametres["class"];
	$_champs = $this->champ_prive_affichage ($pval, $balise, $classe);
	break;	
	
      case "submit": 
	$classe = $parametres["class"];
	$taille = $parametres["size"];
	$taillemax = $parametres["maxlength"];
	if ($taillemax ==0) $taillemax = $taille;
	$_champs = $this->champ_input_submit ($ptype,$pnom, $pval,$classe,$taille, $taillemax);
	break;
	
      case "select":
	
	$taille = $parametres["size"];
	$pclasse = $parametres["class"];
	$_champs = $this->champ_select ($pnom, $pliste, $pval, $taille,$pclasse);
	break;
	
      case "checkbox": case "radio":
	$_champs = $this->champ_buttons($ptype, $pnom, $pliste, $pval);
	break;
	
      default : echo html_message("erreur : $ptype est un type inconnu","erreur");
	break;
      }
      return $_champs;
    }
    //champ de formulaire
    
    
    function champ_valider ($plibelle, $pnom,$pval,$pclasse="-1") {
      
      $parametres["class"] = $pclasse;
      $this->champ_libelle ($plibelle, $pnom, $pval , $ptype ="submit", $parametres);
    }
    //affichage d'un bouton qui validela saisie
    
    
    function debut_table ($pOrientation = VERTICAL,$pNb_lignes=1, $pclasse=-1){
      //Pas de bordure
      if ($pOrientation == VERTICAL) TblDebut (0,-1,-1,-1,$pclasse); 
      $this->_mode_table =TRUE;
      $this->_orientation = $pOrientation;
      $this->_nb_lignes = $pNb_lignes;
      $this->_nb_champs =0;
    }
    //D�but d'une table, mode horizontal ou vertical
    
    
    
    function fin_table (){
      if ($this->_mode_table == TRUE) {
	if ($this->_orientation == HORIZONTAL)
	  {
	    
	    //affichage des libelles
	    TblDebut(0);
	    TblDebutLigne ();
			  	
	    //en-t�tes du tableau
	    for ($i = 0 ; $i < $this->_nb_champs ; $i++)
	      TblCellule ($this->_entetes[$i]);
	    
	    TblFinLigne();
	    
	    //affichage des lignes et colonnes
	    for ($j = 0;  $j < $this->_nb_lignes; $j++)
	      {
		TblDebutLigne ();
		for ($i = 0; $i < $this->_nb_champs; $i++)
		  TblCellule ($this->_champs[$j][$i]);
		TblFinLigne();
	      }
	  }
	TblFin();
      } 
      $this->_mode_table =FALSE;
    }
    //fin d'un tableau
	
    function champ_buttons( $pType, $pNom, $pListe, $pDefaut){
      
      // Toujours afficher dans une table
		$libelles ="";
		$champs = "";
		while (list ($val, $libelle) = each ($pListe))
		  {
		    $libelles .= "<td>$libelle</td>";
		    if ($val == $pDefaut) $checked = "checked=\"checked\"";
		    else $checked = " ";
		    $champs .= "<td><input type=\"$pType\" name=\"$pNom\" value=\"$val\" ". " $checked /> </td>\n";
		  }
		return  "<table border=\"0\"><tr>\n". $libelles .  "</tr>\n<tr>" . $champs . "</tr></table>";
    }
    
    function champ_motdepasse($plibelle,$pnom, $pval, $ptaille) {
      $this->champ_libelle ($plibelle, $pnom, $pval , $ptype ="password", $parametres = array());
      
    }
    //zone de texte mais la saisie sera entr�e avec des " * "
    
    function champ_decimal ($plibelle, $pnom, $pval, $ptaille=6, $ptaillemax=0) {
      
      $this->champ_texte($plibelle,$pnom, str_replace(".",",",$pval), $ptaille);	
    }
    
    function champ_date($plibelle,$pnom,$pdate, $date_par_defaut=""){
      
      if (is_array($pdate)) {
	for ($i=0; $i < count($pdate); $i++) {
	  $pdate[$i] = datefr2datesql ($pdate[$i]);
	}
      }
      else {
	if ($pdate == "") {
	  if ($date_par_defaut == "aujourd'hui") {
	    $pdate = date("d")."/".date("m")."/".date("Y");
	  }
	}
	$pdate=datesql2datefr ("$pdate");
      }
      
      $this->champ_texte($plibelle,$pnom,$pdate,10);		
    }
    
    function champ_coord_gps($plibelle,$pnom,$pgps, $gps_par_defaut=""){
      
      if (is_array($pgps)) {
	for ($i=0; $i < count($pgps); $i++) {
	  $pgps[$i] = float2gps ($pgps[$i]);
	}
      }
      else {
	if ($pgps == "") {
	}
	$pgps=float2gps ($pgps);
      }
      
      if ($gps_par_defaut == "latitude") {
	$this->champ_texte($plibelle,$pnom,affiche_latitude($pgps),15);
      }
      else if ($gps_par_defaut == "longitude") {
	$this->champ_texte($plibelle,$pnom,affiche_longitude($pgps),15);
      }		
      else $this->champ_texte($plibelle,$pnom,$pgps,15);
				
    }
    
    function champ_ip($plibelle,$pnom,$pip, $ip_par_defaut="-1"){
      if (is_array($pip)) {
	for ($i=0; $i < count($pip); $i++) {
	  $pip[$i] = ipsql2ip ($pip[$i]);
	}
      }
      else {
	if ($pip == "") {
	  if ($ip_par_defaut == "-1") {
	    $pip = "157.136..";
	  }
	}
	
	$pip=ipsql2ip ("$pip");
      }
      
      $this->champ_texte($plibelle,$pnom,$pip,16);		
    }
    
    
    
    function ajout_verif_champ_vide ($pnom_champ,$palert){
      
      //on stocke dans les tableaux
      $this->_tab_verif_champ_vide [$pnom_champ]=$palert;
      
    }
    
    function ajout_verif_champ_min_max ($pnom_champ,$palert,$pmin,$pmax){
		
      $this->_tab_verif_champ_min_max [$pnom_champ] = array ('alert'=>$palert,'min'=>$pmin,'max'=>$pmax);
      
    }
    
    
    function ajout_verif_champ_lg ($pnom_champ,$palert,$plgmin,$plgmax){
      
      $this->_tab_verif_champ_lg [$pnom_champ] = array ('lg_alert'=>$palert,'lg_min'=>$plgmin,'lg_max'=>$plgmax);
      
      
    }			
    
    
    
    
    function ajout_confirmation ($pconfirm){
      $this->_confirm=$pconfirm;
      
    }
    
    
    function verification_champ_vide_javascript ($pnom_champ,$palert){
      //$script = "	if ((document.$this->_nom.$pnom_champ.value) == \"\"){\n"
      $script = "	if ((form.$pnom_champ.value) == \"\"){\n";
      
      $script .= "	alert(\"$palert\");\n" ;
      //$script .= "	document.$this->_nom.$pnom_champ.focus();\n";
      $script .= "	form.$pnom_champ.focus();\n";
      $script .= "	return(false);\n";
      $script .= "	}\n";
      
      return $script;
      
    }
    
    
    function verification_valeur_min_max_javascript ($pnom_champ,$palert,$pmin,$pmax){
      
      $script =" if (((form.$pnom_champ.value) <$pmin) || ((document.$this->_nom.$pnom_champ.value) >=$pmax)  ){\n";
      
      $script .= "	alert(\"$palert\");\n" ;
      
      $script .= "	form.$pnom_champ.focus();\n";
      
      
      $script .= "	return(false);\n";
      $script .= "	}\n";
      
      return $script;
      
    }
    
    function verification_longueur_javascript ($pnom_champ,$palert,$plgmin,$plgmax){
      
      $script =" if (  ((form.$pnom_champ.value.length)<$plgmin) || ((form.$pnom_champ.value.length)>$plgmax)  ){\n";
      
      $script .= "	alert(\"$palert\");\n" ;
      
      $script .= "	form.$pnom_champ.focus();\n";
      
      
      $script .= "	return(false);\n";
      $script .= "	}\n";
      
      
      return $script;
    }
    
    
    
  }
}
?>