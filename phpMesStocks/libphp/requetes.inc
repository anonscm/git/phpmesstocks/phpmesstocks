<?


/***************************************************************************
                          requetes.inc  -  construction et ex�cution de requ�tes SQL
                             -------------------
    begin                :  02/05/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
//modifi� le 14/5/2003 par Olivier Langella pour encapsuler les jointures SQL (left_join, left_outer_join ... etc)

//modifi� le 02/12/2002 par Olivier Langella pour ajouter les fonctions  "ligne_suivante_taba($resultat)", "select" et "fin_select"

//modifi� le 02/11/2002 pour ajouter la fonction exec_requete_select

//Modifi� le 20/08/2002 par Valerie cr�ation de la fonction commence_par � partir de ajout_like: 
//ajout_like permet de rechercher une chaine de caractere dans un mot
//commence par permet de rechercher tout les mot commenncants par une cahiane de caracteres donn�e 

/*
	INTERFACE

	function left_outer_join($tab_nomchamps, $table, $alias, $cle)
	function left_outer_join_on($tab_nomchamps, $table, $alias, $clause)
	function left_join($tab_nomchamps, $table, $alias, $cle)
	function left_join_on($tab_nomchamps, $table, $alias, $clause)

*/

if (!isset($fichier_exec_requete)){
  $fichier_exec_requete = 1;
	
  function exec_requete ($requete, $connexion){
    //echo "exec_requete : ".$requete."<br/> ";
    $resultat = mysql_query ($requete, $connexion);
    //execute la requete
    
    if ($resultat) 	{
      return  ( $resultat);
    } 
    else {
      echo "<b>Erreur dans l'ex�cution de la requ�te '$requete'. </b><br>";
      echo "<b> Message de MySQL : </b>" . mysql_error ($connexion);
      exit;
    }
  }
  
  function exec_requete_select ($requete, $connexion){
    //appel exec_requete apr�s avoir v�rifier qu'il n'y a qu'une seule requ�te et que c'est un "select"
    //echo "exec_requete_select : ".$requete."<br/> ";
    //echo $requete;
    if (strncmp ($requete, "SELECT ", 7) == 0)
      return  ( exec_requete ($requete, $connexion));
    else
      return (false);
  }
  
  //recherche de la ligne suivante
  
  function ligne_suivante ($resultat){
    return mysql_fetch_object($resultat);
  }
  //recup�re le r�sultat de la requete,ligne par ligne,chaque valeur demand�e est une variable membre obtenue avec $ligne->Attribut
  
  
  function ligne_suivante_taba($resultat){
    return  mysql_fetch_assoc($resultat);
  }
  
  
  class creer_requete {
    var $_deja_where;
    var $_debut_bloc;
    var $_requete;
    var $_requetefrom;
    var $_nblignes;
    var $_denombrement;
    var $_tri;
    
    function creer_requete ($requete = ""){
      $this->_deja_where=FALSE;
      $this->_debut_bloc=FALSE;
      
      $this->_requete= $requete;
      $this->_requetefrom="";
      $this->_tri="";

      $this->_nblignes = 0;
      $this->_denombrement = 0;
      
    }
    
    function left_join_on($tab_nomchamps, $table, $alias, $clause) {
      //LEFT OUTER JOIN table AS alias UNSING (la cle)
      $this->select($tab_nomchamps, "LEFT JOIN", $table, $alias, "ON (".$clause.")");
    }
    
    function left_join($tab_nomchamps, $table, $alias, $cle) {
      //LEFT OUTER JOIN table AS alias UNSING (la cle)
      if ($alias == "") $alias = -1;
      $this->select($tab_nomchamps, "LEFT JOIN", $table, $alias, "USING (".$cle.")");
    }
    
    function left_outer_join_on($tab_nomchamps, $table, $alias, $clause) {
      //LEFT OUTER JOIN table AS alias UNSING (la cle)
      $this->select($tab_nomchamps, "LEFT OUTER JOIN", $table, $alias, "ON (".$clause.")");
    }
    
    function left_outer_join($tab_nomchamps, $table, $alias, $cle) {
      //LEFT OUTER JOIN table AS alias UNSING (la cle)
      $this->select($tab_nomchamps, "LEFT OUTER JOIN", $table, $alias, "USING (".$cle.")");
    }
    
    function select($tab_nomchamps, $liaison, $table, $alias = -1, $using = -1) {
      for ($i=0; $i < count($tab_nomchamps); $i++) {
	if ($alias == -1) $this->_requete .= $table.".".$tab_nomchamps[$i].", ";
	else $this->_requete .= $alias.".".$tab_nomchamps[$i].", ";
      }
      
      $this->_requetefrom .= " ".$liaison." ".$table." ";
      if ($alias != -1) $this->_requetefrom .= "AS ".$alias." ";
      if ($using != -1) $this->_requetefrom .= $using." ";
    }
    
    function fin_select() {
      $this->_requete = substr($this->_requete,0, strlen($this->_requete) - 2);
      $this->_requete = "SELECT ".$this->_requete.$this->_requetefrom;
    }

    function date_non_depassee ($gauche, $droite="",$operateur="AND"){
      $operateur = " ".$operateur." ";
      if ($this->_debut_bloc == TRUE) {
	$operateur = "";
	$this->_debut_bloc = FALSE;
      }
      if ($this->_deja_where ==FALSE) {
	$this->_requete.=" WHERE ";
	$this->_deja_where=TRUE;
	$operateur = "";	
      }
      
      if ($droite == "") $this->_requete.=$operateur.$gauche.">= NOW()";
      else $this->_requete.=$gauche.">= '$droite'";
    }
    
    function debut_bloc ($operateur="AND"){
      $this->_debut_bloc = TRUE;
      if ($this->_deja_where ==FALSE) {
	$this->_requete.=" WHERE ";
	$this->_deja_where=TRUE;
	
      }
      $this->_requete.= " ".$operateur." ( ";
      
    }
    function fin_bloc (){
      
      if ($this->_deja_where == FALSE) {
	$this->_requete.=" WHERE ";
	$this->_deja_where=TRUE;
	
      }
      $this->_requete.= " ) ";
      
    }
    
    function date_depassee ($gauche, $droite="",$operateur="AND"){
      $operateur = " ".$operateur." ";
      
      if ($this->_deja_where ==FALSE) {
	$this->_requete.=" WHERE ";
	if ($droite == "") $this->_requete.=$gauche."< NOW() ";
	else $this->_requete.=$gauche."< '$droite' ";
	$this->_deja_where=TRUE;
	
      }
      else {
	
	if ($droite == "") $this->_requete.=$operateur.$gauche."< NOW()";
	else $this->_requete.=$gauche."< '$droite'";
      }
      
    }
    
    function ajout_different ($gauche, $droite="",$operateur="AND"){
      $operateur = " ".$operateur." ";
      
      if ($this->_deja_where ==FALSE) {
	$this->_requete.=" WHERE ";
	$this->_requete.=$gauche."!='$droite' ";
	$this->_deja_where=TRUE;
	
      }
      else {
	
	$this->_requete.=$operateur.$gauche."!='$droite'";
      }
      
    }
    
    function ajout_egal ($gauche, $droite="",$operateur="AND"){
      if ($this->_debut_bloc == TRUE) {
	$operateur = "";
	$this->_debut_bloc = FALSE;
      }
	//      if ($this->_debut_bloc == TRUE) $operateur = "";
      $operateur = " ".$operateur." ";
      //$droite = $this->chaine_a_chercher ($chaine);
      $droite = str_replace("'", "\'", $droite);

      if ($this->_deja_where ==FALSE) {
	$this->_requete.=" WHERE ";
	if ($droite != "NULL") $this->_requete.=$gauche."='$droite' ";
	else $this->_requete.=$gauche." IS NULL ";
	$this->_deja_where=TRUE;
	
      }
      else {
	
	if ($droite != "NULL") $this->_requete.=$operateur.$gauche."='$droite'";
	else $this->_requete.=$operateur.$gauche." IS NULL ";
      }
      
      //echo $this->_requete;
    }
    
    function ajout_like_entier ($gauche, $droite="",$operateur="AND"){
      $droite = $this->chaine_a_chercher ($droite);
      
      $operateur = " ".$operateur." ";
      
      if (($this->_deja_where ==FALSE) && ($droite !="")){
	$this->_requete.=" WHERE ";
	
	$this->_requete.=$gauche." LIKE '$droite' ";
	$this->_deja_where=TRUE;
	
      }
      else {
	
	if ($droite !="")
	  $this->_requete.=$operateur.$gauche." LIKE '$droite' ";
      }
      
    }
    
    function ajout_like ($gauche, $droite="",$operateur="AND"){
      $this->ajout_like_entier($gauche, "%".$droite."%", $operateur);
    }
    
    function commence_par ($gauche, $droite="",$operateur="AND"){
      $operateur = " ".$operateur." ";
      
      if (($this->_deja_where ==FALSE) && ($droite !="")){
	$this->_requete.=" WHERE ";
	
	$this->_requete.=$gauche." LIKE '$droite%' ";
	$this->_deja_where=TRUE;
	
      }
      else {
	
	if ($droite !="")
	  $this->_requete.=$operateur.$gauche." LIKE '$droite%' ";
      }
      
    }
    
    function comparaison ($gauche, $min, $max ,$operateur="AND"){
      $operateur = " ".$operateur." ";
      
      if ($this->_deja_where ==FALSE){
	$this->_requete.=" WHERE ";
	$this->_deja_where=TRUE;
      }
      else {
	$this->_requete.= " ".$operateur." ";
      }

      if ($max == "") $max = 0;
      if ($min == "") $min = 0;
      if ($max < $min) {
	$tmp = $min; $min = $max; $max = $tmp;
      }
      $this->_requete .=  "(".$min." < ".$gauche." AND ". $gauche." < ".$max.") ";
    }
    
    function chaine_a_chercher ($chaine) {
      //fonction priv�e !!!
      //transformation d'une chaine de caract�res en vue de son stockage dans la base de donn�es
      // (quoter les apostrophes par exemple)
      $chaine = stripslashes ($chaine);
      $chaine = addslashes ( $chaine);
      //$chaine = str_replace("'", "\'", $chaine);
      // forcer le stockage en ISO-8859-1
      if ($GLOBALS["conv_utf8_iso8859_1"]) $chaine = iconv("UTF-8","ISO-8859-1",$chaine);
      return ($chaine);
    }
    
    function comparaisondate ($reqjour1,$reqmois1,$reqannee1,$reqjour2,$reqmois2,$reqannee2,$pdate){
      /*		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $pdate, $regs );
		$jourfr=$regs[3];
		$moisfr	=$regs[2];
		$anneefr=$regs[1]; 	
		
		if (($reqjour1=<$jourfr) && ($jourfr=<$reqjour2) && ($reqmois1=<$moisfr) && ($moisfr=<$reqmois2) && ($reqannee1=<$anneefr) && ($anneefr=<$reqannee2))
			return TRUE;
		else 	
			return FALSE;
			
      */	
    }

    function ajout_tri ($champ, $sens="ASC") {
      //$sens = ASC pour ascendant ou DESC pour descendant
      $champ = str_replace (" ", "",$champ);
      if ($this->_tri == "") {
	$this->_tri = "ORDER BY ";
      }
      else $this->_tri .= ", ";

      if ($sens == "ASC") $this->_tri .= $champ;
      else if ($sens == "DESC") $this->_tri .= $champ." DESC";
      
    }

    function fin_requete ($connexion=-1, $tri="",$colonne_a_denombrer=""){
      //$this->_requete.=";";
      $tri = ""; //obsol�te et dangereux
      if ($connexion != -1) {
	//compter les lignes renvoy�es par cette requ�te:
	//$requete = str_replace ("FROM",", COUNT(*) FROM",$this->_requete);
	$resultat = exec_requete ($this->_requete, $connexion);
	$this->_nblignes = requete_sql_nb_lignes($connexion);
	$this->_denombrement = $this->_nblignes;
	
	if ($colonne_a_denombrer != "") {
	  $requete = "SELECT DISTINCT ($colonne_a_denombrer) ";
	  $requete .= substr($this->_requete, strpos($this->_requete, "FROM "), strlen($this->_requete));
	  
	  $resultat = exec_requete ($requete, $connexion);
	  $this->_denombrement =  requete_sql_nb_lignes($connexion);
	}
      }
      else {
	$this->_nblignes = -1;
	$this->_denombrement = -1;
      }

      return ($this->_requete.$this->_tri);
    }
  }
  
  
  
  
  
  
  
  
}//fin du test
?>