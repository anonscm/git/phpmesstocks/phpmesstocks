<?
                          
/***************************************************************************
                      gestion_gps.inc  -
           biblioth�que de fonctions pour transformer des coordonn�es gps en float pour les stocker dans la table...
                             -------------------
    begin                :  23/01/2003
    copyright            : (C) 2003 by Mehdi Talata
    email                : mehdi.talata@voila.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if (!isset($module_gestion_gps)){

	$module_gestion_gps =1;
	
	
		
		//on veut r�cup�rer les coordonn�es stock�e dans la table sous forme float et l'afficher en gps sous forme  d m s 
	function  float2gps ($gps) {
	
		bcscale(10);
		
		
		
		
		//echo html_message("coucou :".$gps." ".$gps_degre1,"erreur");
		$gps_degre =bcdiv ( $gps,1,0);
		//on recupere la partie decimale
		$decimale = bcsub($gps,$gps_degre);		
		$decimale = bcmul($decimale,60);		
		$gps_minute = bcdiv ($decimale,1,0 ); 
		$decimale = bcsub($gps_minute,$decimale);
		$gps_seconde =abs( bcmul($decimale,60,2));
		
			
				
				
				
				
		//echo html_message("coucou :".$gps_degre." ".$gps_minute,"erreur");
				
		
 		 		
		$resultat = $gps_degre."�".$gps_minute."'".$gps_seconde."\"";
		$resultat=str_replace(".00","",$resultat);
		$resultat = htmlspecialchars($resultat);		
		echo html_message ("float2gps: ".$resultat,"erreur");
		return ($resultat);					
			
	}
		
		
		
	

  
	function  gps2float ($gps_degre,$gps_minute=-1,$gps_seconde=-1){
		bcscale(10);
			//on accepte en argument des d m s et on renvoie un r�el 
			//soit la fonction accepte 3 chiffre en entr� � ' " soit elle accepte une chaine de caractere "45d23m3s"
		
			// 1) => si 1 argument: transformer en 3 chifres
		
		if (($gps_minute==-1)&&($gps_seconde==-1)){
			$gps_degre = stripslashes($gps_degre);
			
			//echo html_message($gps_degre,"erreur");
			$regs = array();
			
			ereg("([0-9]{1,3})�([0-9]{1,2})'([0-9]{1,2})\"", $gps_degre, $regs );


			if (!isset($regs[3]))  {
			//echo html_message("1er  if du bloc ".$regs[1]."r2 ".$reg[2]."r3 ".$reg[3],"erreur");
				ereg("([0-9]{1,3})�([0-9]{1,2})'", $gps_degre, $regs );
				$regs[3] = 0;
				if (!isset($regs[2]))  {
				//echo html_message("2�me if du bloc ".$regs[1]."r2 ".$reg[2]."r3 ".$reg[3],"erreur");
					ereg("([0-9]{1,3})�", $gps_degre, $regs );
					$regs[2] = 0;
					$regs[3] = 0;
				}
				else if (!isset($regs[1])) {
				//echo html_message("else du bloc ".$regs[1]."r2 ".$reg[2]."r3 ".$reg[3],"erreur");
				//echo html_message("".$regs[1]."r2 ".$reg[2]."r3 ".$reg[3],"erreur");
					$regs[1] = 0;
					$regs[2] = 0;
					$regs[3] = 0;
				}
			}
		
			$gps_degre = $regs[1];
			$gps_minute =$regs[2];
			$gps_seconde =$regs[3];
			
		}
		// 2) => calcul du r�el
		
		//echo html_message("coucou ".bcadd($gps_degre, bcadd(bcdiv($gps_minute,"60","5"),bcdiv($gps_seconde,"3600","5"),"5"),"3"),"erreur");
		//echo html_message("coucou".$regs[1]." ".$reg[2]." ".$reg[3],"erreur");
		$resultat = bcadd($gps_degre, bcadd(bcdiv($gps_minute,"60"),bcdiv($gps_seconde,"3600")));
		echo html_message ("gps2float: ".$resultat,"erreur");
		return   $resultat;
		
	
	}	
	

}
?>