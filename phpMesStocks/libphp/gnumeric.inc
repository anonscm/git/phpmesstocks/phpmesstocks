<?
/***************************************************************************
                      gnumeric.inc  -
           bibliotheque de fonctions pour fabriquer des fichiers gnumeric
                                        -------------------
    begin                :  05/08/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

if (!isset($module_gnumeric)) {
  $module_gnumeric = 1;

  function gnumeric_entete($titre_feuille) {
    $tmp = "";
    //echo "<  ?xml version=\"1.0\" encoding=\"UTF-8\"?  >";
    $tmp .= "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
    $tmp .=  "<gmr:Workbook xmlns:gmr=\"http://www.gnumeric.org/v10.dtd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.gnumeric.org/v8.xsd\">";
    // echo "<gmr:Attributes></gmr:Attributes>";
    $tmp .= "<gmr:Attributes>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::show_horizontal_scrollbar</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::show_vertical_scrollbar</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::show_notebook_tabs</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::do_auto_completion</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::is_protected</gmr:name>
      <gmr:value>FALSE</gmr:value>
    </gmr:Attribute>
  </gmr:Attributes>";
    $tmp .= "<gmr:Summary>
    <gmr:Item>
      <gmr:name>application</gmr:name>
      <gmr:val-string>phpMesEchantillons</gmr:val-string>
    </gmr:Item>
    <gmr:Item>
      <gmr:name>author</gmr:name>
      <gmr:val-string>Olivier Langella Olivier.Langella@pge.cnrs-gif.fr</gmr:val-string>
    </gmr:Item>
  </gmr:Summary>";
    $tmp .= "<gmr:SheetNameIndex>
    <gmr:SheetName>".$titre_feuille."</gmr:SheetName>
  </gmr:SheetNameIndex>";
    $tmp .= "<gmr:Geometry Width=\"922\" Height=\"595\"/>";
   
    $tmp .= "<gmr:Sheets><gmr:Sheet DisplayFormulas=\"false\" HideZero=\"false\" HideGrid=\"false\" HideColHeader=\"false\" HideRowHeader=\"false\" DisplayOutlines=\"true\" OutlineSymbolsBelow=\"true\" OutlineSymbolsRight=\"true\">";

    //    $row=2;
    //    $col = 3;
    $tmp .= "<gmr:Name>".$titre_feuille."</gmr:Name>";
    //    echo "<gmr:MaxCol>".$col."</gmr:MaxCol><gmr:MaxRow>".$row."</gmr:MaxRow>";
    $tmp .= "<gmr:Zoom>1.000000</gmr:Zoom><gmr:PrintInformation><gmr:Margins><gmr:top Points=\"28.35\" PrefUnit=\"cm\"/><gmr:bottom Points=\"28.35\" PrefUnit=\"cm\"/><gmr:left Points=\"56.69\" PrefUnit=\"Pt\"/><gmr:right Points=\"56.69\" PrefUnit=\"Pt\"/><gmr:header Points=\"85.04\" PrefUnit=\"Pt\"/><gmr:footer Points=\"85.04\" PrefUnit=\"Pt\"/></gmr:Margins><gmr:Scale type=\"percentage\" percentage=\"100\"/><gmr:vcenter value=\"0\"/><gmr:hcenter value=\"0\"/>
            <gmr:grid value=\"0\"/>
      <gmr:even_if_only_styles value=\"0\"/>
      <gmr:monochrome value=\"0\"/>
      <gmr:draft value=\"0\"/>
      <gmr:titles value=\"0\"/>
     <gmr:repeat_top value=\"\"/>
      <gmr:repeat_left value=\"\"/>
      <gmr:order>r_then_d</gmr:order>
     <gmr:orientation>landscape</gmr:orientation>
      <gmr:Header Left=\"\" Middle=\"&amp;[TAB]\" Right=\"\"/>
      <gmr:Footer Left=\"\" Middle=\"Page &amp;[PAGE]\" Right=\"\"/>
      <gmr:paper>A4</gmr:paper>
    </gmr:PrintInformation>";
    $tmp .= "<gmr:Styles>
        <gmr:StyleRegion startCol=\"0\" startRow=\"0\" endCol=\"255\" endRow=\"65535\">
          <gmr:Style HAlign=\"1\" VAlign=\"2\" WrapText=\"0\" ShrinkToFit=\"0\" Rotation=\"0\" Shade=\"0\" Indent=\"0\" Locked=\"1\" Hidden=\"0\" Fore=\"0:0:0\" Back=\"FFFF:FFFF:FFFF\" PatternColor=\"0:0:0\" Format=\"General\">
            <gmr:Font Unit=\"10\" Bold=\"0\" Italic=\"0\" Underline=\"0\" StrikeThrough=\"0\">Sans</gmr:Font>
            <gmr:StyleBorder>
              <gmr:Top Style=\"0\"/>
              <gmr:Bottom Style=\"0\"/>
              <gmr:Left Style=\"0\"/>
              <gmr:Right Style=\"0\"/>
              <gmr:Diagonal Style=\"0\"/>
              <gmr:Rev-Diagonal Style=\"0\"/>
            </gmr:StyleBorder>
          </gmr:Style>
        </gmr:StyleRegion>
      </gmr:Styles>
      <gmr:Cols DefaultSizePts=\"48\">
        <gmr:ColInfo No=\"0\" Unit=\"48\" MarginA=\"2\" MarginB=\"2\" Count=\"3\"/>
      </gmr:Cols>
      <gmr:Rows DefaultSizePts=\"12.75\">
        <gmr:RowInfo No=\"0\" Unit=\"12.75\" MarginA=\"0\" MarginB=\"0\" Count=\"2\"/>
      </gmr:Rows>
      <gmr:Selections CursorCol=\"0\" CursorRow=\"0\">
        <gmr:Selection startCol=\"0\" startRow=\"0\" endCol=\"0\" endRow=\"0\"/>
      </gmr:Selections>";
    return($tmp);
  }

  function gnumeric_fin() {
    $tmp ="";
    $tmp .= "<gmr:SheetLayout TopLeft=\"A1\"/>
      <gmr:Solver TargetCol=\"-1\" TargetRow=\"-1\" ProblemType=\"1\" Inputs=\"\" MaxTime=\"0\" MaxIter=\"0\" NonNeg=\"1\" Discr=\"0\" AutoScale=\"0\" ShowIter=\"0\" AnswerR=\"0\" SensitivityR=\"0\" LimitsR=\"0\" PerformR=\"0\" ProgramR=\"0\"/>
      <gmr:Scenarios/>";
    $tmp .= "</gmr:Sheet></gmr:Sheets>";
    $tmp .= "<gmr:UIData SelectedTab=\"0\"/>";
    $tmp .= "</gmr:Workbook>";
    return($tmp);

  }
}
?>