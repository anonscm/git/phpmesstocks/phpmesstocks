<?
                          
/***************************************************************************
                      table.inc  -
           production de tableau HTML
           
                             -------------------
    begin                :  31/7/2002
    copyright            : (C) 2002 by Yves Rigaud
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
// modifi� le 12/9/2002 par Olivier Langella pour enlever toute mise en page HTML
// et ne se servir exclusivement que des classes et des feuilles de styles

if (!isset ($module_table))
{
  $module_table=1;
  
  //module de production du tableau
  function TblDebut ($classe = -1){
    if ( $classe == "") $classe = -1;
    $optionClasse = " ";
    if ($classe != -1) $optionClasse = "class='$classe' ";
    
    echo "<table $optionClasse>\n";	
  }
  
  function TblFin () {
    echo "</table>\n";
  } 
  
  function TblDebutLigne ($classe=-1){
    $optionClasse ="";
    if ($classe!=-1) $optionClasse=" class='$classe'";
    echo "<tr".$optionClasse.">\n";
  }
  
  function TblFinLigne (){
    echo"</tr>\n";
  }
  
  function TblDebutCelluleEnTete ($classe=-1){
    $optionClasse="";
    if ($classe!=-1) $optionClasse="class='$classe'";
    echo "<th $optionClasse>\n";
  }
  
  function TblFinCelluleEnTete () {
    echo "</th>\n";
  }
  
  function TblCelluleEnTete ($contenu, $classe=-1){
    if ($classe == -1) $optionClasse ="";
    else $optionClasse=" class='$classe'";
    echo "<th".$optionClasse.">$contenu</th>\n";
  }
  
  function TblDebutCellule ($classe=-1){
    $optionClasse="";
    if ($classe!=-1) $optionClasse="class=\"$classe\"";
    echo "<td $optionClasse>\n";
  }
  
  function TblFinCellule () {
    echo "</td>\n";
  }
  
  function TblCellule ($contenu, $classe=-1, $nbLig = -1, $nbCol = -1){
    if ($classe == -1) $optionClasse ="";
    else $optionClasse=" class='$classe'";
    if ($nbLig == -1) $nbLig ="";
    else $nbLig=" rowspan='$nbLig'";
    if ($nbCol == -1) $nbCol ="";
    else $nbCol=" colspan='$nbCol'";
    echo "<td".$nbLig.$nbCol.$optionClasse.">$contenu</td>\n";
  }
  //fin du module table	
}


?>
