<?

                           
/***************************************************************************
                      sql_functions.inc  -
           encapsulation de requetes SQL dans des fonctions PHP
                             -------------------
    begin                :  31/7/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 

 // dernire modification : 
// 31/12/2002,  Olivier Langella: 
//	ajout de la fonction "chaine_a_stocker". 
 
if (!isset($module_sql_functions)){
  
  $module_sql_functions = 1;
  
  if (!isset($conv_utf8_iso8859_1)) $conv_utf8_iso8859_1 = FALSE;
  
  function chaine_a_stocker ($chaine) {
    //transformation d'une chaine de caract�res en vue de son stockage dans la base de donn�es
    // (quoter les apostrophes par exemple)
    $chaine = stripslashes ($chaine);
    $chaine = addslashes ( $chaine);
    //$chaine = str_replace("'", "\'", $chaine);
    // forcer le stockage en ISO-8859-1
    if ($GLOBALS["conv_utf8_iso8859_1"]) $chaine = iconv("UTF-8","ISO-8859-1",$chaine);
    return ($chaine);
  }
  
  function requete_sql_insert_into($table,$tableau_valeurs) {
    
    //en mysql:
    $requete = "INSERT INTO $table (";
    $requete_suite = " VALUES (";
    while (list($key,$val) = @each($tableau_valeurs)) {
      $val = chaine_a_stocker ($val);
      
      $requete .= $key;
      $requete .= ", ";
      $requete_suite .= "'".$val."'";
      $requete_suite .= ", ";		
    }
    $requete .= ")";
    $requete_suite .= ")"; // , )
    $requete = $requete.$requete_suite;
    return (str_replace(", )", ")", $requete));
    
  }
  
  function requete_sql_update($table, $tableau_close_where, $tableau_valeurs, $operateur_where="AND") {
    
    //en mysql:
    $requete = "UPDATE ".$table." SET ";
    while (list($key,$val) = @each($tableau_valeurs)) {
      $val = chaine_a_stocker ($val);
      
      $requete .= $key;
      // if (str_replace("<expr>","", $val)) $requete .= "=".$val.", ";
      $requete .= "='".$val."', ";
    }
    $requete .= "WHERE ";
    while (list($key,$val) = @each($tableau_close_where)) {
      $requete .= $key;
      $requete .= "='".$val."' ".$operateur_where." ";
    }
    $requete = str_replace(" ".$operateur_where." ##", "", $requete."##");
    return (str_replace(", WHERE ", " WHERE ", $requete));    
    
  }
  
  function requete_sql_delete($table, $tableau_valeur,$operateur_where="AND"){
    $requete="DELETE FROM ".$table ." WHERE";
    while ( list($key,$val) = @each($tableau_valeur)){
      $requete.=  " ".$key;
      $requete .= "='".$val."' ".$operateur_where;
    }
    $requete = str_replace(" ".$operateur_where."##", "", $requete."##");
    
    return ($requete);
  }
  
  function requete_sql_nb_lignes($connexion=-1){
    if ($connexion==-1) return(mysql_affected_rows());
    else return(mysql_affected_rows($connexion));
  }
}
?>