<?
                          
/***************************************************************************
                      objet_formulaire_const.inc  -
           classe d�riv�e pour l'affichage de donn�es d'un formulaire sans modification
           inspir� du livre MySql PHP, O'Reilly, de Philippe Rigaud
                             -------------------
    begin                :  27/09/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


if (!isset($module_formulaire_const)){
  $module_formulaire_const = 1;
  
  //require "libphp/objet_liste_resultat.inc";
  
  class formulaire_const extends formulaire {
    function formulaire_const($pmethode,$paction,$ptransfertfichier = FALSE , $pnom="formulaire", $ponsubmit = FALSE, $pclasse = "formulaire") {
      
      $this->_mode_table = FALSE; //on initialise mode_table � false c-a-d :on n'est pas en mode table
      $this->_const = TRUE;
      
      if ($pnom=="") $pnom="formulaire";
      $this->_nom=$pnom;
      $this->_i_lignes = 0;
      echo "<div class=\"$pclasse\">";
      echo "<div class=\"formulaireConst\">";
    }
    //fin du formulaire
    function fin(){
      //fin de la table, au cas o�
      $this->fin_table();
      echo "</div></div>\n"; //code HTML pour fermer le formulaire
    }
    
    function champ_valider ($plibelle, $pnom,$pval,$pclasse="-1") {
    }
    function champ_cache ($pnom, $pval) {}
    
    function champ_input ($ptype, $pnom, $pval, $ptaille, $ptaillemax){
      //$toto="<input type=\"$ptype\" name=\"$pnom\"  value=\"$pval\" size=\"$ptaille\"></input>\n";
      $toto = html_message($pval);
      return $toto;
    }
    function champ_textarea ($pnom, $pval, $plig, $pcol,$pclasse){
      $toto = html_message($pval, $pclasse);
      return $toto;
    }
    function champ_select ($pnom, $pliste, $pdefault , $ptaille=10, $pclasse){
      $toto = "";
      while (list ($val, $libelle) = each ($pliste))  {
	if ($val == $pdefault) {
	  $toto = html_message($libelle, $pclasse);
	}		
      }
      return $toto;
    }
    
    function champ_buttons( $pType, $pNom, $pListe, $pDefaut) {
      if ($pType == "checkbox") return;
      // Toujours afficher dans une table
      $libelles ="";
      $champs = "";
      while (list ($val, $libelle) = each ($pListe))
	{
	  $libelles .= "<td>$libelle</td>";
	  if ($val == $pDefaut) $checked = "checked=\"checked\"";
	  else $checked = " ";
	  $champs .= "<td>".html_message($val)."</td>";
	  // " $checked /> ."</td>\n";
	}
      return  "<table border=\"0\"><tr>\n". $libelles .  "</tr>\n<tr>" . $champs . "</tr></table>";
    }
    
    
    
  }
}

?>