<?
/***************************************************************************
                          memoire_req.inc  - 
                   garde en m�moire (variable de session) les arguments d'une requ�te
                             -------------------
    begin                :  06/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 if (!isset($module_memoire_req)){
   $module_memoire_req = 1;

   function rappel_memoire_req(&$HTTP_SESSION_VARS, $nom_req, $tab_req) {
	
     if (isset($GLOBALS[$tab_req[0]])) {
       //nouveaux arguments de requ�te SQL � sauvegarder:
			for ($i=0; $i < count ($tab_req); $i++) {
				if (array_key_exists($tab_req[$i],$GLOBALS))
					$tableau[$tab_req[$i]] = $GLOBALS[$tab_req[$i]];
				else {
					$tableau[$tab_req[$i]] = "";
					$GLOBALS[$tab_req[$i]] = "";
				}
			}
			$GLOBALS["position"] = 0;
			
			$HTTP_SESSION_VARS["tab_req_arg_".$nom_req] = serialize($tableau);
			session_register ("tab_req_arg_".$nom_req); 
		}
		else {
			// il faut prendre les anciens arguments et les remettre dans le circuit
			$tableau = array();
			if (isset($HTTP_SESSION_VARS["tab_req_arg_".$nom_req])) {
				$tableau = unserialize($HTTP_SESSION_VARS["tab_req_arg_".$nom_req]);
			}
			//$tableau = unserialize($GLOBALS["tab_req_arg_".$nom_req]);
			
			for ($i=0; $i < count ($tab_req); $i++) {
					if (array_key_exists($tab_req[$i],$tableau)) $GLOBALS[$tab_req[$i]] = $tableau[$tab_req[$i]];
					else $GLOBALS[$tab_req[$i]] = "";
			}
		}
		
	}

	function rappel_liste_pos(&$HTTP_SESSION_VARS, $nom_rep) {
		if (isset($GLOBALS["position"]) || isset($GLOBALS["pas"])) {
			//nouveaux arguments de position � sauvegarder:
			if (!isset($GLOBALS["pas"])) $GLOBALS["pas"] = 30;
			if (!isset($GLOBALS["position"])) $GLOBALS["position"] = 0;
			$tableau["position"] = $GLOBALS["position"];
			$tableau["pas"] = $GLOBALS["pas"];
			$HTTP_SESSION_VARS["tab_pos_".$nom_rep] = serialize($tableau);
			session_register ("tab_pos_".$nom_rep); 
		}
		else {
			// il faut prendre les anciens arguments et les remettre dans le circuit
			$tableau = array();
			if (isset($HTTP_SESSION_VARS["tab_pos_".$nom_rep])) {
				$tableau = unserialize($HTTP_SESSION_VARS["tab_pos_".$nom_rep]);
			}
			//$tableau = unserialize($GLOBALS["tab_pos_".$nom_rep]);
			if (array_key_exists("position",$tableau)) $GLOBALS["position"] = $tableau["position"];
			else $GLOBALS["position"] = "";
			if (array_key_exists("pas",$tableau)) $GLOBALS["pas"] = $tableau["pas"];
			else $GLOBALS["pas"] = "";
		}
		
	}

	function rappel_tri(&$HTTP_SESSION_VARS, $nom_rep, $tri_par_defaut) {

		if (isset($GLOBALS["tri"])) {
			//nouveau tri � sauvegarder:
			$HTTP_SESSION_VARS["tri_".$nom_rep] = serialize($GLOBALS["tri"]);
			session_register ("tri_".$nom_rep); 
		}
		else if (!isset($HTTP_SESSION_VARS["tri_".$nom_rep]))
		{
			$HTTP_SESSION_VARS["tri_".$nom_rep] = serialize($tri_par_defaut);
			$GLOBALS["tri"] = $tri_par_defaut;
			session_register ("tri_".$nom_rep); 
		}
		else {
			// il faut prendre l'ancien tri et le remettre dans le circuit
			$GLOBALS["tri"] = unserialize($HTTP_SESSION_VARS["tri_".$nom_rep]);
			//$GLOBALS["tri"] = unserialize($GLOBALS["tri_".$nom_rep]);
		}
		
	}

 }
 ?>