<?
require "libphp/HTML.inc";

echo html_message (LOGICIEL_NOM." derni�re version: ".LOGICIEL_VERSION." (".LOGICIEL_DATE.")", "information");

echo html_message ("Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence Publique G�n�rale GNU publi�e par la Free Software Foundation (version 2 ou bien toute autre version ult?rieure choisie par vous).", "paragraphe");

echo html_message ("Ce programme est distribu� car potentiellement utile, mais SANS AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de commercialisation ou d'adaptation dans un but sp�cifique. Reportez-vous � la Licence Publique G�n�rale GNU pour plus de d�tails.","paragraphe");

echo html_message ("Vous devez avoir re�u une copie de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, Etats-Unis.", "paragraphe");

echo html_message ("Les sources en HTML/PHP des formulaires, et la structure de la base de donn�es de ".LOGICIEL_NOM."(MySql) sont disponibles sur demande �: Olivier.Langella@pge.cnrs-gif.fr", "paragraphe");

echo html_message ("Auteurs", "information");

echo html_message ("Conception, programmation: Olivier Langella <br/>Olivier.Langella@pge.cnrs-gif.fr", "paragraphe");


?>
