<?
                          
/***************************************************************************
                      gestion_ip.inc  -
           biblioth�que de fonctions pour manipuler des num�ros IP:
           stockage en 000.000.000.000 (ipsql), affichage en enelevant les 0 (ipdisplay)
                             -------------------
    begin                :  30/09/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
if (!isset($module_ip)){

	$module_ip=1;
	
	function ipsql2ip ($numeroip){
		$numeroip = str_replace(".0",".",$numeroip);
		return (str_replace(".0",".",$numeroip)); //�liminer les z�ros
	
	}

	function chaine2ip($chaine, $prefixe="") {
		while (substr_count($chaine, ".") < 3) {
			$chaine .= ".";
		}
		ereg("([0-9]{0,3}).([0-9]{0,3}).([0-9]{0,3}).([0-9]{0,3})", $chaine, $regs );
		
		if ($prefixe!="") {
			while (substr_count($prefixe, ".") < 3) {
				$prefixe .= ".";
			}
			ereg("([0-9]{1,3}).([0-9]{0,3}).([0-9]{0,3}).([0-9]{0,3})", $prefixe, $regs_prefixe );
			
			if (($regs[4]=="") &&($regs_prefixe[1] != "")) {
				$regs[4] = $regs[3];
				$regs[3] = $regs[2];
				$regs[2] = $regs[1];
				$regs[1] = $regs_prefixe[1];
			}
			if (($regs[4]=="") &&($regs_prefixe[2] != "")) {
				$regs[4] = $regs[3];
				$regs[3] = $regs[2];
				$regs[2] = $regs_prefixe[2];
			}
			if (($regs[4]=="") &&($regs_prefixe[3] != "")) {
				$regs[4] = $regs[3];
				$regs[3] = $regs_prefixe[3];
			}
			
		}
		return 	"$regs[1].$regs[2].$regs[3].$regs[4]";
	}
  
	function ip2ipsql($numeroip){
		
		$numeroip=str_replace(" ","",$numeroip); //�liminer les espaces

		while (substr_count($numeroip, ".") < 3) {
			$numeroip.= ".";
		}
									
					
		ereg("([0-9]{1,3}).([0-9]{0,3}).([0-9]{0,3}).([0-9]{0,3})", $numeroip, $regs );

		
		for ($i=1; $i < 5; $i++) {
			if (isset($regs[$i])) {
				while (strlen($regs[$i]) < 3) {
					$regs[$i] = "0".$regs[$i];
				}
			}
			else $regs[$i] = "000";
		}
			
		return "$regs[1].$regs[2].$regs[3].$regs[4]";
	}



}
?>
