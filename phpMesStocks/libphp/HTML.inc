<?
                          
/***************************************************************************
                      HTML.inc  -
           production de code HTML (ancres, paragraphes...)
           
                             -------------------
    begin                :  31/7/2002
    copyright            : (C) 2002 by Yves Rigaud
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 // modifi� le 12/9/2002 par Olivier Langella pour enlever toute mise en page HTML
 // et ne se servir exclusivement que des classes et des feuilles de styles

//modifier le 08/08/2002 par Valerie Cantonny

if (!isset($module_html)){
  $module_html = 1;

  function ancre ($url, $libelle, $classe=-1){
    $url = str_replace("&", "&amp;", $url);
    $optionClasse = "";
    if ($classe != -1) $optionClasse = " class=\"$classe\"";
    return "<a href=\"".$url."\"".$optionClasse.">".$libelle."</a>";
  }
  
  function ancre_nom ($nom){
    return "<a id=\"$nom\"></a>";
  }
  
  function image ($url, $alt, $classe=-1, $title=""){
    //$alt => obligatoire pour le XHTML
    $attrclasse ="";
    if ($classe!=-1) $attrclasse ="class='$classe'";
    
    return "<img src=\"".$url."\" ".$attrclasse." alt=\"".$alt."\" title=\"".$title."\"/>";
  }
  
  function html_message ($ligne, $classe = -1){
    if ( $classe == "") $classe = -1;
    if ( $classe == -1) return ("<div>".$ligne."</div>");
    else return ("<div class='".$classe."'>".$ligne."</div>");
  }
	
  function html_confirmation ($message,$url_oui,$url_non){
    $tmp_message=$message;
    $tmp_message.=html_message(ancre ($url_oui,"oui"));
    $tmp_message.=html_message(ancre ($url_non,"non"));
    echo html_message($tmp_message,"confirmation");
    
  }	
  
}
?>