<td valign="top" align="left" > 

<table> 
<tr> 
<td align="left"> 
<h3>
<?
/***************************************************************************
                          chgtmotdepasse.inc  -  point d'entr�e changement de mot de passe
                             -------------------
    begin                : 21/01/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
	//include "libphp/phpmonlabo_tab.inc";
	
	$affform = 0;
		
	//include "fonctions_maj.inc";
	if ($action == "") $action = "liste";

	if ($cIdUtilisateur != $IdUtilisateur) {
 		echo "$cIdentifiant n'est pas autoris� � modifier les informations concernant cette personne";
	}
	else if ($action == "enregistrer") {
	//echo " $AdminArticles";		
		if ($IdUtilisateur !=0) {	
			// => cryptage
			//echo "$motdepasse1 et $motdepasse2";
			if ($anc_motdepasse != "") $anc_motdepasse = crypt($anc_motdepasse, $graindesel);
			if ($motdepasse1 != "") {
				$motdepasse1 = crypt($motdepasse1, $graindesel);
				$motdepasse2 = crypt($motdepasse2, $graindesel);
			}
			
			//echo "$motdepasse1 et $motdepasse2";
			// => authentification
			$requeteA = "SELECT IdUtilisateur FROM Utilisateurs";
			$requeteA .= " WHERE IdUtilisateur = '$IdUtilisateur'";
			$requeteA .= " AND UMotdepasse = '$anc_motdepasse'";
			
			$resultat = mysql_query($requeteA, $dbh) or die ("requete motdepasse non valide");
			
			// => mise � jour du mot de passe
			 
			if ($motdepasse1 != $motdepasse2) {
				echo "Erreur de saisie, recommencez s'il vous pla�t:<br/>";
				$affform = 1;
			}
			else if ($row = mysql_fetch_object($resultat)) {
				$requeteP = "UPDATE Utilisateurs SET ";
				$requeteP .= " UMotdepasse = '$motdepasse1'";
				$requeteP .= " WHERE IdUtilisateur = '$IdUtilisateur'";
			
				$resultat = mysql_query($requeteP, $dbh) or die ("requete motdepasse non valide");
				
				echo "Votre nouveau mot de passe a �t� enregistr�<br/>";
				
			}
			else {
				echo "Erreur de saisie de votre ancien mot de passe, recommencez s'il vous pla�t:<br/>";
				$affform = 1;
			}
		}
		else echo "Erreur<br/>";
		
		//date_maj_seminaires($dbh, $IdSeminaire);

		$action = "liste";
	}
	if (($action == "modif") &&  ($IdUtilisateur != 0)) {
		echo "Modification du mot de passe de $cIdentifiant";
		$affform = 1;
	}
?>
</h3>
</td>
</tr>
</table>
<table border="1">
<?  
	if ($affform == 1) {
		include "chgtmotdepasse/form_chgtmotdepasse.inc";
	//	include "pageperso/liste_articlespageperso.inc";		
	}
?>
 
</table> 
</td>