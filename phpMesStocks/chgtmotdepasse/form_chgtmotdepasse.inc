
<?
/***************************************************************************
                          form_chgtmotdepasse.inc  -  formulaire de modification du mot de passe
                             -------------------
    begin                : 21/01/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
//IdPersonne, Identifiant, Admin, AdminEquipe, Nom, Prenom, IdCategorie, IdEquipe, ONpage_perso, Photo, Telephone, Courriel, MotDePasse, Date_maj, t_org_fr, t_org_en

//	include "libphp/parsepge.inc";
	include "libphp/objet_formulaire.inc";
	
	class FormulaireMotdepasse extends Formulaire {
		
		var $IdUtilisateur;

		function FormulaireMotdepasse($IdUtilisateur, $action, $nom, $titre_bouton) {
			//constructeur
			$this->Formulaire($action, $nom, $titre_bouton);
			
			$this->IdUtilisateur = $IdUtilisateur;
		}
		
		function fabrique_corps() {
			$this->champ_hidden("edition", "chgtmotdepasse");
			$this->champ_hidden("action", "enregistrer");
			$this->champ_hidden("IdUtilisateur", $this->IdUtilisateur);
						

			$this->formulaire .= "<p>";
			$this->champ_motdepasse("Ancien mot de passe :", "anc_motdepasse", "", 20);
			$this->formulaire .= "</p>";
			$this->formulaire .= "<p>";
			$this->champ_motdepasse("Nouveau mot de passe :", "motdepasse1", "", 20);
			$this->formulaire .= "</p>";
			$this->formulaire .= "<p>";
			$this->champ_motdepasse("Confirmation :", "motdepasse2", "", 20);
			$this->formulaire .= "</p>";

		}
	}

	$form = new FormulaireMotdepasse($cIdUtilisateur, "index.php","chgtmotdepasse","Changer");
	
	$form->echo_formulaire();


?>
