<?
/***************************************************************************
                          objet_listeproduits.inc  -  objet permettant d'afficher la liste des produits
                             -------------------
    begin                : 03/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 // derni�re modification : 
 
//include "libphp/objet_tableau.inc";

	//include "../../phpMyRezo/parametres/personnalisation.inc";
	
	class ListeProduits extends ListeResultat {		

		function ListeProduits($resultat, $nom) {
			//constructeur
			$this->ListeResultat($resultat, $nom);
		}

		function afficher_titres() {
			//� impl�menter dans les objets d�riv�s
			$this->tabliste->ajouter_case("Produit");
			$this->tabliste->ajouter_case("Stocks");
			$this->tabliste->ajouter_case("Notes");
			//$this->tabliste->ajouter_case("Equipe");
			//$this->tabliste->ajouter_case("Poste");
			$this->tabliste->afficher_ligne_titres();
		}

		function afficher_ligne($row) {

			//� impl�menter dans les objets d�riv�s
						$IdEquipe = $row->IdEquipe;
			$this->tabliste->ajouter_case_lien($row->NomProduit, "index.php?edition=mouvements&IdProduit=$row->IdProduit&action=liste");
			$this->tabliste->ajouter_case("$row->Qrestante $row->Conditionnement");
			$this->tabliste->ajouter_case($row->ProduitNotes);

			$this->ajouter_case_modifier("Modifier","index.php?edition=produits&IdProduit=$row->IdProduit&action=modif&");

			$this->ajouter_case_supprimer("Supprimer","index.php?edition=produits&IdProduit=$row->IdProduit&action=suppr&");
		
			$this->tabliste->afficher_ligne();
		}
	}

?>