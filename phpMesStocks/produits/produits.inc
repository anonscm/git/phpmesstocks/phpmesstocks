<?
/***************************************************************************
                          produits.inc  -  point d'entr�e dans la gestion des produits
                             -------------------
    begin                :  03/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
	//include "libphp/objet_fomulaire.inc";
	include "libphp/phpmesstocks_tab.inc";
	include "produits/req_produits.inc";

//Affichage des personnes
	$requete = "SELECT t1.IdProduit, t1.NomProduit, t1.Conditionnement, t1.Qrestante, t1.ProduitNotes, t2.IdUtilisateur FROM Produits AS t1";
	$requete .= " NATURAL LEFT JOIN PrdtUtilisateurs AS t2";
	//$requete .= " NATURAL LEFT JOIN Mouvements AS t3";

	$wheredejacommence = false;
	if ($creqNomProduit != "") {
		if ($wheredejacommence) $requete = "$requete AND ";
		else {
			$requete = "$requete WHERE ";
			$wheredejacommence = true;
		}
		$requete .= " t1.NomProduit LIKE '%$creqNomProduit%'";
	}
	if ($creqIdUtilisateur != "") {
		if ($wheredejacommence) $requete = "$requete AND ";
		else {
			$requete = "$requete WHERE ";
			$wheredejacommence = true;
		}
		$requete .= " t2.IdUtilisateur = '$creqIdUtilisateur'";
	}
	if ($creqProduitNotes != "") {
		if ($wheredejacommence) $requete = "$requete AND ";
		else {
			$requete = "$requete WHERE ";
			$wheredejacommence = true;
		}
		$requete .= " t1.ProduitNotes LIKE '%$creqProduitNotes%'";
	}
	
	
	$requete .= " ORDER BY t1.NomProduit";

	include "libphp/objet_tableau.inc";
	include "libphp/objet_listeresultat.inc";
	include "produits/objet_listeproduits.inc";
		
	$resultat = mysql_query($requete, $dbh) or die ("requete non valide");
		
	$listeresultat = new ListeProduits($resultat,"listedesequipes");
	if ($cAutModifProduit == 1) $listeresultat->set_ajouter("index.php?edition=produits&action=ajout", "ajouter un produit");
	if ($cAutModifProduit == 1) $listeresultat->set_modifier();
	if ($cAutModifProduit == 1) $listeresultat->set_supprimer();

	$nb_produits = mysql_affected_rows();
	echo "<H1>$nb_produits produits</H1>";
	$listeresultat->afficher_liste();
	
?>
