<?
/***************************************************************************
                          req_produits.inc  -  formulaire de requ�te d'un article
                             -------------------
    begin                : 08/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

	include "libphp/objet_formulaire.inc";
	class FormulaireReqProduits extends Formulaire {
		var $reqIdSalle;
		var $reqNomProduit;
		var $reqProduitNotes;
		
		function FormulaireReqArticles($action, $nom, $titre_bouton) {
			//constructeur
			$this->Formulaire($action, $nom, $titre_bouton);
		}
		
		function fabrique_corps() {
			$this->champ_hidden("edition", "produits");
			$this->champ_hidden("action", "liste");
			$this->champ_liste_deroulante_nulle("", "reqIdUtilisateur", $GLOBALS["tabUtilisateurs"], $GLOBALS["creqIdUtilisateur"]);
			//$this->champ_liste_deroulante_nulle("", "reqIdSalle", $GLOBALS["tabSalles"], $GLOBALS["creqIdSalle"]);
			$this->champ_texte("produit :", "reqNomProduit", $GLOBALS["creqNomProduit"], 10);
			$this->champ_texte("notes :", "reqProduitNotes", $GLOBALS["creqProduitNotes"], 10);
		}
		
	}

	// sauvegarde des �l�ments de la requ�te
	// $reqArtListe, $reqArtAnnee, $reqArtAuteur
	if (session_is_registered("creqIdUtilisateur") == FALSE) session_register("creqIdUtilisateur");
	if (session_is_registered("creqIdSalle") == FALSE) session_register("creqIdSalle");
	if (session_is_registered("creqNomProduit") == FALSE) session_register("creqNomProduit");
	if (session_is_registered("creqProduitNotes") == FALSE) session_register("creqProduitNotes");
	if (isset ($reqIdUtilisateur)) $creqIdUtilisateur = $reqIdUtilisateur;
	if (isset ($reqIdSalle)) $creqIdSalle  = $reqIdSalle;
	if (isset ($reqNomProduit)) $creqNomProduit = $reqNomProduit;
	if (isset ($reqProduitNotes)) $creqProduitNotes = $reqProduitNotes;
		
		
	$form = new FormulaireReqProduits("index.php","reqproduit","Rechercher");
	
	$form->echo_formulaire();


?>
