<p><a href="index.php?edition=mouvements&action=liste">retour � la liste des mouvements</a></p>
<?
/***************************************************************************
                          form_mouvement.inc  -  formulaire de modification/ajout d'un mouvement
                             -------------------
    begin                :  09/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//	include "objet_formulaire.inc";
//	include "phpmonseminaire_tab.inc";

	class FormulaireMouvement extends Formulaire {
		var $row;
		
		function FormulaireMouvement($row, $action, $nom, $titre_bouton) {
			//constructeur
			$this->Formulaire($action, $nom, $titre_bouton);
			$this->row = $row;
		}
		
		function fabrique_corps() {
			$this->champ_hidden("edition", "mouvements");
			$this->champ_hidden("action", "enregistrer");
			$this->champ_hidden("ancienne_quantite", $this->row->MvtQuantite);
			$this->champ_hidden("ancien_typemvt", $this->row->MvtType);
			$this->champ_hidden("IdMouvement", $this->row->IdMouvement);
			$this->champ_hidden("IdUtilisateur", $GLOBALS["cIdUtilisateur"]);
			$this->champ_hidden("IdProduit", $GLOBALS["cIdProduit"]);
			if ($this->row->IdMouvement > 0) { //modification
				$date = $this->row->MvtDate;
			}
			else { //ajout
				//$date = date( "Ymd His", time() );
				$date = date( "Y-m-d", time() );
			}
			$this->formulaire .= "<p><b>";
			$this->champ_liste_deroulante("", "MvtType", $GLOBALS["tabMvtTypes"], $this->row->MvtType);
			$this->formulaire .= "</b></p>";
			$this->formulaire .= "<p>";
			$this->champ_texte("Quantit� :", "MvtQuantite", $this->row->MvtQuantite,10);
			$this->formulaire .= "</p>";
			$this->formulaire .= "<p>";
			$this->champ_date("date : ", "MvtDate", $date);
			$this->formulaire .= "</p>";
			$this->formulaire .= "<p>";
			$this->champ_liste_deroulante("salle : ", "IdSalle", $GLOBALS["tabSalles"], $this->row->IdSalle);
			$this->formulaire .= "</p>";
			
			$this->formulaire .= "<p>";
			$this->champ_texte("Notes :", "MvtNotes", $this->row->MvtNotes,30);
			$this->formulaire .= "</p>";
		}	
	}

	$form = new FormulaireMouvement($row, "index.php","mouvement","Enregistrer");
	
	if (session_is_registered("cAutMajQuantite") == FALSE) session_register("cAutMajQuantite");
	$cAutMajQuantite = true;
	
	$form->echo_formulaire();


?>
