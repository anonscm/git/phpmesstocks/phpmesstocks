<?
/***************************************************************************
                          sql_mouvements.inc  -  enregistrement, mise � jour des mouvements
                             -------------------
    begin                :  10/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 
 	if ($cAutModifProduit == 1) { //autorisation
		if (($action == "suppr") && ($IdMouvement > 0) && ($MvtQuantite != "")) {
			//supprimer un mouvement
			// => mettre � jour la Qrestante
			// => supprimer le mouvement
			$requete = "SELECT IdMouvement FROM Mouvements WHERE IdMouvement='$IdMouvement'";
			$resultat = mysql_query($requete) or die ("requete invalide");
			if (mysql_fetch_row($resultat)) {
				$requete = "DELETE FROM Mouvements WHERE IdMouvement='$IdMouvement'";
				mysql_query($requete) or die ("requete invalide");
				// mise a jour de la quantit� restante
				$requete = "UPDATE Produits Set Qrestante=Qrestante+'$MvtQuantite' WHERE IdProduit = '$cIdProduit'";
				mysql_query($requete) or die ("requete incorrecte");
			}


			$action="liste";
		}
		
		if ($action=="enregistrer") {
			if ($cAutMajQuantite == false) {
				// pas de mise � jour de la quantit� restante
			}
			else { //mise � jour de la quantit� restante
				$cAutMajQuantite = false;
				$quantite = 0;
				if ($IdMouvement > 0) { //modification d'un mouvement existant
					$requete = "UPDATE Mouvements SET";
					$requete .= " IdSalle='$IdSalle', IdUtilisateur='$IdUtilisateur', MvtType='$MvtType', MvtDate='$MvtDateannee-$MvtDatemois-$MvtDatejour', MvtQuantite='$MvtQuantite', MvtNotes = '$MvtNotes'";
					$requete .= " WHERE IdMouvement = '$IdMouvement'";
					
					if ($ancien_typemvt == 0) $quantite -= $ancienne_quantite;
					else $quantite += $ancienne_quantite;
				}
				else { //nouveau
					//cr�ation d'un mouvement
					$requete = "INSERT INTO Mouvements (IdProduit, IdSalle, IdUtilisateur, MvtType, MvtDate, MvtQuantite, MvtNotes)";
					$requete .= " VALUES ('$IdProduit', '$IdSalle', '$IdUtilisateur', '$MvtType', '$MvtDateannee-$MvtDatemois-$MvtDatejour', '$MvtQuantite', '$MvtNotes')";
				}
				mysql_query($requete) or die ("requete 1 non valide");
				if ($MvtType == 0) $quantite += $MvtQuantite;
				else $quantite -= $MvtQuantite;
				
				$requete = "UPDATE Produits Set Qrestante=Qrestante+'$quantite' WHERE IdProduit = '$cIdProduit'";
				mysql_query($requete) or die ("requete incorrecte");
			}
			$action="liste";
		}
	}
?>