<?
/***************************************************************************
                          mouvements.inc  -  point d'entr�e dans la gestion des mouvements pour un produit
                             -------------------
    begin                :  03/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 	include "libphp/objet_formulaire.inc";
 	include "libphp/phpmesstocks_tab.inc";
 
	if ($action == "") $action="liste";
	
	
	$requete = "SELECT t2.IdMouvement, t2.IdProduit, t1.NomProduit, t1.Conditionnement, t2.MvtDate, t2.IdSalle, t3.NomSalle, t2.MvtQuantite, t2.MvtType, t2.MvtNotes FROM Produits AS t1 NATURAL LEFT JOIN Mouvements AS t2 NATURAL LEFT OUTER JOIN Salles AS t3";
	if ($action=="liste") {
		$requete .= " WHERE t2.IdProduit='$cIdProduit'";
	
		if ($reqIdSalle != 0) {
			$requete .= " AND t2.IdSalle='$reqIdSalle'";
		}
		$requete .= " ORDER BY t2.MvtDate DESC";
	
		include "libphp/objet_tableau.inc";
		include "libphp/objet_listeresultat.inc";
		include "mouvements/objet_listemouvements.inc";
		
		$resultat = mysql_query($requete, $dbh) or die ("requete non valide");
			
		$listeresultat = new ListeMouvements($resultat,"listedesmouvements");
		if ($cAutModifProduit == 1) $listeresultat->set_ajouter("index.php?edition=mouvements&action=ajout&", "ajouter un mouvement");
		if ($cAutModifProduit == 1) $listeresultat->set_modifier();
		if ($cAutModifProduit == 1) $listeresultat->set_supprimer();

		$nb_mouvements = mysql_affected_rows();
	
		echo "<p>$nb_mouvements mouvement(s)</p>";
		$listeresultat->afficher_liste();
	}
	
	else if ($action=="modif") { //affichage du fomulaire d'un mouvement
		if  ($IdMouvement != 0) {
			$requete .= " WHERE t2.IdMouvement='$IdMouvement'";
			$resultat = mysql_query($requete, $dbh) or die ("requete non valide");
	
			if ($row = mysql_fetch_object($resultat)) {
				include "mouvements/form_mouvement.inc";
			}
		}
	}
	else if ($action == "ajout") {
		include "mouvements/form_mouvement.inc";
	}
	
?>
