<?
/***************************************************************************
                          objet_listemouvements.inc  -  objet permettant d'afficher la liste des mouvements d'un produit
                             -------------------
    begin                : 03/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 // derni�re modification : 
 
//include "libphp/objet_tableau.inc";

	//include "../../phpMyRezo/parametres/personnalisation.inc";
	
	class ListeMouvements extends ListeResultat {
		var $total_entree;
		var $total_sortie;

		function ListeMouvements($resultat, $nom) {
			//constructeur
			$this->ListeResultat($resultat, $nom);
		}

		function afficher_titres() {
			//� impl�menter dans les objets d�riv�s

			$this->tabliste->ajouter_case("Date");
			$this->tabliste->ajouter_case("Produit");
			$this->tabliste->ajouter_case("Notes");
			$this->tabliste->ajouter_case("Quantit�<br/>Sortie");
			$this->tabliste->ajouter_case("Quantit�<br/>Entr�e");
			$this->tabliste->ajouter_case("Salle de <br/>manip");
			$this->tabliste->afficher_ligne_titres();
		}

		function afficher_ligne($row) {

			//� impl�menter dans les objets d�riv�s
	
			$this->tabliste->ajouter_case(aff_date($row->MvtDate,"d/m/Y"));
			$this->tabliste->ajouter_case($row->NomProduit);
			$this->tabliste->ajouter_case($row->MvtNotes);
			if ($row->MvtType == 1) { //sortie
				$this->tabliste->ajouter_case($row->MvtQuantite);
				$this->tabliste->ajouter_case("");
				$this->total_sortie += $row->MvtQuantite;
			}
			else { //entr�e
				$this->tabliste->ajouter_case("");
				$this->tabliste->ajouter_case($row->MvtQuantite);
				$this->total_entree += $row->MvtQuantite;
			}

			//$this->tabliste->ajouter_case("$row->Qrestante $row->Conditionnement(s)");
			$this->tabliste->ajouter_case($row->NomSalle);

			$this->ajouter_case_modifier("Modifier","index.php?edition=mouvements&IdMouvement=$row->IdMouvement&action=modif&");

			if ($row->MvtType == 1) $this->ajouter_case_supprimer("Supprimer","index.php?edition=mouvements&IdMouvement=$row->IdMouvement&MvtQuantite=$row->MvtQuantite&action=suppr&");
			else $this->ajouter_case_supprimer("Supprimer","index.php?edition=mouvements&IdMouvement=$row->IdMouvement&MvtQuantite=-$row->MvtQuantite&action=suppr&");
		
			$this->tabliste->afficher_ligne();
		}
	}

?>