<?
/***************************************************************************
                          phpesstocks_tab.inc  -  tableaux utilis�s dans "phpMesStocks"
                             -------------------
    begin                : 08/04/2001
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// tableaux utilis�s dans "phpMesStocks"

//$tabLangues["en"]="Anglais";
//$tabLangues["fr"]="Fran�ais";
$tabMvtTypes[0]="Entr�e";
$tabMvtTypes[1]="Sortie";


//--------- Categories
$requete = "SELECT IdSalle, NomSalle";
$requete .= " FROM Salles";
$resultat = mysql_query($requete, $dbh) or die ("requete Salles non valide");

while($lignetab = mysql_fetch_row($resultat)) {
	$tabSalles[$lignetab[0]]=$lignetab[1];
}

$requete = "SELECT IdUtilisateur, UIdentifiant";
$requete .= " FROM Utilisateurs";
$resultat = mysql_query($requete, $dbh) or die ("requete Utilisateurs non valide");

while($lignetab = mysql_fetch_row($resultat)) {
	$tabUtilisateurs[$lignetab[0]]=$lignetab[1];
}

?>