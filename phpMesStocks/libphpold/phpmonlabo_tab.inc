<?
/***************************************************************************
                          phpmonlabo_tab.inc  -  tableaux utilis�s dans "phpMonLabo"
                             -------------------
    begin                : 29/11/2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// tableaux utilis�s dans "phpMonLabo"

$tabLangues["en"]="Anglais";
$tabLangues["fr"]="Fran�ais";


//--------- Categories
$requete = "SELECT IdCategorie, Nom";
$requete .= " FROM Categories";
$resultat = mysql_query($requete, $dbh) or die ("requete Categories non valide");

while($lignetab = mysql_fetch_row($resultat)) {
	$tabCategories[$lignetab[0]]=$lignetab[1];
}

$requete = "SELECT IdEquipe, Nom";
$requete .= " FROM Equipes";
$resultat = mysql_query($requete, $dbh) or die ("requete Equipes non valide");

while($lignetab = mysql_fetch_row($resultat)) {
	$tabEquipes[$lignetab[0]]=$lignetab[1];
}

?>