<?
/***************************************************************************
                          objet_formulaire.inc  -  fobjet permettant la cr�ation de formulaires HTML
                             -------------------
    begin                : 19/11/2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 // derni�re modification : 30/11/2001 (phpMonLabo)
 //21/11/2001

class Formulaire {
	var $titre_bouton;
	var $action;
	var $nom;
	var $formulaire;

	function Formulaire($action, $nom, $titre_bouton) {
		if ($titre_bouton == "") $this->titre_bouton = "Valider";
		else $this->titre_bouton = $titre_bouton;
		$this->action = $action;
		$this->nom = $nom;
	}
	
	function echo_formulaire() {
		$this->formulaire = "<form action=\"$this->action\" method=\"post\" name=\"$this->nom\">";
		$this->fabrique_corps();
		$this->formulaire .= "<input type=\"submit\" value=\"$this->titre_bouton\"></input></form>";
		echo $this->formulaire;
	}
	function fabrique_corps() {
		//� impl�menter dans un objet d�riv�
		echo "Il faut impl�menter la fonction fabrique_corps...";
	}
	function champ_hidden($nom, $valeur) {
		$this->formulaire .= "<input type=\"hidden\" name=\"$nom\" value=\"$valeur\"></input>";
	}
	function champ_texte($titre,$nom, $valeur, $taille) {
		$this->formulaire .= "<b>$titre</b><input name=\"$nom\" size=\"$taille\" type=\"text\" value=\"$valeur\"></input>";
	}
	function champ_motdepasse($titre,$nom, $valeur, $taille) {
		$this->formulaire .= "<b>$titre</b><input name=\"$nom\" size=\"$taille\" type=\"password\" value=\"$valeur\"></input>";
	}
	function champ_texte_etendu($titre,$nom, $valeur, $nblignes,$nbcol) {
		if ($nbcol == 0) $nbcol = 40;
		$this->formulaire .= "<b>$titre</b><br>";
		$this->formulaire .= "<textarea name=\"$nom\" rows=$nblignes cols=$nbcol>";
		$this->formulaire .= "$valeur</textarea>";
	}
	
	function champ_liste_deroulante($titre,$nom, $valeurs, $selectionne) {
		$this->formulaire .= "<b>$titre</b>";
		$this->formulaire .= "<select name=\"$nom\">";
		foreach ($valeurs as $cle => $valeur){
			if ($selectionne == $cle) $this->formulaire .= "<option value=\"$cle\" selected>$valeur</option>";
			else $this->formulaire .= "<option value=\"$cle\">$valeur</option>";
		}
		$this->formulaire .= "</select>";
	}
	
 	function champ_liste_deroulante_nulle($titre,$nom, $valeurs, $selectionne) {
		$this->formulaire .= "<b>$titre</b>";
		$this->formulaire .= "<select name=\"$nom\">";
		if ($selectionne == "") $this->formulaire .= "<option value=\"\" selected>----</option>";
		else $this->formulaire .= "<option value=\"$cle\">----</option>";
		foreach ($valeurs as $cle => $valeur){
			if ($selectionne == $cle) $this->formulaire .= "<option value=\"$cle\" selected>$valeur</option>";
			else $this->formulaire .= "<option value=\"$cle\">$valeur</option>";
		}
		$this->formulaire .= "</select>";
	}
	
//<input TYPE="RADIO" NAME="Option1"  VALUE="True">Installed<input TYPE="RADIO" NAME="Option1" CHECKED VALUE="False">Not Installed

	function champ_case($titre,$nom, $valeur) {
		$this->formulaire .= "<b>$titre</b>";
		 
		if ($valeur == 0) $this->formulaire .= "<input type=\"checkbox\" name=\"$nom\" value=\"1\"></input>";
		else $this->formulaire .= "<input type=\"checkbox\" name=\"$nom\" checked value=\"1\"></input>";
	}

	function champ_radio($titre,$nom, $valeurs, $selectionne) {
		$this->formulaire .= "<b>$titre</b>";
		foreach ($valeurs as $cle => $valeur){
			if ($selectionne == $cle) $this->formulaire .= "<input type=\"RADIO\" name=\"$nom\" value=\"$cle\" checked>$valeur</input>";
			else $this->formulaire .=  "<input type=\"RADIO\" name=\"$nom\" value=\"$cle\">$valeur</input>";
		}
	}

	function champ_date($titre,$nom, $valeur) {
		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $valeur, $regs );
		$this->formulaire .= "<b>$titre</b>";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "jour\" size=\"2\" type=\"text\" value=\"$regs[3]\"></input>/";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "mois\" size=\"2\" type=\"text\" value=\"$regs[2]\"></input>/";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "annee\" size=\"4\" type=\"text\" value=\"$regs[1]\"></input>";
	}

	function champ_date_heure($titre,$nom, $valeur) {
		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})", $valeur, $regs );
		$this->formulaire .= "<b>$titre</b>";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "jour\" size=\"2\" type=\"text\" value=\"$regs[3]\"></input>/";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "mois\" size=\"2\" type=\"text\" value=\"$regs[2]\"></input>/";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "annee\" size=\"4\" type=\"text\" value=\"$regs[1]\"></input>&nbsp;";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "heure\" size=\"2\" type=\"text\" value=\"$regs[4]\"></input>:";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "minute\" size=\"2\" type=\"text\" value=\"$regs[5]\"></input>:";
		$this->formulaire .= "<input name=\"$nom";
		$this->formulaire .= "seconde\" size=\"2\" type=\"text\" value=\"$regs[6]\"></input>";
	}

}
?>