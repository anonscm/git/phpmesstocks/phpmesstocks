


<?

class Article {
	var $Identifiant;
	var $Auteurs;
	var $Date;
	var $Titre;
	var $Journal;
	var $Volume;
	var $Pages;
	var $Numero; // (issue)

	function fct_affHTML_article_PGE () {
		
		$this->Auteurs = strtr("$this->Auteurs","�������","eeaceeo");		
		$this->Auteurs = strtoupper($this->Auteurs);
	
		$tabAuteurs = explode ("|",$this->Auteurs);

		$format_article = implode("<, <",$tabAuteurs);

		if (sizeof ($tabAuteurs) > 1) $format_article = substr_replace($format_article, " and ",strrpos($format_article,"<, <")-3,4);

		$format_article = str_replace("<, <",", ",$format_article);

		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $this->Date, $regs );
		//$Datefr = "$regs[3]/$regs[2]/$regs[1]";
		$Annee = $regs[1];

		$format_article .= " <B>$Annee</B> $this->Titre <I>$this->Journal</I>";
		if ($this->Volume != "") {
			$format_article .= " $this->Volume";
			if ($this->Numero > 0) $format_article .= "($this->Numero)";
			$format_article .= ": ";
		}
		if ($this->Pages != "") $format_article .= $this->Pages;

		return ($format_article);
	}

	function fct_aff_refmanager() {
		$format_article = "";

		if ($this->Volume != "") $format_article = "$format_article%V $this->Volume<BR>";
		if ($this->Titre != "") $format_article = "$format_article%T $this->Titre<BR>";
		if ($this->Pages != "") $format_article = "$format_article%P $this->Pages<BR>";
		if ($this->Numero > 0) $format_article .= "%N $this->Numero<BR>";
		if ($this->Journal != "") $format_article = "$format_article%J $this->Journal<BR>";

		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $this->Date, $regs );
		//$Datefr = "$regs[3]/$regs[2]/$regs[1]";

		$format_article = "$format_article%D $regs[3]/$regs[2]/$regs[1]<BR>";

		$tabAuteurs = explode ("|",$this->Auteurs);

		for ($i=0; $i < sizeof($tabAuteurs); $i++) {
			$format_article = "$format_article%A $tabAuteurs[$i]<BR>";
		}

		return ($format_article);
	}

	function fct_aff_medline() {
		$format_article = "UI  - 0<br>";

		$tabAuteurs = explode ("|",$this->Auteurs);

		for ($i=0; $i < sizeof($tabAuteurs); $i++) {
			$format_article .= "AU  - $tabAuteurs[$i]<br>";
		}
		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $this->Date, $regs );
		//$Datefr = "$regs[3]/$regs[2]/$regs[1]";

		$format_article .= "DP  - $regs[1]<br>";
		if ($this->Journal != "") $format_article .=  "TA  - $this->Journal<br>";

		if ($this->Volume != "") $format_article .= "VI  - $this->Volume<br>";

		//attention, il faut d�couper le titre pour le faire tenir sur une ligne de 80 colonnes --> pas encore fait
		if ($this->Titre != "") $format_article .= "TI  - $this->Titre<br>";


		if ($this->Pages != "") $format_article .= "PG  - $this->Pages<br>";
		if ($this->Numero > 0) $format_article .= "IP  - $this->Numero<br>";

		return ($format_article);
	}

	function fct_aff_bibtex() {
		$format_article = "@Article{";
		$tabAuteurs = explode ("|",$this->Auteurs);
		ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $this->Date, $regs );

		if ($this->Identifiant == '') {
			for ($i=0; $i < sizeof($tabAuteurs); $i++) {
				$this->Identifiant = $this->Identifiant.$tabAuteurs[$i][0];
			}		
			$this->Identifiant = $this->Identifiant.$regs[1];
			$this->Identifiant = $this->Identifiant.$this->Titre[0];
		}
 
		$format_article = "$format_article $this->Identifiant,<BR>";

		$format_article = "$format_article  Author = {{";
//		$auteurs = implode("} and {",$tabAuteurs);
		$format_article = $format_article.implode("} and {",$tabAuteurs)."}},<BR>";
		if ($this->Titre != "") $format_article = "$format_article  Title = \{$this->Titre},<BR>";
		if ($this->Journal != "") $format_article = "$format_article  Journal = \{$this->Journal},<BR>";
		if ($this->Volume != "") $format_article = "$format_article  Volume = \{$this->Volume},<BR>";
		if ($this->Numero > 0) $format_article .= "  Number = \{$this->Numero},<BR>";
		if ($this->Pages != "") $format_article = "$format_article  Pages = \{$this->Pages},<BR>";

		if ($regs[1] != "") $format_article = "$format_article  year = $regs[1],<BR>";

		$format_article = $format_article."}<BR><BR>";
		return ($format_article);
	}
}

/*
@Article{BRJLM97,
  Author         = {{Bruce Ranala} and {Joanna L. Mountain}},
  Title          = {Detecting immigration by using multilocus genotypes},
  Journal        = {Proc.Natl. Acad. Sci. USA},
  Volume         = {94},
  Pages          = {9197-9201},
  Note           = {August 1997},
  month          = {13 } # jun,
  year           = 1997,
}
*/

?>