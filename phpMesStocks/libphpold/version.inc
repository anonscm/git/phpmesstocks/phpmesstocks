<?
/***************************************************************************
                          version.inc  -  num�ro de version de phpMesStocks
                             -------------------
    begin                :  10/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 	$logiciel_nom = "phpMesStocks";
  	$logiciel_version = "0.3.1";
	$logiciel_date = "03/06/2002";

/*
	- correction de l'affichage des dates
	
  	$logiciel_version = "0.3";
	$logiciel_date = "23/04/2002";

	-d�marrage sur les produits de l'utilisateur
	
 	$logiciel_version = "0.2";
	$logiciel_date = "18/04/2002";

	- possibilit� d'afficher phpMesStocks sans les css 	

	premi�re version fonctionnelle:
 	- gestion des mouvements de produits par utilisateurs
 		
	$logiciel_version = "0.1";
	$logiciel_date = "03/04/2002";
*/
?>