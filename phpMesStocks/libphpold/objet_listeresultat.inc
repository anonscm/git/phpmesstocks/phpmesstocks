<?
/***************************************************************************
                          objet_listeresultat.inc  -  objet permettant la liste de r�sultats de requ�tes
                             -------------------
    begin                : 19/02/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 // derni�re modification : 22/04/2002 (phpMesStocks)
// 26/03/2002 (phpMonLabo)
// 13/03/2002 (phpMyRezo)
 
//include "objet_tableau.inc";
 
class ListeResultat {
	var $resultat;
	var $Omodifier;
	var $Oajouter;
	var $Osupprimer;
	var $Lajouter;
	var $tabliste;
	var $argretour;
	var $idprecedent; //pour �viter d'afficher plusieurs fois la m�me chose
	var $compteur; //pour �viter d'afficher plusieurs fois la m�me chose
	var $classe; //classe css
	var $titre_ajout;
	var $nb_lignes;

	function ListeResultat ($resultat, $nom, $classe="") {
		if ($classe == "") $this->classe = "liste_resultat";
		else $this->classe = $classe;
		echo "<div class=\"$this->classe\" align=\"center\">";
		$this->tabliste = new TableauHTML($nom, $this->classe);
		$this->resultat = $resultat;
		$this->Oajouter = 0;
		$this->Omodifier = 0;
		$this->Osupprimer = 0;
		$this->nb_lignes = mysql_affected_rows();
	}
	function set_ajouter ($Lajouter, $titre_ajout="") {
		$this->Oajouter = 1;
		$this->titre_ajout = $titre_ajout;
		$this->Lajouter = $Lajouter.$this->argretour;
	}
	function set_modifier () {
		$this->Omodifier = 1;
	}
	function set_supprimer () {
		$this->Osupprimer = 1;
	}
	
	function afficher_ajouter() {
		//� impl�menter dans les objets d�riv�s
		$this->Lajouter .= $this->argretour;
		if ($this->Oajouter == 1) echo "<div id=ajout><a href=\"$this->Lajouter\">$this->titre_ajout</a></div>";
		else echo "<!-- y'a un schmill quelque part $this->Oajouter  $this->Lajouter $this->titre_ajout-->";

	}

	function afficher_titres() {
		//� impl�menter dans les objets d�riv�s
	}

	function afficher_ligne($row) {
		//� impl�menter dans les objets d�riv�s

	}

	function ajouter_case_modifier($case, $lien , $couleur="", $attributs="") {
		if ($this->Omodifier == 1)
			$this->tabliste->ajouter_case_lien($case, $lien.$this->argretour, $couleur, $attributs);
		else 
			$this->tabliste->ajouter_case($case, $couleur, $attributs);
	}
	function ajouter_case_supprimer($case ,$lien , $couleur="", $attributs="") {
		
		if ($this->Osupprimer == 1)
			$this->tabliste->ajouter_case_lien($case,$lien.$this->argretour, $couleur, $attributs);
		else if ($case == "Supprimer") $this->tabliste->ajouter_case("");
		else
			$this->tabliste->ajouter_case($case, $couleur, $attributs);
	}
		
	function afficher_liste($idbase="") {
		$this->compteur = 0;
		$this->afficher_ajouter();
		$this->tabliste->ouvrir_tableau();
		//ligne de titres des colonnes:
		
		$this->afficher_titres();
		
 		while($row = mysql_fetch_object($this->resultat))
		{
			if (($idbase != "") && ($this->idprecedent == $row->$idbase)) {
				$this->tabliste->i--;
				if ($this->tabliste->i < 0) $this->tabliste->i = $this->tabliste->i_max;
			}
			$this->afficher_ligne($row);
			if ($idbase != "") $this->idprecedent = $row->$idbase;

		}
		$this->tabliste->fermer_tableau();
		$this->afficher_ajouter();
		echo "</div>";
	}

}
?>