<?
/***************************************************************************
                          objet_tableau.inc  -  fobjet permettant la cr�ation de tableaux HTML
                             -------------------
    begin                : 19/02/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 // derni�re modification : 13/03/2002 (phpMyRezo)
 
class TableauHTML {
	var $nom;
	var $ligne;
	var $classe_css;
	var $i;
	var $i_max;

	function TableauHTML ($nom, $classe_css="") {
		$this->i = 0;
		$this->i_max = 1;
		$this->nom = $nom;
		$this->classe_css = $classe_css;
		//$this->ligne = "<tr id=\"".$i."\">";
	}
	
	function ouvrir_tableau($attributs="") {
		echo "\n<!-- tableau cr�er par l'objet TableauHTML, libphp/objet_tableau.inc -->\n";
		echo "<table";
		if ($this->classe_css != "") echo " class=\"$this->classe_css\" ";
		if ($attributs != "") echo " $attributs";
		else echo " name=\"'$this->nom\">";
	}
	function fermer_tableau() {
		echo "</table>\n";
		
	}
	function ajouter_case($case, $id="", $attributs="") {
		$this->ligne .= "<td";
		if ($attributs != "") $this->ligne .= " $attributs ";
		if ($id != "") $this->ligne .= " id=\"$id\">$case</td>";
		else $this->ligne .= ">$case</td>";
	}
	function ajouter_case_lien($case ,$lien , $id="", $attributs="") {
		$this->ligne .= "<td ahref=\"$lien\"";
		if ($attributs != "") $this->ligne .= " $attributs ";
		if ($id != "") $this->ligne .= " id=\"$id\"><a href=\"$lien\">$case</a></td>";
		else $this->ligne .= "><a href=\"$lien\">$case</a></td>";
	}
	
	function afficher_ligne_titres() {
		echo "<tr id=\"titres\">$this->ligne</tr>";
		$this->ligne = "";
	}

	function afficher_ligne() {
		echo "<tr id=\"$this->i\">$this->ligne</tr>";
		$this->i++;
		if ($this->i > $this->i_max) $this->i = 0;
		$this->ligne = "";
	}

}
?>