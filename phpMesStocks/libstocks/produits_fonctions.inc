<?
if (!isset($fichier_produits_fonctions)){
	$fichier_produits_fonctions = 1;


/***************************************************************************
                          produits_fonctions.inc  - fonctions pour la gestion des produits
                             -------------------
    begin                :  21/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
	function cherche_IdProduit($nom) {
		// IdUtilisateur     UIdentifiant     UNom
		$requete = "SELECT  p.IdProduit  ,     p.NomProduit ";
		$requete .= " FROM Produits AS p";
		//$requete .= " NATURAL LEFT JOIN  PrdtUtilisateurs AS pu";
		$requete .= " WHERE p.NomProduit='$nom'";
		//$requete .= " ORDER BY NomProduit";
		$resultat = exec_requete_select ($requete, $GLOBALS["connexion"]) or die ("requete non valide");
	
		while($ligne = ligne_suivante ($resultat)) {
			return $ligne->IdProduit;
			//$tab_equipe[$ligne->IdProduit]=$ligne->NomProduit;
		}
		exit();
		//return ($tab_equipe);
	}

	function cherche_quantite($IdProduit) {
		// IdUtilisateur     UIdentifiant     UNom
		$requete = "SELECT  p.IdProduit  ,     p.Qrestante ";
		$requete .= " FROM Produits AS p";
		//$requete .= " NATURAL LEFT JOIN  PrdtUtilisateurs AS pu";
		$requete .= " WHERE p.IdProduit='$IdProduit'";
		//$requete .= " ORDER BY NomProduit";
		$resultat = exec_requete_select ($requete, $GLOBALS["connexion"]) or die ("requete non valide");
	
		while($ligne = ligne_suivante ($resultat)) {
			return $ligne->Qrestante;
			//$tab_equipe[$ligne->IdProduit]=$ligne->NomProduit;
		}
		exit();
		//return ($tab_equipe);
	}
	
	function html_etat_stock($IdProduit) {
		$requete = "SELECT  p.IdProduit  , p.NomProduit  ,    p.Qrestante, p.Conditionnement ";
		$requete .= " FROM Produits AS p";
		//$requete .= " NATURAL LEFT JOIN  PrdtUtilisateurs AS pu";
		$requete .= " WHERE p.IdProduit='$IdProduit'";
		
		$resultat = exec_requete_select ($requete, $GLOBALS["connexion"]) or die ("requete non valide");
	
		while($ligne = ligne_suivante ($resultat)) {
			if ($ligne->Conditionnement != "")
				return html_message("Stock de ".$ligne->NomProduit." : ".$ligne->Qrestante." (". $ligne->Conditionnement.")","info");
			else return html_message("Stock de ".$ligne->NomProduit." : ".$ligne->Qrestante,"info");
			//$tab_equipe[$ligne->IdProduit]=$ligne->NomProduit;
		}
		//exit();
	}

	function cherche_ancien_mouvement($IdMouvement) {
		$requete = "SELECT  m.IdMouvement  ,   m.IdProduit ,  m.MvtQuantite,   m.MvtType";
		$requete .= " FROM Mouvements AS m";
		//$requete .= " NATURAL LEFT JOIN  PrdtUtilisateurs AS pu";
		$requete .= " WHERE m.IdMouvement='$IdMouvement'";
		//$requete .= " ORDER BY NomProduit";
		$resultat = exec_requete_select ($requete, $GLOBALS["connexion"]) or die ("requete non valide");
//echo "coucou";	
		while($ligne = ligne_suivante ($resultat)) {
			$tab_ancien["IdProduit"] = $ligne->IdProduit;
			$tab_ancien["MvtType"] = $ligne->MvtType;
			$tab_ancien["MvtQuantite"] = $ligne->MvtQuantite;
			return($tab_ancien);
			//$tab_equipe[$ligne->IdProduit]=$ligne->NomProduit;
		}
//echo "coucou";	
		exit();
	
	}

}

?>