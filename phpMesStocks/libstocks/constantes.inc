<?php
require_once ('../libpms/settings.inc');

if (!isset ($module_constantes)) {
	$module_constantes = 1;

	define("VERTICAL", "0");
	define("HORIZONTAL", "1");
	define("NOM", DB_LOGIN);
	define("SERVEUR", DB_SERVER);
	define("MOTPASSE", DB_PASSWORD);
	define("BASE", DB_NAME);
	define("TEMPS", "600");

}
?>