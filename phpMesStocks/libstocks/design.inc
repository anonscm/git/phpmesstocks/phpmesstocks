<?
                           
/***************************************************************************
                      design.inc  -
           fonction de creation d'en tete, pied de page...
                             -------------------
    begin                :  14/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
require "libphp/table.inc";

if (!isset($module_design)){
	
	$module_design=1;
	require "libphp/HTML.inc";

	function entete ($titre,$texte){
					
		TblDebut("entete");

		TblDebutLigne ();
		//TblCellule((image('./images/compaq.png',"compaq","Tux en moto")),"");		
		TblCellule(LOGICIEL_NOM." ".LOGICIEL_VERSION."<br/>".$texte,"titre",1,100);
		//TblCellule((image('./images/wstux.png',"wstux","Tux sur son ordinateur")),"centre");
		TblFinLigne();
		TblFin();
	}
	
	function menu() {
		$menu = array("Accueil" => "index.php?repertoire=accueil",
                     "D�connexion" => "index.php?action=deconnexion&repertoire=accueil",
                     "Produits" => "index.php?repertoire=produit&action=liste",
			"Mouvements" => "index.php?repertoire=mouvement&action=liste",
			"Fournisseurs" => "index.php?repertoire=fournisseur&action=liste",
                     "Recommandations" => "index.php?repertoire=recommandations");	 				
		for ($i=0; $i < 5; $i++) {
			$tab_selection[$i] = FALSE;
		}
		switch ($GLOBALS["repertoire"]) {
			case "accueil":
				$tab_selection[0] = TRUE;
				break;
			case "produit":
				$tab_selection[2] = TRUE;
				break;
			case "mouvement":
				$tab_selection[3] = TRUE;
				break;
			case "fournisseur":
				$tab_selection[4] = TRUE;
				break;
			case "recommandations":
				$tab_selection[5] = TRUE;
				break;
		}
		
	//affichage du menu : premi�re ligne avec une ligne et une cellule pour obtenir un fond color�
		TblDebut("menu");
			TblDebutLigne ("menu");
		
			TblDebutCellule();
		
	//deuxi�me table imbriqu�e dans la premi�re avec une ligne et autant de cellules que de choix de menus
			TblDebut (0);
				TblDebutLigne("menu");
			
		//choix de menu
				$i=0;
				while (list ($libelle, $ancre) = each ($menu)) {
					if ($tab_selection[$i]) {
						if ($GLOBALS["action"] != "liste") TblCellule (html_message(ancre($ancre,$libelle),"menusurligne"),"menusurligne");
						else TblCellule (html_message($libelle,"menusurligne"),"menusurligne");
					}
					else TblCellule (html_message(ancre($ancre,$libelle,"menu"),"menu"));
					$i++;
				}
				TblFinLigne();
			TblFin();
			TblFinCellule();
			TblFinLigne ();
		TblFin();						
	}
	
	
	function pied_de_page (){
		TblDebut("pieddepage");
		TblDebutLigne ();
		TblCellule(ancre("http://validator.w3.org/",image("./images/valid-xhtml11.png",""),"discret"),"centre");
		TblCellule(html_message(ancre("index.php?action=copyright","copyright de ".LOGICIEL_NOM), "paragraphe"),"centre");
		TblCellule(ancre("http://jigsaw.w3.org/css-validator/",image("./images/valid-css.png",""),"discret"),"centre");
		TblFinLigne();
		TblFin();
		echo "</body></html>";
		exit;
	}
	
	
	
	
	
}
?>
