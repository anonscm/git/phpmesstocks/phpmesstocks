<?
if (!isset($fichier_tab_salles)){
	$fichier_tab_salles = 1;


/***************************************************************************
                          tab_salles.inc  -  remplissage du tableau $tab_salles
                             -------------------
    begin                :  21/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
	function charge_tableSalles($IdUtilisateur) {
		$tab_equipe = array();
		// IdUtilisateur     UIdentifiant     UNom
		$requete = "SELECT   s.IdSalle, s.NomSalle";
		$requete .= " FROM Salles AS s";
		//$requete .= " NATURAL LEFT JOIN  PrdtUtilisateurs AS pu";
		//$requete .= " WHERE pu.IdUtilisateur='$IdUtilisateur'";
		$requete .= " ORDER BY NomSalle";
		$resultat = exec_requete ($requete, $GLOBALS["connexion"]) or die ("requete non valide");
	
		while($ligne = ligne_suivante ($resultat)) {
			$tab_equipe[$ligne->IdSalle]=$ligne->NomSalle;
		}
		return ($tab_equipe);
	}

}

?>