<?
if (!isset($fichier_tab_utilisateurs)){
	$fichier_tab_utilisateurs = 1;


/***************************************************************************
                          tab_utilisateurs.inc  -  remplissage du tableau $tab_utilisateurs
                             -------------------
    begin                :  20/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
	function charge_tableUtilisateurs() {
		$tab_equipe = array();
		// IdUtilisateur     UIdentifiant     UNom
		$requete = "SELECT  IdUtilisateur  ,     UNom";
		$requete .= " FROM Utilisateurs";
		$requete .= " ORDER BY UNom";
		$resultat = exec_requete ($requete, $GLOBALS["connexion"]) or die ("requete non valide");
	
		while($ligne = ligne_suivante ($resultat)) {
			$tab_equipe[$ligne->IdUtilisateur]=$ligne->UNom;
		}
		return ($tab_equipe);
	}

}

?>