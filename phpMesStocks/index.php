<?php
 
/***************************************************************************
 index.php  -
 point d'entr�e dans l'application phpMesStocks
 -------------------
 begin                :  14/11/2002
 copyright            : (C) 2002 by Olivier Langella
 email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//chargement des fonctions communes
//echo "coucou d�connexion ".$action."<br/>";

session_start ();
require "./libphp/php_ini_registerglobal_off.inc";


require "./libstocks/design.inc";
require "./libstocks/constantes.inc";

require "./libstocks/version.inc";

require "./libstocks/connexion.inc";
require "./libphp/requetes.inc";
require "./libphp/sql_functions.inc";
require "./libphp/HTML.inc";
require "./libphp/xhtml.inc";
require "./libphp/objet_formulaire.inc";

//echo "connexion au serveur et  la base de donne";
$connexion = connexion (NOM, MOTPASSE, BASE, SERVEUR);

if ($action =="deconnexion") {
	$requete = requete_sql_delete("Session", array('IdSession' => session_id()));
	$resultat = exec_requete ($requete, $connexion) or die ("requete supprimer les valeurs de la table Session non valide");

	session_destroy();  //on dtruit toutes les informations associes  la session
}

echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
<title>phpMesStocks</title>
<link rel="stylesheet" href="./libcss/stocks.css" type="text/CSS" />
</head>

<body>
<?

require "./libstocks/session.inc";

entete ("phpMyStocks","Gestion de stocks");


//		echo html_message("coucou", "menu");

//authentification,controle d'accs

controle_acces ("index.php", $form_login ,$form_password, $HTTP_SESSION_VARS, $objet_session, $objet_adminBD, session_id() );


//navigation avec les URL
menu();

//echo "index.php: Controle d' acces ok <br/>";
//mise  jour des tables

if ($action == "") {

	if ($repertoire == "recommandations") {
		require "libstocks/recommandations.inc";
		pied_de_page();
	}
	else {
		TblDebut("centre");
		TblDebutLigne ("");
		TblCellule((image('./images/logolibertelight.png',"gnu generation","gnu generation")),"");

		TblDebutCellule();
		echo html_message(ancre("index.php?repertoire=moncompte&action=chgtmotdepasse","changer mon <br/>mot de passe"), "menu");
		TblFinCellule();

		TblDebutCellule("centre");
		echo html_message("bienvenue ".$objet_adminBD->PrenomAdminBd.", vous �tes sur la page d'accueil.", "information");
		echo html_message("Nous sommes le ".date("d")." / ".date("m")." / ".date("Y"), "information");

		TblFinCellule();
		TblFinLigne ("");
		TblFin("");
	}

	pied_de_page();
}

if (!isset($repertoire)) $action = "copyright";

//gestion des utilisateurs : entre un utilisateur dans la table (gr seulement par l'administrateur)
//				      pouvoir changer son mot de passe n'importe quand. (pour tout le monde)

if ($action=="copyright") {
	include "./libphp/copyright.inc";
}
else if ($repertoire == "moncompte") {
	require "moncompte/index.inc";
}
else {
	//v�rification du droit de "lecture":
	$var = $repertoire."Vue";
	if ($objet_adminBD->$var != "1") {
		echo html_message("Vous n'avez pas les droits requis","erreur");
		pied_de_page();
	}


	//echo  "index.php  <br/>".$HTTP_SESSION_VARS["session_deja_enregistre"]. " " .$action;
	if(($action!="ajouter") && ($action!="modifier")){
		$HTTP_SESSION_VARS["session_deja_enregistre"]="";
	}
	else {
		//if ($HTTP_SESSION_VARS["session_deja_enregistre"] == 1) {
		//	$action ="liste";
		//}
	}


	if (($action!="liste")&&($action!="form")&&($action!="form_recherche")) {
		require $repertoire."/".$repertoire."_verif_droits.inc"; //fichier de blocage
		//echo  "index.php  <br/>".$HTTP_SESSION_VARS["session_deja_enregistre"]. " " .$action;
		require $repertoire."/".$repertoire."_mise_a_jour.inc";
	}

	//echo  $repertoire."/".$repertoire."__objet_derive.inc<br/>";
	require $repertoire."/".$repertoire."_objet_derive.inc";

	if ($action=="liste")
	require $repertoire."/".$repertoire."_liste.inc";

	else if  ($action=="form")
	require $repertoire."/".$repertoire."_affichage.inc";
	else if  ($action=="form_recherche")
	require $repertoire."/".$repertoire."_recherche.inc";
}


pied_de_page();

?>