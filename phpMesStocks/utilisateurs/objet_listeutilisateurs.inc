<?
/***************************************************************************
                          objet_listeutilisateurs.inc  -  objet permettant d'afficher la liste des produits
                             -------------------
    begin                : 03/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 // derni�re modification : 
 
//include "libphp/objet_tableau.inc";

	//include "../../phpMyRezo/parametres/personnalisation.inc";
	
	class ListeUtilisateurs extends ListeResultat {		

		function ListeUtilisateurs($resultat, $nom) {
			//constructeur
			$this->ListeResultat($resultat, $nom);
		}

		function afficher_titres() {
			//� impl�menter dans les objets d�riv�s
			$this->tabliste->ajouter_case("");
			$this->tabliste->ajouter_case("Identifiant");
			$this->tabliste->ajouter_case("Nom");
			$this->tabliste->ajouter_case("Administrateur");
			$this->tabliste->afficher_ligne_titres();
		}

		function afficher_ligne($row) {

			//� impl�menter dans les objets d�riv�s
			$cNomProduit = $GLOBALS["cNomProduit"];
			if ($GLOBALS["cIdProduit"] > 0) {
				if ($row->IdProduit == $GLOBALS["cIdProduit"]) $this->tabliste->ajouter_case_lien("d�lier � $cNomProduit", "index.php?edition=utilisateurs&IdUtilisateur=$row->IdUtilisateur&action=delier&");
				else $this->tabliste->ajouter_case_lien("lier � $cNomProduit", "index.php?edition=utilisateurs&IdUtilisateur=$row->IdUtilisateur&action=lier&");
			}
			else $this->tabliste->ajouter_case("");
			$this->tabliste->ajouter_case($row->UIdentifiant);
			$this->tabliste->ajouter_case($row->Nom);
			$this->tabliste->ajouter_case($row->UAdmin);

			$this->ajouter_case_modifier("Modifier","index.php?edition=utilisaterurs&IdUtilisateur=$row->IdUtilisateur&action=modif&");

			$this->ajouter_case_supprimer("Supprimer","index.php?edition=produits&IdProduit=$row->IdProduit&action=suppr&");
		
			$this->tabliste->afficher_ligne();
		}
	}

?>