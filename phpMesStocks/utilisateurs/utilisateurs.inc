<?
/***************************************************************************
                          utilisateurs.inc  -  point d'entr�e dans la gestion des utilisateurs
                             -------------------
    begin                :  03/04/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
//Affichage des personnes
	if (($action == "delier") && ($cAdmin == 1)) { //v�rification des droits
		if (($cIdProduit > 0) && ($IdUtilisateur > 0)){
			//lier un produit avec un utilisateur
			$requete = "DELETE FROM PrdtUtilisateurs";
			$requete .= " WHERE IdUtilisateur='$IdUtilisateur' AND IdProduit='$cIdProduit'";
			$resultat = mysql_query($requete, $dbh) or die ("effacement impossible");
			
			echo "Ce produit n'est plus associ� � cet utilisateur<br/>";
		}
		else echo "op�ration impossible<br/>";
		
		$action=liste;
	}

	if (($action == "lier") && ($cAdmin == 1)) { //v�rification des droits
		if (($cIdProduit > 0) && ($IdUtilisateur > 0)){
			//lier un produit avec un utilisateur
			$requete = "INSERT INTO PrdtUtilisateurs (IdUtilisateur, IdProduit)";
			$requete .= " VALUES ('$IdUtilisateur', '$cIdProduit')";
			$resultat = mysql_query($requete, $dbh) or die ("liaison impossible");
			
			echo "liaison Utilisateur/Produit enregistr�e<br/>";
		}
		else echo "op�ration impossible<br/>";
		
		$action=liste;
	}

	//------------------------------------------------
	// affichage des utilisateurs en liste
	//------------------------------------------------
	
	if (($cAdmin == 1) && ($action==liste)) {
		$requete = "SELECT t1.IdUtilisateur, t1.UIdentifiant, t1.UNom, t1.UAdmin, t2.IdProduit FROM Utilisateurs AS t1";
		$requete .= " LEFT JOIN PrdtUtilisateurs AS t2";
		$requete .= " ON t1.IdUtilisateur = t2.IdUtilisateur AND t2.IdProduit = '$cIdProduit'";
		//$requete .= " WHERE t2.IdProduit='$cIdProduit'";
		//$requete .= " WHERE (t2.IdProduit='$cIdProduit' OR t2.IdProduit IS NULL)";

		$requete .= " ORDER BY t1.UIdentifiant";

		include "libphp/objet_tableau.inc";
		include "libphp/objet_listeresultat.inc";
		include "utilisateurs/objet_listeutilisateurs.inc";
		
		$resultat = mysql_query($requete, $dbh) or die ("requete non valide");
		
		$listeresultat = new ListeUtilisateurs($resultat,"listedesutilisateurs");
		if ($cAdmin == 1) $listeresultat->set_ajouter("formmachine.php?");
		//f  ($cAdmin == 1) $listeresultat->set_modifier();
		if  ($cAdmin == 1) $listeresultat->set_supprimer();

		$nb_utilisateurs = mysql_affected_rows();
		echo "<b>$nb_utilisateurs utilisateurs</b>";
		$listeresultat->afficher_liste();
	
	}
	
?>
