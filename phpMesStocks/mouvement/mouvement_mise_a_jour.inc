<?

/***************************************************************************
                          mouvement_mise_a_jour.inc  -  mise � jour des mouvements
                             -------------------
    begin                :  20/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

require "libphp/gestion_de_date.inc";

require "libphp/sql_functions.inc";
require "libstocks/produits_fonctions.inc"; 
//require "./libcampus/tab_OS.inc";
//require "./libcampus/tab_baie.inc";
//$tab_NomBaie= charge_tableBaie($objet_adminBD->IdLabo);

//dans ce fichier,on va traiter tous les changements possibles sur la base : ajouter un nouvel element, en supprimer un ou en modifier un.

// IdTelephone   IdPriseMurale   IdLabo   NumTelephone   TypeTelephone


$tabMvtTypes = array ("0" => "Entr�e", "1"=> "Sortie");


if (($action=="ajouter") || ($action=="modifier")) {
	//tableau des champs � enregistrer (en modif ou en ajout, r�cup�r�s dans le formulaire)
	// IdMouvement     IdProduit     IdSalle     IdUtilisateur     MvtType     MvtDate     MvtQuantite     MvtNotes
	$tmp_tableau = array(
			'IdProduit' => $form_IdProduit,
			'IdSalle' => $form_IdSalle,
			'IdUtilisateur' => $objet_adminBD->IdUtilisateur,
			'MvtType' => $form_MvtType,
			'MvtDate' => datefr2datesql($form_MvtDate),
			'MvtQuantite' => $form_MvtQuantite,
			'MvtNotes' => $form_MvtNotes
		);
	//if ($session_deja_enregistre==$tmp_tableau['NumTelephone']) {
	//	$action="liste";
	//	$repertoire="telephone";	
	//}
	$quantite = cherche_quantite($form_IdProduit);
}

if (($repertoire=="mouvement") && ($action=="ajouter") && ($form_IdMouvement==0)){
	
	//cr�ation du mouvement:
	$ins_mouvement = requete_sql_insert_into ("Mouvements", $tmp_tableau);
	//$HTTP_SESSION_VARS["session_deja_enregistre"]=$tmp_tableau['NumTelephone']; 
	
	$resultat = exec_requete ($ins_mouvement, $connexion) or die ("requete ajouter non valide");

	//mise � jour de la quantit�
	if ($form_MvtType == 0) $quantite += $form_MvtQuantite;
	else $quantite -= $form_MvtQuantite;
				
	$resultat = exec_requete (requete_sql_update("Produits", array('IdProduit' => $form_IdProduit), array('Qrestante' => $quantite)), $connexion) or die ("requete modifier non valide");
	
	if ($choix=="1") {
		$action="liste";
		$repertoire="mouvement";
	}
	else{
		$url_IdMouvement = 0;
		$action="form";
		$repertoire="mouvement";
	}
}

//echo "coucou 	$repertoire $action $form_IdMouvement";

if (($repertoire=="mouvement") && ($action=="modifier") && ($form_IdMouvement != 0)){

	//mise � jour de la quantit�
	$tab_ancien = cherche_ancien_mouvement($form_IdMouvement);
//echo "coucou 	$quantite $ancien_quantite $ancien_typemvt";

	if ($tab_ancien["MvtType"] == $form_MvtType) {
		$form_MvtQuantite -= $tab_ancien["MvtQuantite"];
		$quantite += $form_MvtQuantite;
	}
	else {
		$form_MvtQuantite += $tab_ancien["MvtQuantite"];
		if ($form_MvtType == 0) $quantite += $form_MvtQuantite;
		else $quantite -= $form_MvtQuantite;
	}
	
	
	//mise � jour du mouvement
	$resultat = exec_requete (requete_sql_update("Mouvements", array('IdMouvement' => $form_IdMouvement),  $tmp_tableau), $connexion) or die ("requete modifier non valide");


	$resultat = exec_requete (requete_sql_update("Produits", array('IdProduit' => $form_IdProduit), array('Qrestante' => $quantite)), $connexion) or die ("requete modifier non valide");
	
	$action="liste";
	$repertoire="mouvement";
}



//On a demander une suppression
if ( ($repertoire=="mouvement") && ($action=="supprimer") && ($url_IdMouvement!=0)){
	
	if($confirmer==1){
		//suppresion_telephone($url_IdTelephone);
		//mise � jour de la quantit�
		$tab_ancien = cherche_ancien_mouvement($url_IdMouvement);
		$quantite = cherche_quantite($tab_ancien["IdProduit"]);

		if ($tab_ancien["MvtType"] == 0) $quantite -= $tab_ancien["MvtQuantite"];
		else $quantite +=$tab_ancien["MvtQuantite"];

		$resultat = exec_requete (requete_sql_update("Produits", array('IdProduit' => $tab_ancien["IdProduit"]), array('Qrestante' => $quantite)), $connexion) or die ("requete modifier non valide");
		
		$resultat = exec_requete (requete_sql_delete("Mouvements", array('IdMouvement' => $url_IdMouvement)), $connexion) or die ("requete modifier non valide");
		
		$action="liste";
		$repertoire="mouvement";
	}
	else{
		//la suppresion est demand�e mais non confirm�e
		
		//Affichage de confirmation de supression
		
		$message="Etes vous certain de vouloir supprimer ce mouvement ?";
		echo html_confirmation($message, "index.php?repertoire=mouvement&action=supprimer&url_IdMouvement=".$url_IdMouvement."&confirmer=1","index.php?repertoire=mouvement&action=liste");

		pied_de_page ();
	}
}
	
?>