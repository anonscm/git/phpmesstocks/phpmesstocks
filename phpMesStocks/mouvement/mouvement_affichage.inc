<?

/***************************************************************************
                          mouvement_affichage.inc  -  affichage d'un mouvement dans un fomulaire
                             -------------------
    begin                :  21/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

if ($objet_user->Login!= "anonyme") 

{
	require "./libstocks/tab_salles.inc";
	$tab_req_salles = charge_tableSalles($objet_adminBD->IdUtilisateur);
	//$tab_req_salles[0] = "----";

//	require "./libcampus/tab_TypeTelephone.inc";
//	require "./libcampus/tab_baie.inc";
//	$tab_TypeTel = charge_tableTypeTelephone();
//	$tab_NomBaie= charge_tableBaie($objet_adminBD->IdLabo);

	$tabMvtTypes = array ("0" => "Entr�e", "1"=> "Sortie");
	
	$tab_nomchamps = array ("NomProduit","Conditionnement","IdMouvement","IdProduit","MvtDate","MvtQuantite","MvtType","MvtNotes","IdSalle","NomSalle");

	if ($url_IdMouvement > 0) //si $url_IdMouvement != 0 on a cliquer sur modifier
	{
		$objet_requete= new creer_requete();
		$objet_requete->select(array("NomProduit","Conditionnement"),"FROM","Produits","p");
		$objet_requete->select(array("IdMouvement","IdProduit","MvtDate","MvtQuantite","MvtType","MvtNotes"),"NATURAL LEFT JOIN","Mouvements","m");
		$objet_requete->select(array("IdSalle","NomSalle"),"NATURAL LEFT OUTER JOIN","Salles","s");
		$objet_requete->fin_select();
		
		$objet_requete->ajout_egal ("IdMouvement",$url_IdMouvement, "AND");
		$requete = $objet_requete->fin_requete();
		
		$resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
		//$ligne = ligne_suivante ($resultat);
		$tab_champs = ligne_suivante_taba($resultat);
		
		$tmp_valider="enregistrer cette modification";
		$tmp_action="modifier";
	}
	
	else{//on est sur ajouter
		//initialiser le tableau de champs
		$tab_champs = array ();
		for ($i=0; $i < count($tab_nomchamps); $i++) {
			$tab_champs[$tab_nomchamps[$i]] =  "";
		}

		$tab_champs["IdMouvement"] = 0;
		$tab_champs["IdProduit"] = $urlIdProduit;

		$tmp_valider="Ajouter ce mouvement";
		$tmp_action="ajouter";
	}
	
	// on fait un formulaire qui sera commun aux ajouts et aux modification d'OS

	$form = new formulaire("post","index.php",FALSE,"form_mouvement",TRUE);
		
	$form->champ_cache("choix",1); //retour � la liste

	$form->champ_cache("action",$tmp_action);
	$form->champ_cache("repertoire","mouvement");
	$form->champ_cache("form_IdProduit",$tab_champs["IdProduit"]);
	$form->champ_cache("form_IdMouvement",$tab_champs["IdMouvement"]);
	
	$form->debut_table(HORIZONTAL,1);

	$form->champ_liste_deroulante("", "form_MvtType", $tabMvtTypes, $tab_champs["MvtType"]);
	$form->champ_texte("Quantit� :", "form_MvtQuantite", $tab_champs["MvtQuantite"],10);
	$form->fin_table();

	$form->debut_table(VERTICAL,1);

	$form->champ_date("date : ", "form_MvtDate", $tab_champs["MvtDate"],"aujourd'hui");
	$form->champ_liste_deroulante("salle : ", "form_IdSalle", $tab_req_salles, $tab_champs["IdSalle"]);
	$form->champ_texte("Notes :", "form_MvtNotes", $tab_champs["MvtNotes"],30);
	
	//$form->champ_liste_deroulante ("Type de t�l�phone","form_TypeTelephone", $tab_TypeTel,$tab_champs["TypeTelephone"]);
	
	
	$form->champ_valider("","bouton",$tmp_valider);
	
	$form->ajout_verif_champ_vide("form_MvtQuantite","Vous devez saisir une quantit�");
	
	$form->fin_table();
	
	$form->fin();	
	
}	
	
else echo html_message ("Vous n'avez pas l'autorisation de manipuler la base","erreur");	
	
?>