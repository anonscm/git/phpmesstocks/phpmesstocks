<?

/***************************************************************************
                          mouvement_liste.inc  - 
                   Affichage des mouvements en liste
                             -------------------
    begin                :  14/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

require "./libstocks/tab_utilisateurs.inc";

require "./libstocks/produits_fonctions.inc";
$tab_req_utilisateurs = charge_tableUtilisateurs();
$tab_req_utilisateurs[0] = "----";

require "./libphp/memoire_req.inc";
rappel_memoire_req($HTTP_SESSION_VARS, $repertoire, array ("req_IdProduit","req_IdUtilisateur","req_MvtType"));
rappel_liste_pos($HTTP_SESSION_VARS, $repertoire);

$tab_tri["ORDER BY u.NomUtilisateur"] = "Noms";
//$tab_tri["ORDER BY u.PrenomUtilisateur"] = "Pr�noms";

rappel_tri($HTTP_SESSION_VARS, $repertoire, "dates");


if ($req_IdUtilisateur == "") $req_IdUtilisateur = $objet_adminBD->IdUtilisateur;
require "./libstocks/tab_produits.inc";
$tab_req_produits = charge_tableProduits($req_IdUtilisateur);
$tab_req_produits[0] = "----";


/************************************************
	FABRICATION de la barre de recherche/tri
*************************************************/

TblDebut("trirecherche");
//ligne de titres des colonnes:
		
TblDebutLigne();
/*	
TblDebutCellule();
// formulaire de s�lection du tri:
$form = new formulaire("post","index.php?repertoire=produit&action=liste",FALSE,"form",FALSE);
$form->debut_table(HORIZONTAL,1);	
$form->champ_liste_deroulante("Tri","tri",$tab_tri, $tri);
$form->champ_valider("","bouton","trier","trier");
	
$form->fin_table();

$form->fin();
TblFinCellule();
*/
TblDebutCellule();

 // forumulaire de recherche rapide:

$form = new formulaire("post","index.php?repertoire=mouvement&action=liste",FALSE,"form",FALSE);

$form->debut_table(HORIZONTAL,1);	
//$form->champ_texte("Nom","req_IUtilisateur",$req_NomUtilisateur,10);
//$form->champ_texte("Pr�nom","req_PrenomUtilisateur",$req_PrenomUtilisateur,10);
//$form->champ_texte("Courriel","req_EmailUtilisateur",$req_EmailUtilisateur,10);

$form->champ_liste_deroulante("produits","req_IdProduit",$tab_req_produits, $req_IdProduit);
$form->champ_liste_deroulante("utilisateurs","req_IdUtilisateur",$tab_req_utilisateurs, $req_IdUtilisateur);

$tabMvtTypes = array ("-1" => "----", "0" => "Entr�e", "1"=> "Sortie");
if (!isset($req_MvtType)) $req_MvtType = "-1";
else if ($req_MvtType =="") $req_MvtType = "-1";
$form->champ_liste_deroulante("type","req_MvtType",$tabMvtTypes, $req_MvtType);

$form->champ_valider("","bouton","rechercher","rechercher");

$form->fin_table();

$form->fin();

TblFinCellule();

TblFinLigne();
TblFin();

/************************************************
	FIN de la barre de recherche/tri
*************************************************/


/************************************************
	FABRICATION de la requ�te
*************************************************/
//requete qui permetra d'afficher la liste

$objet_requete= new creer_requete();
$objet_requete->select(array("IdUtilisateur"),"FROM","PrdtUtilisateurs","pu");
$objet_requete->select(array("NomProduit", "Conditionnement"),"LEFT JOIN","Produits","p", "USING (IdProduit)");
$objet_requete->select(array("IdMouvement", "IdProduit",  "MvtType", "MvtNotes", "MvtDate", "IdSalle", "MvtQuantite"),"LEFT JOIN","Mouvements","m", "USING (IdProduit)");
$objet_requete->select(array("NomSalle"),"LEFT JOIN","Salles","s", "USING (IdSalle)");
$objet_requete->fin_select();


//$objet_requete->ajout_egal ("pu.IdUtilisateur",$objet_adminBD->IdUtilisateur, "AND");

if ($req_IdProduit < 1) $objet_adminBD->mouvementAjout=0;
else echo html_etat_stock($req_IdProduit);

if ($req_IdUtilisateur != $objet_adminBD->IdUtilisateur) {
	$objet_adminBD->mouvementAjout=0;
	$objet_adminBD->mouvementModif=0;
	$objet_adminBD->mouvementSuppr=0;
}

if ($req_IdProduit > 0) $objet_requete->ajout_egal ("m.IdProduit",$req_IdProduit, "AND");

if ($req_MvtType > -1) $objet_requete->ajout_egal ("m.MvtType",$req_MvtType, "AND");

if ($req_IdUtilisateur > 0) $objet_requete->ajout_egal ("pu.IdUtilisateur",$req_IdUtilisateur, "AND");

/*
if ($req_PrenomUtilisateur != "") $objet_requete->ajout_like ("u.PrenomUtilisateur",$req_PrenomUtilisateur, "AND");

if ($req_EmailUtilisateur != "") $objet_requete->ajout_like ("u.EmailUtilisateur",$req_EmailUtilisateur, "AND");

if ($req_equipe > 0) $objet_requete->ajout_egal ("ue.IdEquipe",$req_equipe, "AND");

if ($req_equipe == 0) $objet_requete->ajout_egal ("ue.IdEquipe","NULL", "AND");

if ($req_IdCategorie > 0) $objet_requete->ajout_egal ("u.IdCategorie",$req_IdCategorie, "AND");
//if ($req_IdCategorie == 0) $objet_requete->ajout_egal ("u.IdCategorie",0, "AND");
*/

if ($tri == "dates") {
   $objet_requete->ajout_tri("m.MvtDate", "DESC");
}
//$tri = " ORDER BY m.MvtDate DESC";		
$requete = $objet_requete->fin_requete($connexion);


/************************************************
	fin de la FABRICATION de la requ�te
*************************************************/


$page_par_page = new objet_page($position,$objet_requete->_nblignes,$pas);

//execution de la requete si repertoire = machine et action = liste on execute la requete 

if (($repertoire=="mouvement") &($action=="liste")){
	$resultat = exec_requete_select ($page_par_page->modif_requete($requete), $connexion) or die ("requete non valide");
}
		
////on regarde les droits de celui qui s'est connect� � la base ces droits sont contenus dans l'objet global $objet_user	
	
// on implemente un nouvel objet deriv� specifique aux machines pour l'affichage de la liste
		
$listeresultat= new liste_derivee_mouvement ($resultat, "liste_mouvement", $page_par_page);
			
// suivant les droits de la personne qui s'est connnect�e on affiche ou pas les boutons ajouter modifier..


if ($objet_adminBD->mouvementAjout==1)
	$listeresultat->set_ajouter ("index.php?repertoire=mouvement&action=form&urlIdProduit=".$req_IdProduit, "ajouter un mouvement");

if ($objet_adminBD->mouvementModif==1)
	$listeresultat->set_modifier () ;
		
if ($objet_adminBD->mouvementSuppr==1)
	$listeresultat->set_supprimer (); 

				
	
if ($objet_requete->_nblignes=="0") {
	echo html_message ("Il n'y a pas de mouvement correspondant � votre demande","erreur");
	$listeresultat->afficher_liste();
}	
else
	$listeresultat->afficher_liste();
/*	        
//formulaire pour faire une recherche : on ajoute le bouton rechercher
 
$form = new formulaire("post","index.php?repertoire=utilisateur&action=form_recherche",FALSE,"",FALSE);
$form->debut_table(HORIZONTAL,1);	
if (($repertoire=="machine") &($action=="requete"))
	$form->champ_valider("","bouton","faire une nouvelle recherche","valider_liste");
	
else
	$form->champ_valider("","bouton","faire une  recherche","valider_liste");
	
$form->fin_table();

$form->fin();
*/
	
	
		
	






?>
