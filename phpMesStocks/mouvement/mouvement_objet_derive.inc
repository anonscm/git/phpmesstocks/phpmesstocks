<?

/***************************************************************************
                          mouvement_objet_derive.inc  - 
                   Affichage des mouvements
                             -------------------
    begin                :  14/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

  
 //on fait une classe liste derivee_machine a partir de la classe objet_liste_resultat 
// on va pouvoir afficher la liste
//require "libphp/gestion_ip.inc";
require "libphp/gestion_de_date.inc";



if (!isset($module_liste_derivee_mouvement)){
	
	$module_liste_derivee_mouvement = 1;
	
	require "libphp/objet_liste_resultat.inc";
	
	class	liste_derivee_mouvement extends liste_resultat {
		
		function liste_derivee_mouvement ($resultat, $nom, $page) {
			//constructeur
			$this->liste_resultat($resultat,$nom, $page);
			
		}
	
//on affiche les titres	
				
		function afficher_effectif() {
			//pour afficher le nombre de lignes s�lectionnn�es
			
			echo html_message($this->_nb_lignes." mouvements s�lectionn�s","info");
		}

		function afficher_titres(){
			
			TblCellule ("Date");
			TblCellule ("Produit");
			TblCellule ("Notes");
			TblCellule ("Quantit�<br/>Sortie");
			TblCellule ("Quantit�<br/>Entr�e");
			TblCellule ("Salle de <br/>manip");
			
		}

			//on met les donn�es de la tables machine dans la liste	
		function afficher_ligne($row){
						
					
			TblCellule (datesql2datefr ($row->MvtDate));
			TblCellule ($row->NomProduit);
			TblCellule ($row->MvtNotes);
					
			if ($row->MvtType == 1) { //sortie
				TblCellule ($row->MvtQuantite);
				TblCellule ("");
				//$this->total_sortie += $row->MvtQuantite;
			}
			else { //entr�e
				TblCellule ("");
				TblCellule ($row->MvtQuantite);
				//$this->total_entree += $row->MvtQuantite;
			}
			TblCellule ($row->NomSalle);
					
			//On ajoute en fin de ligne les boutons Versions Modifier, supprimer	
			
			$this->ajouter_case_modifier("Modifier","index.php?repertoire=mouvement&action=form&url_IdMouvement=$row->IdMouvement");
				
			$this->ajouter_case_supprimer("Supprimer","index.php?repertoire=mouvement&action=supprimer&url_IdMouvement=$row->IdMouvement");
				//bouton selectionner
			
			//$this->ajouter_case_selectionner ("Selectionner","index.php?repertoire=mouvement&action=selectionner&url_IdUtilisateur=$row->IdUtilisateur");
			
			//$this->_tabliste->afficher_ligne();
		}
	}
}	

?>