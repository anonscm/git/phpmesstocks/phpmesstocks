
    <H4><CENTER><? echo "$logiciel_nom $logiciel_version"; ?> Copyright (C) 2002, Olivier Langella,
    CNRS</CENTER></H4>
    <H4><CENTER>derni�re version: <? echo "$logiciel_date"; ?></CENTER></H4>
    
    <P><CENTER>Ce programme est libre, vous pouvez le redistribuer
    et/ou le modifier selon les termes de la <A HREF="http://www.gnu.org/copyleft/gpl.html">Licence
    Publique G&eacute;n&eacute;rale GNU</A> publi&eacute;e par la
    Free Software Foundation (version 2 ou bien toute autre version
    ult&eacute;rieure choisie par vous).</CENTER></P>
    <P><CENTER>Ce programme est distribu&eacute; car potentiellement
    utile, mais SANS AUCUNE GARANTIE, ni explicite ni implicite,
    y compris les garanties de commercialisation ou d'adaptation
    dans un but sp&eacute;cifique. Reportez-vous &agrave; la <A 
    HREF="http://www.gnu.org/copyleft/gpl.html">Licence Publique
    G&eacute;n&eacute;rale GNU</A> pour plus de d&eacute;tails.</CENTER></P>
	<CENTER>
    <P>Vous devez avoir re&ccedil;u une copie de la Licence
    Publique G&eacute;n&eacute;rale GNU en m&ecirc;me temps que ce
    programme ; si ce n'est pas le cas, &eacute;crivez &agrave; la
    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
    MA 02111-1307, &Eacute;tats-Unis.</P>

	<P>
		Les sources en HTML/PHP des formulaires, et la structure de la base de donn�es de <? echo "$logiciel_nom $logiciel_version"; ?> (MySql) sont disponibles sur demande �:
		Olivier.Langella@pge.cnrs-gif.fr
	</P>
	<P>
		<A HREF="http://www.pge.cnrs-gif.fr/bioinfo" target="TOP">Autres logiciels</A> (site de Bioinformatique) 
	</P>
