<?
/***************************************************************************
                      produit_verif_droits.inc  -
           verification des droits d'acces en modification, ajout ou suppression pour les mouvements
                             -------------------
    begin                :  21/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 
 //v�rification des droits de l'internaute pour $action = supprimer, ajouter ou modifier
 
 $tmp_OK = "0";
 
 if(($action=="modifier")&&($objet_adminBD->produitModif =="1")){
 	$tmp_OK="1";
 }
 
 //echo "tmp_OK=".$tmp_OK."<br/>";
 if(($action=="ajouter")&&($objet_adminBD->produitAjout  =="1")){
 	$tmp_OK="1";
 }

if(($action=="supprimer")&&($objet_adminBD->produitSuppr  =="1")){
 	$tmp_OK="1";
 }
 
 if ($tmp_OK =="0") {
 	//on a pas trouv� les droits d'acc�s ad�quat pour cet internaute: on arr�te index.php
 	echo html_message("Vous n'avez pas les droits requis", "erreur");
 	pied_de_page();
 }
 
 ?>