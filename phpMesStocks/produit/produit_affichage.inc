<?

/***************************************************************************
                          produit_affichage.inc  -  affichage d'un produit dans un fomulaire
                             -------------------
    begin                :  21/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

if ($objet_user->Login!= "anonyme") 
     
{
  $tab_nomchamps = array ("IdProduit", "NomProduit", "Qrestante","Conditionnement", "ProduitNotes", "CodeNomenclature");
  
  if ($url_IdProduit > 0) //si $url_IdProduit != 0 on a cliquer sur modifier
    {
      //recherche des fournisseurs de ce produit:
      $objet_requete= new creer_requete();
      $objet_requete->select(array ("IdProduit", "IdFournisseur", "RefPrdtFournisseur", "PrixPrdtFournisseur", "RemisePrdtFournisseur"), "FROM", "PrdtFournisseurs", "pf");
      $objet_requete->select(array ("IdFournisseur", "NomFournisseur"), "NATURAL LEFT JOIN", "Fournisseurs", "f");
      $objet_requete->fin_select();
      
      $objet_requete->ajout_egal ("pf.IdProduit",$url_IdProduit, "AND");
      $requete = $objet_requete->fin_requete(). "ORDER BY f.NomFournisseur";
      
      $resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
      $i = 0;
      $tab_IdFournisseur = array();
      $tmp_ancre = "index.php?repertoire=fournisseur&action=form&url_IdFournisseur=";
      
      while ($ligne_tmp = ligne_suivante ($resultat)){
	$ligne = $ligne_tmp;
	$tab_IdFournisseur[$i] = $ligne->IdFournisseur;
	$tab_NomFournisseur[$i] = $ligne->NomFournisseur;
	$tab_RefPrdtFournisseur[$i] = $ligne->RefPrdtFournisseur;
	$tab_PrixPrdtFournisseur[$i] = $ligne->PrixPrdtFournisseur;
	$tab_RemisePrdtFournisseur[$i] = $ligne->RemisePrdtFournisseur;
	$tab_PrixRFournisseur [$i] = $ligne->PrixPrdtFournisseur;
	if ($tab_RemisePrdtFournisseur[$i] > 0) $tab_PrixRFournisseur [$i] -= ($tab_PrixRFournisseur [$i] * $tab_RemisePrdtFournisseur[$i])/100;
	$tab_PrixRFournisseur[$i] = str_replace (".",",",$tab_PrixRFournisseur[$i]);
	$tab_supprimer_pf[$i] = -1;
	$tab_lien_modif_f[$i] = ancre($tmp_ancre.$ligne->IdFournisseur,"modifier");
	$i++;
      }
      
      //recherche des utilisateurs de ce produit:
      $objet_requete= new creer_requete();
      $objet_requete->select(array ("IdProduit", "IdUtilisateur"), "FROM", "PrdtUtilisateurs", "pu");
      $objet_requete->select(array ("IdUtilisateur",  "UIdentifiant", "UNom"), "NATURAL LEFT JOIN", "Utilisateurs", "u");
      $objet_requete->fin_select();
      
      $objet_requete->ajout_egal ("pu.IdProduit",$url_IdProduit, "AND");
      $requete = $objet_requete->fin_requete(). "ORDER BY u.UIdentifiant";
      
      $resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
      $i = 0;
      $tab_IdUtilisateur = array();
      //$tmp_ancre = "index.php?repertoire=fournisseur&action=form&url_IdFournisseur=";
      
      while ($ligne_tmp = ligne_suivante ($resultat)){
	$ligne = $ligne_tmp;
	$tab_IdUtilisateur[$i] = $ligne->IdUtilisateur;
	$tab_UIdentifiant[$i] = $ligne->UIdentifiant;
	$tab_UNom[$i] = $ligne->UNom;
	//$tab_PrdtUtilisateurs[$i] = $ligne->PrdtUtilisateurs;
	$tab_supprimer_pu[$i] = -1;
	//$tab_lien_modif_f[$i] = ancre($tmp_ancre.$ligne->IdFournisseur,"modifier");
	$i++;
      }
      
      
      // requete principale :
      
      $objet_requete= new creer_requete();
      $objet_requete->select($tab_nomchamps, "FROM", "Produits");
      $objet_requete->fin_select();
      
      $objet_requete->ajout_egal ("IdProduit",$url_IdProduit, "AND");
      $requete = $objet_requete->fin_requete();
      
      $resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
      $tab_champs = ligne_suivante_taba($resultat);
      
      $tmp_valider="enregistrer cette modification";
      $tmp_action="modifier";
    }
  
  else{//on est sur ajouter
    //initialiser le tableau de champs
    $tab_champs = array ();
    for ($i=0; $i < count($tab_nomchamps); $i++) {
      $tab_champs[$tab_nomchamps[$i]] =  "";
    }
    
    $tab_champs["IdProduit"] = 0;
    $tmp_valider="Ajouter ce produit";
    $tmp_action="ajouter";
    $ligne ="";
  }
  
  // on fait un formulaire qui sera commun aux ajouts et aux modification d'OS
  
  $form = new formulaire("post","index.php",FALSE,"form_produit",TRUE);
  
  //$form->champ_cache("choix",1); //retour � la liste
  $form->champ_cache("choix",0); //retour au formulaire
  
  $form->champ_cache("action",$tmp_action);
  $form->champ_cache("repertoire","produit");
  $form->champ_cache("form_IdProduit",$tab_champs["IdProduit"]);
  
  // IdProduit     NomProduit     Conditionnement     Qrestante     ProduitNotes
  $form->debut_table(VERTICAL,1);
  
  //	$form->champ_liste_deroulante("", "form_MvtType", $tabMvtTypes, $ligne->MvtType);
  $form->champ_texte("Nom du produit :", "form_NomProduit", $tab_champs["NomProduit"],30);
  $form->champ_texte("Conditionnement :", "form_Conditionnement", $tab_champs["Conditionnement"],10);
  $form->champ_texte("Quantit� en stock :", "form_Qrestante", $tab_champs["Qrestante"],10);
  $form->champ_texte("Code nomenclature :", "form_CodeNomenclature", $tab_champs["CodeNomenclature"],10);
  $form->champ_texte_etendu("Notes :", "form_ProduitNotes", $tab_champs["ProduitNotes"],5,40);
  
  //$form->champ_liste_deroulante ("Type de t�l�phone","form_TypeTelephone", $tab_TypeTel,$ligne->TypeTelephone);
  $form->champ_valider("","bouton",$tmp_valider);
  $form->fin_table();
  
  if ($tmp_action=="modifier") {
    if (count($tab_IdFournisseur) > 0) {
      $form->debut_table(VERTICAL,1);
      $form->champ_affichage ("liste des fournisseurs:", "","form_information");
      $form->fin_table();
      
      
      $form->debut_table(HORIZONTAL,count($tab_IdFournisseur), "liste");
      $form->champ_cache("tab_IdFournisseur[]", $tab_IdFournisseur);
      $form->champ_affichage ("Fournisseur", $tab_NomFournisseur,"form_information");
      $form->champ_texte ("R�f. du fournisseur","tab_RefPrdtFournisseur[]", $tab_RefPrdtFournisseur,"20");
      $form->champ_decimal ("Prix (euros)","tab_PrixPrdtFournisseur[]", $tab_PrixPrdtFournisseur,"6");
      $form->champ_decimal ("Remise (%)","tab_RemisePrdtFournisseur[]", $tab_RemisePrdtFournisseur,"6");

      $form->champ_decimal ("prix remis�","tab_PrixRFournisseur[]", $tab_PrixRFournisseur,"6");
      // $form->champ_affichage ("Prix remis�", $tab_PrixRFournisseur,"form_information");
      $form->champ_checkbox("supprimer","tab_supprimer_pf[]", $tab_supprimer_pf, $tab_IdFournisseur);
      $form->champ_affichage ("", $tab_lien_modif_f,"form_information");
      
      $form->fin_table();
      
    }
    
    //affichage des fournisseurs du produit
    $form->debut_table(VERTICAL,1);
    $form->champ_texte("Ajouter un fournisseur :", "form_AjoutFournisseur", "",30);
    $form->fin_table();
    
    
    if (count($tab_IdUtilisateur) > 0) {
      $form->debut_table(VERTICAL,1);
      $form->champ_affichage ("liste des utilisateurs :", "","form_information");
      $form->fin_table();
      
      
      $form->debut_table(HORIZONTAL,count($tab_IdUtilisateur), "liste");
      $form->champ_cache("tab_IdUtilisateur[]", $tab_IdUtilisateur);
      $form->champ_affichage ("Identifiant", $tab_UIdentifiant,"form_information");
      $form->champ_affichage ("Nom", $tab_UNom,"form_information");
      //$form->champ_texte ("Nom","tab_UNom[]", $tab_RefPrdtFournisseur);
      $form->champ_checkbox("supprimer","tab_supprimer_pu[]", $tab_supprimer_pu, $tab_IdUtilisateur);
      //$form->champ_affichage ("", $tab_lien_modif_f,"form_information");
      
      $form->fin_table();
      
      $form->debut_table(VERTICAL,1);
    }
    
    //affichage des utilisateurs du produit
    $form->champ_texte("Ajouter un utilisateur (taper son identifiant):", "form_AjoutUtilisateur", "",30);
    $form->fin_table();
  }
  
  $form->debut_table(VERTICAL,1);
  $form->champ_valider("","bouton",$tmp_valider);
  
  $form->ajout_verif_champ_vide("form_NomProduit","Vous devez saisir un nom pour ce produit");
  
  $form->fin_table();
  
  $form->fin();	
  
}	

else echo html_message ("Vous n'avez pas l'autorisation de manipuler la base","erreur");	

?>