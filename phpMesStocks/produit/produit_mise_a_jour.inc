<?

/***************************************************************************
                          produit_mise_a_jour.inc  -  mise � jour des produits
                             -------------------
    begin                :  21/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//dans ce fichier,on va traiter tous les changements possibles sur la base : ajouter un nouvel element, en supprimer un ou en modifier un.

require "produit/produit_fonctions.inc";

if (($action=="ajouter") || ($action=="modifier")) {
  //tableau des champs � enregistrer (en modif ou en ajout, r�cup�r�s dans le formulaire)
  // IdProduit     NomProduit     Conditionnement     Qrestante     ProduitNotes
  $tmp_tableau = array(
		       'NomProduit' => $form_NomProduit,
		       'Conditionnement' => $form_Conditionnement,
		       'CodeNomenclature' => $form_CodeNomenclature,
		       'Qrestante' => $form_Qrestante,
		       'ProduitNotes' => $form_ProduitNotes,
		       );
}

if (($repertoire=="produit") && ($action=="ajouter") && ($form_IdProduit==0)){
  
  //cr�ation du produit:
  $ins_produit = requete_sql_insert_into ("Produits", $tmp_tableau);
  //$HTTP_SESSION_VARS["session_deja_enregistre"]=$tmp_tableau['NumTelephone']; 
  
  $resultat = exec_requete ($ins_produit, $connexion) or die ("requete ajouter non valide");
  
  //cr�ation du lien produit <-> utilisateur:
  require "./libstocks/produits_fonctions.inc";
  $IdProduit = cherche_IdProduit($form_NomProduit);
  $resultat = exec_requete (requete_sql_insert_into ("PrdtUtilisateurs", array('IdProduit' => $IdProduit, 'IdUtilisateur' => $objet_adminBD->IdUtilisateur)), $connexion) or die ("requete modifier non valide");
  
  if ($choix=="1") {
    $action="liste";
    $repertoire="produit";
  }
  else{
    $url_IdProduit = 0;
    $action="form";
    $repertoire="produit";
  }
}

//echo "coucou 	$repertoire $action $form_IdMouvement";

if (($repertoire=="produit") && ($action=="modifier") && ($form_IdProduit != 0)){
  
  //mise � jour du produit
  $resultat = exec_requete (requete_sql_update("Produits", array('IdProduit' => $form_IdProduit),  $tmp_tableau), $connexion) or die ("requete modifier non valide");
  
  //mise � jour des fournisseurs
  mise_a_jour_produit_fournisseur ($form_IdProduit, $connexion, $tab_IdFournisseur, $tab_supprimer_pf, $form_AjoutFournisseur, $tab_RefPrdtFournisseur, $tab_PrixPrdtFournisseur, $tab_RemisePrdtFournisseur, $tab_PrixRFournisseur);
  
  //mise � jour des utilisateurs
  mise_a_jour_produit_utilisateur ($form_IdProduit, $connexion, $tab_IdUtilisateur, $tab_supprimer_pu, $form_AjoutUtilisateur);
  
  if ($choix=="1") {
    $action="liste";
    $repertoire="produit";
  }
  else{
    $url_IdProduit = $form_IdProduit;
    $action="form";
    $repertoire="produit";
  }
}



//On a demander une suppression
if ( ($repertoire=="produit") && ($action=="supprimer") && ($url_IdProduit!=0)){
  
  if($confirmer==1){
    suppression_produit($url_IdProduit);
    
    $action="liste";
    $repertoire="mouvement";
  }
  else{
    //la suppresion est demand�e mais non confirm�e
    
    //Affichage de confirmation de supression
    
    $message="Etes vous certain de vouloir supprimer ce produit ?";
    echo html_confirmation($message, "index.php?repertoire=produit&action=supprimer&url_IdProduit=".$url_IdProduit."&confirmer=1","index.php?repertoire=produit&action=liste");
    
    pied_de_page ();
  }
}
	
?>