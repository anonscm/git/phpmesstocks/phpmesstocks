<?

/***************************************************************************
                          produit_liste.inc  - 
                   Affichage des produits en liste
                             -------------------
    begin                :  20/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

require "./libstocks/tab_utilisateurs.inc";
//require "./libcampus/tab_categorie.inc";
$tab_req_utilisateurs = charge_tableUtilisateurs();
$tab_req_utilisateurs[0] = "----";

 
require "./libphp/memoire_req.inc";
rappel_memoire_req($HTTP_SESSION_VARS, $repertoire, array ("req_IdUtilisateur", "req_NomProduit","req_Conditionnement","req_ProduitNotes", "req_CodeNomenclature", "req_NomFournisseur"));
rappel_liste_pos($HTTP_SESSION_VARS, $repertoire);

if ($req_IdUtilisateur == "") $req_IdUtilisateur = $objet_adminBD->IdUtilisateur;
//$tab_tri["ORDER BY u.NomUtilisateur"] = "Noms";
//$tab_tri["ORDER BY u.PrenomUtilisateur"] = "Pr�noms";

//$tri = " ORDER BY p.NomProduit";
//rappel_tri($HTTP_SESSION_VARS, $repertoire, "ORDER BY u.NomUtilisateur");

rappel_tri($HTTP_SESSION_VARS, $repertoire, "produits");

/************************************************
	FABRICATION de la barre de recherche/tri
*************************************************/

TblDebut("trirecherche");
//ligne de titres des colonnes:
		
TblDebutLigne();
/*	
TblDebutCellule();
// formulaire de s�lection du tri:
$form = new formulaire("post","index.php?repertoire=produit&action=liste",FALSE,"form",FALSE);
$form->debut_table(HORIZONTAL,1);	
$form->champ_liste_deroulante("Tri","tri",$tab_tri, $tri);
$form->champ_valider("","bouton","trier","trier");
	
$form->fin_table();

$form->fin();
TblFinCellule();
*/
TblDebutCellule();

 // forumulaire de recherche rapide:

$form = new formulaire("post","index.php?repertoire=produit&action=liste",FALSE,"form",FALSE);

$form->debut_table(HORIZONTAL,1);	
//$form->champ_texte("Nom","req_IUtilisateur",$req_NomUtilisateur,10);
//$form->champ_texte("Pr�nom","req_PrenomUtilisateur",$req_PrenomUtilisateur,10);

$form->champ_liste_deroulante("utilisateurs","req_IdUtilisateur",$tab_req_utilisateurs, $req_IdUtilisateur);

$form->champ_texte("Produit","req_NomProduit",$req_NomProduit,10);
$form->champ_texte("Conditionnement","req_Conditionnement",$req_Conditionnement,10);
$form->champ_texte("Nomenclature","req_CodeNomenclature",$req_CodeNomenclature,10);
$form->champ_texte("Notes","req_ProduitNotes",$req_ProduitNotes,10);
$form->champ_texte("Fournisseur","req_NomFournisseur",$req_NomFournisseur,10);
//$form->champ_cache("req_IdFournisseur",$req_IdFournisseur);

$form->champ_valider("","bouton","rechercher","rechercher");

$form->fin_table();

$form->fin();

TblFinCellule();

TblFinLigne();
TblFin();


/************************************************
	FIN de la barre de recherche/tri
*************************************************/


/************************************************
	FABRICATION de la requ�te
*************************************************/
//requete qui permetra d'afficher la liste
$objet_requete= new creer_requete();
$objet_requete->select(array("IdProduit", "NomProduit",  "Conditionnement", "Qrestante", "ProduitNotes", "CodeNomenclature"),"FROM","Produits","p");
//$objet_requete->select(array("IdUtilisateur"),"NATURAL LEFT JOIN","PrdtUtilisateurs","pu");
//$objet_requete->select(array("IdFournisseur"),"NATURAL LEFT JOIN","PrdtFournisseurs","pf");
//$objet_requete->select(array("IdFournisseur, NomFournisseur"),"NATURAL LEFT JOIN","Fournisseurs","f");
$objet_requete->select(array("IdUtilisateur"),"LEFT JOIN","PrdtUtilisateurs","pu", "USING (IdProduit)");
if ($req_NomFournisseur != "") ($objet_requete->select(array("IdFournisseur"),"LEFT JOIN","PrdtFournisseurs","pf", "USING (IdProduit)"));
if ($req_NomFournisseur != "") $objet_requete->select(array("IdFournisseur, NomFournisseur"),"LEFT JOIN","Fournisseurs","f", "USING (IdFournisseur)");
$objet_requete->fin_select();


//$objet_requete->ajout_egal ("pu.IdUtilisateur",$objet_adminBD->IdUtilisateur, "AND");
if ($req_IdUtilisateur != $objet_adminBD->IdUtilisateur) {
	$objet_adminBD->produitAjout=0;
	$objet_adminBD->produitModif=0;
	$objet_adminBD->produitSuppr=0;
}

if ($req_IdProduit > 0) $objet_requete->ajout_egal ("p.IdProduit",$req_IdProduit, "AND");

if ($req_IdUtilisateur > 0) $objet_requete->ajout_egal ("pu.IdUtilisateur",$req_IdUtilisateur, "AND");

//if ($req_IdFournisseur > 0) $objet_requete->ajout_egal ("pf.IdFournisseur",$req_IdFournisseur, "AND");
if ($req_NomFournisseur != "") $objet_requete->ajout_like("f.NomFournisseur",$req_NomFournisseur, "AND");

if ($req_NomProduit != "") $objet_requete->ajout_like ("p.NomProduit",$req_NomProduit, "AND");

if ($req_Conditionnement != "") $objet_requete->ajout_like ("p.Conditionnement",$req_Conditionnement, "AND");

if ($req_CodeNomenclature != "") $objet_requete->ajout_like ("p.CodeNomenclature",$req_CodeNomenclature, "AND");

if ($req_ProduitNotes != "") $objet_requete->ajout_like ("p.ProduitNotes",$req_ProduitNotes, "AND");

/*
if ($req_equipe > 0) $objet_requete->ajout_egal ("ue.IdEquipe",$req_equipe, "AND");

if ($req_equipe == 0) $objet_requete->ajout_egal ("ue.IdEquipe","NULL", "AND");

if ($req_IdCategorie > 0) $objet_requete->ajout_egal ("u.IdCategorie",$req_IdCategorie, "AND");
//if ($req_IdCategorie == 0) $objet_requete->ajout_egal ("u.IdCategorie",0, "AND");
*/
//$tri = " ORDER BY e.EchNumero,s.SeqNom";
if ($tri == "produits") {
  $objet_requete->ajout_tri("p.NomProduit");
}		
$requete = $objet_requete->fin_requete($connexion);


/************************************************
	fin de la FABRICATION de la requ�te
*************************************************/


$page_par_page = new objet_page($position,$objet_requete->_nblignes,$pas);

//execution de la requete si repertoire = machine et action = liste on execute la requete 

if (($repertoire=="produit") &($action=="liste")){
	$resultat = exec_requete_select ($page_par_page->modif_requete($requete), $connexion) or die ("requete non valide");
}
		
////on regarde les droits de celui qui s'est connect� � la base ces droits sont contenus dans l'objet global $objet_user	
	
// on implemente un nouvel objet deriv� specifique aux machines pour l'affichage de la liste
		
$listeresultat= new liste_derivee_produit ($resultat, "liste_produit", $page_par_page);
			
// suivant les droits de la personne qui s'est connnect�e on affiche ou pas les boutons ajouter modifier..


if ($objet_adminBD->produitAjout==1)
	$listeresultat->set_ajouter ("index.php?repertoire=produit&action=form&urlIdProduit=0", "ajouter un produit");

if ($objet_adminBD->produitModif==1)
	$listeresultat->set_modifier () ;
		
if ($objet_adminBD->produitSuppr==1)
	$listeresultat->set_supprimer (); 

				
	
if ($objet_requete->_nblignes=="0") {
	echo html_message ("Il n'y a pas de produit correspondant � votre demande","erreur");
	$listeresultat->afficher_liste();
}	
else
	$listeresultat->afficher_liste();
/*	        
//formulaire pour faire une recherche : on ajoute le bouton rechercher
 
$form = new formulaire("post","index.php?repertoire=utilisateur&action=form_recherche",FALSE,"",FALSE);
$form->debut_table(HORIZONTAL,1);	
if (($repertoire=="machine") &($action=="requete"))
	$form->champ_valider("","bouton","faire une nouvelle recherche","valider_liste");
	
else
	$form->champ_valider("","bouton","faire une  recherche","valider_liste");
	
$form->fin_table();

$form->fin();
*/
	
	
		
	






?>
