<?
/***************************************************************************
                      produit_fonctions.inc  -
           bibliotheque de fonctions specifiques aux produits
                                        -------------------
    begin                :  02/01/2003
    copyright            : (C) 2003 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
	 
//require "libphp/gestion_ip.inc";

if(!isset($module_produit_fonctions)){
  $module_produit_fonctions=1;
  
  require "libphp/sql_functions.inc";
  
  function cherche_utilisateur ($req_utilisateur, $connexion) {
    $objet_requete= new creer_requete();
    $objet_requete->select(array("IdUtilisateur","UIdentifiant"), "FROM", "Utilisateurs");
    $objet_requete->fin_select();
    
    $objet_requete->ajout_like ("UIdentifiant",$req_utilisateur, "AND");
    $requete = $objet_requete->fin_requete();
    
    $resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
    return ligne_suivante ($resultat);
  }
  
  function cherche_fournisseur ($req_fournisseur, $connexion) {
    $objet_requete= new creer_requete();
    $objet_requete->select(array("IdFournisseur","NomFournisseur"), "FROM", "Fournisseurs");
    $objet_requete->fin_select();
    
    $objet_requete->ajout_like ("NomFournisseur",$req_fournisseur, "AND");
    $requete = $objet_requete->fin_requete();
    
    $resultat = exec_requete ($requete, $connexion) or die ("requete non valide");	
    return ligne_suivante ($resultat);
  }
  
  //$tab_IdTelephone, $tab_NumTelephone  $tab_NomPiece  $tab_supprimer_tel  $tab_affichage_tel   $tab_TypeTelephone
  function mise_a_jour_produit_fournisseur ($IdProduit, $connexion, $tab_IdFournisseur, $tab_supprimer, $AjoutFournisseur, $tab_RefPrdtFournisseur, $tab_PrixPrdtFournisseur, $tab_RemisePrdtFournisseur, $tab_PrixRFournisseur) 
    {
      //modifier
      for ($i=0; $i < count ($tab_IdFournisseur); $i++) {
	$tab_PrixPrdtFournisseur[$i] = str_replace(",",".",$tab_PrixPrdtFournisseur[$i]);
	$tab_RemisePrdtFournisseur[$i] = str_replace(",",".",$tab_RemisePrdtFournisseur[$i]);
	$tab_PrixRFournisseur[$i] = str_replace(",",".",$tab_PrixRFournisseur[$i]);
	if ($tab_PrixRFournisseur[$i] > 0) {
	  if ($tab_RemisePrdtFournisseur[$i] == "") $tab_RemisePrdtFournisseur[$i] = (($tab_PrixPrdtFournisseur[$i] - $tab_PrixRFournisseur[$i]) / ($tab_PrixPrdtFournisseur[$i])) * 100;
	  if ($tab_PrixPrdtFournisseur[$i] == "") $tab_PrixPrdtFournisseur[$i] = ($tab_PrixRFournisseur[$i] * 100)/ (100 - $tab_RemisePrdtFournisseur[$i]);
	}
	$requete = requete_sql_update("PrdtFournisseurs", array('IdProduit' => $IdProduit, 'IdFournisseur' => $tab_IdFournisseur[$i]),  array("RefPrdtFournisseur" => $tab_RefPrdtFournisseur[$i], "PrixPrdtFournisseur" => $tab_PrixPrdtFournisseur[$i], "RemisePrdtFournisseur" => $tab_RemisePrdtFournisseur[$i]));
	$resultat = exec_requete ($requete, $connexion) or die ("requete modifier non valide");
      }
      
      //ajout d'un fournisseur (form_AjoutFournisseur)
      if ($AjoutFournisseur != "") {
	//ajout
	$tmp_tableau = array(
			     'IdProduit' => $IdProduit,
			     'RefPrdtFournisseur' => ""
			     );
	
	$ligne = cherche_fournisseur ($AjoutFournisseur, $connexion);
	if (is_object($ligne)) {
	  $tmp_tableau["IdFournisseur"] = $ligne->IdFournisseur;
	  
	  $resultat = exec_requete (requete_sql_insert_into("PrdtFournisseurs", $tmp_tableau), $connexion) or die ("requete ajouter non valide");
	}
	else {
	  echo html_message("Le fournisseur (".$AjoutFournisseur.") n'a pas �t� trouv�", "erreur");
	}
      }
      
      //supprimer des fournisseurs pour ce produit
      for ($i=0; $i < count ($tab_supprimer); $i++) {
	if ($tab_supprimer[$i] < (count ($tab_IdFournisseur))) {
	  $resultat = exec_requete (requete_sql_delete("PrdtFournisseurs",array('IdProduit' => $IdProduit , 'IdFournisseur' => $tab_IdFournisseur[$tab_supprimer[$i]])), $connexion) or die ("requete effacer non valide");				
	}
      }
      
    }
  
  function suppression_produit($IdProduit){
    
    $resultat = exec_requete (requete_sql_delete("PrdtFournisseurs", array('IdProduit' => $IdProduit)), $GLOBALS["connexion"]) or die ("requete modifier non valide");
    $resultat = exec_requete (requete_sql_delete("PrdtUtilisateurs", array('IdProduit' => $IdProduit)), $GLOBALS["connexion"]) or die ("requete modifier non valide");
    $resultat = exec_requete (requete_sql_delete("Mouvements", array('IdProduit' => $IdProduit)), $GLOBALS["connexion"]) or die ("requete modifier non valide");
    $resultat = exec_requete (requete_sql_delete("Produits", array('IdProduit' => $IdProduit)), $GLOBALS["connexion"]) or die ("requete modifier non valide");
    
    return ($resultat);
    
  }
  
  function mise_a_jour_produit_utilisateur ($IdProduit, $connexion, $tab_IdUtilisateur, $tab_supprimer, $AjoutUtilisateur) 
    {
      //modifier
      //for ($i=0; $i < count ($tab_IdUtilisateur); $i++) {
      //	$requete = requete_sql_update("PrdtFournisseurs", array('IdProduit' => $IdProduit, 'IdFournisseur' => $tab_IdFournisseur[$i]),  array("RefPrdtFournisseur" => $tab_RefPrdtFournisseur[$i]));
      //	$resultat = exec_requete ($requete, $connexion) or die ("requete modifier non valide");
      //}
      
      //ajout d'un utilisateur (form_AjoutUtilisateur)
      if ($AjoutUtilisateur != "") {
	//ajout
	$tmp_tableau = array(
			     'IdProduit' => $IdProduit
			     );
	
	$ligne = cherche_utilisateur ($AjoutUtilisateur, $connexion);
	if (is_object($ligne)) {
	  $tmp_tableau["IdUtilisateur"] = $ligne->IdUtilisateur;
	  
	  $resultat = exec_requete (requete_sql_insert_into("PrdtUtilisateurs", $tmp_tableau), $connexion) or die ("requete ajouter non valide");
	}
	else {
	  echo html_message("L'utilisateur (".$AjoutUtilisateur.") n'a pas �t� trouv�", "erreur");
	}
      }
      
      //supprimer des utilisateurs pour ce produit
      for ($i=0; $i < count ($tab_supprimer); $i++) {
	if (count ($tab_IdUtilisateur)==count ($tab_supprimer)){
	  echo html_message("il doit y avoir au moins un utilisateur par produit","erreur");
	  break;
	}
	if ($tab_supprimer[$i] < (count ($tab_IdUtilisateur))) {
	  $resultat = exec_requete (requete_sql_delete("PrdtUtilisateurs",array('IdProduit' => $IdProduit , 'IdUtilisateur' => $tab_IdUtilisateur[$tab_supprimer[$i]])), $connexion) or die ("requete effacer non valide");				
	}
      }	
    }
}
	
?>




