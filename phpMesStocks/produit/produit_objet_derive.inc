<?

/***************************************************************************
                          produit_objet_derive.inc  - 
                   Affichage des produits
                             -------------------
    begin                :  20/11/2002
    copyright            : (C) 2002 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

  
 //on fait une classe liste derivee_machine a partir de la classe objet_liste_resultat 
// on va pouvoir afficher la liste
//require "libphp/gestion_ip.inc";



if (!isset($module_liste_derivee_produit)){
	
	$module_liste_derivee_produit = 1;
	
	require "libphp/objet_liste_resultat.inc";
	
	class	liste_derivee_produit extends liste_resultat {
		
		function liste_derivee_produit ($resultat, $nom, $page) {
			//constructeur
			$this->liste_resultat($resultat,$nom, $page);
			
		}
	
//on affiche les titres	
				
		function afficher_effectif() {
			//pour afficher le nombre de lignes sélectionnnées
			
			echo html_message($this->_nb_lignes." produits sélectionnés","info");
		}

		function afficher_titres(){
			
			TblCellule ("Nom");
			TblCellule ("mouvements");
			TblCellule ("Conditionnement");
			TblCellule ("Quantité");
			TblCellule ("Code<br/>Nomenclature");
			TblCellule ("Notes");
			
		}

			//on met les données de la tables machine dans la liste	
		function afficher_ligne($row){
						
		// IdProduit     NomProduit     Conditionnement     Qrestante     ProduitNotes
			//if ($GLOBALS["req_IdUtilisateur"]==$GLOBALS["AdminBD"]->IdUtilisateur)
			TblCellule ($row->NomProduit);
			TblCellule (ancre("index.php?repertoire=mouvement&action=liste&req_IdProduit=".$row->IdProduit."&req_IdUtilisateur=".$GLOBALS["req_IdUtilisateur"],"liste"));
			TblCellule ($row->Conditionnement);
			TblCellule ($row->Qrestante);
			TblCellule ($row->CodeNomenclature);
			TblCellule ($row->ProduitNotes);
					
			//On ajoute en fin de ligne les boutons Versions Modifier, supprimer	
			
			$this->ajouter_case_modifier("fiche du produit","index.php?repertoire=produit&action=form&url_IdProduit=".$row->IdProduit);
				
			$this->ajouter_case_supprimer("Supprimer","index.php?repertoire=produit&action=supprimer&url_IdProduit=".$row->IdProduit);
				//bouton selectionner
			
			$this->ajouter_case_selectionner ("Selectionner","index.php?repertoire=produit&action=selectionner&url_IdProduit=$row->IdProduit");
			
			//$this->_tabliste->afficher_ligne();
		}
	}
}	

?>