<?php


/***************************************************************************
                      index.php  -
           point d'entrée dans l'application phpMesStocks
                             -------------------
    begin                :  12/09/2005
    copyright            : (C) 2005 by Olivier Langella
    email                : Olivier.Langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// index.php

define('APP_ROOT_RELATIVE_PATH', './');
define('HOME_PAGE', APP_ROOT_RELATIVE_PATH.'index.php');

require_once (APP_ROOT_RELATIVE_PATH.'shared_lib/php/generic/magic_quotes_off.inc.php');
require_once (APP_ROOT_RELATIVE_PATH."libpms/version.inc");
require_once (APP_ROOT_RELATIVE_PATH."libpms/settings.inc");
//require_once (APP_ROOT_RELATIVE_PATH."libpms/tableaux_communs.inc");
require_once (APP_ROOT_RELATIVE_PATH.'web_view/lib/php/generic/html_functions.inc.php');

session_start();
//to ensure that the current session is really a "phpMyCampus" session
if (array_key_exists('SOFTWARE_NAME', $_SESSION)) {
	if ($_SESSION['SOFTWARE_NAME'] != SOFTWARE_NAME) {
		session_destroy();
		session_start();
		$_SESSION['SOFTWARE_NAME'] = SOFTWARE_NAME;
	}
} else {
	session_destroy();
	session_start();
	$_SESSION['SOFTWARE_NAME'] = SOFTWARE_NAME;
}

require_once (APP_ROOT_RELATIVE_PATH."libpms/xhtml_pms_page.inc");
require_once (APP_ROOT_RELATIVE_PATH."libpms/xhtml_pms_form.inc");
$page = new xhtml_pms_page();

require_once (APP_ROOT_RELATIVE_PATH."./libpms/db_functions.inc");
require_once (APP_ROOT_RELATIVE_PATH."./libpms/db_select_pmc.inc");
$connect_error = '';
$db_link = db_connect($connect_error);

if ($db_link == false) {
	$page->xhtml_message($connect_error, 'error');
	//$page->xhtml_message('Consultez le manuel d\'installation pour corriger: '.html_anchor('','installation'), 'information');
	//require (APP_ROOT_RELATIVE_PATH.'view/installation.inc');
	$page->xhtml_pms_page_display();
	exit;
}

$page->goto_zone('pms_body');

//IDENTIFICATION if needed
if (array_key_exists("session", $_GET)) {
	//destroy session
	// session_start();
	if ($_GET["session"] == "logout") {
		session_destroy();
		session_start();
	} else
		if ($_GET["session"] == "login") {
			require (APP_ROOT_RELATIVE_PATH."dba/login.inc");
		}
}

// if the user is not identified or identification failed: display the login form
if (array_key_exists("view", $_GET) and (($_GET['view'] == 'credits') or ($_GET['view'] == 'installation'))) {
	//no login required for these views
} else {
	if (!array_key_exists('s_person_id', $_SESSION)) {
		require (APP_ROOT_RELATIVE_PATH.'web_view/login.inc');
		$page->xhtml_pms_page_display();
		exit;
	}
}

// HERE, $_SESSION["s_labo_id"] is -1 if no labo is selected and contains the labo id if it has been selected

require APP_ROOT_RELATIVE_PATH."libpms/visitor.inc";
$visitor = array ();
$error_message = '';
if (array_key_exists('s_person_id', $_SESSION)) {
	if (!set_visitor($db_link, $visitor, $error_message, $_SESSION['s_person_id'])) {
		$page->xhtml_message($error_message, 'error');
	}
} else {
	set_visitor($db_link, $visitor, $error_message);
}

if ($visitor['pms_id'] > 0) {
	if ($visitor['stock_id'] == '') {
		//print_r($_GET);
		if (array_key_exists('choixstock_id', $_GET) and (set_visitor_stock($db_link, $visitor, $error_message, $_GET['choixstock_id']) == true)) {
		} else {
			//problème l'utilisateur n'a pas de stock à gérer: choisir un stock
			$page->goto_zone('pms_body');
			$page->xhtml_message('Vous n\'avez pas choisi de stock à gérer.', 'error');
			$page->xhtml_message('Vous devez choisir un stock, ou le créer', 'error');
		}

	}
	//HERE, an array $visitor exists containing all usefull informations about the current user
	$page->xhtml_pms_page_set_title(SOFTWARE_NAME." page principale");
	$page->goto_zone('visitor_status');
	$page->xhtml_message('Utilisateur: '.$visitor['last_name'], 'visitor');
	if ($visitor['stock_id'] > 0) {
		$page->xhtml_message('Stock: '.$visitor['stock'], 'visitor');
	}

	if (array_key_exists("hmenu", $_GET)) {
		$page->display_identified_menus($visitor, $_GET['hmenu']);
	} else {
		$page->display_identified_menus($visitor, 'gestion');
	}
	require APP_ROOT_RELATIVE_PATH.'libpms/db_access.inc';

}

$page->goto_zone("pms_body");
if (array_key_exists("view", $_GET)) {
	if ($_GET["view"] != -1) {
		$array_directory = split('_', $_GET['view']);

		//vérification des droits
		$ok = false;
		if (($array_directory[0] != 'stock') and ($visitor['stock_id'] == '')) {
			//l'utilisateur n'a pas choisi/créer de stock
			$_GET['view'] = 'stock_choix';
			$array_directory = split('_', $_GET['view']);
			$ok = true;
		} else {
			if (count($array_directory) == 1) {
				if ($visitor['pms_id'] == '') {
					$page->display_not_identified_menus();
				}
				$ok = true;
			} else {
				if (array_key_exists($array_directory[0].'Vue', $visitor) and ($visitor[$array_directory[0].'Vue'])) { //ok
					$ok = true;
				}
			}
		}

		if ($ok) {

			//includes file to dipslay the protic body
			$tmp_file = APP_ROOT_RELATIVE_PATH.'web_view/'.str_replace('_', '/', $_GET['view']).'.inc';
			if (file_exists($tmp_file)) {
				require $tmp_file;
			} else {
				$page->xhtml_message('Ce fichier : '.$tmp_file.' n\'existe pas, il faut contacter l\'administrateur de '.SOFTWARE_NAME, 'error');
			}
		} else {
			$page->xhtml_message('Vous n\'avez pas le droit de consulter cette page', 'error');
		}
	}
}

db_close($db_link);

$page->xhtml_pms_page_display();
?>
