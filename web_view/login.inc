<?php

// view/login.inc

$page->xhtml_page_set_title("login de phpMesStocks");

$page->xhtml_message('Login', 'form_title');
$form = new xhtml_form($page, 'pms_login');
//$form->xhtml_form_set("post", HOME_PAGE."?session=login&view=labo_choix");
$form->xhtml_form_set('post', HOME_PAGE.'?session=login&hmenu=gestion&view=produit_liste');
$form->set_display_free("div");
$form->new_zone_fieldset("select_login", "formulaire de connexion à ".SOFTWARE_NAME);
$form->set_lazy_mode($_POST);

$form->input_text("login", "login", "");
$form->add_check_fill('login', 'Hep ! votre login SVP ...');
$form->input_password("password", "password", "");

$form->input_submit("envoyer");

$page->goto_zone('pms_body');

//$text = SOFTWARE_NAME." is a new web-based application designed for large-scale plant proteomic programs. This software allows to store and query data related to: (i) the experimental design (including plant, protein sample description and 2D-PAGE protocol), (ii) the behaviour of individual spots (in respect to qualitative and quantitative protein expression), and (iii) the identification of the proteins. Database feeding can be achieved by uploading different formatted files or using web forms. The query of data proceeds from a graphical tool that uses a clickable annotated image of the 2D-gel. Links to external databases are available. The biological relationship between spots (e.g. allelic variants, identical function, post-translational modification) is defined, controlled and stored; eventually a network of the connected spots is made available. Furthermore, to allow different experiments to be jointly analyzed (e.g. to obtain the correlation between protein expressions), different types of objects (plants, samples, gels, spots) can be aggregated. ".SOFTWARE_NAME." is based on the Oracle or the PostgreSQL DBMS and is available upon request.";
//$page->xhtml_message($text, "text");

//echo "coucou";
?>