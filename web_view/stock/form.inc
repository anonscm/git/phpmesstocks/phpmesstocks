<?php

/** \brief formulaire d'édition/création d'un stock
 *
 */



if (array_key_exists('NomStock', $_POST)) { //on a déjà   posté le formulaire, on reprend les données du post
	$res = $_POST;
} else { // c'est nouveau:
	$res = false;
}

/*
 REQUETE pour rechercher la fiche
 */

$IdStock = 0;
if (array_key_exists('IdStock', $_GET))
$IdStock = $_GET['IdStock'];
else
if (array_key_exists('IdStock', $_POST))
$IdStock = $_POST['IdStock'];

if ($IdStock > 0) {
	$dba = 'update';

	if ($res == false) {
		$objet_requete = new db_select_pmc();

		$objet_requete->select_from(array ('IdStock','NomStock'), 'Stocks','s');

		$objet_requete->where_equal('s.IdStock', $IdStock);

		$objet_requete->execute_select($db_link);
		if ($objet_requete->is_sql_error()) {
			$page->xhtml_message($objet_requete->get_error(), 'error');
			return;
		}
		$res = $objet_requete->get_result_first();
	} else {
	}
} else {
	$dba = 'new';
	$res = array ();
	$res['IdUtilisateur'] = $visitor['pms_id'];
}
/*
 FIN de la REQUETE
 */
$form = new xhtml_pms_form($page, 'stock_form');
$form->xhtml_form_set("post", HOME_PAGE.'?hmenu=gestion&view=stock_choix'); //&IdAdminBD='.$req_IdAdminBD);

$form->input_hidden('dba', $dba);
$form->input_hidden('dbobject', 'stock_form');
$form->input_hidden('back2view', '');
$form->input_hidden('go2view', '');

//print_r($res);
$form->set_lazy_mode($res);
$form->set_display_vtable();

$form->input_hidden('IdStock', '');
$form->input_hidden('IdUtilisateur', '');
$form->input_text('Nom du stock :', 'NomStock', '', 20);
$form->add_check_fill('NomStock', 'Le nom du stock est requis');

$form->input_submit('enregistrer');

?>