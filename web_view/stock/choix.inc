<?php


/** \brief choix d'un stock
 *
 */

/*
 REQUETE pour fabriquer la liste
 */

$objet_requete = new db_select_pmc();

$objet_requete->select_from(array ('IdStock', 'NomStock'), 'Stocks', 's');
$objet_requete->left_join(array (), 'StockUtilisateur', 'su', 'IdStock');
$objet_requete->where_equal('su.IdUtilisateur', $visitor['pms_id']);

$objet_requete->order_by('s.NomStock');


$nb_lignes = $objet_requete->count_rows($db_link);
//echo $nb_lignes;
if ($objet_requete->is_sql_error()) {
	$page->xhtml_message($objet_requete->get_error(), 'error');
}

/*
 FIN de la REQUETE
 */
$page->xhtml_message(html_anchor(HOME_PAGE.'?hmenu=gestion&view=stock_form&IdStock=0', 'nouveau stock'), '');
//	echo $objet_requete -> get_sql_string();

if ($nb_lignes > 0) {
	require_once (APP_ROOT_RELATIVE_PATH.'libpms/xhtml_pmc_list.inc');
	$liste = new xhtml_pmc_list($page, 'stock_choix');
	$liste->xhtml_form_set('post', HOME_PAGE.'?hmenu=gestion&view=stock_choix');

	$objet_requete->limit($liste->_position, $liste->_offset, $nb_lignes);

	$objet_requete->execute_select($db_link);
	//print_r ($res);

	$liste->list_pmc_browse($nb_lignes);
	if ($objet_requete->is_sql_error()) {
		echo $objet_requete->get_sql_string();
	} else {
		if ($res = $objet_requete->get_result()) {
			$liste->set_display_htable(count($res['IdStock']));
			$liste->set_htable_row_diff('NomStock[]');

			$liste->set_lazy_mode($res);

			$liste->input_message("NomStock", "NomStock[]", "");
			$lien = HOME_PAGE.'?hmenu=gestion&view=produit_form&choixstock_id=';
			$liste->input_list_link('', 'select[]', $lien, $res['IdStock'], 'sélectionner');
			if ($visitor['stockModif']) {
				$lien = HOME_PAGE.'?hmenu=gestion&view=stock_form&IdStock=';
				$liste->input_list_link('', 'modifier[]', $lien, $res['IdStock'], 'modifier');
			}
			if ($visitor['stockSuppr']) {
				$lien = HOME_PAGE.'?hmenu=gestion&view=stock_choix&dba=delete&dbobject=stock_form&IdStock=';
				$liste->input_list_link('', 'supprimer[]', $lien, $res['IdStock'], 'supprimer');
			}
		}
	}
}
?>