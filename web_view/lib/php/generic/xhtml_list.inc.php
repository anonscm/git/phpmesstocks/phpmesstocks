<?php


/** \file web_view/lib/php/generic/xhtml_list.inc.php
 * \brief object to easily build lists, derived from xhtml_form.inc
 * 
*
* \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
* \date 21/9/2004
*/

class xhtml_list extends xhtml_form {
	var $_position;
	var $_offset;
	var $_total;

	function xhtml_list(& $xhtmlpage, $pname) {
		$this->xhtml_form(& $xhtmlpage, $pname);
	}

	function list_browse($total, $position, $offset, $list_of = '') {
		//
		/*
		$this->_position = $position;
		$this->_offset = $offset;
		$this->_total = $total;
		*/
		if ($position < 0)
			$position = 0;
		if ($position >= $total)
			$position = $total -1;
		$this->set_display_free("span", "list_nav");
		$this->input_hidden("list_position", $position);
		// $this->input_text(" ","list_pas", $pas);
		$to = $position + $offset;
		if ($to > $total)
			$to = $total;

		$this->xhtml_message($list_of . ' ' . ($position +1) . '-' . $to . ' of ' . $total, 'list_position');
		$this->add_post_button("previous", "previous", -1, $param = array (
			"list_position" => ($position - $offset
		)));
		$this->input_text(" ", "list_offset", $offset, 3);
		$this->add_post_button("next", "next", -1, $param = array (
			"list_position" => ($position + $offset
		)));

		$this->_position = $position;
		$this->_offset = $offset;
		$this->_total = $total;

	}

	function input_list_link($title_column, $http_var_name, $link_prefix, $arr_add_link_values, $arr_link_title) {
		$arr_link = array ();
		for ($i = 0; $i < count($arr_add_link_values); $i++) {
			if (is_array($arr_link_title))
				$arr_link[$i] = html_anchor($link_prefix . $arr_add_link_values[$i], $arr_link_title[$i]);
			else
				$arr_link[$i] = html_anchor($link_prefix . $arr_add_link_values[$i], $arr_link_title);
		}
		$this->input_message($title_column, $http_var_name, $arr_link);

	}

}
?>