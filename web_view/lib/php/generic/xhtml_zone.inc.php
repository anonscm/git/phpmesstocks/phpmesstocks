<?php


/** \file web_view/lib/php/generic/xhtml_zone.inc.php
 * \brief object to navigate in an xhtml document
 * 
*
* \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
* \date 13/01/2004
*/

/* xhtml_zone object interface:
xhtml_zone() //constructor

//set the current_node pointer to the zone named $name
goto_zone($name)
//create a new zone in the tag $tag named $name, and set the pointer to it
new_zone($tag, $name, $class=-1)
//create a new div zone named $name, and set the pointer to it
new_zone_div($name, $class=-1)
//create a new span zone named $name, and set the pointer to it
new_zone_span($name, $class=-1)
//create a new ul (unordered list) zone named $name, and set the pointer to it
new_zone_ul($name, $class=-1)
*/

/** \brief object to navigate in an xhtml document.
*
* define and name "zones" in your xhtml document, and move the "current_node" between those zones
*/

class xhtml_zone extends xhtml_base {
	/** \brief private: array to store xml pointer references and the name corresponding to a zone
	*/
	var $_tab_nodes;
	/** \brief private: if this boolean is true, "goto_zone" are not possible 
	*/
	var $_lock_zone;

	/** \brief constructor
	*/
	function xhtml_zone() {
		$this->xhtml_base();
		$this->_lock_zone = false;
	}

	/** \brief go to a named zone
	*
	* set the current node of the xhtml document to the node that was previously named $name
	* \param $name name of the zone to go to
	*/
	function goto_zone($name) {
		$this->priv_goto_zone($name);
	}

	/** \brief private: go to a named zone
	*/
	function priv_goto_zone($name) {
		$name = $this->priv_get_unique_id($name);
		//$this->xhtml_message($name);
		if (!array_key_exists($name, $this->_tab_nodes)) {
			$this->xhtml_message("ERROR: this zone doesn't exists: " . $name, "error");
			return;
		}
		if ($this->_lock_zone == false)
			$this->_currentnode = & $this->_tab_nodes[$name];
		else { //there is a human error
		}
	}

	/** \brief create and name a new zone
	*
	* \param $tag the tag to create for this zone (div, ul, span...)
	* \param $name name of the zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone($tag, $name, $class = -1) {
		$this->priv_new_zone($tag, $name, $class);
	}

	/** \brief creates a new zone with the fieldset xhtml tag
	*
	* creates an xhtml element "fieldset" and the corresponding zone (Cf: the xhtml_zone documentation), to eaysily move the current node.
	* \param $name is the name of the fieldset, needed to identify this widget and move between zones
	* \param $legend is the title displayed for this zone
	* \param $class is the CSS class, optional
	*/
	function new_zone_fieldset($name, $legend, $class = -1) {
		$this->priv_new_zone('fieldset', $name, $class = -1);
		if ($legend != '') {
			$node = & $this->_xhtmldoc->create_element('legend');
			$node->set_content(utf8_ensure($legend));
			$this->_currentnode->append_child($node);
		}
	}

	/** \brief create and name a new div zone
	*
	* \param $name name of the div zone to create
	* \param $class the CSS class name, optional
	* \return xhtml_id of this element in the page
	*/
	function new_zone_div($name, $class = -1) {
		$this->priv_new_zone('div', $name, $class);
		return ($this->priv_get_unique_id($name));
	}

	/** \brief create and name a new span zone
	*
	* \param $name name of the span zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone_span($name, $class = -1) {
		$this->priv_new_zone('span', $name, $class);
	}

	/** \brief create and name a new ul zone
	*
	* ul is an unordered list in xhtml
	* \param $name name of the ul zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone_ul($name, $class = -1) {
		$this->priv_new_zone('ul', $name, $class);
	}
	function new_zone_ol($name, $class = -1) {
		$this->priv_new_zone('ol', $name, $class);
	}
	/** \brief create and name a new li zone
	*
	* li is a list element in xhtml
	* \param $name name of the li zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone_li($name, $class = -1) {
		$this->priv_new_zone('li', $name, $class);
	}
	/** \brief create and name a new table zone
	*
	* \param $name name of the table zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone_table($name, $class = -1) {
		$table = & $this->_xhtmldoc->create_element('table');
		$this->_currentnode->append_child($table);
		//  $table->append_child($tbody);
		$this->_currentnode = & $table;

		$this->priv_new_zone('tbody', $name, $class);
	}
	/** \brief create and name a new tr zone
	*
	* tr is a table row
	* \param $name name of the tr zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone_tr($name, $class = -1) {
		$this->priv_new_zone('tr', $name, $class);
	}
	/** \brief create and name a new td zone
	*
	* td is a table cell
	* \param $name name of the td zone to create
	* \param $class the CSS class name, optional
	*/
	function new_zone_td($name, $class = -1) {
		$this->priv_new_zone('td', $name, $class);
	}

	/** \brief add list elements to a particular ul zone
	*
	* add list elements from the array $arr_li to a previously named ul zone
	* if the array is a hastable, it creates automatically named zones for the concerned li elements
	* \param $name name of the ul zone where the list elements will be inserted
	* \param $arr_li 
	* \param $class the CSS class name, optional
	*/
	function zone_ul_add_li($name, $arr_li, $class = -1) {
		//$this->xhtml_message('Please avoid zone_ul_add_li in your xhtml page','debug');
		$current = "-1";
		if (is_array($arr_li)) {
			reset($arr_li);
			while (list ($i, $content) = each($arr_li)) {
				//for ($i = 0; $i < count($array); $i++){
				// if ($array[$i] != "") {
				if ($content != "") {
					$li = & $this->_xhtmldoc->create_element("li");
					//$this->xhtml_insert($array[$i],$li);
					$this->priv_xhtml_insert($content, $li);
					if (is_string($i)) {
						$this->_tab_nodes[$i] = & $li;
					}
					$this->_tab_nodes[$name]->append_child($li);
				}
			}
		}
	}

	/** \brief private: creates and name a new zone
	*/
	function priv_new_zone($tag, $name, $class = -1) {
		$name = $this->priv_get_unique_id($name);
		$node = & $this->_xhtmldoc->create_element($tag);
		$node->set_attribute("id", $name);
		if ($class != -1)
			$node->set_attribute("class", $class);
		$this->_currentnode->append_child($node);
		$this->_tab_nodes[$name] = & $node;
		$this->_currentnode = & $node;
	}

	/** \brief private: generates unique id
	*
	* \param $name the name to convert into a unique id for the xhtml document
	*/
	function priv_get_unique_id($name) {
		return ($name);
	}

}
?>