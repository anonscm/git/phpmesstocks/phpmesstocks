<?php


/** \file web_view/lib/php/generic/html_functions.inc.php
 * \brief basic html functions
 *
 * derived from the work of Philippe Rigaud
 *
 * \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 * \date 19/1/2002
 */

//require_once (APP_ROOT_RELATIVE_PATH . 'shared_lib/php/generic/utf8.inc.php');

/** \brief generate an anchor tag "a" with "infobulle"
 *
 * \param $url the url to point to
 * \param $libelle the title of the link
 * \param $classe optional, the CSS class ot the tag OR a hash table with all possible optional parameters
 * \return string html well formed tag
 */
function html_anchor_info($url, $libelle, $info_title, $info_alt = null, $classe = null) {
	if (($url == '') & ($libelle == ''))
	return '';
	$url = str_replace('&', '&amp;', $url);
	//$url = urlencode($url);
	if (is_array($classe)) {
	} else {
		if ($classe != null) {
			$classe = array (
				'class' => $classe
			);
		} else {
			$classe = array ();
		}
	}
	$tag = '<a href="' . $url . '" title="' . $info_title . '"';
	if ($info_alt != null) {
		$tag .= ' alt="' . $info_alt . '"';
	}
	foreach ($classe as $name => $value) {
		$tag .= ' ' . $name . '="' . $value . '"';
	}
	return $tag . '>' . $libelle . '</a>';

}

/** \brief generate an anchor tag "a" with parameters
 *
 * \param $url the url to point to
 * \param $libelle the title of the link
 * \param $options a hash table containing optional parameters of a "a" tag
 * \return string html well formed tag
 */
function html_anchor_options($url, $content, $arr_options = array ()) {
	if (($url == '') & ($content == ''))
	return '';
	$url = str_replace('&', '&amp;', $url);

	$options = '';
	foreach ($arr_options as $key => $val) {
		$options .= ' ' . $key . '="' . $val . '"';
	}
	return '<a href="' . $url . '"' . $options . '>' . $content . '</a>';

}

/** \brief generate an anchor tag "a" with parameters
 *
 * \param $url the url to point to
 * \param $libelle the title of the link
 * \param $classe optional, the CSS class ot the tag OR a hash table with all possible optional parameters
 * \return string html well formed tag
 */
function html_anchor($url, $libelle, $classe = -1) {
	if (($url == '') & ($libelle == ''))
	return '';
	$url = str_replace('&', '&amp;', $url);

	$options = '';
	if (is_array($classe)) {
		foreach ($classe as $name => $value) {
			$options .= ' ' . $name . '="' . $value . '"';
		}
	} else {
		if ($classe != -1)
		$options = ' class="' . $classe . '"';
	}
	return '<a href="' . $url . '"' . $options . '>' . $libelle . '</a>';

}

/** \brief generate an anchor tag "a" with parameters, the link is open in a new windows (or tab)
 *
 * \param $url the url to point to
 * \param $libelle the title of the link
 * \param $classe optional, the CSS class ot the tag OR a hash table with all possible optional parameters
 * \return string html well formed tag
 */
function html_anchor_nw($url, $libelle, $classe = -1) {
	if (($url == '') & ($libelle == ''))
	return '';
	$url = str_replace('&', '&amp;', $url);

	$options = '';
	if (is_array($classe)) {
		foreach ($classe as $name => $value) {
			$options .= ' ' . $name . '="' . $value . '"';
		}
	} else {
		if ($classe != -1)
		$options = ' class="' . $classe . '"';
	}
	return '<a href="' . $url . '"' . $options . ' onclick="window.open(this.href); return false;">' . $libelle . '</a>';

}

/** \brief generate a code tag
 *
 * \param $code the code to display
 * \param $parameters a hash table with all possible optional parameters
 * \return html code in a string
 */
function html_code($code, $parameters = array ()) {

	//$alt => obligatoire pour le XHTML
	$tag = '<code';

	foreach ($parameters as $name => $value) {
		$tag .= ' ' . $name . '="' . $value . '"';
	}
	$tag .= '>'.$code.'</code>';

	return $tag;
}

/** \brief generate an img tag
 *
 * \param $url the location of the image to display
 * \param $alt an alternative text to display if the image is not supported by the browser or not downloaded
 * \param $parameters a hash table with all possible optional parameters
 * \return html code in a string
 */
function html_image($url, $alt, $parameters = array ()) {

	//$alt => obligatoire pour le XHTML
	$tag = '<img src="' . $url . '" alt="' . $alt . '"';

	foreach ($parameters as $name => $value) {
		$tag .= ' ' . $name . '="' . $value . '"';
	}
	$tag .= '/>';

	return $tag;
}

/** \brief generate an object tag to display SVG (Scalable Vector Graphic)
 *
 * \param $url the location of the SVG file to display
 * \param $width the width of the object in pixels
 * \param $height the height of the object in pixels
 * \return html code in a string
 */

function html_svg($url, $width, $height) {

	$type = 'image/svg+xml';
	//$alt => obligatoire pour le XHTML
	$tag = '<object data="' . $url . '" width="' . $width . '" height="' . $height . '" type="' . $type . '"';
	$tag .= '/>';

	return $tag;
}

/** \brief generate a simple div tag
 *
 * \param $ligne the string to display
 * \param $classe the css class of the div tag
 * \return html code in a string
 */

function html_message($ligne, $classe = -1) {
	if ($classe == '')
	$classe = -1;
	if ($classe == -1)
	return ('<div>' . $ligne . '</div>');
	else
	return ('<div class="' . $classe . '">' . $ligne . '</div>');
}
?>