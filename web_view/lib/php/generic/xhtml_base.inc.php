<?php


/** \file web_view/lib/php/generic/xhtml_base.inc.php
 * \brief base object to navigate in an xhtml document
 *
 *
 * \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 * \date 19/01/2004
 */

/* xhtml_base interface
 xhtml_base() //constructor

 get_xhtml_doc() // get xml document pointer
 get_current_node() // get pointer to the current xml node in the document

 // methods to produce xhtml code:
 xhtml_hr($class=-1)
 xhtml_br($class=-1)
 xhtml_header($title, $level = 0, $class = -1)
 // produce an "ul" list from the array
 xhtml_ul($array, $class=-1, $parameters=array())
 // insert a "div" section containing the given text at the current node
 xhtml_message($text, $class=-1)
 // same as xhtml_message, but with explicit "div" tag
 xhtml_insert_div($html_text, $class=-1)
 // insert a "span" section containing the given text
 xhtml_insert_span($html_text, $class=-1)
 // insert any xhtml tag the user want in the document, at the current node or the given node
 // beware that the $html_text have to be a well formed node
 xhtml_insert($html_text, $insert_node = -1)
 */

//modified by Olivier Langella 07/06/2004 to add xhtml_header
/*
 if (!extension_loaded('domxml')) {
 die("The extension 'domxml' couldn't be found.\n"." This extension is only available if PHP was configured with --with-dom[=DIR]. DIR is the libxml version 2.4.14 or superior base install directory\n");
 exit;
 //	return false;
 }
 */

if (APP_ROOT_RELATIVE_PATH != '') {
	require_once (APP_ROOT_RELATIVE_PATH . 'shared_lib/php/generic/utf8.inc.php');
}

/** \brief base object to navigate in an xhtml document.
 *
 * provides a base object that is used to produce basic xhtml tags
 */

class xhtml_base {
	/**
	 this is a reference to the xhtml document, consider this as private
	 */
	var $_xhtmldoc;
	/**
	 this is a reference to the current node of the xhtml document, consider this as private
	 */
	var $_currentnode;

	/** \brief this constructor should be called only by other xhtml_objects.
	 *
	 * this is used as a base constructor by xhtml_page, xhtml_form, xhtml_list, xhtml_table
	 */
	function xhtml_base() {
		$this->_lock_zone = false;
	}

	/**
	 \return the pointer to the current xhtml document
	 */
	function & get_xhtml_doc() {
		return $this->_xhtmldoc;
	}

	/**
	 \return the pointer to the current xhtml node
	 */
	function & get_current_node() {
		return $this->_currentnode;
	}

	function priv_no_amp($text) {
		//return(str_replace("& ","&amp;",$text));
	}

	/** \brief inserts a horizontal rule tag <hr/>
	
	\param $class is the CSS class to use
	*/

	function xhtml_hr($class = -1) {
		$node = & $this->_xhtmldoc->create_element("hr");
		if ($class != -1)
			$node->set_attribute("class", $class);
		$this->_currentnode->append_child($node);
	}

	/** \brief inserts a header tag h1, h2 .. hlevel
	
	\param $title is the string used as header
	\param $level is the type of header, 1 is bigger than 2 bigger than 3
	\param $class is the CSS class to use
	*/
	function xhtml_header($title, $level = 1, $class = -1, $parameters = array()) {
		$node = & $this->_xhtmldoc->create_element("h" . $level);
		if ($class != -1)
			$node->set_attribute("class", $class);
		
		foreach ($parameters as $key => $value) {
			$node->set_attribute($key, $value);
		}
		$this->_currentnode->append_child($node);
		$this->priv_xhtml_insert(utf8_ensure($title), $node);
	}

	/** \brief inserts an image tag in the xhtml document
	
	\param $url is the url of the image file
	\param $alt is the alternative description (used if the browser can not display the image)
	\param $parameters is an associative array that can contain optionals xhtml parameters, such as the CSS class in $parameters['class'] or a title for the image $parameters['title']
	*/
	function xhtml_image($url, $alt, $parameters = array ()) {
		$node = & $this->_xhtmldoc->create_element('img');
		$node->set_attribute("src", $url);
		$node->set_attribute("alt", $alt);

		foreach ($parameters as $key => $value) {
			$node->set_attribute($key, $value);
		}
		$this->_currentnode->append_child($node);
	}

	/** \brief inserts a break line <br/> in the xhtml document
	
	\param $class is the CSS class to use
	*/
	function xhtml_br($class = -1) {
		$node = & $this->_xhtmldoc->create_element("br");
		if ($class != -1)
			$node->set_attribute("class", $class);
		$this->_currentnode->append_child($node);
	}

	/** \brief inserts an unordered list 'ul' in the xhtml document
	
	\param $array is a list, each element will be inserted in an 'li' tag
	\param $class is the CSS class to use
	\param $parameters is an associative array that can contain optionals xhtml parameters,
	such as the current li tag $parameters["current"] that contains a string that match with the li content that will be set as current for CSS
	*/
	function xhtml_ul($array, $class = -1, $parameters = array ()) {

		$ul = & $this->_xhtmldoc->create_element("ul");
		if ($class != -1)
			$ul->set_attribute("class", $class);
		$current = -1;
		if (array_key_exists("current", $parameters)) {
			$current = $parameters["current"];
		}
		//$this->xhtml_message($current);
		if (is_array($array)) {
			reset($array);
			while (list ($i, $content) = each($array)) {
				//for ($i = 0; $i < count($array); $i++){
				// if ($array[$i] != "") {
				if ($content != "") {
					$li = & $this->_xhtmldoc->create_element("li");
					//$this->xhtml_insert($array[$i],$li);
					$this->priv_xhtml_insert(utf8_ensure($content), $li);

					if (($current != -1) and ($current != ''))  {
						//echo "|".$current."|";
						if (strpos($content, $current) !== false) {
							$li->set_attribute("class", "current");
						}
					}
					$ul->append_child($li);
				}
			}
		}
		//$this->xhtml_insert();

		$this->_currentnode->append_child($ul);
	}

	/** \brief inserts an ordered list 'ol' in the xhtml document
	
	\param $array is a list, each element will be inserted in an 'li' tag
	\param $class is the CSS class to use
	\param $parameters is an associative array that can contain optionals xhtml parameters,
	such as the current li tag $parameters["current"] that contains a string that match with the li content that will be set as current for CSS
	*/
	function xhtml_ol($array, $class = -1, $parameters = array ()) {

		$ol = & $this->_xhtmldoc->create_element("ol");
		if ($class != -1)
			$ol->set_attribute("class", $class);
		$current = -1;
		if (array_key_exists("current", $parameters)) {
			$current = $parameters["current"];
		}
		//$this->xhtml_message($current);
		if (is_array($array)) {
			reset($array);
			while (list ($i, $content) = each($array)) {
				//for ($i = 0; $i < count($array); $i++){
				// if ($array[$i] != "") {
				if ($content != "") {
					$li = & $this->_xhtmldoc->create_element("li");
					//$this->xhtml_insert($array[$i],$li);
					$this->priv_xhtml_insert(utf8_ensure($content), $li);

					if (($current != -1) and ($current != ''))  {
						//echo "|".$current."|";
						if (strpos($content, $current) !== false) {
							$li->set_attribute("class", "current");
						}
					}
					$ol->append_child($li);
				}
			}
		}
		//$this->xhtml_insert();

		$this->_currentnode->append_child($ol);
	}

	/** \brief inserts an li tag
	
	\param $class is the CSS class to use
	*/

	function new_li($class = -1) {
		$li = & $this->_xhtmldoc->create_element("li");
		if ($class != -1)
			$li->set_attribute("class", $class);
		$this->_currentnode->append_child($li);
		$this->_currentnode = & $li;
	}

	/** \brief inserts an div tag that contains a message
	
	\param $text the message to display
	\param $class is the CSS class to use
	*/
	function xhtml_message($text, $class = -1) {
		$this->xhtml_insert_div($text, $class);
	}

	/** \brief inserts an div tag that contains a message
	
	\param $html_text the message to display
	\param $class is the CSS class to use
	*/
	function xhtml_insert_div($html_text, $class = -1) {
		// insert $html_div in the current node
		//$dom = domxml_new_doc("1.0");

		$div = & $this->_xhtmldoc->create_element("div");
		if ($class != -1) {
			$div->set_attribute("class", $class);

		}

		if (!is_object($this->_currentnode)) {
			echo ("ERROR: the field " . $html_text . " is not consistent (xhtml_base object).");
			echo ("please contact the administrator");
			exit;
		}

		$this->_currentnode->append_child($div);

		$this->priv_xhtml_insert(utf8_ensure($html_text), $div);
		// $this->priv_xhtml_insert($html_text);
	}

	/** \brief inserts an span tag that contains a message
	
	\param $html_text the message to display
	\param $class is the CSS class to use
	*/
	function xhtml_insert_span($html_text, $class = -1) {
		// insert $html_div in the current node
		//$dom = domxml_new_doc("1.0");
		$div = & $this->_xhtmldoc->create_element("span");
		if ($class != -1) {
			$div->set_attribute("class", $class);

		}
		$this->_currentnode->append_child($div);

		$this->priv_xhtml_insert(utf8_ensure($html_text), $div);
	}

	/** \brief inserts a code tag that contains a message
	
	\param $text the message to display
	\param $class is the CSS class to use
	*/
	function xhtml_code($text, $class = -1) {
		$this->xhtml_insert_tag('code', $text, $class);
	}

	/** \brief function to insert any xhtml tag, and its content
	
	\param $tag the tag that will be created
	\param $html_text the message to display
	\param $class is the CSS class to use
	*/
	function xhtml_insert_tag($tag, $html_text, $class = -1) {
		$div = & $this->_xhtmldoc->create_element($tag);
		if ($class != -1) {
			$div->set_attribute("class", $class);

		}
		$this->_currentnode->append_child($div);

		$this->priv_xhtml_insert(utf8_ensure($html_text), $div);
	}

	/** \brief private function to insert any text in a given node
	
	\param $html_text the text to insert in the node
	\param &$insert_node the reference of the node that will contain $html_text
	*/
	function priv_xhtml_insert($html_text, & $insert_node) {
		// insert $html_div in the current node
		if (!is_object($insert_node)) {
			$insert_node = & $this->_currentnode;
		}
		$insert_node->set_content($html_text);
	}
	
	/** \brief inserts an p tag that contains a message
	
	\param $html_text the message to display
	\param $class is the CSS class to use
	*/
	function xhtml_p($html_text, $class = -1) {
		// insert $html_div in the current node
		//$dom = domxml_new_doc("1.0");
		$div = & $this->_xhtmldoc->create_element("p");
		if ($class != -1) {
			$div->set_attribute("class", $class);

		}
		$this->_currentnode->append_child($div);

		$this->priv_xhtml_insert(utf8_ensure($html_text), $div);
	}
	function xhtml_strong($html_text, $class = -1) {
		// insert $html_div in the current node
		//$dom = domxml_new_doc("1.0");
		$div = & $this->_xhtmldoc->create_element("strong");
		if ($class != -1) {
			$div->set_attribute("class", $class);

		}
		$this->_currentnode->append_child($div);

		$this->priv_xhtml_insert(utf8_ensure($html_text), $div);
	}
	/** \brief inserts a list 'dl' in the xhtml document only available for one to one relation
	
	\param $array is a associative array with qui is for dt tag and value for dd tag
	\param $class is the CSS class to use
	\param $parameters is an associative array that can contain optionals xhtml parameters,
	such as the current li tag $parameters["current"] that contains a string that match with the li content that will be set as current for CSS
	*/
	function xhtml_dl($array, $class = -1, $parameters = array ()) {

		$dl = & $this->_xhtmldoc->create_element("dl");
		if ($class != -1)
			$dl->set_attribute("class", $class);
		$current = -1;
		if (array_key_exists("current", $parameters)) {
			$current = $parameters["current"];
		}
		//$this->xhtml_message($current);
		if (is_array($array)) {
			//reset($array);
			$array_keys = array_keys($array);
			foreach($array_keys as $dl_value) {
				$di = & $this->_xhtmldoc->create_element("di");
				
				//for ($i = 0; $i < count($array); $i++){
				// if ($array[$i] != "") {
				if ($dl_value != "") {
					$dt = & $this->_xhtmldoc->create_element("dt");
					//$this->xhtml_insert($array[$i],$li);
					$this->priv_xhtml_insert(utf8_ensure($dl_value), $dt);

					if (($current != -1) and ($current != ''))  {
						//echo "|".$current."|";
						if (strpos($dl_value, $current) !== false) {
							$dt->set_attribute("class", "current");
						}
					}
					$di->append_child($dt);
				}
				if ($array[$dl_value] != ""){
					$dd = & $this->_xhtmldoc->create_element("dd");
					$this->priv_xhtml_insert(utf8_ensure($array[$dl_value]), $dd);
					if (($current != -1) and ($current != ''))  {
						//echo "|".$current."|";
						if (strpos($array[$dl_value], $current) !== false) {
							$dd->set_attribute("class", "current");
						}
					}
					$di->append_child($dd);
				}
				$dl->append_child($di);
			}
		}
		//$this->xhtml_insert();

		$this->_currentnode->append_child($dl);
	}

}
?>