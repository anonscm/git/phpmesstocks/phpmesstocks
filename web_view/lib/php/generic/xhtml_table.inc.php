<?php


/** \file web_view/lib/php/generic/xhtml_table.inc.php
 * \brief base class to produce xhtml tables
 * 
*
* \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
* \date 27/01/2004
*/

/* xhtml_table object interface:
//constructor, parameters: xml document, current node, form name
xhtml_table($xhtmldoc, $node, $id)

*/

/** \brief base class to produce xhtml tables
*
* manipulates xhtml tables
*/
class xhtml_table extends xhtml_zone {
	// var $_xhtmldoc;
	var $_table;
	var $_caption;
	var $_thead;
	var $_tbody;
	var $_theadtr;
	var $_tbodytr;
	// var $_currentnode;

	/** \brief constructor
	*
	* \param  $xhtmldoc the reference to the xhtml document
	* \param $node reference to the node in the xhtml document where the table will be included
	* \param $id the name of the table, used to identify the table in the xhtml page. This should be unique in a page
	* \param $class CSS class to use for this table, optional
	*/
	function xhtml_table(& $xhtmlpage, $id, $class = -1) {

		$this->_page = & $xhtmlpage;
		$this->_xhtmldoc = $xhtmlpage->get_xhtml_doc();
		$node = $xhtmlpage->get_current_node();

		$this->xhtml_zone();
		//$this->_xhtmldoc = & $xhtmldoc;
		$this->_table = & $this->_xhtmldoc->create_element('table');
		$node->append_child($this->_table);
		$this->_caption = & $this->_xhtmldoc->create_element('caption');
		//$this->_caption->set_content(' ');
		$this->_table->append_child($this->_caption);

		$this->_currentnode = & $this->_table;
		$this->_tab_nodes['ground0'] = & $this->_table;
		$this->_table->set_attribute('id', $id);
		if ($class != -1)
			$this->_table->set_attribute('class', $class);

		$this->_thead = & $this->_xhtmldoc->create_element('thead');
		$this->_thead->set_content(' ');
		$this->_table->append_child($this->_thead);
		$this->_theadtr = null;
		$this->_tbodytr = null;

		$this->_tbody = & $this->_xhtmldoc->create_element('tbody');
		$this->_tbody->set_attribute('id', $id . '_tbody');
		//$this->_tbody->set_content(' ');
		$this->_table->append_child($this->_tbody);
	}

	function set_caption($caption) {
		$this->_caption->unlink_node();
		$this->_caption->set_content(utf8_ensure($caption));
	}

	/** create a table cell in the current row of tbody
	 *  and place the current node to this cell
	 * 
	 */

	function new_cell($param = array ()) {
		if (is_object($this->_tbodytr)) {
			//we have a row
		} else {
			//create a row
			$tr = & $this->_xhtmldoc->create_element('tr');
			$this->_tbody->append_child($tr);
			$this->_tbodytr = & $tr;
		}
		//create the cell:

		$td = & $this->_xhtmldoc->create_element('td');
		foreach ($param as $key => $value) {
			$td->set_attribute($key, utf8_ensure($value));
		}
		$this->_tbody->append_child($td);
		$this->_currentnode = & $td;
	}

	/** \brief add a line (row) in the table
	*
	* \param  $tab an array containing the content of each cell for this lines
	*/
	function add_line($tab = array ()) {
		if (!is_array($tab))
			return;
		$tr = & $this->_xhtmldoc->create_element('tr');
		$this->_tbody->append_child($tr);
		$this->_tbodytr = & $tr;
		for ($i = 0; $i < count($tab); $i++) {
			$this->priv_xhtml_insert_td($tab[$i], $tr);
		}
	}
	/** \brief add a cell in the current table row
	*
	* \param  $text content of the cell
	* \param $class CSS class to use for this table, optional
	*/
	function add_cell($text, $class = -1) {
		if (is_object($this->_tbodytr)) {
			//we have a row
		} else {
			//create a row
			$tr = & $this->_xhtmldoc->create_element('tr');
			$this->_tbody->append_child($tr);
			$this->_tbodytr = & $tr;
		}
		//$tr = $this->_thead->first_child();
		if (is_array($class)) {
			$param = $class;
		} else {
			$param = array ();
			if ($class != -1)
				$param['class'] = $class;
		}

		$this->priv_xhtml_insert_td($text, $this->_tbodytr, $param);

	}

	/** \brief add a cell header in the current table row
	*
	* \param  $text content of the cell
	* \param $class CSS class to use for this table, optional
	*/
	function add_cell_header($text, $parameters = array ()) {
		if (is_object($this->_tbodytr)) {
			//we have a row
		} else {
			//create a row
			$tr = & $this->_xhtmldoc->create_element('tr');
			$this->_tbody->append_child($tr);
			$this->_tbodytr = & $tr;
		}
		$this->priv_xhtml_insert_th($text, $this->_tbodytr, $parameters);

	}

	/** \brief add a a list of terms in a cell of the current table row
	*
	* \param  $list the list of terms 
	* \param $class CSS class to use for this table, optional
	*/
	function add_list_in_cell($list, $class = -1) {
		//	$text = '<div>'.join($list, '</div><div>').'</div>';
		$text = '<ul><li>' . join($list, '</li><li>') . '</li></ul>';
		$this->add_cell($text, $class);
	}

	/** \brief add a cell in the current table header line
	*
	* \param  $text content of the cell
	* \param $colspan number of columns to group (colspan)
	* \param $parameters optional parameters, such as 'class' or 'rowspan'
	*/
	function add_header($text, $colspan = -1, $parameters = array ()) {
		if (is_object($this->_theadtr)) {
			//we have a row
		} else {
			//create a row
			$tr = & $this->_xhtmldoc->create_element('tr');
			$this->_thead->append_child($tr);
			$this->_theadtr = & $tr;
		}
		//$tr = $this->_thead->first_child();
		$param = array ();
		if ($colspan != -1)
			$parameters['colspan'] = $colspan;
		$this->priv_xhtml_insert_th($text, $this->_theadtr, $parameters);

	}

	/** \brief add a line (row) in the table header
	*
	* \param  $tab an array containing the content of each cell for this lines
	*/
	function add_headers($tab = array ()) {
		if (!is_array($tab))
			return;
		//$tr = $this->_thead->first_child();
		$tr = & $this->_xhtmldoc->create_element('tr');
		$this->_thead->append_child($tr);
		$this->_theadtr = & $tr;
		for ($i = 0; $i < count($tab); $i++) {
			$this->priv_xhtml_insert_th($tab[$i], $tr);
		}
	}

	/** \brief private: insert a cell in the table
	*
	* \param $html_text content of the cell
	* \param $node where the cell will be inserted
	* \param $parameters optional parameters, such as 'class' or 'colspan'
	*/
	function priv_xhtml_insert_td(& $html_text, & $node, $parameters = array ()) {
		$this->priv_xhtml_insert_cell('td', $html_text, $node, $parameters);
	}

	/** \brief private: insert a cell in the table as column header
	*
	* \param $html_text content of the cell
	* \param $node where the cell will be inserted
	* \param $parameters optional parameters, such as 'class' or 'colspan'
	*/
	function priv_xhtml_insert_th(& $html_text, & $node, $parameters = array ()) {
		$this->priv_xhtml_insert_cell('th', $html_text, $node, $parameters);
	}

	/** \brief private: insert a cell in the table
	*
	* \param $td_or_th the tag of the cell (td: table detail, th: table header)
	* \param $html_text content of the cell
	* \param $node where the cell will be inserted
	* \param $parameters optional parameters, such as 'class' or 'colspan'
	*/
	function priv_xhtml_insert_cell($td_or_th, & $html_text, & $node, $parameters = array ()) {
		// insert $html_div in the current node

		//$dom = domxml_new_doc("1.0");
		$html_text = $html_text . '</' . $td_or_th . '>';
		$begin = '<' . $td_or_th;
		foreach ($parameters as $name => $value) {
			if ($name == 'href') {
				$value = str_replace('&', '&amp;', $value);
			}
			$begin .= ' ' . $name . '="' . $value . '"';
		}

		$html_text = $begin . '>' . $html_text;
		$this->priv_xhtml_insert(utf8_ensure($html_text), $node);
	}

}
?>