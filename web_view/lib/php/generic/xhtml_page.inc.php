<?php


/** \file web_view/lib/php/generic/xhtml_page.inc.php
 * \brief object to manipulate xhtml page
 *
 *
 * \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 * \date 12/01/2004
 */

/* xhtml_page object interface:
 //constructor, creates the xhtml document, with basic attibutes needed to
 // produce valid xhtml code
 xhtml_page()

 //set the title of the xhtml page:
 xhtml_page_set_title($title)
 // add a CSS url reference for this xhtml page
 xhtml_page_add_css($css)
 // print the xhtml code to the output (usually, the remote browser)
 xhtml_page_display()
 */

/** \brief object to generate a complete XHTML page
 *
 * this object creates an xml document and the basic xhtml elements to produce
 * a well formed XHTML page:
 * - html tag root
 * - xml namespace
 * - language used
 * - header with title, icons, css
 * - body
 */

class xhtml_page extends xhtml_zone {
	//var $_xhtmldoc;
	//var $_currentnode;
	/** \brief private: reference to the html root tag
	*/
	var $_root;
	/** \brief private: reference to the head tag
	 */
	var $_head;
	/** \brief private: reference to the title tag
	 */
	var $_head_title;

	/** \brief private: reference to the body tag
	 */
	var $_body;
	//var $_xpath;
	/** \brief private: default charset to use
	*/
	var $_lang;
	/** \brief private: language of the content to use
	 */
	var $_charset;

	/** \brief array to store js path to avoid to load the same file a second time
	 */
	var $_priv_array_js_src;

	/** \brief indicates if the load_xml_doc javascript function is added 
	 * 
	 */
	var $_loaded_xml_doc_js;

	var $_content_type;
	
	
	/** 
	 * the node that contains javascript code
	 * @var cdata xml node
	 */
	var $_cdata_js;
	
	var $_on_load_javascript_code;

	/** \brief constructor
	 *
	 * build the XHTML page from an xml document
	 * set xml namespace, preferred language, content type, character set
	 * creates basic tags: header with title, body
	 *
	 * \param $lang optional the language to use
	 * \param $charset optional the charaster set to use to display xhtml
	 */
	function xhtml_page($lang = 'en', $charset = 'iso-8859-1') { //constructor
		$this->_on_load_javascript_code = null;
		$this->_cdata_js = null;
		$this->_priv_array_js_src = array ();
		$this->_charset = $charset;
		$this->_lang = $lang;
		//$this->_content_type = 'application/xhtml+xml';
		$this->_content_type = 'text/html';
		$this->xhtml_zone();
		//$this -> _xhtmldoc = domxml_new_doc("1.0");

		$this->_xhtmldoc = new oxml_document("1.0");

		$this->_root = $this->_xhtmldoc->create_element("html");
		$this->_xhtmldoc->append_child($this->_root);

		$this->_root = & $this->_xhtmldoc->document_element();
		$this->_root->set_attribute("xmlns", "http://www.w3.org/1999/xhtml");
		//$this->_root->set_attribute("xml:lang", $this->_lang);
		$this->_root->set_attribute("lang", $this->_lang);

		$this->_head = & $this->_xhtmldoc->create_element("head");
		$node = $this->_xhtmldoc->create_element("meta");
		$node->set_attribute("http-equiv", "Content-Type");
		$node->set_attribute("content", 'text/html; charset=' . $this->_charset);

		$this->_head->append_child($node);
		$this->_head_title = & $this->_xhtmldoc->create_element("title");
		$this->_head->append_child($this->_head_title);

		$this->_root->append_child($this->_head);
		$this->_body = & $this->_xhtmldoc->create_element("body");
		$this->_root->append_child($this->_body);

		$this->_currentnode = & $this->_body;

		$this->_tab_nodes["ground0"] = & $this->_body;
		$this->_loaded_xml_doc_js = false;
	}

	/** \brief set a title for this page
	 *
	 * \param $title the title of the page
	 */
	function xhtml_page_set_title($title) {
		//if ($this->_xhtmldoc->has_attribute("title"))
		//      $xpresult = xpath_eval($this->xpath, "/html/head/title");
		$this->_head_title->unlink_node();
		//$this -> _head_title = &$this -> _xhtmldoc -> create_element("title");
		//$this -> _head -> append_child($this -> _head_title);
		$this->_head_title->set_content(utf8_ensure($title));

	}

	/** \brief add an icon for this page
	 *
	 * \param $icon_ref the URL path to a picture file (png, jpg)
	 */
	function xhtml_page_add_icon($icon_ref) {
		//<link rel ="stylesheet" href="./web_view/css/echantillons.css" type="text/CSS"/>
		$node = & $this->_xhtmldoc->create_element("link");
		$node->set_attribute("rel", "shortcut icon");
		$node->set_attribute("href", $icon_ref);
		$node->set_attribute("type", "images/x-icon");
		//$node->set_content(utf8_ensure($title));
		$this->_head->append_child($node);
	}

	/** \brief add reference on a css file
	 *
	 * \param $css the URL path to a css file (Cascading Style Sheet)
	 */
	function xhtml_page_add_css($css, $media = '') {
		//<link rel ="stylesheet" href="./web_view/css/echantillons.css" type="text/CSS"/>
		$node = & $this->_xhtmldoc->create_element("link");
		$node->set_attribute("rel", "stylesheet");
		$node->set_attribute("href", $css);
		if ($media != '')
			$node->set_attribute("media", $media);
		$node->set_attribute("type", "text/CSS");
		//$node->set_content(utf8_ensure($title));
		$this->_head->append_child($node);
	}

	/** \brief add reference on a css file
	 *
	 * \param $css the URL path to a rss file 
	 */
	function xhtml_page_add_rss($rss, $media = '') {
		//<link rel ="stylesheet" href="./web_view/css/echantillons.css" type="text/CSS"/>
		$node = & $this->_xhtmldoc->create_element("link");
		$node->set_attribute("title", "RSS 2.0");
		$node->set_attribute("rel", "alternate");
		$node->set_attribute("href", $rss);
		if ($media != '')
			$node->set_attribute("media", $media);
		$node->set_attribute("type", "application/rss+xml");
		//$node->set_content(utf8_ensure($title));
		$this->_head->append_child($node);
	}

	/** \brief include ecmascript code from url src
	 *
	 * <SCRIPT language='javascript' src='../global/magicmenu2.js'></SCRIPT>
	 */
	function xhtml_page_add_js_src($src_path) {

		if (array_key_exists($src_path, $this->_priv_array_js_src)) {
			//already added in this form
		} else {
			$node = & $this->_xhtmldoc->create_element('script');
			$node->set_attribute('src', $src_path);
			$node->set_attribute('type', 'text/javascript');
			$node->set_content(' ');

			//$cdata = & $this->_xhtmldoc->create_cdata_section(file_get_contents($src_path));
			//$node->append_child($cdata);
			$this->_head->append_child($node);
		}

	}

	/** \brief include ecmascript code from a text file
	 *
	 * <SCRIPT language='javascript' src='../global/magicmenu2.js'></SCRIPT>
	 */
	function xhtml_page_add_js_from_file($src_path) {
		$node = & $this->_xhtmldoc->create_element('script');
		$node->set_attribute('type', 'text/javascript');
		//$node->set_attribute('src', $url_src);

		$cdata = & $this->_xhtmldoc->create_cdata_section(file_get_contents($src_path));
		$node->append_child($cdata);
		$this->_head->append_child($node);

	}

	/** \brief include ecmascript code from string
	 *
	 * <SCRIPT language='javascript' src='../global/magicmenu2.js'></SCRIPT>
	 */
	function xhtml_page_add_js($js_code) {
		//$node->set_attribute('src', $url_src);

		if ($this->_cdata_js == null) {
			$node = & $this->_xhtmldoc->create_element('script');
			$node->set_attribute('type', 'text/javascript');
			$this->_cdata_js = & $this->_xhtmldoc->create_cdata_section($js_code);
			$node->append_child($this->_cdata_js);
			$this->_head->append_child($node);
		}
		else{
			$this->_cdata_js->add_content($js_code);
		}
		//$cdata = & $this->_xhtmldoc->create_cdata_section($js_code);

	}

	/** \brief dump the xhtml_page object to a string
	 *
	 * the xhtml code is generated from the xml document. this code is sent to the browser and displayed
	 */
	function xhtml_page_display() {
		//last function to use un a script
		//$doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//FR\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
		$doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
		//$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
		//$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN" "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd">';
		//HTML 5
		//$doctype = '<!DOCTYPE html >';
		$render = $this->_xhtmldoc->dump_mem_xhtml(1, $this->_charset, $doctype, true);
		header('Content-Type: ' . $this->_content_type . '; charset=' . $this->_charset);
		echo $render;
	}

	/** \brief dump the content of the page body as XHTML
	 *
	 * \return string containing xhtml tags
	 */
	function inner_render() {
		$returned_string = '';
		//$this->_body->priv_dump_mem_xhtml($returned_string, 1, $this->_charset);
		foreach ($this->_body->_arr_content as $case) {
			if (is_object($case)) {
				$case->priv_dump_mem_xhtml($returned_string, 1, $this->_charset);
			}
		}
		return ($returned_string);
	}

	/** \brief retrieve messages from the page
	 *
	 * this retrieve all messages written to that page with the css class $css_class.
	 * \param $css_class the css class of the messages to retrieve
	 * \return a string each div separated by \n
	 */
	function retrieve_messages_to_string($css_class) {
		$messages = $this->_xhtmldoc->dump_element_content('div', array (
			'class' => $css_class
		));
		$messages = str_replace('<br/>', "\n", $messages);
		return ($messages);
	}

	/** \brief private: add javascript code to the onload function
	 *
	 * this function will place javascript code
	 * that will be executed automatically when loading the page
	 * \param $javascript the javascript code
	 */
	function add_onload_javascript($javascript_code) {
		if ($this->_on_load_javascript_code == null) {
			$this->_on_load_javascript_code = '';
		}
		$this->_on_load_javascript_code .= utf8_ensure($javascript_code);
	}

	function replace_XML($name, $url_xmldoc, $url_xslt) {

		$name = $this->priv_get_unique_id($name);

		if (!array_key_exists($name, $this->_tab_nodes)) {
			$this->xhtml_message("ERROR: this zone doesn't exists: " . $name, "error");
			return;
		}
		$node = & $this->_tab_nodes[$name];

		$node->unlink_node();

		$this->insert_XML($name, $url_xmldoc, $url_xslt);

	}

	/** \brief insert XML data inside the element with id = name using XSLT transformation
	 * 
	 */
	function insert_XML($name, $url_xmldoc, $url_xslt) {

		$name = $this->priv_get_unique_id($name);
		//$this->xhtml_message($name);
		if (!array_key_exists($name, $this->_tab_nodes)) {
			$this->xhtml_message("ERROR: this zone doesn't exists: " . $name, "error");
			return;
		}
		//if ($this->_lock_zone == false)
		//$this->_currentnode = & $this->_tab_nodes[$name];

		//$name = $this->priv_get_unique_id($name);
		$node = & $this->_tab_nodes[$name];
		//$node->set_attribute('id', $name);
		//$this->_currentnode->append_child($node);

		$javascript = '';
		if ($this->_loaded_xml_doc_js == false) {

			$javascript = 'function loadXMLDoc(dname) {';
			$javascript .= ' if (window.XMLHttpRequest) {';
			$javascript .= '  xhttp=new XMLHttpRequest();}';
			$javascript .= ' else {';
			$javascript .= '  xhttp=new ActiveXObject("Microsoft.XMLHTTP");}';
			$javascript .= ' xhttp.open("GET",dname,false);';
			$javascript .= ' xhttp.send("");';
			$javascript .= ' return xhttp.responseXML;';
			$javascript .= '}';

			$this->xhtml_page_add_js($javascript);
			$this->_loaded_xml_doc_js = true;
		}

		//$javascript = 'console.log("hello world");'."\n";
		$javascript .= 'var xml = loadXMLDoc("' . $url_xmldoc . '");' . "\n";
		$javascript .= 'var xsl = loadXMLDoc("' . $url_xslt . '");' . "\n";
		//$javascript .= ' console.log("hello world2");'."\n";
		//$javascript .= '// code for IE';
		$javascript .= 'if (window.ActiveXObject) {' . "\n";
		//$javascript .= ' console.log("hello world3");'."\n";
		$javascript .= ' var ex=xml.transformNode(xsl);' . "\n";
		$javascript .= ' document.getElementById("' . $name . '").innerHTML=ex;' . "\n";
		$javascript .= '}' . "\n";
		//$javascript .= '// code for Mozilla, Firefox, Opera, etc.';
		$javascript .= 'else if (document.implementation && document.implementation.createDocument) {' . "\n";
		//$javascript .= 'else {' . "\n";
		$javascript .= ' var xsltProcessor=new XSLTProcessor();' . "\n";
		$javascript .= ' xsltProcessor.importStylesheet(xsl);' . "\n";
		$javascript .= ' var insertPoint = document.getElementById("' . $name . '");' . "\n";
		$javascript .= ' var resultDocument = xsltProcessor.transformToFragment(xml,document);' . "\n";
		//$javascript .= ' insertPoint.appendChild(resultDocument);' . "\n";
		$javascript .= ' try { insertPoint.appendChild(resultDocument); }' . "\n";
		//$javascript .= ' catch (err) { alert("resultDocument '.$name.' not found "+resultDocument+err.message); }' . "\n";
		$javascript .= ' catch (err) {   }' . "\n";
		$javascript .= '}' . "\n";
		//$javascript .= ' console.log("hello world4");'."\n";

		$this->add_onload_javascript($javascript);
	}

	function set_content_type($content_type) {
		$this->_content_type = $content_type;
	}
	/** \brief authenticate the browser on a remote site
	 *
	 */

	// function js_auth($url_auth, $login, $password) {

	/*
		* http://www.peej.co.uk/articles/http-auth-with-html-forms.html
		*/
	/*
	 $this->xhtml_page_add_js('function getHTTPObject() { if (typeof XMLHttpRequest != \'undefined\') { return new XMLHttpRequest(); } try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {} } return false; }');
	
	 $javascript = 'console.log("http:"); var http = getHTTPObject();console.log("http:", http);';
	 $javascript .= 'http.open("get", "' . $url_auth . '", false, "' . $login . '", "' . $password . '"); http.send("");';
	 $javascript .= 'if (http.status == 200) { document.location = this.action; alert("ok.")}';
	 $javascript .= ' else { alert("Incorrect username and/or password."); }console.log("finito");';
	 $javascript .= 'return false; ';
	 $this->add_onload_javascript($javascript);
	 }
	 */
}
?>