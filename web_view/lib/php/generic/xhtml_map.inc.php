<?php


/** \file web_view/lib/php/generic/xhtml_map.inc.php
 * 
 * base class to produce xhtml maps
 * 
 * \author Olivier Langella <langella@moulon.inra.fr>
 * \date 16/05/2007
 */
require_once (APP_ROOT_RELATIVE_PATH . 'web_view/lib/php/generic/xhtml_base.inc.php');
require_once (APP_ROOT_RELATIVE_PATH . 'web_view/lib/php/generic/xhtml_zone.inc.php');

/** \brief base class to produce xhtml tables
*
* manipulates xhtml tables
*/
class xhtml_map extends xhtml_zone {

	var $_map;

	/** \brief constructor
	*
	* \param  $page the reference to the xhtml_page document
	*/
	function xhtml_map(& $xhtmlpage, $id) {
		$this->xhtml_zone();
		$this->_xhtmldoc = $xhtmlpage->get_xhtml_doc();
		$node = $xhtmlpage->get_current_node();
		$this->_map = & $this->_xhtmldoc->create_element('map');
		$node->append_child($this->_map);

		$this->_currentnode = & $this->_map;
		$this->_tab_nodes['ground0'] = & $this->_map;
		$this->_map->set_attribute('id', $id);
		$this->_map->set_attribute('name', $id);
	}

	function priv_add_area($shape, $coords, $href, $alt, $optional_parameters = array ()) {
		//<area  SHAPE="rect" COORDS="0,0,100,100" HREF="http://www.foo.com/test.htm">
		$area = & $this->_xhtmldoc->create_element('area');

		$href = str_replace('&amp;', '&', $href);
		$href = str_replace('&', '&amp;', $href);

		$area->set_attribute('shape', $shape);
		$area->set_attribute('coords', $coords);
		$area->set_attribute('href', $href);
		$area->set_attribute('alt', $alt);

		foreach ($optional_parameters as $key => $value) {
			//for ($i=0; $i < count($optinal_parameters);$i++) {
			$area->set_attribute($key, $value);
		}
		$this->_map->append_child($area);
	}

	/** add a rect area in the map
	 * 
	 * \param $x1 x coordinate of the upper left corner
	 * \param $y1 y coordinate of the upper left corner
	 * \param $x2 x coordinate of the bottom right corner
	 * \param $y2 y coordinate of the bottom right corner
	 * \param $url the url pointed by this area
	 * \param $alt alternative text describing this area
	 * \param $optional_parameters associative array containing optional attribute name ad values
	 */
	function add_rect($x1, $y1, $x2, $y2, $url, $alt, $optional_parameters = array ()) {

		$x1 = round($x1);
		$x2 = round($x2);
		$y1 = round($y1);
		$y2 = round($y2);
		$coords = join(',', array (
			$x1,
			$y1,
			$x2,
			$y2
		));
		$this->priv_add_area('rect', $coords, $url, $alt, $optional_parameters);
	}

	/** add a circle area in the map
	 * 
	 * \param $x x coordinate of the center of the circle
	 * \param $y y coordinate of the center of the circle
	 * \param $z the radius of the circle
	 * \param $url the url pointed by this area
	 * \param $alt alternative text describing this area
	 * \param $optional_parameters associative array containing optional attribute name ad values
	 */

	function add_circle($x, $y, $z, $url, $alt, $optional_parameters = array ()) {

		$x = round($x);
		$y = round($y);
		$z = round($z);
		$coords = join(',', array (
			$x,
			$y,
			$z
		));
		$this->priv_add_area('circle', $coords, $url, $alt, $optional_parameters);
	}

}
?>