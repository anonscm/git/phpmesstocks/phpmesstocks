<?php

//

require_once 'oxml_document.inc.php';
require_once 'oxml_element.inc.php';

$page = new oxml_document('1.0');

$element = $page->create_element("html");
$page->append_child($element);

$root = & $page->document_element();
$root->set_attribute("xmlns", "http://www.w3.org/1999/xhtml");
$root->set_attribute("xml:lang", "fr");
$root->set_attribute("lang", "fr");

$head = $page->create_element("head");
$node = $page->create_element("meta");
$node->set_attribute("http-equiv", "Content-Type");
$node->set_attribute("content", "text/html; charset=iso-8859-1");
//$node->set_attribute("type","text/CSS");
//$node->set_content(utf8_encode($title));
$head->append_child($node);

$root->append_child($head);

$body = $page->create_element("body");
$root->append_child($body);

$paragraphe = $page->create_element("div");

$trop_fort = $page->create_element("strong");
$trop_fort->set_content('trop fort !');

$paragraphe->append_child($trop_fort);
$body->append_child($paragraphe);

$paragraphe->set_content("ouais, mais est-ce qu'on peut faire mieux ?");
$retour = $page->create_element("br");
$paragraphe->append_child($retour);
$paragraphe->set_content("ben ouais, j'ai l'impression, <i>même si je mets des balises dedans</i>");

$render = $page->dump_mem_xhtml(1, "iso-8859-1");
echo $render;
?>