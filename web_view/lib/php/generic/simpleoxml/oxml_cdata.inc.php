<?php


/** \file web_view/lib/php/generic/simpleoxml/oxml_cdata.inc.php
 * \brief simple xml document object
*
* try to fit with the official API for DOMXML document
* 
*/

class oxml_cdata {
	var $_content;

	function oxml_cdata(& $doc) {
		$this->_content = '';
	}

	function set_content($text) {
		$this->_content = $text;
	}

	function get_content() {
		return ($this->_content);
	}

	function & priv_dump_mem_xhtml(& $returned_string, $style, $code) {
		$returned_string .= "<!--/*--><![CDATA[//><!--\n" . $this->_content . "//--><!]]>";
		return ($returned_string);
	}
	
	function add_content($add_content) {
		$this->_content .= $add_content;
	}
	
}
?>