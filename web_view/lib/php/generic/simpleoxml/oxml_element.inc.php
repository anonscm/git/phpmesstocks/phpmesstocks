<?php


/** \file web_view/lib/php/generic/simpleoxml/oxml_element.inc.php
 * \brief simple xml document object
*
* try to fit with the official API for DOMXML document
* 
*/

class oxml_element {
	//var $_original_doc;
	var $_name;
	var $_arr_attributes;
	var $_arr_content;

	function oxml_element(& $doc, $element_name) {
		//$this->_original_doc = $doc;
		$this->_name = $element_name;
		$this->_arr_attributes = array ();
		$this->_arr_content = array (); //this contains 2 types of keys: element or text
	}

	function unlink_node() {
		//unlink ($this->_arr_content);
		$this->_arr_content = array (); //this contains 2 types of keys: element or text
		//echo count ($this->_arr_content);
	}

	function get_attribute($tag_name) {
		return ($this->_arr_attributes[$tag_name]);
	}

	function append_child(& $element) {
		$this->_arr_content[] = & $element;
		//$this->_root_element = $element;
	}

	function set_attribute($name, $value) {
		$this->_arr_attributes[$name] = $value;
	}

	function set_content($text) {
		$this->_arr_content[] = $text;
	}

	function priv_dump_mem_xhtml(& $returned_string, $style, $code) {
		$returned_string .= '<' . $this->_name;
		if (count($this->_arr_attributes) != 0) {
			foreach ($this->_arr_attributes as $key => $value) {
				$returned_string .= ' ' . $key . '="' . $value . '"';
			}
		}

		if (count($this->_arr_content) == 0) {
			switch ($this->_name) {
				case 'div' :
				case 'textarea' :
					$returned_string .= '></' . $this->_name . ">\n";
					break;
				case 'ul' :
					$returned_string .= '><li/></' . $this->_name . ">\n";
					break;
				default :
					$returned_string .= '/>';
					break;
			}
			return;
		} else
			$returned_string .= '>';

		foreach ($this->_arr_content as $case) {
			if (is_object($case)) {
				$returned_string .= "\n";
				$case->priv_dump_mem_xhtml($returned_string, $style, $code);
			} else {
				//echo $case;
				$returned_string .= $case;
			}
		}

		if ($this->_name != 'textarea')
			$returned_string .= "\n";
		$returned_string .= '</' . $this->_name . '>';

		//return ($returned_string);
	}

	/** \brief private, retrieve content of specific elements
	*
	* get the content of specific elements, corresponding to $element_name and with specific arguments (options)*
	* \param $returned_string reference to the string to return
	* \param $element_name the name of the element
	* \param $element_options array containing specific options
	*/
	function priv_dump_element_content(& $returned_string, $element_name, $element_options) {
		if ($this->_name == $element_name) {
			$get_this = true;
			foreach ($element_options as $arg => $value) {
				if (!array_key_exists($arg, $this->_arr_attributes)) {
					$get_this = false;
					break;
				}
				if ($this->_arr_attributes[$arg] != $value) {
					$get_this = false;
					break;
				}
			}
		} else {
			$get_this = false;
		}
		foreach ($this->_arr_content as $case) {
			if (is_object($case)) {
				$case->priv_dump_element_content($returned_string, $element_name, $element_options);
			} else {
				if ($get_this) {
					$returned_string .= $case . "\n";
				}
			}

		}
	}
}
?>