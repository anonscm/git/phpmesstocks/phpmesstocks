<?php


/** \file web_view/lib/php/generic/simpleoxml/oxml_document.inc.php
 * \brief simple xml document object
*
* try to fit with the official API for DOMXML document
* 
*/

class oxml_document {
	var $_root_element;
	var $_version;
	var $_current_element;

	function oxml_document($xml_version) {
		$this->_version = $xml_version;
		$this->_root_element = null;

	}

	function & create_element($tag_name) {
		$ref = & new oxml_element($this, $tag_name);
		return ($ref);
	}

	function & create_cdata_section($cdata) {
		$ref = & new oxml_cdata($this);
		$ref->set_content($cdata);
		return ($ref);
	}

	function & document_element() {
		return ($this->_root_element);
	}

	function append_child(& $element) {
		$this->_root_element = & $element;
	}

	function dump_mem_xhtml($style, $code, $doctype) {
		$returned_string = '<?xml version="' . $this->_version . '"';
		if ($code != 'utf-8') {
			$returned_string .= ' encoding="' . strtoupper($code) . '"?>';
		} else {
			$returned_string .= '?>';
		}
		$returned_string .= "\n" . $doctype . "\n";

		$this->_root_element->priv_dump_mem_xhtml($returned_string, $style, $code);

		if ($code == 'iso-8859-1')
			return (utf8_decode($returned_string));
		else
			return ($returned_string);
	}

	/** \brief retrieve content of specific elements
	*
	* get the content of specific elements, corresponding to $element_name and with specific arguments (options)*
	* \param $element_name the name of the element
	* \param $element_options array containing specific options (example: get div elements with argument "class"="error", dump_element_content('div', array('class'=>'error')))
	*/
	function dump_element_content($element_name, $element_options) {
		$returned_string = '';
		$this->_root_element->priv_dump_element_content($returned_string, $element_name, $element_options);
		return ($returned_string);
	}
}
?>