<?php


/** \file web_view/lib/php/generic/xhtml_form.inc.php
 * \brief base object to build xhtml forms
 *
 * this object is inspired from the Philippe Rigaud's work: PHP & MySql, O'Reilly
 *
 * \author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 * \date 13/01/2004
 */

// this object is inspired from the Philippe Rigaud's work: PHP & MySql, O'Reilly

/* xhtml_form object interface:
 //constructor, parameters: xml document, current node, form name
xhtml_form($xhtmldoc, $node, $pname)
// set the basics attributes required for an xhtml form:
// method post or get, action: URL to submit form content
this.action =xhtml_form_set($pmethod, $paction, $pclass = "xhtml_form")

// methods to control layout of the form
// to display form field as text, no input box, no submit button: set_const(true)
set_const($is_const)
// to display the fields in a vertical table:
set_display_vtable()
// to display the fields in an horizontal table:
set_display_htable($line_number=1)
// to display the fields freely, without container by default,
// but the user can set $free_mode to "div" or "span" to contain the fields.
set_display_free($free_mode="")

// this method assigns an array containing field name with values to the form.
set_lazy_mode($tab_values)

//methods to produce xhtml code specific to forms
// set a correct "enctype" attribute to upload files
set_file_transfer ($file_transfer=true)
// display fields in an elegant "fieldset" tag
new_zone_fieldset($name, $legend, $class=-1)
// submit image button
input_submit_image ( $src, $alt, $pclass=-1)
// display a select list
input_select($ptag,$pname, $pdefault, $plist_val ,   $pclass=-1)
// submit button with a title in $pval
input_submit ($pval,$pclass=-1)
// button to reset form with a title in $pval
input_reset ($pval,$pclass=-1)
// text field
input_text ($ptag, $pname, $pval, $psize=10, $pmaxlength=-1, $pclass=-1)
// text to display only
input_message ($ptag, $pname, $pval, $pclass=-1)
// hidden field
input_hidden ($pname, $pval, $pclass=-1)
// button to browse file locally for an upload
input_file ($ptag, $pname, $pval, $psize=10, $pmaxlength=-1, $pclass=-1)
// password field
input_password ($ptag, $pname, $pval, $psize=10, $pmaxlength=-1, $pclass=-1)
// free input in a wide textarea
input_textarea ($ptag, $pname, $pval, $row, $cols, $pclass=-1)
// checkbox
input_checkbox ($ptag, $pname, $pval,  $pcheck, $pclass=-1)

//Check input:
//add a piece of javascript code to control if the given field is filled
// display the alert message if not, and doesn't validate the form
add_check_fill($field_name, $alert_message)
// add a special link to post the current form and go the the URL:
add_post_link($url, $text)

*/

/** object to build xhtml forms.
 *
 * This object allows to eaysily create xhtml forms.
 * You can control the design of the form, controlled by CSS or with tables that will display widget in horizontal or vertical mode
 * all the XHTML widget form will be produced by member functions
 * You can produce automatically Javascript code to check values on the client side
 * This is inspired from the book: MySql PHP, O'Reilly, Philippe Rigaud
 * @author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 */
class xhtml_form extends xhtml_zone {
	// var $_xhtmldoc;
	/** \brief contains the pointer to the base xhtml page
	*/
	var $_page;
	// var $_currentnode;
	/** \brief render mode with tables
	*
	* this can take values:
	* "HORIZONTAL" for horizontal table display
	* "VERTICAL" for vertical table display
	* "" (empty) to not use table
	* @access private
	*/
	var $_table_mode;
	/** \brief render mode without tables
	 *
	 * if no table is used, xhtml widget will be included in an element of that name
	 * this can take values:
	 * "div" to include widget in a div
	 * "span" to include widget in a span
	 * "" (empty) to not include widget
	 */
	var $_free_mode;
	/** \brief css class to use for free_mode
	 */
	var $_free_mode_class;
	/** \brief private use: to save a pointer temporary
	 */
	var $_svg_node;
	/** \brief constant form
	 *
	 * if true : the form does not allow any edition, the widget are translated to simple div tags
	 * if false (default) : the form allows edition of the values
	 */
	var $_const;

	/** \brief internal use, title row for the table mode
	 */
	var $_title_row;
	/** \brief internal use, row for the table mode
	 */
	var $_tab_value_row;
	/** \brief internal use, needed by the xhtml radio button widget
	 */
	var $_radio_number;
	/** \brief hash table to store valued of all the widgets
	 *
	 * this is used by the lazy mode.
	 * each key of this array is the name of the field to display
	 * the value corresponds to the value of this field
	 * the value could be an array of values: this is used to display several lines in HORIZONTAL mode
	 */
	var $_tab_values;

	/** \brief change row style in horizontal tables
	 *
	 * this contain the name of a field that represent a group of rows for horisontal table
	 * usually empty, no difference will appear between table rows
	 * if this contains a field, when the value is different from the last row =>
	 * change the css style of the whole row, making groups of rows
	 */
	var $_htable_row_diff;

	/** \brief constructor
	 *
	 * \param  $xhtmldoc the reference to the xhtml document
	 * \param $node reference to the node in the xhtml document where the form will be included
	 * \param $pname the name of the form, used to identify the form in the xhtml page. This should be unique in a page
	 */
	function xhtml_form(& $xhtmlpage, $pname) {

		$this->_page = & $xhtmlpage;
		$this->_xhtmldoc = $xhtmlpage->get_xhtml_doc();
		$node = $xhtmlpage->get_current_node();

		$this->_form = & $this->_xhtmldoc->create_element("form");
		$node->append_child($this->_form);
		$this->_currentnode = & $this->_form;
		$this->_form->set_attribute("id", 'F' . $pname);
		//$this->_form->set_attribute("name",$pname);
		$this->_tab_nodes[$this->priv_get_unique_id("form")] = & $this->_form;
		$this->_const = false;
		$this->_table_mode = "";
		$this->_free_mode = "";
		//$this->_form->set_attribute("id",$pname);
		$this->_radio_number = 0;
		$this->_tab_values = array ();
		$this->_tab_values_row = array ();

		$this->new_zone_div("ground0");

	}

	/** \brief set the basic xhtml form properties
	 *
	 * \param $pmethod is the method used to send values to the http server ("post" or "get")
	 * \param $paction is the URL to send the form values to
	 * \param $pclass is the CSS class, xhtml_form by default
	 */
	function xhtml_form_set($pmethod, $paction, $pclass = "xhtml_form") {

		$this->_form->set_attribute("method", strtolower($pmethod));
		$this->_form->set_attribute("class", $pclass);
		//check that all & will be replaced by &amp;
		$this->_form->set_attribute("action", str_replace('&', '&amp;', str_replace('&amp;', '&', $paction)));
	}

	/** \brief allows file transfer
	 *
	 * use set_file_transfer(true) to allow file uploads in your form
	 * \param $file_transfer true if you want to set correct form properties to upload files
	 */
	function set_file_transfer($file_transfer = true) {
		if ($file_transfer)
			$this->_form->set_attribute("enctype", "multipart/form-data");
	}

	/** \brief turn the form to editable or not editable
	 *
	 * this will produce not editable form if is_const is set to true
	 * \param $is_const boolean, true if not editable, false to edit
	 */
	function set_const($is_const) {
		$this->_const = $is_const;
	}

	/** \brief give the values of all the field displayed in the form in only one table
	 *
	 *please let cut/paste for the others... use the lazy mode:
	 *just use this method once to set the associative array containing
	 *field name and correspondant values
	 *then use input methods with only field name... the value will be automatically filled
	 * \param $tab_values is an associative array (hash table). each key of this array is the name of the field to display.
	 * the value corresponds to the value of this field.
	 * the value could be an array of values: this is used to display several lines in HORIZONTAL mode.
	 */
	function set_lazy_mode($tab_values) {
		if (is_array($tab_values))
			$this->_tab_values = $tab_values;
		else {
			$this->xhtml_message("ERROR in xhtml_form object: the argument passed to set_lazy_mode is not an array", "error");
		}
	}

	/** \brief set the display of the form in a vertical table
	 *
	 * all the xhtml widget will be displayed in a vertical table: perfectly aligned, label in the first column, values in the second
	 * \param $class, CSS class to use for the table
	 */
	function set_display_vtable($class = -1) {
		if ($this->_table_mode != "")
			$this->_currentnode = & $this->_svg_node;
		$table = & $this->_xhtmldoc->create_element("table");
		if ($class != -1)
			$table->set_attribute("class", $class);
		$tbody = & $this->_xhtmldoc->create_element("tbody");
		$table->append_child($tbody);

		$this->_currentnode->append_child($table);
		$this->_svg_node = & $this->_currentnode;
		$this->_currentnode = & $tbody;
		$this->_table_mode = "VERTICAL";
		$this->_lock_zone = true;
	}

	/** \brief set the display of the form in a horizontal table
	 *
	 * This will display the xhtml widgets in horizontal table: the first row of the table contains the labels,
	 * the following lines (one or more) contains the values.
	 * \param $line_number number of lines to display values. 1 by default, if more, the values passed to the widget have to be stored in an array:
	 * each element of the array corresponds to the number of the line the value will be displayed
	 */
	function set_display_htable($line_number = 1) {
		//$this->_tab_values = array();
		$this->_tab_values_row = array ();
		if ($this->_table_mode != "")
			$this->_currentnode = & $this->_svg_node;
		$this->_table_mode = "HORIZONTAL";

		if ($line_number == 0) {
			return;
		}

		$table = & $this->_xhtmldoc->create_element("table");
		$this->_currentnode->append_child($table);
		$this->_svg_node = & $this->_currentnode;
		$this->_currentnode = & $table;
		//$this->_table_mode = "HORIZONTAL";
		$this->_lock_zone = true;

		$thead = & $this->_xhtmldoc->create_element("thead");
		$this->_title_row = & $this->_xhtmldoc->create_element("tr");
		$thead->append_child($this->_title_row);
		$table->append_child($thead);

		$tbody = & $this->_xhtmldoc->create_element("tbody");
		$table->append_child($tbody);
		for ($i = 0; $i < $line_number; $i++) {
			$this->_tab_value_row[$i] = & $this->_xhtmldoc->create_element("tr");
			$tbody->append_child($this->_tab_value_row[$i]);
		}

	}

	/** \brief check if the value of an element of a form is not empty
	 *
	 * automatically generates a javascript code that will display a warning message if a particular field is empty, the script will not allow to submit the form until the value is filled.
	 * \param $field_name the name of the field to check
	 * \param $alert_message the message to display if the given field is empty
	 */
	function add_check_fill($field_name, $alert_message) {
		$field_name = "form." . $this->priv_get_unique_id($field_name);
		//$field_name = "form.".$field_name;
		//$javascript = " alert(\"".$alert_message."\");\n";

		$javascript = "if(" . $field_name . ".value != \"\") {\n";
		//      $javascript .= " alert(\"".$alert_message."\");\n";
		//$javascript .= " return true;\n";
		$javascript .= "}\n";
		$javascript .= "else {\n";
		$javascript .= " " . $field_name . ".focus();\n";
		$javascript .= " alert(\"" . $alert_message . "\");\n";
		$javascript .= " return false;\n";
		$javascript .= "}\n";

		$svg_node = & $this->_currentnode;

		$this->priv_add_check_javascript($javascript);
		$this->_currentnode = & $svg_node;

	}


	/** \brief check if the email address provided by the user is correct
	 *
	 * This generates Javascript for this field to check that the email address given by the user
	 * is of the form name@domain
	 * \param $field_name the name of the field to check
	 */
	function add_check_email($field_name, $alert_message) {
		$field_name = "form." . $this->priv_get_unique_id($field_name);
		/* The following pattern is used to check if the entered e-mail address
		 fits the user@domain format.  It also is used to separate the username
		from the domain. */

		$javascript = 'var emailPat=/^(.+)@(.+)$/' . "\n";

		/* Begin with the coarse pattern to simply break up user@domain into
		 different pieces that are easy to analyze. */
		$javascript .= 'var matchArray=' . $field_name . '.value.match(emailPat)' . "\n";
		$javascript .= 'if (matchArray==null) {' . "\n";
		$javascript .= ' ' . $field_name . '.focus();' . "\n";
		$javascript .= ' alert("' . $alert_message . '");' . "\n";
		$javascript .= " return false;\n";
		$javascript .= "}\n";

		$svg_node = & $this->_currentnode;

		$this->priv_add_check_javascript($javascript);
		$this->_currentnode = & $svg_node;
	}

/** \brief check if the phone number provided by the user is correct
	 *
	 * This generates Javascript for this field to check that the phone number given by the user
	 * is of the form name@domain
	 * \param $field_name the name of the field to check
	 */
	function add_check_tel($field_name, $alert_message) {
		$field_name = "form." . $this->priv_get_unique_id($field_name);
		/* The following pattern is used to check if the entered e-mail address
		 fits the user@domain format.  It also is used to separate the username
		from the domain. */

		$javascript = 'var telPat="^0[1-9]([-. ]?[0-9]{2}){4}$"' . "\n";

		/* Begin with the coarse pattern to simply break up user@domain into
		 different pieces that are easy to analyze. */
		$javascript .= 'var matchArray=' . $field_name . '.value.match(telPat)' . "\n";
		$javascript .= 'if (matchArray==null) {' . "\n";
		$javascript .= ' ' . $field_name . '.focus();' . "\n";
		$javascript .= ' alert("' . $alert_message . '");' . "\n";
		$javascript .= " return false;\n";
		$javascript .= "}\n";

		$svg_node = & $this->_currentnode;

		$this->priv_add_check_javascript($javascript);
		$this->_currentnode = & $svg_node;
	}

	/** \brief add a special link to post the current form and go the the URL
	 *
	 * set a "superlink" in a div tag that will submit the form using javascript code, and then go to the url
	 * \param $name the name of this link
	 * \param $text the text of the link
	 * \param $param optional array containing parameters
	 */
	function add_post_link($name, $text, $param = array ()) {
		$a = & $this->_xhtmldoc->create_element("div");
		$a->set_content($text);
		$a->set_attribute("class", "superlink");
		$a->set_attribute("onclick", "");
		$this->_currentnode->append_child($a);

		$javascript_function_name = $this->priv_get_unique_id($name);
		$a->set_attribute("onclick", $javascript_function_name . "()");
		$this->priv_javascript_post_button_code($javascript_function_name, $param);

	}

	/** \brief add a button with javascript code
	 *
	 * \param $name the name of the button
	 * \param $text the text to display on the button
	 * \param $javascript_code the javascript code to link
	 * \param $class CSS class to use to render the button
	 * \param $param optional array containing parameters
	 */
	function add_js_button($name, $text, $javascript_code, $class = "-1", $param = array ()) {
		//$button->set_attribute("onclick",$javascript_function_name."()");

		$javascript_function_name = $this->priv_get_unique_id($name);
		$this->priv_input(-1, -1, $text, $ptype = "button", array (
				"class" => $class,
				"onclick" => $javascript_function_name . "()"
		));
		//$this->priv_javascript_post_button_code($javascript_function_name, $param);
		$javascript_code = "\nfunction " . $javascript_function_name . " () {\n" . $javascript_code . "}\n";

		$this->priv_add_javascript($javascript_code);

	}

	/** \brief add a special button to post the current form and go the the URL
	 *
	 * \param $name the name of the button
	 * \param $text the text to display on the button
	 * \param $class CSS class to use to render the button
	 * \param $param optional array containing parameters
	 */
	function add_post_button($name, $text, $class = "-1", $param = array ()) {
		//$button->set_attribute("onclick",$javascript_function_name."()");

		$javascript_function_name = $this->priv_get_unique_id($name);
		$this->priv_input(-1, -1, $text, $ptype = "button", array (
				"class" => $class,
				"onclick" => $javascript_function_name . "()"
		));
		$this->priv_javascript_post_button_code($javascript_function_name, $param);

	}

	/** \brief private: generates javascript code to submit a form
	 *
	 * generates javascript code to submit a form and modify the value of particular field before submitting. used by several member functions.
	 * this feature is used in particular to allow the user to go to an other form and get back when done.
	 * \param $unique_function_name the name of the javascript function
	 * \param $param hash table: keys are field names to change before submitting, values are the new values to use
	 */
	function priv_javascript_post_button_code($unique_function_name, $param) {
		//JAVASCRIPT CODE
		$javascript_code = "\nfunction " . $unique_function_name . " () {\n";
		$javascript_code .= "var form=document.forms['" . $this->_form->get_attribute("id") . "'];\n";
		foreach ($param as $key => $val) {
			$javascript_code .= "form.elements['" . $this->priv_get_unique_id($key) . "'].value = \"" . $val . "\";\n";
		}
		$javascript_code .= "form.submit();\n";
		$javascript_code .= " return true;\n";
		$javascript_code .= "}\n";
		$this->priv_add_javascript($javascript_code);
	}

	/** \brief private: generates javascript code to submit a form
	 *
	 * generates javascript code to submit a form and modify the value of particular field before submitting. used by several member functions.
	 * this feature is used in particular to allow the user to go to an other form and get back when done.
	 * \param $unique_function_name the name of the javascript function
	 * \param $param hash table: keys are field names to change before submitting, values are the new values to use
	 */
	function goto_zone($name) {
		//set the current node to the zone "name"
		$this->_table_mode = "";
		$this->_lock_zone = false;
		$this->priv_goto_zone($name);
	}

	/** \brief change the display mode to "free"
	 *
	 * This switch the method to render xhtml form to "free". that means no table to display the widgets: they are only included into div or span element or no element at all.
	 * \param $free_mode this should contain div or span or nothing to include xhtml form elements
	 * \param $class CSS class to use with the div or span element
	 */
	function set_display_free($free_mode = "", $class = -1) {
		if ($this->_table_mode != "")
			$this->_currentnode = & $this->_svg_node;
		$this->_table_mode = "";
		$this->_free_mode = $free_mode;
		$this->_free_mode_class = $class;
		$this->_lock_zone = false;
	}

	/** \brief generates textarea xhtml form element
	 *
	 * insert a textarea element in the form
	 * \param $ptag the label displayed near the textarea box
	 * \param $pname the html variable name used to send data when the form is submitted
	 * \param $pval the value of the textarea, could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $rows number of rows the textarea represents in the form
	 * \param $cols number of columns the textarea represents in the form
	 * \param $pclass CSS class used to display the textarea element
	 */
	function input_textarea($ptag, $pname, $pval, $rows, $cols, $pclass = -1) {
		$parameters["class"] = $pclass;
		$parameters["rows"] = $rows;
		$parameters["cols"] = $cols;
		//if ($pval == '') $pval = ' ';
		$this->priv_input($ptag, $pname, $pval, "textarea", $parameters);
	}

	/** \brief input date field (year, month, day)
	 *
	 * this widget accepts date in the format YYYY-MM-DD (example : 2007-09-14)
	 * it allows the user to choose a date and the posted value will be in the same format (YYYY-MM-DD)
	 *
	 *
	 * \param $ptag the label of the widget
	 * \param $pname the html variable name used to send data when the form is submitted
	 * \param $pdefault the value used to select the default item in the list
	 * \param $parameters hash table containing optional parameters: $parameters["class"] for the CSS class, $parameters["size"] for the size of the list
	 */
	function input_date($ptag, $pname, $pdefault_date, $parameters = array ()) {
		//check date format
		//$ptag, $pname, $pval, $psize = 10, $pmaxlength = -1, $pclass = -1
		$this->input_text($ptag, $pname, $pdefault_date, 10, 10);
		//$this->priv_input($ptag, $pname, $pdefault_date, 'date', array ());
	}

	/** \brief generates a clickable list (xhtml select element)
	 *
	 * insert a list in the form (select element) to choose a value between multiple options
	 * \param $ptag the label of the widget
	 * \param $pname the html variable name used to send data when the form is submitted
	 * \param $pdefault the value used to select the default item in the list
	 * \param $plist_val hash table containing as key, the text to display in the list, and value, the corresponding value
	 * \param $parameters hash table containing optional parameters: $parameters["class"] for the CSS class, $parameters["size"] for the size of the list
	 */
	function input_select($ptag, $pname, $pdefault, $plist_val, $parameters = array ()) {
		if (!is_array($parameters)) {
			$class = $parameters;
			$parameters = array ();

			$parameters["class"] = $class;
		}
		if (!array_key_exists("class", $parameters))
			$parameters["class"] = -1;
		if (!array_key_exists("size", $parameters))
			$parameters["size"] = -1;
		//      $parameters["class"] = $pclass;
		if (count($plist_val) == 0)
			$plist_val = array (
					"" => ""
			);
		reset($plist_val);
		$this->priv_input($ptag, $pname, $pdefault, "select", $parameters, $plist_val);
	}

	/** \brief generates an image submit button
	 *
	 * \param $src URL of the picture
	 * \param $alt alternative text to display instead of the picture (if the browser can not display this)
	 * \param $pclass the CSS class, optional
	 */
	function input_submit_image($src, $alt, $pclass = -1) {
		$this->priv_input(-1, $src, $alt, $ptype = "image", array (
				"class" => $pclass
		));
	}

	/** \brief generates a submit button
	 *
	 * \param $pval title of the submit button
	 * \param $pclass the CSS class, optional
	 */
	function input_submit($pval, $pclass = -1) {
		$this->priv_input(-1, -1, $pval, $ptype = "submit", array (
				"class" => $pclass
		));
	}

	/** \brief generates a reset button
	 *
	 * \param $pval title of the reset button
	 * \param $pclass the CSS class, optional
	 */
	function input_reset($pval, $pclass = -1) {
		$this->priv_input(-1, -1, $pval, $ptype = "reset", array (
				"class" => $pclass
		));
	}

	/** \brief generates a radio button
	 *
	 * insert a radio button in the form. This is used to choose a value for a variable between several radio button.
	 * Clicking on a radio button af a group will disable the previous selection for that variable.
	 * \param $ptag label of the radio button
	 * \param $pname name of the xhtml variable, if the same $pname is used several times,
	 * this will generate a group of radio button to choose one value for a single variable
	 * \param $pval the value of this radio button. if this is equal to $pcheck, then this will appear as selected.
	 * this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $pcheck the value that the variable will take if this widget is selected
	 * \param $pclass the CSS class, optional
	 */
	function input_radio($ptag, $pname, $pval, $pcheck, $pclass = -1) {
		if ($pval === true) {
			$pval = 'true';
		}
		elseif ($pval === false) {
			$pval = 'false';
		}
		//pval is an array: display vertical array
		// to have a unique ID:
		$parameters["check"] = $pcheck;
		$parameters["class"] = $pclass;
		$parameters["id"] = $this->priv_get_unique_id($pname) . $this->_radio_number;
		$this->_radio_number++;
		$this->priv_input($ptag, $pname, $pval, "radio", $parameters);
	}

	/** \brief generates a check box
	 *
	 * insert a check box in the form.
	 * \param $ptag label of the check box
	 * \param $pname name of the xhtml variable
	 * \param $pval the value of this check box. if this is equal to $pcheck, then this will appear as selected.
	 * this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $pcheck the value that the variable will take if this widget is selected
	 * \param $pclass the CSS class, optional
	 */
	function input_checkbox($ptag, $pname, $pval, $pcheck, $pclass = -1) {
		//pval is an array: display vertical array
		$parameters['check'] = $pcheck;
		$parameters["class"] = $pclass;
		//if ($pval == "") $pval = "OK";
		$this->priv_input($ptag, $pname, $pval, "checkbox", $parameters);
	}

	/** \brief generates a text input
	 *
	 * insert a text input
	 * \param $ptag label of the widget
	 * \param $pname name of the xhtml variable
	 * \param $pval the value of this text input, this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $psize the size of the widget in numbers of caracters (10 if omitted)
	 * \param $pmaxlength the maximum size of the string, optional
	 * \param $pclass the CSS class, optional
	 */
	function input_text($ptag, $pname, $pval, $psize = 10, $pmaxlength = -1, $pclass = -1) {
		$this->priv_input($ptag, $pname, $pval, "text", array (
				"class" => $pclass,
				"size" => $psize,
				"maxlength" => $pmaxlength
		));
	}

	/** \brief display a message in the form
	 *
	 * insert a simple text, not editable. used if the form is set to "const" or if we only need to display informations that do not need to be modified.
	 * \param $ptag label of the widget
	 * \param $pname name of the xhtml variable
	 * \param $pval the string to display, this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $pclass the CSS class, optional
	 */
	function input_message($ptag, $pname, $pval, $pclass = -1) {
		$this->priv_input($ptag, $pname, $pval, "message", array (
				"class" => $pclass
		));
	}

	/** \brief generates hidden input value
	 *
	 * insert a hidden value in the form
	 * \param $pname name of the xhtml variable
	 * \param $pval the value of this variable, this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $pclass the CSS class, optional
	 */
	function input_hidden($pname, $pval, $pclass = -1) {
		$this->priv_input(-1, $pname, $pval, "hidden", array (
				"class" => $pclass,
				"size" => -1,
				"maxlength" => -1
		));
	}

	/** \brief generates an upload browse file button
	 *
	 * insert a widget to choose a file to upload. this have to be used with set_file_transfer(true) to work properly
	 * \param $ptag label of the widget
	 * \param $pname name of the xhtml variable
	 * \param $pval not taken into account
	 * \param $psize size of the widget in caracters (10 if omitted)
	 * \param $pmaxlength max size of the file path, optional
	 * \param $pclass the CSS class, optional
	 */
	function input_file($ptag, $pname, $pval, $psize = 10, $pmaxlength = -1, $pclass = -1) {
		$this->priv_input($ptag, $pname, $pval, "file", array (
				"class" => $pclass,
				"size" => $psize,
				"maxlength" => $pmaxlength
		));
	}

	/** \brief generates a password input
	 *
	 * insert a widget to type passwords
	 * \param $ptag label of the widget
	 * \param $pname name of the xhtml variable
	 * \param $pval the value of this variable
	 * \param $psize size of the widget in caracters (10 if omitted)
	 * \param $pmaxlength max length of the string, optional
	 * \param $pclass the CSS class, optional
	 */
	function input_password($ptag, $pname, $pval, $psize = 10, $pmaxlength = -1, $pclass = -1) {
		$this->priv_input($ptag, $pname, $pval, "password", array (
				"class" => $pclass,
				"size" => $psize,
				"maxlength" => $pmaxlength
		));
	}

	/** \brief private: essential member function
	 *
	 * this private function is called by all input_* functions: this generates labels for all widgets, and choose the display mode,
	 * then call priv_create_node to generate the widget itself.
	 * \param $ptag label of the widget
	 * \param $pname name of the xhtml variable
	 * \param $pval the value of this variable, this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $ptype the type of the widget: "text" when called by input_text, "checkbox" for a check box widget... etc
	 * \param $parameters any parameters used by different kind of widgets, this will contain, for example, $parameters['class'] or
	 * $parameters['maxlength'] for the text input widget
	 * \param $plist used by the select widget, this contains the list of options to display
	 */
	function priv_input($ptag, $pname, $pval, $ptype = "text", $parameters = array (), $plist = array ()) {

		if ($this->_table_mode == "HORIZONTAL") {
			if (count($this->_tab_value_row) < 1) {
				return;
			}
			// lazy mode: $pval takes precedence
			$temp = str_replace("[]", "", $pname);
			if (($pval == "") & (array_key_exists($temp, $this->_tab_values)))
				$pval = $this->_tab_values[$temp];
		}

		// lazy mode: $pval takes precedence
		if (($pval == "") & (array_key_exists($pname, $this->_tab_values)))
			$pval = $this->_tab_values[$pname];
		//encodage en UTF8
		$ptag = utf8_ensure($ptag);
		if (!is_array($pval))
			$pval = utf8_ensure($pval);
		//création du champ
		if (!array_key_exists("class", $parameters))
			$parameters["class"] = -1;
		$pclass = $parameters["class"];
		$node = & $this->_currentnode;

		if (!is_array($pval))
			$field_node = & $this->priv_create_node($ptype, $pname, $pval, $parameters, $plist, $pclass);

		// label creation
		if ($ptag == -1)
			$tag_node = false;
		else {
			$tag_node = & $this->_xhtmldoc->create_element("label");
			if ($ptype != "radio") {
				if ($this->_const == false) {
					$tag_node->set_attribute("for", $this->priv_get_unique_id($pname));
				}
			} else
				$tag_node->set_attribute("for", $parameters["id"]);
			if ($ptag != "")
				$tag_node->set_content($ptag);
		}

		//$this->xhtml_message("coucouV");
		//affichage du champ en tenant compte de la présentation
		if ($this->_table_mode != "") {
			if ($this->_table_mode == "VERTICAL") {

				//nouvelle ligne, avec libellé et champ dans deux cellules
				$row = & $this->_xhtmldoc->create_element("tr");
				$node->append_child($row);
				$cell = & $this->_xhtmldoc->create_element("td");
				if ($ptag != -1)
					$cell->append_child($tag_node);
				$row->append_child($cell);

				$cell = & $this->_xhtmldoc->create_element("td");
				$cell->append_child($field_node);
				$row->append_child($cell);

			} else {
				if (count($this->_tab_value_row) < 1) {
					return;
				}

				$th = & $this->_xhtmldoc->create_element("th");
				if ($pname != -1) {
					if ($tag_node == false)
						$tag_node = & $this->_xhtmldoc->create_element("label");
					$tag_node->set_attribute("for", $this->priv_get_unique_id(str_replace("[]", "0", $pname)));
					$th->append_child($tag_node);
					$this->_title_row->append_child($th);
				}
				if (!is_array($pval)) {
					$td = & $this->_xhtmldoc->create_element("td");
					$td->append_child($field_node);
					$this->_tab_value_row[0]->append_child($td);
				} else {
					$c = 0;
					for ($i = 0; $i < count($pval); $i++) {
						if ($i >= count($this->_tab_value_row)) {
							$this->xhtml_message("ERROR: the field " . $pname . " is not consistent. Not enough rows declared, contact the administrator (in the object xhtml_form)", "error");
							$this->xhtml_message("i = " . $i . ", max value = " . count($this->_tab_value_row), "error");
							$this->xhtml_message("table of values to display: " . serialize($pval), "error");
							return;
						}

						$td = & $this->_xhtmldoc->create_element("td");
						$parameters["id"] = $this->priv_get_unique_id(str_replace("[]", $i, $pname));
						if (($ptype == 'checkbox') or ($ptype == 'radio')) {
							if (is_array($parameters['check'])) {
								//check values provided
								$td->append_child($this->priv_create_node($ptype, $pname, $pval[$i], array (
										"id" => $parameters["id"],
										"check" => $parameters['check'][$i]
								), $plist, $pclass));
							} else {
								//check values not provided => this will return the number of the row if checked
								$tmp_val = "-1";
								if ($parameters["check"] == $pval[$i]) {
									// echo $parameters["check"]."/". $pval[$i]."]";
									$tmp_val = $i;
								}
								//$parameters["check"] = $i;
								$td->append_child($this->priv_create_node($ptype, $pname, $tmp_val, array (
										"id" => $parameters["id"],
										"check" => $i
								), $plist, $pclass));
							}
						} else {
							$parameters['special_index'] = $i;
							if (array_key_exists($i, $pval) == false)
								$pval[$i] = '';
							$td->append_child($this->priv_create_node($ptype, $pname, utf8_ensure($pval[$i]), $parameters, $plist, $pclass));
						}
						if ($this->_htable_row_diff == $pname) {
							//change the class of the row if the field $pname contains a value different from the last one
							if (($i > 0) and ($pval[$i] != $pval[$i -1])) { //change
								$c++;
								if ($c == 2)
									$c = 0;
							}
							$this->_tab_value_row[$i]->set_attribute('class', 'trcolor' . $c);

						}
						$this->_tab_value_row[$i]->append_child($td);

					}
				}
			}
		} else {
			//free mode display (without table)
			if (($ptype != "hidden") & ($this->_free_mode != "")) {
				$container = & $this->_xhtmldoc->create_element($this->_free_mode);
				if ($this->_free_mode_class != -1) {
					$container->set_attribute("class", $this->_free_mode_class);
					$contain_field = & $this->_xhtmldoc->create_element("span");
					$contain_field->append_child($field_node);
					$field_node = & $contain_field;
					if ($tag_node == false) {
						$contain_tag = & $this->_xhtmldoc->create_element("span");
						//$contain_tag -> set_content(" ");
						$tag_node = & $contain_tag;
					}

				}

				if (!is_object($node)) {
					echo ("ERROR: the field " . $pname . ", value :" . serialize($pval) . " is not consistent (xhtml_form object).");
					echo ("please contact the administrator");
					exit;
				}

				$node->append_child($container);
				$node = & $container;

			}
			if ($tag_node != false) {
				$node->append_child($tag_node);
				//$node = $tag_node;
			}
			$node->append_child($field_node);
		}

	}

	/** \brief private: used to generate a form widget
	 *
	 * this private function is called by all priv_input, it reformats parameters depending on the type of widget and finally calls a specific
	 * priv_create_node_* function.
	 * \param $ptype the type of the widget: "text" when called by input_text, "checkbox" for a check box widget... etc
	 * \param $pname name of the xhtml variable
	 * \param $pval the value of this variable, this could be an array of value if used in HORIZONTAL mode with n lines
	 * \param $parameters any parameters used by different kind of widgets, this will contain, for example,
	 * $parameters['maxlength'] for the text input widget
	 * \param $plist used by the select widget, this contains the list of options to display
	 * \param $pclass CSS class to use (-1 if none)
	 * \return a reference on the new xhtml element
	 */
	function & priv_create_node($ptype, $pname, $pval, $parameters, $plist, $pclass) {
		//      if (!array_key_exists("class", $parameters)) $parameters["class"] = "";
		//print_r ($parameters);
		if (!array_key_exists("size", $parameters))
			$parameters["size"] = -1;
		if (!array_key_exists("maxlength", $parameters))
			$parameters["maxlength"] = -1;
		if (!array_key_exists("onclick", $parameters))
			$parameters["onclick"] = -1;
		if (array_key_exists("id", $parameters))
			$id = $parameters["id"];
		else
			$id = $this->priv_get_unique_id($pname);

		switch ($ptype) { //on traite les cas des differents types

			case "text" :
			case "password" :
			case "reset" :
			case "file" :
			case "hidden" :
			case "submit" :
			case "image" :
			case "button" :
				$size = $parameters["size"];
				$maxlength = $parameters["maxlength"];
				//$onclick = $parameters["onclick"];
				if ($maxlength == 0) {
					$parameters['maxlength'] = $size;
				}
				//$class = $parameters["class"];
				$new_node = & $this->priv_create_node_input($id, $ptype, $pname, $pval, $parameters);
				break;

			case "textarea" :
				$rows = $parameters["rows"];
				$cols = $parameters["cols"];
				$class = $parameters["class"];
				$new_node = & $this->priv_create_node_textarea($id, $pname, $pval, $rows, $cols, $class);
				break;

			case "select" :
				$new_node = & $this->priv_create_node_select($id, $pname, $plist, $pval, $parameters);
				break;

			case "checkbox" :
			case "radio" :
				$new_node = & $this->priv_create_node_input_checkbox($id, $ptype, $pname, $pval, $pclass, $parameters);
				break;

			case "message" :
				$new_node = & $this->priv_create_node_message($id, $pname, $pval, $pclass, $parameters);
				break;
			case 'date' :
				$new_node = & $this->priv_create_node_date($id, $pname, $pval, $pclass, $parameters);
				break;
			default :
				echo html_message("erreur : $ptype est un type inconnu", "erreur");
				break;
		}
		return $new_node;
	}

	/** \brief private: generates elements to display a date
	 *
	 * this private function is called by priv_create_node for the widget type "date"
	 * \param $id the unique xhtml id of this widget
	 * \param $pname not used
	 * \param $pval the value displayed
	 * \param $pclass CSS class to use (-1 if none)
	 * \param $parameters
	 * \return a reference on the newly created date element
	 */
	function & priv_create_node_date($id, $pname, $pval, $pclass, $parameters) {

		$node_date_display = & $this->_xhtmldoc->create_element('span');
		$node = & $this->_xhtmldoc->create_element('input');
		$node->set_attribute('type', 'hidden');
		$node->set_attribute('id', $id);
		$node->set_attribute('name', $pname);
		$node->set_attribute('value', $pval);

		$node_date_display->append_child($node);

		$node_date_display->set_content($pval);

		return $node_date_display;

	}

	/** \brief private: generates a span element
	 *
	 * this private function is called by priv_create_node for the widget type "message"
	 * \param $id the unique xhtml id of this widget
	 * \param $pname not used
	 * \param $pval the value displayed
	 * \param $pclass CSS class to use (-1 if none)
	 * \param $parameters not used
	 * \return a reference on the newly created message element
	 */
	function & priv_create_node_message($id, $pname, $pval, $pclass, $parameters) {
		$node = & $this->_xhtmldoc->create_element('span');
		$node->set_attribute("id", $id);
		if ($pclass != -1)
			$node->set_attribute("class", $pclass);
		$this->priv_xhtml_insert($pval, $node);
		return $node;
	}

	/** \brief private: generates a textarea element
	 *
	 * this private function is called by priv_create_node for the widget type "textarea"
	 * \param $id not used
	 * \param $pname the name of the xhtml variable
	 * \param $pval the string to display
	 * \param $rows the number of rows for the text area
	 * \param $cols the number of columns for the text area
	 * \param $pclass CSS class to use (-1 if none)
	 * \return a reference on the newly created textarea element
	 */
	function & priv_create_node_textarea($id, $pname, $pval, $rows, $cols, $pclass) {
		$pval = str_replace("&", "&#038;", $pval);

		if ($this->_const == true) {
			$node = & $this->_xhtmldoc->create_element("span");
			if ($pval != "")
				$node->set_content($pval);
		} else {
			$node = & $this->_xhtmldoc->create_element("textarea");
			$node->set_content($pval);
			$node->set_attribute("id", $this->priv_get_unique_id($pname));
			$node->set_attribute("name", $pname);
			$node->set_attribute("rows", $rows);
			$node->set_attribute("cols", $cols);
			if ($pclass != -1)
				$node->set_attribute("class", $pclass);
		}
		return $node;
	}

	/** \brief private: generates a select element
	 *
	 * this private function is called by priv_create_node for the widget type "select"
	 * \param $id the unique xhtml id of this widget
	 * \param $pname the name of the xhtml variable
	 * \param $plist hash table: keys are text to display, values, the corresponding values
	 * \param $pdefault the default value (selected item)
	 * \param $parameters, hash table with optional parameters: $parameters["size"], $parameters["class"]
	 * \return a reference on the newly created select element
	 */

	function & priv_create_node_select($id, $pname, $plist, $pdefault, $parameters) {
		if ($this->_const == true) {
			$node = & $this->_xhtmldoc->create_element("span");
			$node->set_content($pdefault);
		} else {
			$node = & $this->_xhtmldoc->create_element("select");
			//$node->set_content($pdefault);
			$node->set_attribute("name", $pname);
			$node->set_attribute("id", $id);
			if ($parameters["size"] != -1)
				$node->set_attribute("size", $parameters["size"]);
			if ($parameters["class"] != -1)
				$node->set_attribute("class", $parameters["class"]);

			$this->priv_create_node_select_options($node, $plist, $pdefault);

		}
		return $node;
	}

	/** \brief private: generates options elements for the select widget
	 *
	 * this private function is called by priv_create_node_select to generate options of the select list.
	 * \param $node reference of the node that will contains those options
	 * \param $plist hash table: keys are text to display, values, the corresponding values. if an element contains an other hash table, this
	 * will call recursively the same functions, creating an "optgroup".
	 * \param $pdefault the default value (selected item)
	 */

	function priv_create_node_select_options(& $node, $plist, $pdefault) {
		if (!is_array($plist))
			return;
		while (list ($tag, $val) = each($plist)) {
			if (is_array($val)) {
				if (count($val)) {
					$option = & $this->_xhtmldoc->create_element("optgroup");
					$option->set_attribute("label", utf8_ensure($tag));
					//  $node->append_child($option);
					$this->priv_create_node_select_options($option, $val, $pdefault);
				} else
					break;
			} else {
				$val = utf8_ensure($val);
				$tag = utf8_ensure($tag);
				$tag = htmlspecialchars($tag);
				$option = & $this->_xhtmldoc->create_element("option");
				//  $node->append_child($option);
				$option->set_attribute("value", $val);
				//	echo $tag."<br/>";
				$option->set_content($tag);

				if ($val == $pdefault) {
					$option->set_attribute("selected", "selected");
				}
			}
			$node->append_child($option);

		}
	}

	/** \brief private: generates a unique id for an element in the form
	 *
	 * simply creates a unique string, by prefixing $id with the id of the form
	 * \param $id the string to prefix
	 * \return unique id string
	 */
	function priv_get_unique_id($id) {
		return ($this->_form->get_attribute("id") . "_" . $id);
	}

	/** \brief private: generates a checkbox element
	 *
	 * this private function is called by priv_create_node for the widget type "checkbox"
	 * \param $id unique xhtml id for this element
	 * \param $ptype the type of the element
	 * \param $pname the xhtml name of the variable
	 * \param $pval the value of the variable
	 * \param $pclass CSS class to use (-1 if none)
	 * \param $parameters hash table containing optional parameters, $parameters["check"] for the check value
	 * \return a reference on the newly created checkbox element
	 */
	function & priv_create_node_input_checkbox($id, $ptype, $pname, $pval, $pclass, $parameters) {
		if ($this->_const == true) {
			$node = & $this->_xhtmldoc->create_element("span");
			if ($pval == $parameters["check"])
				$node->set_content("yes");
			else
				$node->set_content("no");
		} else {
			$node = & $this->_xhtmldoc->create_element("input");
			$node->set_attribute("type", $ptype);
			if ($pname != -1) {
				if ($ptype != "radio")
					$node->set_attribute("id", $id);
				else
					$node->set_attribute("id", $parameters["id"]);
				$node->set_attribute("name", $pname);
			}
			//if ($ptype == 'radio')
			//	$node -> set_attribute('value', $pval);
			//else
			$node->set_attribute('value', $parameters['check']);
			if ($pval == $parameters["check"]) {
				$node->set_attribute("checked", "checked");
			}
		}
		if ($pclass != -1)
			$node->set_attribute("class", $pclass);
		return $node;

	}

	/** \brief private: generates an input element
	 *
	 * this private function is called by priv_create_node to generates an input element, works for all widget based on the
	 * xhtml tag "input"
	 * \param $id unique xhtml id for this element
	 * \param $ptype the type of the element
	 * \param $pname the xhtml name of the variable
	 * \param $pval the value of the variable
	 * \param $pclass CSS class to use (-1 if none)
	 * \param $psize size of the widget
	 * \param $pmaxlength maximum length of the widget
	 * \param $ponclick action to perform on click
	 * \return a reference on the newly created node element
	 */
	//function & priv_create_node_input($id, $ptype, $pname, $pval, $pclass, $psize, $pmaxlength, $ponclick) {
	function & priv_create_node_input($id, $ptype, $pname, $pval, $param = array ()) {
		if ($this->_const == true) {
			$node = & $this->_xhtmldoc->create_element("span");
			if ($ptype == "text")
				$node->set_content($pval);
			if (array_key_exists('class', $param))
				$node->set_attribute('class', $param['class']);
		} else {
			$node = & $this->_xhtmldoc->create_element("input");
			$node->set_attribute("type", $ptype);
			if ($ptype == "image") {
				$node->set_attribute("src", $pname);
				$node->set_attribute("alt", $pval);
				return $node;
			}
			if ($pname != -1) {
				$node->set_attribute("id", $id);
				$node->set_attribute("name", $pname);
			}
			$node->set_attribute("value", $pval);

			foreach ($param as $field => $val) {
				if ($val != -1) {
					$node->set_attribute($field, $val);
				}
			}
		}
		return $node;

	}

	/** \brief private: put javascript code in an xhtml script element
	 *
	 * this private function will place javascript code into a script tag
	 * \param $javascript the javascript code
	 */
	function priv_add_javascript($javascript) {
		//add a javascript code to the form
		//	$this->xhtml_message("coucou".$javascript_code);

		// 1) is the validate function there ?
		if (array_key_exists($this->priv_get_unique_id("javascript"), $this->_tab_nodes)) {
			//	$this->xhtml_message("coucou1".$javascript_code);
			$javascript_node = & $this->_tab_nodes[$this->priv_get_unique_id("javascript")];
			$cdata = & $this->_tab_nodes[$this->priv_get_unique_id("javascript_cdata_section")];
			$javascript_code = $cdata->get_content();
			$javascript_code = str_replace("//string_to_replace", $javascript . "//string_to_replace\n", $javascript_code);
		} else {
			//create the cdata node to add the javascript code
			//	$node = $this->_xhtmldoc->create_element("script");
			$javascript_node = & $this->_xhtmldoc->create_element("script");
			$this->_tab_nodes[$this->priv_get_unique_id("javascript")] = & $javascript_node;
			//$this->_currentnode->append_child($javascript_node);
			$this->_tab_nodes[$this->priv_get_unique_id("ground0")]->append_child($javascript_node);
			$javascript_node->set_attribute("type", "text/javascript");

			$javascript_code = $javascript . "//string_to_replace\n";

			$cdata = & $this->_xhtmldoc->create_cdata_section($javascript_code);

			$this->_tab_nodes[$this->priv_get_unique_id("javascript_cdata_section")] = & $cdata;

			$javascript_node->append_child($cdata);

		}
		$cdata->set_content($javascript_code);
	}

	/** \brief private: generates javascript code to check the form on submit
	 *
	 * this private function will place javascript code dedicated to check the values of a form: this can be used by any other function
	 * to add javascript code to check valued before submitting
	 * \param $javascript the javascript code
	 */
	function priv_add_check_javascript($javascript) {
		//add a javascript code to the form
		//	$this->xhtml_message("coucou".$javascript_code);

		// 1) is the validate function there ?
		if (array_key_exists($this->priv_get_unique_id("javascript_check"), $this->_tab_nodes)) {
			//	$this->xhtml_message("coucou1".$javascript_code);
			$javascript_node = & $this->_tab_nodes[$this->priv_get_unique_id("javascript_check")];
			$cdata = & $this->_tab_nodes[$this->priv_get_unique_id("javascript_check_cdata_section")];
			$javascript_code = utf8_ensure($cdata->get_content());
			$javascript_code = str_replace("//string_to_replace", $javascript . "//string_to_replace\n", $javascript_code);
			//echo $javascript_code;
		} else {
			$javascript_node = & $this->_xhtmldoc->create_element("script");
			$this->_tab_nodes[$this->priv_get_unique_id("javascript_check")] = & $javascript_node;
			//$this->_currentnode->append_child($javascript_node);
			$this->_tab_nodes[$this->priv_get_unique_id("ground0")]->append_child($javascript_node);
			$javascript_node->set_attribute("type", "text/javascript");

			//	$this->xhtml_message("coucou2".$javascript_code);
			//set the onsubmit parameter to call the future javascript code:
			$javascript_function_name = "check_on_submit_" . $this->_form->get_attribute("id");
			$this->_form->set_attribute("onsubmit", "return " . $javascript_function_name . "(this)");

			$javascript_code = "\nfunction " . $javascript_function_name . " (form) {\n";
			$javascript_code .= $javascript;
			$javascript_code .= "//string_to_replace\n";
			$javascript_code .= " return true;\n";
			$javascript_code .= "}\n";
			//$javascript_code .= "//string_to_replace\n"
			$cdata = $this->_xhtmldoc->create_cdata_section($javascript_code);
			//$cdata = $this->_xhtmldoc->create_text_node ($javascript_code);
			$this->_tab_nodes[$this->priv_get_unique_id("javascript_check_cdata_section")] = & $cdata;
			//	$$javascript_nodenode->set_content($legend);

			$javascript_node->append_child($cdata);

		}
		$cdata->set_content(utf8_ensure($javascript_code));
	}

	/** \brief private: add javascript code to the onload function of this form
	 *
	 * this function will place javascript code
	 * that will be executed automatically when loading the form
	 * \param $javascript the javascript code
	 */
	function add_onload_javascript($javascript) {
		//add a javascript code to the form
		//	$this->xhtml_message("coucou".$javascript_code);

		// 1) is the validate function there ?
		if (array_key_exists($this->priv_get_unique_id('javascript_onload'), $this->_tab_nodes)) {
			//	$this->xhtml_message("coucou1".$javascript_code);
			$javascript_node = & $this->_tab_nodes[$this->priv_get_unique_id('javascript_onload')];
			$cdata = & $this->_tab_nodes[$this->priv_get_unique_id('javascript_onload_cdata_section')];
			$javascript_code = utf8_ensure($cdata->get_content());
			$javascript_code = str_replace("//string_to_replace", $javascript . "//string_to_replace\n", $javascript_code);
			//echo $javascript_code;
		} else {
			$javascript_node = & $this->_xhtmldoc->create_element('script');
			$this->_tab_nodes[$this->priv_get_unique_id('javascript_onload')] = & $javascript_node;
			//$this->_currentnode->append_child($javascript_node);
			$this->_form->append_child($javascript_node);
			$javascript_node->set_attribute('type', 'text/javascript');

			//"function onload () {initAutoComplete(document.getElementById('Fform-test_champ-texte'))};";
			$javascript_code = "\nfunction onload () {\n";
			$javascript_code .= $javascript;
			$javascript_code .= "//string_to_replace\n";
			$javascript_code .= "}\n";
			//$javascript_code .= "//string_to_replace\n"
			$cdata = $this->_xhtmldoc->create_cdata_section($javascript_code);
			//$cdata = $this->_xhtmldoc->create_text_node ($javascript_code);
			$this->_tab_nodes[$this->priv_get_unique_id('javascript_onload_cdata_section')] = & $cdata;
			//	$$javascript_nodenode->set_content($legend);

			$javascript_node->append_child($cdata);
			$this->_form->set_attribute('onload', 'javascript:onload()');

		}
		$cdata->set_content(utf8_ensure($javascript_code));
	}

}
?>