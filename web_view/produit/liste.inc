<?php


/***************************************************************************
                      view/produit/liste.inc  -
           liste des produits de phpMesStocks
                             -------------------
    begin                :  24/9/2005
    copyright            : (C) 2005 by Olivier Langella
    email                : Olivier.Langella@moulon.inra.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/************************************************
	FABRICATION de la barre de recherche/tri
*************************************************/

// formulaire de sélection du tri:
$form = new xhtml_pms_form($page, 'produit_tri');
$form->xhtml_form_set('get', 'index.php'); //&IdAdminBD='.$req_IdAdminBD);
$arr_tri = $form->get_query_parameters(array ('tri'));
$form->input_hidden('hmenu', 'gestion');
$form->input_hidden('view', 'produit_liste');
$form->input_select('Tri', 'tri', '', array ("numeros" => "Numéros", "prises" => "Prises", "pieces" => "Pièces"), '');
$form->input_submit('trier');

// formulaire de recherche rapide:
$form = new xhtml_pms_form($page, 'produit_recherche');
$form->xhtml_form_set('get', 'index.php'); //&IdAdminBD='.$req_IdAdminBD);
$form->input_hidden('hmenu', 'parc');
$form->input_hidden('view', 'telephone_liste');
$recherche = $form->get_query_parameters(array ('req_NomProduit', 'req_IdUtilisateur', 'req_ProduitNotes'));
$form->input_text("Numéro", "req_NumTelephone", '', 10);

$form->input_submit('rechercher');

/************************************************
	FIN de la barre de recherche/tri
*************************************************/

/*
REQUETE pour fabriquer la liste
*/

$objet_requete = new db_select_pmc();

//	$requete = "SELECT t1.IdProduit, t1.NomProduit, t1.Conditionnement, t1.Qrestante, t1.ProduitNotes, t2.IdUtilisateur FROM Produits AS t1";
//	$requete .= " NATURAL LEFT JOIN PrdtUtilisateurs AS t2";
$objet_requete->select_from(array ('IdProduit', 'NomProduit', 'Conditionnement', 'Qrestante', 'ProduitNotes'), 'Produits', 'p');
$objet_requete->left_join(array ('IdUtilisateur'), 'PrdtUtilisateurs', 'pu', 'IdProduit');
//$objet_requete->left_outer_join(array ("IdBaie", "NomBaie"), "baie", "b", "IdBaie");

if ($recherche['req_NomProduit'] != '') {
	//$objet_requete->commence_par ("i.NumeroIpInterface",$req_NumeroIpInterface, "AND");
	$objet_requete->where_like('p.NomProduit', $recherche['req_NomProduit']);
}
if ($recherche['req_IdUtilisateur'] > 0) {
	//$objet_requete->commence_par ("i.NumeroIpInterface",$req_NumeroIpInterface, "AND");
	$objet_requete->where_equal('pu.IdUtilisateur', $recherche['req_IdUtilisateur']);
}
if ($recherche['req_ProduitNotes']  != '') {
	//$objet_requete->commence_par ("i.NumeroIpInterface",$req_NumeroIpInterface, "AND");
	$objet_requete->where_like('p.ProduitNotes', $recherche['req_ProduitNotes']);
}

$objet_requete->order_by('p.NomProduit');
/*
if ($arr_tri['tri'] == "numeros") {
	$objet_requete->order_by("t.NumTelephone");
} else {
	if ($arr_tri['tri'] == "prises") {
		$objet_requete->order_by("pm.NumeroPriseMurale");
		$objet_requete->order_by("t.NumTelephone");
	} else {
		if ($arr_tri['tri'] == "pieces") {
			$objet_requete->order_by("pm.PiecePriseMurale");
			$objet_requete->order_by("t.NumTelephone");
		}
	}
}
*/

$nb_lignes = $objet_requete->count_rows($db_link);

if ($objet_requete->is_sql_error()) {
	$page->xhtml_message($objet_requete->get_error(), 'error');
}

/*
FIN de la REQUETE
*/
$page->xhtml_message(html_anchor(HOME_PAGE.'?hmenu=gestion&view=produit_form&IdProduit=0', 'nouveau produit'), '');
//	echo $objet_requete -> get_sql_string();

if ($nb_lignes > 0) {
	require_once (APP_ROOT_RELATIVE_PATH.'libpms/xhtml_pmc_list.inc');
	$liste = new xhtml_pmc_list($page, 'produit_liste');

	$liste->xhtml_form_set('post', HOME_PAGE.'?hmenu=gestion&view=produit_liste');

	$objet_requete->limit($liste->_position, $liste->_offset, $nb_lignes);

	$res = $objet_requete->execute_select($db_link);
	$res = $objet_requete->get_result();
	//print_r ($res);

	$liste->list_pmc_browse($nb_lignes);
	if ($res != false) {
		$liste->set_display_htable(count($res['IdProduit']));
		$liste->set_htable_row_diff('NomProduit[]');

		$liste->set_lazy_mode($res);

		$liste->input_message("Nom", "NomProduit[]", "");
		$liste->input_message("Conditionnement", "Conditionnement[]", "");
		$liste->input_message("Quantité", "Qrestante[]", "");
		$liste->input_message("Notes", "ProduitNotes[]", "");
		if ($visitor['produitModif']) {
			$lien = HOME_PAGE.'?hmenu=gestion&view=produit_form&IdProduit=';
			$liste->input_list_link('', 'modifier[]', $lien, $res['IdProduit'], 'modifier');
		}
		if ($visitor['produitSuppr']) {
			$lien = HOME_PAGE.'?hmenu=gestion&view=produit_liste&dba=delete&dbobject=produit_form&IdProduit=';
			$liste->input_list_link('', 'supprimer[]', $lien, $res['IdProduit'], 'supprimer');
		}
	}
}
?>