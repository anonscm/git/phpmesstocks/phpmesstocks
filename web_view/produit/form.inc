<?php


/***************************************************************************
 view/produit/form.inc  -
 formulaire d'un produit de phpMesStocks
 -------------------
 begin                :  24/9/2005
 copyright            : (C) 2005 by Olivier Langella
 email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

if (array_key_exists('NomProduit', $_POST)) { //on a déjà   posté le formulaire, on reprend les données du post
	$res = $_POST;
} else { // c'est nouveau:
	$res = false;
}

/*
 REQUETE pour rechercher la fiche
 */

$IdProduit = 0;
if (array_key_exists('IdProduit', $_GET))
$IdProduit = $_GET['IdProduit'];
else
if (array_key_exists('IdProduit', $_POST))
$IdProduit = $_POST['IdProduit'];

$tab_pf = array();
if ($IdProduit > 0) {
	$dba = 'update';

	if ($res == false) {
		$objet_requete = new db_select_pmc();

		$objet_requete->select_from(array ('*'), 'Produits', 'p');

		$objet_requete->where_equal('p.IdProduit', $IdProduit);

		$res = $objet_requete->execute_select($db_link);
		if ($res == false)
		return;
		$res = $objet_requete->get_result_first();

		//recherche des fournisseurs de ce produit:
		$objet_requete = new db_select_pmc();
		$objet_requete->select_from(array ('IdProduit', 'IdFournisseur', 'RefPrdtFournisseur', 'PrixPrdtFournisseur', 'RemisePrdtFournisseur'), 'PrdtFournisseurs', 'pf');
		$objet_requete->left_join(array ('IdFournisseur', 'NomFournisseur'), 'Fournisseurs', 'f', 'IdProduit');

		//$objet_requete->where_equal('pf.IdProduit', $url_IdProduit, "AND");
		$objet_requete->order_by('f.NomFournisseur');
		$res_pf = $objet_requete->execute_select($db_link);
		$tab_pf = $objet_requete->get_result();

		/*
		 $resultat = exec_requete($requete, $connexion) or die("requete non valide");
		 $i = 0;
		 $tab_IdFournisseur = array ();
		 $tmp_ancre = "index.php?repertoire=fournisseur&action=form&url_IdFournisseur=";

		 while ($ligne_tmp = ligne_suivante($resultat)) {
		 $ligne = $ligne_tmp;
		 $tab_IdFournisseur[$i] = $ligne->IdFournisseur;
		 $tab_NomFournisseur[$i] = $ligne->NomFournisseur;
		 $tab_RefPrdtFournisseur[$i] = $ligne->RefPrdtFournisseur;
		 $tab_PrixPrdtFournisseur[$i] = $ligne->PrixPrdtFournisseur;
		 $tab_RemisePrdtFournisseur[$i] = $ligne->RemisePrdtFournisseur;
		 $tab_PrixRFournisseur[$i] = $ligne->PrixPrdtFournisseur;
		 if ($tab_RemisePrdtFournisseur[$i] > 0)
		 $tab_PrixRFournisseur[$i] -= ($tab_PrixRFournisseur[$i] * $tab_RemisePrdtFournisseur[$i]) / 100;
		 $tab_PrixRFournisseur[$i] = str_replace(".", ",", $tab_PrixRFournisseur[$i]);
		 $tab_supprimer_pf[$i] = -1;
		 $tab_lien_modif_f[$i] = ancre($tmp_ancre.$ligne->IdFournisseur, "modifier");
		 $i ++;
		 }
		 */
		//else $objet_requete ->transpose_res
	} else {
	}
} else {
	$dba = 'new';
	$res = array ();
	$res['IdUtilisateur'] = $visitor['pms_id'];
}
/*
 FIN de la REQUETE
 */
$form = new xhtml_pms_form($page, 'produit_form');
$form->xhtml_form_set("post", HOME_PAGE.'?hmenu=gestion&view=produit_liste'); //&IdAdminBD='.$req_IdAdminBD);

$form->input_hidden('dba', $dba);
$form->input_hidden('dbobject', 'produit_form');
$form->input_hidden('back2view', '');
$form->input_hidden('go2view', '');

//print_r($res);
$form->set_lazy_mode($res);
$form->set_display_vtable();

$form->input_hidden('IdProduit', '');
$form->input_text('Nom du produit :', 'NomProduit', '', 20);
$form->add_check_fill('NomProduit', 'Le nom du produit est requis');
$form->input_text('Conditionnement :', 'Conditionnement', '', 20);
$form->input_text('Quantité en stock :', 'Qrestante', '', 10);
$form->input_text('Code nomenclature :', 'CodeNomenclature', '', 10);
$form->input_textarea('Notes :', 'ProduitNotes', '', 5, 40);

//$form->input_select('Type : ', 'TypeTelephone', '', charge_tableTypeTel(), '');
//$form->input_linked_lists('Baie', 'Prise murale', 'IdPriseMurale', '', charge_tableBaie($db_link, $res['IdLabo']), charge_tableBaiesPm_liees($db_link, $res['IdLabo']));
//$form -> input_text("Longueur :", 'LongueurPrise', '', 5);
$form->input_submit('enregistrer');

if (count($tab_pf) > 0) {

	$form->set_display_htable(count($tab_pf['IdFournisseur']));
	$form->input_hidden('IdFournisseur[]', $tab_pf['IdFournisseur']);
	$form->input_message("Numéro de tel", 'tab_NumTelephone[]', $tab_pf['NumTelephone']);
	$form->input_message("Type de Tél", 'tab_TypeTelephone[]', $tab_pf['TypeTelephone']);
	$form->input_message("Pièce", '$tab_PiecePriseMurale[]', $tab_pf['PiecePriseMurale']);
	//suppression:
	$check = array ();
	for ($i = 0; $i < count($tab_telephone['IdTelephone']); $i ++)
	$check[$i] = -1;

	$form->input_checkbox("Supprimer", "tab_SupprIdTelephone[]", $check, $tab_telephone['IdTelephone']);
	$form->goto_zone('telephones');
} else {
	$form->xhtml_message('pas de téléphone pour l\'instant', 'form_information');
}
/*
 if ($dba == 'update') {
 if (count($tab_IdFournisseur) > 0) {
 $form->debut_table(VERTICAL, 1);
 $form->champ_affichage("liste des fournisseurs:", "", "form_information");
 $form->fin_table();

 $form->debut_table(HORIZONTAL, count($tab_IdFournisseur), "liste");
 $form->champ_cache("tab_IdFournisseur[]", $tab_IdFournisseur);
 $form->champ_affichage("Fournisseur", $tab_NomFournisseur, "form_information");
 $form->champ_texte("R�f. du fournisseur", "tab_RefPrdtFournisseur[]", $tab_RefPrdtFournisseur, "20");
 $form->champ_decimal("Prix (euros)", "tab_PrixPrdtFournisseur[]", $tab_PrixPrdtFournisseur, "6");
 $form->champ_decimal("Remise (%)", "tab_RemisePrdtFournisseur[]", $tab_RemisePrdtFournisseur, "6");

 $form->champ_decimal("prix remis�", "tab_PrixRFournisseur[]", $tab_PrixRFournisseur, "6");
 // $form->champ_affichage ("Prix remis�", $tab_PrixRFournisseur,"form_information");
 $form->champ_checkbox("supprimer", "tab_supprimer_pf[]", $tab_supprimer_pf, $tab_IdFournisseur);
 $form->champ_affichage("", $tab_lien_modif_f, "form_information");

 $form->fin_table();

 }

 //affichage des fournisseurs du produit
 $form->debut_table(VERTICAL, 1);
 $form->champ_texte("Ajouter un fournisseur :", "form_AjoutFournisseur", "", 30);
 $form->fin_table();

 if (count($tab_IdUtilisateur) > 0) {
 $form->debut_table(VERTICAL, 1);
 $form->champ_affichage("liste des utilisateurs :", "", "form_information");
 $form->fin_table();

 $form->debut_table(HORIZONTAL, count($tab_IdUtilisateur), "liste");
 $form->champ_cache("tab_IdUtilisateur[]", $tab_IdUtilisateur);
 $form->champ_affichage("Identifiant", $tab_UIdentifiant, "form_information");
 $form->champ_affichage("Nom", $tab_UNom, "form_information");
 //$form->champ_texte ("Nom","tab_UNom[]", $tab_RefPrdtFournisseur);
 $form->champ_checkbox("supprimer", "tab_supprimer_pu[]", $tab_supprimer_pu, $tab_IdUtilisateur);
 //$form->champ_affichage ("", $tab_lien_modif_f,"form_information");

 $form->fin_table();

 $form->debut_table(VERTICAL, 1);
 }

 //affichage des utilisateurs du produit
 $form->champ_texte("Ajouter un utilisateur (taper son identifiant):", "form_AjoutUtilisateur", "", 30);
 $form->fin_table();
 }
 */
?>